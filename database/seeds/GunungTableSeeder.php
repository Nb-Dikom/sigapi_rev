<?php

use App\Models\Gunung;
use Illuminate\Database\Seeder;

class GunungTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $init_gunung_id = 'GN-01';
        $daftar_gunung = [
            'AGUNG', 'AMBANG', 'ANAK KRAKATAU', 'ANAK RANAKAH', 'ARJUNO WELIERANG',
            'AWU', 'BANDA API', 'BANUA WUHU', 'BATUR', 'BATUBARA', 'BROMO',
            'BUR NI TELONG', 'CIREMAI', 'COLO', 'DEMPO', 'DIENG', 'DUKONO', 'EBULOBO', 'EGON', 'GALUNGGUNG', 'GAMALAMA', 'GAMKONORA', 'GEDE', 'GUNTUR', 'HOBAL', 'IBU', 'IJEN', 'ILI LEWOTOLOK', 'ILI WERUNG', 'ILIBOLENG', 'INI RIE', 'INIELIKA', 'IYA', 'KABA', 'KARANGETANG', 'KELIMUTU', 'KELUD', 'KERINCI', 'KIE BESI', 'KRAKATAU', 'LAMONGAN', 'LAWARKARWA', 'LEGATALA', 'LEREBOLENG', 'LEWOTOBI LAKI-LAKI', 'LEWOTOBI PEREMPUAN', 'LOKON', 'MAHAWU', 'MARAPI', 'MERAPI', 'NIEUWERKERK', 'PAPANDAYAN', 'PEUT SAGOE', 'RAUNG', 'RINJANI', 'ROKATENDA', 'RUANG', 'SALAK', 'SANGEANGAPI', 'SEMERU', 'SERAWERNA', 'SEULAWAH AGAM', 'SINABUNG', 'SIRUNG', 'SLAMET', 'SOPUTAN', 'SORIK MARAPI', 'SUBMARINE', 'SUMBING', 'SUNDORO', 'TALANG', 'TAMBORA', 'TANDIKAT', 'TANGKOKO', 'TANGKUBAN PERAHU', 'WETAR', 'WURLALI'
        ];

        for($i=0; $i<count($daftar_gunung); $i++){
            DB::BeginTransaction();
            try {
                $gunung = new Gunung();
                $gunung->id_gunung = $init_gunung_id;
                $gunung->gunung = $daftar_gunung[$i];
                $gunung->save();

                $init_gunung_id ++;
                
                DB::commit();

            } catch (\Exception $e) {
                DB::rollback();
                throw new \Exception($e, 1);
            }
        }

    }
}
