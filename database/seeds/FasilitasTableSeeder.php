<?php

use App\Models\Fasilitas;
use App\Models\Gunung;
use App\Models\JenisFasilitas;
use App\Models\Validasi;
use App\Models\Verifikasi;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FasilitasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Gunung $gunung)
    {
        $faker = Faker\Factory::create('en_NZ');
        
        $daftar_verifikasi = Verifikasi::all();
        $daftar_validasi = Validasi::all();
        $daftar_jenis_fasilitas = JenisFasilitas::all();
        $semeru = $gunung->get_by_name('SEMERU');
        $pegawai_id = 'PGW-0003';
        $init_fasilitas_id = 'FST-000001';
        $raw = [
            [1, 12, 3, 3, 'BANTUR', 'Jl. Raya Bantur No. 203, Kec. Bantur', '112.5804443', '-8.3158933'],
            [2, 12, 3, 3, 'SUMBER MANJING KULON', 'Jl. A. Yani No.4, Kec. Pagak', '112.4980737', '-8.302122'],
            [3, 12, 3, 3, 'DONOMULYO', 'Jl. Raya Donomulyo No. 343, Kec. Donomulyo', '112.4251296', '-8.2952429'],
            [4, 12, 3, 3, 'PAGAK', 'Jl. Hamid Rusdi No. 84, Kec. Pagak', '112.5127537', '-8.2438065'],
            [5, 12, 3, 1, 'KALIPARE', 'Jl. Raya No. 210, Kec. Kalipare', '112.4659828', '-8.2032429'],
            [6, 11, 3, 3, 'RSU Bala Keselamatan', 'Jl. A.Yani 91 Turen , Kab.Malang', '112.6997932', '-8.1703025'],
            [7, 12, 3, 3, 'SUMBER PUCUNG', 'Jl. Jend. Sudirman No.2, Sumberpucung, Sumber Pucung', '112.4835447', '-8.155514'],
            [8, 11, 3, 1, 'RSU Kanjuruhan Kepanjen', 'Jl. Panji No. 100 Kepanjen, Malang', '112.5677392', '-8.1435602'],
            [9, 12, 3, 1, 'KROMENGAN', 'Jl. Nailun Utara, Kec. Kromengan', '112.4890143', '-8.118939'],
            [10, 11, 3, 3, 'RSU Wava Husada', 'Jl. Panglima Sudirman No. 99A, Dilem, Kepanjen', '112.5723903', '-8.116267'],
            [11, 11, 3, 1, 'RS Wijaya Kusuma', 'Jl. A. Yani No.149 Lumajang', '113.2306038', '-8.1091192'],
            [12, 12, 3, 1, 'NGAJUM', 'Jl. Ahmad Yani No.18, Kec. Ngajum', '112.5459613', '-8.0976239'],
            [13, 12, 3, 3, 'KEPANJEN', 'Jl. Raya Jatirejoyoso No.4, Kec. Kepanjen', '112.5842346', '-8.0947497'],
            [14, 12, 1, 1, 'KASEMBON', 'Jl. Raya Kasembon, Kec. Kasembon', '112.669605', '-8.0884301'],
            [15, 12, 1, 1, 'WONOSARI', 'Kebobang, Wonosari, Malang', '112.5047176', '-8.0628215'],
            [16, 11, 1, 1, 'RSU Universitas Muhammadiyah Malang', 'JL. RAYA TLOGOMAS NO 45 MALANG', '112.5970991', '-7.9259408'],
            [17, 12, 1, 1, 'DAU', 'Jl. Raya Mulyoagung No.121, Kec. Dau', '112.5848733', '-7.9138769'],
            [18, 12, 1, 1, 'PUJON', 'Jl. Brigjen Abdul Manan Wijaya, Kec. Pujon', '112.4720768', '-7.8452258'],
            [19, 11, 1, 1, 'RSU Islam Madinah Kasembon', 'Jl. Raya Sukosari No.32, Kasembon Malang', '112.3055376', '-7.7765468'],
            [20, 12, 1, 1, 'SUMBERSARI', 'Jl. Raya Sumbersari, Kec. Rowokangkung', '111.6918899', '-7.523726'],
            [21, 12, 1, 1, 'GEDANGAN', 'Jl. Hasanudin No. 60, Kec. Gedangan', '112.6489157', '-8.2935864'],
            [22, 12, 1, 1, 'SUMBER MANJING WETAN', 'Jl. Magenda No. 50, Kec. Sumbermanjing', '112.7019047', '-8.2633287'],
            [23, 12, 1, 1, 'PAGELARAN', 'Jl. Raya Sidorejo, Kec. Pagelaran', '112.6429441', '-8.2167525'],
            [24, 12, 1, 1, 'GONDANG LEGI', 'Jl. Diponegoro No. 62, Kec. Gondanglegi', '112.6340842', '-8.1757099'],
            [25, 11, 1, 1, 'RS Islam Gondang Legi', 'Jl. Hayam Wuruk 66 Gondang Legi Malang', '112.6375686', '-8.1731941'],
            [26, 12, 1, 1, 'TUREN', 'Jl. Panglima Sudirman No. 120, Kec. Turen', '112.6882221', '-8.171389'],
            [27, 12, 1, 1, 'KETAWANG', 'Jl. Raya Ketawang, Kec. Gondanglegi', '112.6430955', '-8.1403511'],
            [28, 11, 1, 1, 'RSU Mitra Delima', 'Jl. Raya Bulupayung No. 1B Krebet - Bululawang - Malang', '112.6343315', '-8.111892'],
            [29, 12, 1, 1, 'BULULAWANG', 'Jl. Stasiun No. 11-13, Kec. Bululawang', '112.6415612', '-8.076695'],
            [30, 12, 1, 1, 'PAKISAJI', 'Jl. Raya Pakisaji No.19, Kec. Pakisaji', '112.599954', '-8.0634031'],
            [31, 11, 1, 1, 'RSU Ben Mari', 'JL.Raya Kendalpayak No.17, Kec. Pakisaji ', '112.6269653', '-8.0523333'],
            [32, 12, 1, 1, 'TAJINAN', 'Jl. Raya Tajinan, Kec. Tajinan', '112.6900702', '-8.0503194'],
            [33, 12, 3, 3, 'PAKIS', 'Jl. Raya Pakis No. 70, Kec. Pakis', '112.7286084', '-7.9688573'],
            [34, 11, 1, 1, 'RSIA Husada Bunda', 'Jl. Pahlawan Trip No.2', '112.6223179', '-7.968027'],
            [35, 11, 1, 1, 'RS. TNI AU Lanud Abdulrachman Saleh', 'Jl. Dr. M. Munir No.18, Malang', '112.7019977', '-7.9337842'],
            [36, 11, 1, 1, 'RSU Marsudi Waluyo', 'Jl. Raya Mondoroko Km. 9 Singosari Malang', '112.6570451', '-7.9069525'],
            [37, 11, 1, 1, 'RSU Prasetya Husada', 'Jl. Raya Ngijo No.25 Karangploso, Malang', '112.6063543', '-7.9046535'],
            [38, 12, 3, 3, 'SINGOSARI', 'Jl. Tohjoyo Gg. III, Kec. Singosari', '112.6635103', '-7.8975289'],
            [39, 12, 1, 1, 'KARANGPLOSO', 'Jl. Panglima Budiman No.65, Kec. Karangploso', '112.5931713', '-7.8955142'],
            [40, 12, 1, 1, 'ARDIMULYO', 'Jl. Raya Ardimulyo No.2, Kec. Singosari', '112.6786378', '-7.8729036'],
            [41, 11, 1, 1, 'RSU Lawang Medika', 'Jl. Dr. Cipto 8 Bedali, Lawang', '112.6924015', '-7.8532198'],
            [42, 11, 1, 1, 'RSB Siti Mirian', 'Jl. Dr. Wahidin No.101, Lawang', '112.6945362', '-7.8482963'],
            [43, 12, 1, 1, 'LAWANG', 'Jl. Kartini No.5, Kec. Lawang', '112.6997731', '-7.8439438'],
            [44, 11, 1, 1, 'RSUD Lawang', 'Jl. RA Kartini 5, Lawang Kab. Malang', '112.6943691', '-7.832029'],
            [45, 11, 1, 1, 'RS Jiwa Dr. Radjiman Wediodiningrat', 'Jl. Jend. A.Yani Lawang Kab. Malang', '112.7151244', '-7.8244987'],
            [46, 12, 1, 1, 'BADES', 'Jl. Raya Gondoruso, Kec. Pasirian', '113.0925219', '-8.2368101'],
            [47, 12, 3, 3, 'AMPEL GADING', 'Jl. Raya Tirtomarto No.75, Kec. Ampelgading', '112.8783678', '-8.2344094'],
            [48, 12, 1, 1, 'TIRTOYUDO', 'Jl. Raya Tirtoyudo No.66, Kec. Tirtoyudo', '112.8056431', '-8.2320334'],
            [49, 12, 3, 3, 'PASIRIAN', 'Jl. Raya Pasirian, Kec. Pasirian', '113.1167249', '-8.2199413'],
            [50, 12, 3, 3, 'KUNIR', 'Jl. PB.Sudirman Kunir, Kec. Kunir', '113.2246225', '-8.2195882'],
            [51, 11, 1, 1, 'RSB Permata Hati', 'Jl. Pasar Baru No.99 Dampit', '112.7528788', '-8.2128916'],
            [52, 12, 1, 1, 'DAMPIT', 'Jl. Semeru Selatan, Kec. Dampit', '112.7490625', '-8.2116937'],
            [53, 11, 1, 1, 'RSUD PASIRIAN KABUPATEN LUMAJANG', 'Jalan Raya Pasirian No. 225 A, Kecamatan Pasirian', '113.115305', '-8.2089677'],
            [54, 12, 1, 1, 'TEMPEH', 'Jl. Raya Tempeh No.12, Kec. Tempeh', '113.1746783', '-8.203231'],
            [55, 12, 1, 1, 'PAMOTAN', 'Jl. Raya Pamotan, Kec. Dampit', '112.7348019', '-8.1997419'],
            [56, 12, 1, 1, 'CANDIPURO', 'Jl. PB.Sudirman No.94, Kec. Candipuro', '113.0767562', '-8.1908335'],
            [57, 12, 1, 1, 'GESANG', 'Jl. Rahmat Jaya No.1, Gesang, Kec. Tempeh', '113.1467795', '-8.1792658'],
            [58, 12, 1, 1, 'TEKUNG', 'Jl. Raya Tekung, Kec. Tekung', '113.2824293', '-8.1751154'],
            [59, 12, 1, 1, 'LABRUK KIDUL', 'Jl. Raya Labruk, Kec. Sumbersuko', '113.2002293', '-8.1515432'],
            [60, 11, 1, 1, 'RSU Islam Lumajang', 'Jl. Kyai Muksin no 19 Lumajang', '113.2193628', '-8.1340482'],
            [61, 11, 1, 1, 'RSU Bhayangkara Lumajang', 'Jl. Kapt. Kyai Ilyas No. 7', '113.2212262', '-8.1335425'],
            [62, 12, 1, 1, 'ROGOTRUNAN', 'Jl. Brantas No.5, Kec. Lumajang', '113.2297195', '-8.1317894'],
            [63, 12, 1, 1, 'PASRUJAMBE', 'Jl. Rangga No.23, Kec. Pasrujambe', '113.0590242', '-8.11889'],
            [64, 12, 1, 1, 'SUKODONO', 'Jl. Sukarno Hatta No.24, Kec. Sukodono', '113.2329989', '-8.1026313'],
            [65, 12, 1, 1, 'SENDURO', 'Jl. Raya Senduro, Kec. Senduro', '113.0978786', '-8.1014497'],
            [66, 12, 1, 1, 'PADANG', 'Jl. Raya Padang, Kec. Padang', '113.1912482', '-8.0859329'],
            [67, 12, 1, 1, 'KEDUNGJAJANG', 'Jl. Raya Kedungjajang No.118, Kec. Kedungjajang', '113.236182', '-8.050942'],
            [68, 12, 3, 3, 'GUCIALIT', 'Jl. PB.Sudirman No.215, Kec. Gucialit', '113.1408353', '-8.0491265'],
            [69, 12, 1, 1, 'PONCOKUSUMO', 'Jl. Kusnan Marzuki No.101 Wonomulyo, Kec. Poncokusumo', '112.769962', '-8.0440964'],
            [70, 11, 1, 1, 'RS Sumber Sentosa', 'Jl. Kebonsari No. 221 Malang', '112.7563669', '-8.0133654'],
            [71, 12, 3, 3, 'TUMPANG', 'Jl. Setiawan No.227, Kec. Tumpang', '112.7605476', '-8.0089602'],
            [72, 12, 1, 1, 'KLAKAH', 'Jl. Gunung Ringgit No.58, Kec. Klakah', '113.2479994', '-7.99173'],
            [73, 12, 1, 1, 'WAJAK', 'Jl. Panglima Sudirman No. 161, Kec. Wajak', '112.6385464', '-7.9725449'],
            [74, 12, 3, 3, 'RANUYOSO', 'Jl. Raya Ranuyoso No.254, Kec. Ranuyoso', '113.2601062', '-7.9484564'],
            [75, 12, 1, 1, 'JABUNG', 'Jl. Kemantren No.40, Kec. Jabung', '112.7481804', '-7.945589'],
            [76, 12, 1, 1, 'TEMPURSARI', 'Jl. Dahlia No.152, Kec. Tempursari', '112.9727226', '-8.292594'],
            [77, 1, 3, 3, 'POLSEK TEMPURSARI', 'JL. RAYA TEMPURSARI ', '112.972848', '-8.2881082'],
            [78, 1, 1, 1, 'POLSEK SUMBERMANJING WETAN', 'Jl. Raya Sumbermanjing Wetan, Argotirto, Sumbermanjing', '112.6902744', '-8.2793026'],
            [79, 1, 3, 3, 'POLSEK AMPEL GADING', 'Jl. Raya Tirtomarto, Tirtomarto, Ampelgading', '112.8780909', '-8.2348245'],
            [80, 12, 1, 1, 'YOSOWILANGUN', 'Jl. Stadion No.334, Kec.Yosowinangun', '113.3091565', '-8.232876'],
            [81, 1, 1, 1, 'POLSEK YOSOWILANGUN', 'JL. RAYA YOSOWILANGUN ', '113.2983848', '-8.2303643'],
            [82, 1, 3, 3, 'POLSEK PRONOJIWO ', 'JL. RAYA PRONOJIWO', '112.9334651', '-8.2252428'],
            [83, 1, 1, 1, 'POLSEK KUN', 'JL. RAYA K', '113.2279298', '-8.2207266'],
            [84, 12, 1, 1, 'PRONOJIWO', 'Jl. Raya Pronojiwo, Kec. Pronojiwo', '112.9434257', '-8.2114805'],
            [85, 12, 1, 1, 'JATIROTO', 'Jl. Dr.Soetomo No.21, Kec. Jatiroto', '113.3585645', '-8.1203688'],
            [86, 11, 1, 1, 'RSU Djatiroro', 'JL PB SUDIRMAN 81 LUMAJANG', '113.3582851', '-8.1182192'],
            [87, 12, 1, 1, 'RANDUAGUNG', 'Jl. Raya Randuagung No.92, Kec. Randuagung', '113.305642', '-8.0713649'],
            [88, 12, 1, 1, 'TUNJUNG', 'Jl. Raya Kalipenggung, Kec. Randuangung', '113.3343456', '-8.0678131'],
            [89, 1, 3, 3, 'POLSEK DAMPIT', 'Jl. Semeru Sel., Dampit', '112.7487896', '-8.2115649'],
            [90, 1, 1, 1, 'POLSEK PASIRIAN ', 'JL. RAYA PASIRIAN', '113.1152245', '-8.2102527'],
            [91, 1, 1, 1, 'POLSEK TEMPEH', 'JL. RAYA TEMPEH', '113.1763265', '-8.2005342'],
            [92, 1, 1, 1, 'POLSEK ROWOKANGKUNG', 'L. RAYA DESA ROWOKANGKUNG', '113.3073142', '-8.1873104'],
            [93, 1, 1, 1, 'POLSEK CANDIPURO', 'JLN. JEND. SUDIRMAN N0. 35 CANDIPURO', '113.0688228', '-8.1869398'],
            [94, 1, 1, 1, 'POLSEK GONDANG LEGI', 'Jl. Suropati No.22, Gondanglegi Wetan, Gondanglegi', '112.640521', '-8.1771191'],
            [95, 1, 1, 1, 'POLSEK TEKUNG', 'JL. RAYA TEKUNG ', '113.2790873', '-8.1737569'],
            [96, 1, 1, 1, 'POLSEK TUREN', 'Jl. Raya Ahmad Yani No. 4, Turen', '112.698303', '-8.1704549'],
            [97, 1, 1, 1, 'POLSEK  SUMBERSUKO', 'JL. RAYA SUMBERSUKO ', '113.1890843', '-8.1655751'],
            [98, 1, 1, 1, 'POLRES LUMAJANG', 'Jl. Alun Alun Utara No.11, Rogotrunan, Kec. Lumajang', '113.2235264', '-8.1337555'],
            [99, 1, 1, 1, 'POLSEK LUMAJANG ', 'JL. JUANDA LUMAJANG', '113.2314018', '-8.1238893'],
            [100, 1, 1, 1, 'POLRES MALANG', 'Jalan Jend. A. Yani No. 1, Kepanjen, Ardirejo', '112.5729017', '-8.1224289'],
            [101, 1, 1, 1, 'POLSEK JATIROTO', 'JL. RAYA JATIR', '113.3603825', '-8.1213805'],
            [102, 1, 1, 1, 'POLSEK PASRUJAMBE', 'JL. RAYA PASRUJAMBE ', '113.0465394', '-8.1153228'],
            [103, 1, 1, 1, 'POLSEK SENDURO', 'JL. RAYA SENDURO ', '113.0903817', '-8.0989819'],
            [104, 1, 1, 1, 'POLSEK SUKODONO ', 'JL. SUKARNO HATTA LUMAJANG', '113.2344802', '-8.0974465'],
            [105, 1, 1, 1, 'POLSEK BULULAWANG', 'Jl. Raya Diponegoro No.15, Bululawang', '112.6399722', '-8.0782947'],
            [106, 8, 3, 3, 'TERMINAL BUS MINAK KONCAR LUMAJANG', 'JL. RAYA WONOREJO', '113.2367376', '-8.074862'],
            [107, 1, 1, 1, 'POLSEK RANDUAGUNG', 'JL. RAYA RANDUAGUNG ', '113.303401', '-8.0699954'],
            [108, 1, 1, 1, 'POLSEK GUCIALIT', 'JL. RAYA GUCIALIT', '113.1407203', '-8.0489013'],
            [109, 1, 1, 1, 'POLSEK PONCOKUSUMO', 'jl. sutomo no. 52 Dsn. wates, Wonomulyo, Poncokusumo', '112.7689565', '-8.0417842'],
            [110, 1, 1, 1, 'POLSEK KEDUNGJAJANG', 'JL. RAYA KEDUNGJAJANG', '113.2330588', '-8.0355812'],
            [111, 1, 1, 1, 'POLSEK KLAKAH', 'JL. RAYA KLAKAH', '113.2448225', '-8.0070091'],
            [112, 1, 1, 1, 'POLSEK TUMPANG', 'Jl. Raya Tumpang No.3, Tumpang', '112.7617984', '-8.0029789'],
            [113, 9, 3, 3, 'STASIUN KERETA API KLAKAH', 'JL. STASIUN NO. 11,', '113.249588', '-7.9939967'],
            [114, 1, 1, 1, 'POLSEK RANUYOSO', 'JL. RAYA RANUYOSO', '113.2606193', '-7.9486463']
        ];

        $limit = count($raw);

        for ($i = 0; $i < $limit; $i++) {
            DB::BeginTransaction();
            try {
                $fasilitas = Fasilitas::create([
                    'id_fasilitas' => $init_fasilitas_id,
                    'id_jenis_fasilitas' => $daftar_jenis_fasilitas[($raw[$i][1])-1]->id_jenis_fasilitas,
                    'id_gunung' => $semeru->id_gunung,
                    'id_verifikasi' => $daftar_verifikasi[($raw[$i][3])-1]->id_verifikasi,
                    'id_validasi' => $daftar_validasi[($raw[$i][3])-1]->id_validasi,
                    'id_pegawai' => $pegawai_id,
                    'nama_fasilitas' => $raw[$i][4],
                    'alamat_fasilitas' => $raw[$i][5],
                    'longitude' => $raw[$i][6],
                    'latitude' => $raw[$i][7],
                    'tanggal_fasilitas' => $faker->dateTimeBetween($starDate = '-2 years', $endDate = 'now')
                ]);
                $init_fasilitas_id ++;
                DB::commit();

            } catch (\Exception $e) {
                DB::rollback();
                throw new \Exception($e, 1);
            }
	    }
    }
}
