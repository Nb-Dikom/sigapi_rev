<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VerifikasiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::BeginTransaction();
        try {
            DB::table('verifikasi')->insert([
                ['id_verifikasi' => 'VRF-01', 'verifikasi' => 'Pending'],
                ['id_verifikasi' => 'VRF-02', 'verifikasi' => 'Tidak Lengkap'],
                ['id_verifikasi' => 'VRF-03', 'verifikasi' => 'Lengkap'],
            ]);

            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            throw new \Exception($e, 1);
        }
    }
}
