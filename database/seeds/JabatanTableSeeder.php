<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JabatanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::table('jabatan')->insert([
                ['id_jabatan' => 'JB-01', 'jabatan' => 'Admin'],
                ['id_jabatan' => 'JB-02', 'jabatan' => 'Sub Bagian Umum dan Kepegawaian'],
                ['id_jabatan' => 'JB-03', 'jabatan' => 'Pengolah Data Pra Bencana'],
                ['id_jabatan' => 'JB-04', 'jabatan' => 'Kepala Seksi Kesiapsiagaan'],
                ['id_jabatan' => 'JB-05', 'jabatan' => 'Kepala Bidang Pencegahan dan Kesiapsiagaan'],
                ['id_jabatan' => 'JB-06', 'jabatan' => 'Kepala BPBD Jawa Timur'],
                ['id_jabatan' => 'JB-07', 'jabatan' => 'Sub Bagian Penanggulangan Bencana'],
            ]);
            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            throw new \Exception($e, 1);

        }
    }
}
