<?php
use App\Models\Level;
use App\Models\Organisasi;
use App\Models\StatusOrganisasi;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrganisasiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create('en_NZ');
        $instansi = [
        	'Pecinta Alam', 'Komunitas Sosial', 'Mahasiswa', 'WWF', 'LSM'
        ];
        $daftar_level = Level::all();
        $daftar_status_organisasi = StatusOrganisasi::all();
        
        $limit = 20;
        $latest_user = User::orderBy('id_user', 'desc')->first();
        $init_user_id = $latest_user->id_user;
        $init_organisasi_id = 'ORG-0000001';

        for($i=0; $i<$limit; $i++){
        	try {
                $init_user_id ++;
                $user = new User();
                $user->id_user = $init_user_id;
                $user->id_level = $daftar_level[1]->id_level;
                $user->username = $faker->userName;
                $user->kata_sandi = bcrypt('12345678');

                $organisasi = new Organisasi();
                $organisasi->id_organisasi = $init_organisasi_id;
                $organisasi->user()->associate($user);
                $organisasi->id_status_organisasi = $daftar_status_organisasi[$faker->numberBetween(0,2)]->id_status_organisasi;
	            $organisasi->nama_organisasi = $faker->name;
                $organisasi->alamat_organisasi = $faker->address;
                $organisasi->foto_sertifikat = md5(time()) . ".png";
                $organisasi->no_telp_organisasi = str_replace(" ", "", $faker->tollFreeNumber);
                $organisasi->handphone_organisasi = str_replace(" ", "", $faker->tollFreeNumber);
                $organisasi->email_organisasi = $faker->freeEmail;
                $organisasi->fax = str_replace(" ", "", $faker->tollFreeNumber);
                $organisasi->kode_pos = $faker->numberBetween(1111, 15555);
                $organisasi->web = strtolower(str_replace(" ", "", $faker->name)) . "com";
                $init_organisasi_id ++;
                
                $user->save();
	            $organisasi->save();

	            DB::commit();

	        } catch (\Exception $e) {
	            DB::rollback();
	            throw new \Exception($e, 1);

	        }
        }
    }
}
