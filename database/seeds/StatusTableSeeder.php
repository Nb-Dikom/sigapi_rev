<?php

use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //
        DB::table('status')->insert([
            ['id_status' => 'SG-01', 'status' => 'AWAS'],
            ['id_status' => 'SG-02', 'status' => 'SIAGA'],
            ['id_status' => 'SG-03', 'status' => 'WASPADA'],
            ['id_status' => 'SG-04', 'status' => 'NORMAL'],
        ]);
    }
}
