<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LevelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::BeginTransaction();
        try {
            DB::table('level')->insert([
                ['id_level' => 'LV-01', 'level' => 'Pegawai'],
                ['id_level' => 'LV-02', 'level' => 'Organisasi'],
            ]);

            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            throw new \Exception($e, 1);
        }
    }
}
