<?php

use App\Models\Gunung;
use App\Models\Pengamatan;
use App\Models\Validasi;
use App\Models\Verifikasi;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PengamatanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Gunung $gunung)
    {
        $faker = Faker\Factory::create('en_NZ');
        $limit = 12;
        
        $daftar_verifikasi = Verifikasi::all();
        $daftar_validasi = Validasi::all();
        $semeru = $gunung->get_by_name('SEMERU');
        $pegawai_id = 'PGW-0003';
        $init_pengamatan_id = 'PGN-000001';

        for ($i = 0; $i < $limit; $i++) {
            DB::BeginTransaction();
            try {
                $pengamatan = Pengamatan::create([
                    'id_pengamatan' => $init_pengamatan_id,
                    'id_gunung' => $semeru->id_gunung,
                    'id_verifikasi' => $daftar_verifikasi[2]->id_verifikasi,
                    'id_validasi' => $daftar_validasi[2]->id_validasi,
                    'id_pegawai' => $pegawai_id,
                    'nama_pengamatan' => $faker->name,
                    'foto_pengamatan' => md5(time()) . ".png",
                    'tanggal_pengamatan' => $faker->dateTimeBetween($startDate = '-25 days', $endDate = '25 days')
                ]);
                $init_pengamatan_id ++;
                DB::commit();

            } catch (\Exception $e) {
                DB::rollback();
                throw new \Exception($e, 1);
            }
	    }
    }
}
