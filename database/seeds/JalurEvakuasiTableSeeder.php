<?php

use App\Models\Gunung;
use App\Models\JalurEvakuasi;
use App\Models\Validasi;
use App\Models\Verifikasi;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JalurEvakuasiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Gunung $gunung)
    {
        $faker = Faker\Factory::create('en_NZ');
        $limit = 3;

        $daftar_verifikasi = Verifikasi::all();
        $daftar_validasi = Validasi::all();
        $semeru = $gunung->get_by_name('SEMERU');
        $pegawai_id = 'PGW-0003';
        $init_jalur_evakuasi_id = 'JEV-000001';

        DB::BeginTransaction();
        try {
            $jalur_evakuasi = JalurEvakuasi::create([
                'id_jalur_evakuasi' => $init_jalur_evakuasi_id,
                'id_gunung' => $semeru->id_gunung,
                'id_verifikasi' => $daftar_verifikasi[2]->id_verifikasi,
                'id_validasi' => $daftar_validasi[2]->id_validasi,
                'id_pegawai' => $pegawai_id,
                'file_jalur_evakuasi' => "https://services7.arcgis.com/SyInLC41qCR7fZ9v/arcgis/rest/services/jalur_evakuasi/FeatureServer/0",
                'tanggal_jalur_evakuasi' => Carbon::now()
            ]);
            $init_jalur_evakuasi_id ++;
            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            throw new \Exception($e, 1);
        }
    }
}
