<?php

use App\Models\Info;
use App\Models\Validasi;
use App\Models\Verifikasi;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InfoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create('en_NZ');
        $limit = 4;

        $daftar_verifikasi = Verifikasi::all();
        $daftar_validasi = Validasi::all();
        $pegawai_id = 'PGW-0003';
        $init_info_id = 'INF-000001';
        $nama_konten = [
            'Langkah Kewaspadaan', 'Bahaya Gunung Api (Primer)',
            'Bahaya Gunung Api (Sekunder)', 'Penanganan Gunung Api'
        ];
        $konten = [
            '<h3>Tingkat RAWAN I<br></h3><p><b>Normal:&nbsp;</b>masyarakat dapat melakukan kegiatan sehari-hari<br></p><p><b>Waspada:</b> masyarakat masih dapat melakukan kegiatannya dengan meningkatkan kewaspadaan<br></p><p><b>Siaga:</b> masyarakat meningkatkan kewaspadaan dengan tidak melakukan aktivitas di sekitar lembah sungai yang berhulu di daerah puncak<br></p><p><b>Awas:</b> masyarakat segera mengungsi berdasarkan perintah dari pemerintah daerah setempat sesuai rekomendasi teknis dari Kementrian Energi dan Sumber Daya Mineral<br></p><p><br></p><h3>Tingkat RAWAN II</h3><p><b>Normal:</b> masyarakat dapat melakukan kegiatan sehari-hari</p><p><b>Waspada:</b> masyarakat masih dapat melakukan kegiatannya dengan meningkatkan kewaspadaan terhadap ancaman bahaya<br></p><p><b>Siaga:</b> masyarakat mulai menyiapkan diri untuk mengungsi sambil menunggu perintah dari pemerintah daerah sesuai rekomendasi teknis dari Kementrian Energi dan Sumber Daya Mineral<br></p><p><b>Awas:</b> masyarakat segera mengungsi berdasarkan perintah dari pemerintah daerah sesuai rekomendasi teknis dari Kementrian Energi dan Sumber Daya Mineral<br></p><p><br></p><h3>Tingkat RAWAN III</h3><p><b>Normal:</b> masyarakat dapat melakukan kegiatan sehari-hari dengan tetap mematuhi ketentuan peraturan dari pemerintah daerah setempat sesuai dengan rekomendasi teknis dari Kementrian Energi dan Sumber Daya Mineral</p><p><b>Waspada:</b> masyarakat direkomendasikan tidak melakukan aktivitas di sekitar kawah<br></p><p><b>Siaga:</b> masyarakat tidak diperbolehkan melakukan aktivitas dan mulai menyiapkan diri untuk mengungsi<br></p><p><b>Awas:</b> masyarakat tidak diperbolehkan melakukan aktivitas dan segera mengungsi<br></p>',
            '<b>Leleran Lava,&nbsp;</b>merupakan cairan lava pekat dan panas dapat merusak segala infrakstruktur yang dilaluinya. Kecepatan aliran lava tergantung dari kekentalan magmanya, makin rendah kekentalannya maka makin jauh jangkauan alirannya. Suhu lava pada saat dierupsikan berkisar antara 800 s/d 1200 C. Pada umumnya di Indonesia leleran lava yang dierupsikan gunung api komposisi magmanya menengah sehingga pergerakannya cukup lamban sehingga manusia dapat menghindarkan diri dari terjangannya.<br><br><blockquote><img alt="" src="https://rolandgoeslaw.files.wordpress.com/2012/04/t.jpg"></blockquote><br><b>Aliran Piroklastik (awan panas),&nbsp;</b>aliran piroklastik dapat terjadi akibat runtuhan tiang asap erupsi plinian. Letusan berlangsung satu arah dari guguran kubah lava atau lidah lava dan aliran pada permukaan tanah (surge). Aliran piroklastik sangat dikontrol oleh gravitasi&nbsp; dan cenderung mengalir melalui daerah rendah atau lembah. Mobilitas tinggi aliran piroklastik dipengaruhi oleh pelepasan gas dari magma atau lava atau dari udara yang terpanaskan pada saat mengalir. Kecepatan aliran dapat mencapai 150 s/d 200 km/jam dan jangkauan aliran dapat mencapai puluhan kilometer walaupun bergerak di atas air/laut.<br><br><img alt="" src="http://static.republika.co.id/uploads/images/headline_slide/awan_panas_merapi_101028180118.jpg"><br><br><b>Jatuhan Piroklastik,</b>&nbsp;terjadi dari letusan yang membentuk tiang asap cukup tinggi, pada saat energinya habis abu akan menyebar sesuai dengan arah angin kemudian jatuh lagi ke permukaan bumi. Hujan abu ini bukan merupakan bahaya langsung bagi manusia, tetapi endapan abunya akan merontokkan daun-daun dan pepohonan kecil sehingga merusak agro dan pada ketebalan tertentu dapat merobohkan atap rumah. Sebaran abu di udara dapat menggelapkan bumi beberapa saat serta mengancam bahaya bagi jalur penerbangan.<br><br><b>Lahar Letusan,</b>&nbsp;terjadii pada gunung api yang mempunyai danau kawah. Apabila volume air alam kawah cukup besar akan menjadi ancaman langsung saat terjadi letusan dengan menumpahkan lumpur panas.<br><br><b>Gas Vulkanik Beracun,</b>&nbsp;umumnya muncul pada gunung api aktif berupa CO, CO2, HCN, H2S, SO2 dll. Bila konsentrasi berada diatas ambang batas gas tersebut sangat membahayakan, bahkan bisa membunuh.<br><blockquote><blockquote></blockquote></blockquote><blockquote><blockquote></blockquote></blockquote>',
            '<b>Lahar Hujan,</b> terjadi apabila endapan material lepas hasil erupsi gunung api yang diendapkan pada puncak dan lereng terangkut oleh hujan. Aliran lahar ini berupa aliran lumpur yang sangat pekat sehingga dapat mengangkut material berbagai ukuran. Bongkahan batu besar berdiameter lebih dari 5m dapat mengapung pada aliran lumpur ini. Lahar juga dapat merubah topografi sungai yang dilaluinya dan merusak infrastruktur.<br><br><b>Banjir Bandang,</b> terjadi akibat longsoran material vulkanik lama pada lereng gunung api karena jenuh air atau curah hujan cukup tinggi. Aliran lumpur disini tidak begitu pekat seperti lahar, tetapi cukup membahayakan bagi penduduk yang bekerja disungai dimana dengan tiba-tiba terjadi aliran lumpur.<br><br><b>Longsoran Vulkanik</b>, dapat terjadi akibat letusan gunung api, eksplosi uap air, alterasi batuan pada tubuh gunung api sehingga menjadi rapuh, atau terkena gempa bumi berintensitas kuat. Longsoran vulkanik ini jarang terjadi di gunung api secara umum, sehingga dalam peta kawasan rawan bencana tidak mencantumkan bahaya akibat longsoran vulkanik.<br>',
            '<h4>Ketika Terjadi Letusan Gunung Api</h4><ul><li>Hindari daerah rawan bencana seperti lereng gunung api, lembah dan daerah aliran lahar.</li><li>Ditempat terbuka, lindungi diri dari abu letusan gunung api.</li><li>Jangan memakai lensa kontak.</li><li>Pakai masker atau kain untuk menutup mulut dan hidung.</li><li>Kenakan pakaian yang melindungi tubuh seperti baju lengan panjang, celana panjang dan topi</li></ul><h4>Setelah Terjadi Letusan Gunung Api</h4><ul><li>Jauhi willayah yang terkena hujan abu.</li><li>Hindari mengendarai mobil di daerah yang terkena hujan abu vulkanik sebaba bisa merusak mesin kendaraan seperti rem, persneling hingga pengapian.</li><li>Bersihkan atap dari timbunan debu vulkanik, karena beratnya bisa merobohkan dan merusak atap bangunan.</li></ul>'
        ];

        for ($i = 0; $i < $limit; $i++) {
            DB::BeginTransaction();
            try {
                $info = Info::create([
                    'id_info' => $init_info_id,
                    'id_verifikasi' => $daftar_verifikasi[2]->id_verifikasi,
                    'id_validasi' => $daftar_validasi[2]->id_validasi,
                    'id_pegawai' => $pegawai_id,
                    'nama_info' => $nama_konten[$i],
                    'konten' => $konten[$i],
                    'tanggal_info' => $faker->dateTimeBetween($starDate = '-2 years', $endDate = 'now')
                ]);
                $init_info_id ++;
                DB::commit();

            } catch (\Exception $e) {
                DB::rollback();
                throw new \Exception($e, 1);
            }
	    }
    }
}
