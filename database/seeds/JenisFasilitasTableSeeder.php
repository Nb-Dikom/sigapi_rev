<?php

use Illuminate\Database\Seeder;

class JenisFasilitasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('jenis_fasilitas')->insert([
            ['id_jenis_fasilitas' => 'JF-01', 'jenis_fasilitas' => 'Kantor Polisi'],
            ['id_jenis_fasilitas' => 'JF-02', 'jenis_fasilitas' => 'Kantor Pemadam Kebakaran'],
            ['id_jenis_fasilitas' => 'JF-03', 'jenis_fasilitas' => 'Kantor Pemerintahan'],
            ['id_jenis_fasilitas' => 'JF-04', 'jenis_fasilitas' => 'Pertokoan'],
            ['id_jenis_fasilitas' => 'JF-05', 'jenis_fasilitas' => 'Pusat Perbelanjaan'],
            ['id_jenis_fasilitas' => 'JF-06', 'jenis_fasilitas' => 'Pasar Tradisional'],
            ['id_jenis_fasilitas' => 'JF-07', 'jenis_fasilitas' => 'Tempat Rekreasi (Buatan dan Alami di Pegunungan)'],
            ['id_jenis_fasilitas' => 'JF-08', 'jenis_fasilitas' => 'Terminal Bis'],
            ['id_jenis_fasilitas' => 'JF-09', 'jenis_fasilitas' => 'Stasiun Kereta Api'],
            ['id_jenis_fasilitas' => 'JF-10', 'jenis_fasilitas' => 'Posko (Rupusdalops)'],
            ['id_jenis_fasilitas' => 'JF-11', 'jenis_fasilitas' => 'Rumah Sakit'],
            ['id_jenis_fasilitas' => 'JF-12', 'jenis_fasilitas' => 'Puskesmas'],
        ]);
    }
}
