<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ValidasiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::BeginTransaction();
        try {
            DB::table('validasi')->insert([
                ['id_validasi' => 'VLD-01', 'validasi' => 'Pending'],
                ['id_validasi' => 'VLD-02', 'validasi' => 'Tidak Valid'],
                ['id_validasi' => 'VLD-03', 'validasi' => 'Valid'],
            ]);

            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            throw new \Exception($e, 1);
        }
    }
}
