<?php

use App\Models\Jabatan;
use App\Models\Level;
use App\Models\Pegawai;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PegawaiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $username = [
            'admin', 'subag_umum', 'staf_bencana', 'kasi_bencana', 'kabid_bencana', 'kepala_bpbd', 'subag_bencana'
        ];
        $nama = [
            'Diar Ichrom Septianto',
            'Agung Laksono',
            'Ahmad Djunaedi',
            'Rivai Amin',
            'Tompi Hariadi',
            'Syahriga Syahrul',
            'Muhammad Anas',
        ];
        $email = [
            'diar@mail.com',
            'agung@mail.com',
            'djuned@mail.com',
            'amin@mail.com',
            'tompi@mail.com',
            'riga@mail.com',
            'anas@mail.com',
        ];

        $daftar_level = Level::all();
        $daftar_jabatan = Jabatan::all();
        $init_user_id = 'USR-000001';
        $init_pegawai_id = 'PGW-0001';

        $instansi = [
            'Badan Penanggulangan Bencana Daerah Tingkat I', 'Pemerintah Daerah Jawa Timur'
        ];

        $limit = 7;

        for($i=0; $i<$limit; $i++){
            DB::BeginTransaction();
            try {
                $user = new User();
                $user->id_user = $init_user_id;
                $user->id_level = $daftar_level[0]->id_level;
                $user->username = $username[$i];
                $user->kata_sandi = bcrypt('12345678');
                $init_user_id ++;
                
                $pegawai = new Pegawai();
                $pegawai->id_pegawai = $init_pegawai_id;
                $pegawai->id_jabatan = $daftar_jabatan[$i]->id_jabatan;
                $pegawai->user()->associate($user);
                $pegawai->nama_pegawai = $nama[$i];
                $pegawai->email_pegawai = $email[$i];
                $pegawai->no_telp_pegawai = '08' . rand(10000000000, 10000000000);
                $pegawai->instansi_pegawai = $instansi[0];
                if($i == 5){
                    $pegawai->instansi_pegawai = $instansi[1];
                }
                $init_pegawai_id ++;
                
                $user->save();
                $pegawai->save();
                
                DB::commit();

            } catch (\Exception $e) {
                DB::rollback();
                throw new \Exception($e, 1);
            }
        }
    }
}
