<?php

use App\Models\Gunung;
use App\Models\Status;
use App\Models\StatusGunung;
use App\Models\Validasi;
use App\Models\Verifikasi;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusGunungTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Gunung $gunung)
    {
        $faker = Faker\Factory::create('en_NZ');
        
        $daftar_verifikasi = Verifikasi::all();
        $daftar_validasi = Validasi::all();
        $daftar_status = Status::all();
        $pegawai_id = 'PGW-0003';
        $init_status_id = 'STG-000001';
        $raw = [
            [0, 'SINABUNG', '02-06-2015'],
            [1, 'AGUNG', '10-02-2018'],
            [2, 'ILI LEWOTOLOK', '07-10-2017'],
            [2, 'BANDA API', '05-04-2017'],
            [2, 'DEMPO', '05-04-2017'],
            [2, 'BROMO', '20-10-2016'],
            [2, 'RINJANI', '27-09-2016'],
            [2, 'LOKON', '22-08-2016'],
            [2, 'SOPUTAN', '21-04-2016'],
            [2, 'KARANGETANG', '16-03-2016'],
            [2, 'GAMALAMA', '10-03-2015'],
            [2, 'SANGEANGAPI', '17-06-2014'],
            [2, 'ROKATENDA', '07-04-2014'],
            [2, 'IBU', '10-12-2013'],
            [2, 'GAMKONORA', '01-07-2013'],
            [2, 'SEMERU', '02-05-2012'],
            [2, 'ANAK KRAKATAU', '26-01-2012'],
            [2, 'MARAPI', '03-08-2011'],
            [2, 'DUKONO', '15-06-2008'],
            [2, 'KERINCI', '09-09-2007'],
        ];
        $limit = count($raw);

        for ($i = 0; $i < $limit; $i++) {
            $search_gn = $gunung->get_by_name($raw[$i][1]);
            $date = Carbon::createFromFormat('d-m-Y', $raw[$i][2]);
            DB::BeginTransaction();
            try {
                $status_gunung = StatusGunung::create([
                    'id_status_gunung' => $init_status_id,
                    'id_status' => $daftar_status[$raw[$i][0]]->id_status,
                    'id_gunung' => $search_gn->id_gunung,
                    'id_verifikasi' => $daftar_verifikasi[2]->id_verifikasi,
                    'id_validasi' => $daftar_validasi[2]->id_validasi,
                    'id_pegawai' => $pegawai_id,
                    'tanggal_status_gunung' => $date,
                ]);
                $init_status_id ++;
                DB::commit();

            } catch (\Exception $e) {
                DB::rollback();
                throw new \Exception($e, 1);
            }
	    }
    }
}
