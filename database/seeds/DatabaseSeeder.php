<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // seeding data for level user
        $this->call(LevelTableSeeder::class);

        // for pegawai
        $this->call(JabatanTableSeeder::class);
        $this->call(PegawaiTableSeeder::class);

        // for organisasi
        $this->call(StatusOrganisasiTableSeeder::class);
        $this->call(OrganisasiTableSeeder::class);

        // for gunung
        $this->call(GunungTableSeeder::class);

        // for authentication
        $this->call(VerifikasiTableSeeder::class);
        $this->call(ValidasiTableSeeder::class);

        // for info
        $this->call(InfoTableSeeder::class);

        // for pengamatan
        $this->call(PengamatanTableSeeder::class);

        // for kawasan rawan bencana
        $this->call(JenisKrbTableSeeder::class);
        $this->call(KrbTableSeeder::class);

        // for status gunung
        $this->call(StatusTableSeeder::class);
        $this->call(StatusGunungTableSeeder::class);

        // for fasilitas
        $this->call(JenisFasilitasTableSeeder::class);
        $this->call(FasilitasTableSeeder::class);

        // for jalur evakuasi
        $this->call(JalurEvakuasiTableSeeder::class);
    }
}
