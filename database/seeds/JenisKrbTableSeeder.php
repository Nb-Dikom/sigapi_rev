<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JenisKrbTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jenis_krb')->insert([
            ['id_jenis_krb' => 'JK-01', 'jenis_krb' => 'Rawan I'],
            ['id_jenis_krb' => 'JK-02', 'jenis_krb' => 'Rawan II'],
            ['id_jenis_krb' => 'JK-03', 'jenis_krb' => 'Rawan III']
        ]);
    }
}
