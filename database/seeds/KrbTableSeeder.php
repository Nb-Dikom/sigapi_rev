<?php

use App\Models\Gunung;
use App\Models\Krb;
use App\Models\JenisKrb;
use App\Models\Validasi;
use App\Models\Verifikasi;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KrbTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Gunung $gunung)
    {
        $faker = Faker\Factory::create('en_NZ');
        $limit = 3;

        $daftar_verifikasi = Verifikasi::all();
        $daftar_validasi = Validasi::all();
        $daftar_jenis_krb = JenisKrb::all();
        $semeru = $gunung->get_by_name('SEMERU');
        $pegawai_id = 'PGW-0003';
        $init_krb_id = 'KRB-000001';

        for ($i = 0; $i < $limit; $i++) {
            DB::BeginTransaction();
            try {
                $krb = Krb::create([
                    'id_krb' => $init_krb_id,
                    'id_jenis_krb' => $daftar_jenis_krb[$i]->id_jenis_krb,
                    'id_gunung' => $semeru->id_gunung,
                    'id_verifikasi' => $daftar_verifikasi[2]->id_verifikasi,
                    'id_validasi' => $daftar_validasi[2]->id_validasi,
                    'id_pegawai' => $pegawai_id,
                    'file_krb' => "https://services7.arcgis.com/SyInLC41qCR7fZ9v/arcgis/rest/services/kawasan_rawan_bencana/FeatureServer/" . ($i),
                    'tanggal_krb' => Carbon::now()
                ]);
                $init_krb_id ++;
                DB::commit();

            } catch (\Exception $e) {
                DB::rollback();
                throw new \Exception($e, 1);
            }
	    }
    }
}
