<?php

use Illuminate\Database\Seeder;

class StatusOrganisasiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('status_organisasi')->insert([
            ['id_status_organisasi' => 'SO-01', 'status_organisasi' => 'Pending'],
            ['id_status_organisasi' => 'SO-02', 'status_organisasi' => 'Setuju'],
            ['id_status_organisasi' => 'SO-03', 'status_organisasi' => 'Tidak Setuju'],
        ]);
    }
}
