<?php

use App\Models\Krb;
use App\Models\Fasilitas;
use App\Models\JenisFasilitas;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class MitigasiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('verifikasi')->insert([
            ['verifikasi' => 'Pending'],
            ['verifikasi' => 'Lengkap'],
            ['verifikasi' => 'Tidak Lengkap'],
        ]);
        DB::table('validasi')->insert([
            ['validasi' => 'Pending'],
            ['validasi' => 'Valid'],
            ['validasi' => 'Tidak Valid'],
        ]);
        DB::table('jenis_fasilitas')->insert([
            ['jenis_fasilitas' => 'Kantor Polisi'],
            ['jenis_fasilitas' => 'Kantor Pemadam Kebakaran'],
            ['jenis_fasilitas' => 'Kantor Pemerintahan'],
            ['jenis_fasilitas' => 'Pertokoan'],
            ['jenis_fasilitas' => 'Pusat Perbelanjaan'],
            ['jenis_fasilitas' => 'Pasar Tradisional'],
            ['jenis_fasilitas' => 'Tempat Rekreasi (Buatan dan Alami di Pegunungan)'],
            ['jenis_fasilitas' => 'Terminal Bis'],
            ['jenis_fasilitas' => 'Stasiun Kereta Api'],
            ['jenis_fasilitas' => 'Posko (Rupusdalops)'],
            ['jenis_fasilitas' => 'Rumah Sakit'],
            ['jenis_fasilitas' => 'Puskesmas'],
        ]);

        for($i = 1; $i <= 3; $i++){
            $krb = Krb::create([
                'id_jenis_krb' => $i,
                'id_gunung' => 15,
                'id_verifikasi' => 1,
                'id_validasi' => 1,
                'id_pegawai' => 3,
                'file_krb' => "https://services7.arcgis.com/SyInLC41qCR7fZ9v/arcgis/rest/services/kawasan_rawan_bencana/FeatureServer/" . ($i-1),
                'tanggal_krb' => Carbon::now(),
            ]);
        }

        // $coordinate = [
        //     [112.98560415832, -8.2143108303889],
        //     [112.99980469826, -8.1949842634537],
        //     [113.07462706262, -8.2561408037356],
        //     [113.16899129345, -8.2813523228839],
        //     [113.10650069252, -8.2910783388041],
        //     [112.92634552735, -8.3088786157196],
        //     [112.94387488687, -8.3314198396149],
        // ];

        // for($i = 1; $i <= 6; $i++){
        //     if($i <= 2){
        //         $jenis_fasilitas = JenisFasilitas::find(10);
        //     }
        //     else if($i >= 2 && $i <= 4){
        //         $jenis_fasilitas = JenisFasilitas::find(11);
        //     }
        //     else {
        //         $jenis_fasilitas = JenisFasilitas::find(12);
        //     }

        //     $fasilitas = Fasilitas::create([
        //         'id_jenis_fasilitas' => $jenis_fasilitas->id_jenis_fasilitas,
        //         'id_gunung' => 15,
        //         'id_verifikasi' => 1,
        //         'id_validasi' => 1,
        //         'id_pegawai' => 3,
        //         'nama_fasilitas' => 'Fasilitas',
        //         'alamat_fasilitas' => 'Lorem ipsum col de ame',
        //         'longitude' => $coordinate[$i-1][0],
        //         'latitude' => $coordinate[$i-1][1],
        //         'tanggal_fasilitas' => Carbon::now(),
        //     ]);
        // }

        
        DB::table('info')->insert([
            ['id_info' => 1, 'nama_info' => 'Langkah Kewaspadaan', 'konten' => '<h3>Tingkat RAWAN I<br></h3><p><b>Normal:&nbsp;</b>masyarakat dapat melakukan kegiatan sehari-hari<br></p><p><b>Waspada:</b> masyarakat masih dapat melakukan kegiatannya dengan meningkatkan kewaspadaan<br></p><p><b>Siaga:</b> masyarakat meningkatkan kewaspadaan dengan tidak melakukan aktivitas di sekitar lembah sungai yang berhulu di daerah puncak<br></p><p><b>Awas:</b> masyarakat segera mengungsi berdasarkan perintah dari pemerintah daerah setempat sesuai rekomendasi teknis dari Kementrian Energi dan Sumber Daya Mineral<br></p><p><br></p><h3>Tingkat RAWAN II</h3><p><b>Normal:</b> masyarakat dapat melakukan kegiatan sehari-hari</p><p><b>Waspada:</b> masyarakat masih dapat melakukan kegiatannya dengan meningkatkan kewaspadaan terhadap ancaman bahaya<br></p><p><b>Siaga:</b> masyarakat mulai menyiapkan diri untuk mengungsi sambil menunggu perintah dari pemerintah daerah sesuai rekomendasi teknis dari Kementrian Energi dan Sumber Daya Mineral<br></p><p><b>Awas:</b> masyarakat segera mengungsi berdasarkan perintah dari pemerintah daerah sesuai rekomendasi teknis dari Kementrian Energi dan Sumber Daya Mineral<br></p><p><br></p><h3>Tingkat RAWAN III</h3><p><b>Normal:</b> masyarakat dapat melakukan kegiatan sehari-hari dengan tetap mematuhi ketentuan peraturan dari pemerintah daerah setempat sesuai dengan rekomendasi teknis dari Kementrian Energi dan Sumber Daya Mineral</p><p><b>Waspada:</b> masyarakat direkomendasikan tidak melakukan aktivitas di sekitar kawah<br></p><p><b>Siaga:</b> masyarakat tidak diperbolehkan melakukan aktivitas dan mulai menyiapkan diri untuk mengungsi<br></p><p><b>Awas:</b> masyarakat tidak diperbolehkan melakukan aktivitas dan segera mengungsi<br></p>', 'tanggal_info' => '2017-11-20', 'id_pegawai' => 1],
            ['id_info' => 2, 'nama_info' => 'Bahaya Gunung Api (Primer)', 'konten' => '<b>Leleran Lava,&nbsp;</b>merupakan cairan lava pekat dan panas dapat merusak segala infrakstruktur yang dilaluinya. Kecepatan aliran lava tergantung dari kekentalan magmanya, makin rendah kekentalannya maka makin jauh jangkauan alirannya. Suhu lava pada saat dierupsikan berkisar antara 800 s/d 1200 C. Pada umumnya di Indonesia leleran lava yang dierupsikan gunung api komposisi magmanya menengah sehingga pergerakannya cukup lamban sehingga manusia dapat menghindarkan diri dari terjangannya.<br><br><blockquote><img alt="" src="https://rolandgoeslaw.files.wordpress.com/2012/04/t.jpg"></blockquote><br><b>Aliran Piroklastik (awan panas),&nbsp;</b>aliran piroklastik dapat terjadi akibat runtuhan tiang asap erupsi plinian. Letusan berlangsung satu arah dari guguran kubah lava atau lidah lava dan aliran pada permukaan tanah (surge). Aliran piroklastik sangat dikontrol oleh gravitasi&nbsp; dan cenderung mengalir melalui daerah rendah atau lembah. Mobilitas tinggi aliran piroklastik dipengaruhi oleh pelepasan gas dari magma atau lava atau dari udara yang terpanaskan pada saat mengalir. Kecepatan aliran dapat mencapai 150 s/d 200 km/jam dan jangkauan aliran dapat mencapai puluhan kilometer walaupun bergerak di atas air/laut.<br><br><img alt="" src="http://static.republika.co.id/uploads/images/headline_slide/awan_panas_merapi_101028180118.jpg"><br><br><b>Jatuhan Piroklastik,</b>&nbsp;terjadi dari letusan yang membentuk tiang asap cukup tinggi, pada saat energinya habis abu akan menyebar sesuai dengan arah angin kemudian jatuh lagi ke permukaan bumi. Hujan abu ini bukan merupakan bahaya langsung bagi manusia, tetapi endapan abunya akan merontokkan daun-daun dan pepohonan kecil sehingga merusak agro dan pada ketebalan tertentu dapat merobohkan atap rumah. Sebaran abu di udara dapat menggelapkan bumi beberapa saat serta mengancam bahaya bagi jalur penerbangan.<br><br><b>Lahar Letusan,</b>&nbsp;terjadii pada gunung api yang mempunyai danau kawah. Apabila volume air alam kawah cukup besar akan menjadi ancaman langsung saat terjadi letusan dengan menumpahkan lumpur panas.<br><br><b>Gas Vulkanik Beracun,</b>&nbsp;umumnya muncul pada gunung api aktif berupa CO, CO2, HCN, H2S, SO2 dll. Bila konsentrasi berada diatas ambang batas gas tersebut sangat membahayakan, bahkan bisa membunuh.<br><blockquote><blockquote></blockquote></blockquote><blockquote><blockquote></blockquote></blockquote>', 'tanggal_info' => '2017-11-20', 'id_pegawai' => 1],
            ['id_info' => 3, 'nama_info' => 'Bahaya Gunung Api (Sekunder)', 'konten' => '<b>Lahar Hujan,</b> terjadi apabila endapan material lepas hasil erupsi gunung api yang diendapkan pada puncak dan lereng terangkut oleh hujan. Aliran lahar ini berupa aliran lumpur yang sangat pekat sehingga dapat mengangkut material berbagai ukuran. Bongkahan batu besar berdiameter lebih dari 5m dapat mengapung pada aliran lumpur ini. Lahar juga dapat merubah topografi sungai yang dilaluinya dan merusak infrastruktur.<br><br><b>Banjir Bandang,</b> terjadi akibat longsoran material vulkanik lama pada lereng gunung api karena jenuh air atau curah hujan cukup tinggi. Aliran lumpur disini tidak begitu pekat seperti lahar, tetapi cukup membahayakan bagi penduduk yang bekerja disungai dimana dengan tiba-tiba terjadi aliran lumpur.<br><br><b>Longsoran Vulkanik</b>, dapat terjadi akibat letusan gunung api, eksplosi uap air, alterasi batuan pada tubuh gunung api sehingga menjadi rapuh, atau terkena gempa bumi berintensitas kuat. Longsoran vulkanik ini jarang terjadi di gunung api secara umum, sehingga dalam peta kawasan rawan bencana tidak mencantumkan bahaya akibat longsoran vulkanik.<br>', 'tanggal_info' => '2017-11-20', 'id_pegawai' => 1],
            ['id_info' => 4, 'nama_info' => 'Penanganan Gunung Api', 'konten' => '<h4>Ketika Terjadi Letusan Gunung Api</h4><ul><li>Hindari daerah rawan bencana seperti lereng gunung api, lembah dan daerah aliran lahar.</li><li>Ditempat terbuka, lindungi diri dari abu letusan gunung api.</li><li>Jangan memakai lensa kontak.</li><li>Pakai masker atau kain untuk menutup mulut dan hidung.</li><li>Kenakan pakaian yang melindungi tubuh seperti baju lengan panjang, celana panjang dan topi</li></ul><h4>Setelah Terjadi Letusan Gunung Api</h4><ul><li>Jauhi willayah yang terkena hujan abu.</li><li>Hindari mengendarai mobil di daerah yang terkena hujan abu vulkanik sebaba bisa merusak mesin kendaraan seperti rem, persneling hingga pengapian.</li><li>Bersihkan atap dari timbunan debu vulkanik, karena beratnya bisa merobohkan dan merusak atap bangunan.</li></ul>', 'tanggal_info' => '2017-11-20', 'id_pegawai' => 1],
        ]);

        DB::table('file_peta')->insert([
            ['id_file_peta' => 1, 'nama_file_peta' => 'Kawasan Rawan Bencana', 'shapefile' => 'a7d26d1cda634051b38763c5e79b1322', 'tanggal_file_peta' => '2017-11-29', 'id_verifikasi' => 1, 'id_validasi' => 1, 'id_gunung' => 15, 'id_pegawai' => 2],
            ['id_file_peta' => 2, 'nama_file_peta' => 'Administrasi', 'shapefile' => '9444ae08e32b46568328dfdab19fddeb', 'tanggal_file_peta' => '2017-10-31', 'id_verifikasi' => 1, 'id_validasi' => 1, 'id_gunung' => 15, 'id_pegawai' => 2],
        ]);
        
        DB::table('pengamatan')->insert([
            ['id_pengamatan' => 1, 'nama_pengamatan' => 'CCTV Semeru Feb 2016', 'foto_pengamatan' => '0ab960b556ec01c04f4c5fdbc1a717d5.jpg', 'tanggal_pengamatan' => '2016-02-29', 'id_verifikasi' => 1, 'id_validasi' => 1, 'id_gunung' => 15, 'id_pegawai' => 3],
            ['id_pengamatan' => 2, 'nama_pengamatan' => 'CCTV Semeru Des 2013', 'foto_pengamatan' => '9ad2dee900ad5351b74d0f4c496739bb.jpg', 'tanggal_pengamatan' => '2013-12-31', 'id_verifikasi' => 1, 'id_validasi' => 1, 'id_gunung' => 15, 'id_pegawai' => 3],
        ]);
    }
}
