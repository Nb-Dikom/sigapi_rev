<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusGunungTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status_gunung', function (Blueprint $table) {
            $table->string('id_status_gunung', 10)->primary();
            $table->string('id_status', 5)->index();
            $table->string('id_gunung', 5)->index();
            $table->string('id_verifikasi', 6)->index();
            $table->string('id_validasi', 6)->index();
            $table->string('id_pegawai', 8)->index();
            $table->date('tanggal_status_gunung');
        });

        Schema::table('status_gunung', function(Blueprint $table){
            $table->foreign('id_status')->references('id_status')->on('status');
            $table->foreign('id_gunung')->references('id_gunung')->on('gunung');
            $table->foreign('id_verifikasi')->references('id_verifikasi')->on('verifikasi');
            $table->foreign('id_validasi')->references('id_validasi')->on('validasi');
            $table->foreign('id_pegawai')->references('id_pegawai')->on('pegawai');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status_gunung');
    }
}
