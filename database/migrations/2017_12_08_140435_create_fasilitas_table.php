<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFasilitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fasilitas', function (Blueprint $table) {
            $table->string('id_fasilitas', 10)->primary();
            $table->string('id_jenis_fasilitas', 5)->index();
            $table->string('id_gunung', 5)->index();
            $table->string('id_verifikasi', 6)->index();
            $table->string('id_validasi', 6)->index();
            $table->string('id_pegawai', 8)->index();
            $table->string('nama_fasilitas', 48);
            $table->string('alamat_fasilitas', 64);
            $table->string('foto_fasilitas', 38)->nullable();
            $table->string('longitude', 16);
            $table->string('latitude', 16);
            $table->date('tanggal_fasilitas');
        });

        Schema::table('fasilitas', function(Blueprint $table){
            $table->foreign('id_jenis_fasilitas')->references('id_jenis_fasilitas')->on('jenis_fasilitas');
            $table->foreign('id_gunung')->references('id_gunung')->on('gunung');
            $table->foreign('id_verifikasi')->references('id_verifikasi')->on('verifikasi');
            $table->foreign('id_validasi')->references('id_validasi')->on('validasi');
            $table->foreign('id_pegawai')->references('id_pegawai')->on('pegawai');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fasilitas');
    }
}
