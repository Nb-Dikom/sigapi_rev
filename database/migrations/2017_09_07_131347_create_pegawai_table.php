<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePegawaiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('pegawai', function (Blueprint $table) {
            $table->string('id_pegawai', 8)->primary();
            $table->string('id_jabatan', 5)->index();
            $table->string('id_user', 10)->index();
            $table->string('nama_pegawai', 32);
            $table->string('email_pegawai', 32)->nullable();
            $table->string('no_telp_pegawai', 13)->nullable();
            $table->string('instansi_pegawai', 64)->nullable();
            $table->string('foto_pegawai', 38)->nullable();
        });

        Schema::table('pegawai', function (Blueprint $table) {
            $table->foreign('id_jabatan')->references('id_jabatan')->on('jabatan');
            $table->foreign('id_user')->references('id_user')->on('user')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('pegawai');
    }
}
