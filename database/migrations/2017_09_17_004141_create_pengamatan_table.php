<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengamatanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengamatan', function (Blueprint $table) {
            $table->string('id_pengamatan', 10)->primary();
            $table->string('id_gunung', 5)->index();
            $table->string('id_verifikasi', 6)->index();
            $table->string('id_validasi', 6)->index();
            $table->string('id_pegawai', 8)->index();
            $table->string('nama_pengamatan', 32);
            $table->string('foto_pengamatan', 38);
            $table->date('tanggal_pengamatan');
    });

        Schema::table('pengamatan', function (Blueprint $table) {
            $table->foreign('id_gunung')->references('id_gunung')->on('gunung');
            $table->foreign('id_verifikasi')->references('id_verifikasi')->on('verifikasi');
            $table->foreign('id_validasi')->references('id_validasi')->on('validasi');
            $table->foreign('id_pegawai')->references('id_pegawai')->on('pegawai');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengamatan');
    }
}
