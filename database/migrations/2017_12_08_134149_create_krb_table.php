<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKrbTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('krb', function (Blueprint $table) {
            $table->string('id_krb', 10)->primary();
            $table->string('id_jenis_krb', 5)->index();
            $table->string('id_gunung', 5)->index();
            $table->string('id_verifikasi', 6)->index();
            $table->string('id_validasi', 6)->index();
            $table->string('id_pegawai', 8)->index();
            $table->string('file_krb', 128);
            $table->date('tanggal_krb');
        });

        Schema::table('krb', function (Blueprint $table) {
            $table->foreign('id_jenis_krb')->references('id_jenis_krb')->on('jenis_krb');
            $table->foreign('id_gunung')->references('id_gunung')->on('gunung');
            $table->foreign('id_verifikasi')->references('id_verifikasi')->on('verifikasi');
            $table->foreign('id_validasi')->references('id_validasi')->on('validasi');
            $table->foreign('id_pegawai')->references('id_pegawai')->on('pegawai');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('krb');
    }
}
