<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info', function (Blueprint $table) {
            $table->string('id_info', 10)->primary();
            $table->string('id_verifikasi', 6)->index();
            $table->string('id_validasi', 6)->index();
            $table->string('id_pegawai', 8)->index();
            $table->string('nama_info', 64);
            $table->text('konten')->nullable();
            $table->date('tanggal_info');
        });

        Schema::table('info', function (Blueprint $table) {
            $table->foreign('id_verifikasi')->references('id_verifikasi')->on('verifikasi');
            $table->foreign('id_validasi')->references('id_validasi')->on('validasi');
            $table->foreign('id_pegawai')->references('id_pegawai')->on('pegawai');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('info');
    }
}
