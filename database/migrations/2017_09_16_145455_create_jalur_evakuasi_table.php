<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJalurEvakuasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jalur_evakuasi', function (Blueprint $table) {
            $table->string('id_jalur_evakuasi', 10)->primary();
            $table->string('id_gunung', 5)->index();
            $table->string('id_verifikasi', 6)->index();
            $table->string('id_validasi', 6)->index();
            $table->string('id_pegawai', 8)->index();
            $table->string('file_jalur_evakuasi', 128);
            $table->date('tanggal_jalur_evakuasi');
        });

        Schema::table('jalur_evakuasi', function (Blueprint $table) {
            $table->foreign('id_gunung')->references('id_gunung')->on('gunung');
            $table->foreign('id_verifikasi')->references('id_verifikasi')->on('verifikasi');
            $table->foreign('id_validasi')->references('id_validasi')->on('validasi');
            $table->foreign('id_pegawai')->references('id_pegawai')->on('pegawai');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jalur_evakuasi');
    }
}
