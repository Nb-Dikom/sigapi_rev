<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganisasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organisasi', function (Blueprint $table) {
            $table->string('id_organisasi', 11)->primary();
            $table->string('id_user', 10)->index();
            $table->string('id_status_organisasi', 5)->index();
            $table->string('nama_organisasi', 64);
            $table->string('alamat_organisasi', 64);
            $table->string('foto_sertifikat', 38);
            $table->string('no_telp_organisasi', 13);
            $table->string('handphone_organisasi', 13)->nullable();
            $table->string('email_organisasi', 32);
            $table->string('fax', 13)->nullable();
            $table->string('kode_pos', 6);
            $table->string('web', 32)->nullable();
        });

        Schema::table('organisasi', function (Blueprint $table) {
            $table->foreign('id_user')->references('id_user')->on('user')->onDelete('cascade');
            $table->foreign('id_status_organisasi')->references('id_status_organisasi')->on('status_organisasi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organisasi');
    }
}
