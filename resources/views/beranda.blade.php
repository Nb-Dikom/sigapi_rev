@extends('layouts._app_frontend')
@section('custom_css')
<link href="{{ asset('assets/plugins/owl.carousel/assets/owl.carousel.css') }}" rel="stylesheet">
<style>
    .carousel-inner > .item > img{
        margin:auto;
    }
    a.btn-transparent {
        color: #7c858e;
        font-size: 16px;
        padding: 8px 18px;
        white-space: nowrap;
        text-decoration: none;
        border: solid 1px #7c858e;
        background: none;
    }
    td {
        padding-top: 5px;
    }
</style>
@endsection
@section('content')
<div class="container">
    <div class="row margin-bottom-25">
        <div class="col-md-12 col-sm-12">
            <div class="col-md-9">
                <div class="front-carousel">
                    <div id="myCarousel" class="carousel slide text-center">
                        <div class="carousel-inner">
                        <?php $total = count($list_pengamatan); $index = 0; ?>
                            @foreach($list_pengamatan as $pengamatan)
                            <div class="item text-center @if($index==0) active @endif">
                                <img class="img-thumbnail" src="{{ asset('/upload/pengamatan') .'/'. $pengamatan->foto_pengamatan }}" alt="{{ $pengamatan->nama_pengamatan }}" style="height:400px;">
                                <div class="carousel-caption">
                                    <p>{{ $pengamatan->nama_pengamatan }}</p>
                                </div>
                            </div>
                            <?php $index++; ?>
                            @endforeach
                        </div>
                        <a class="carousel-control left" href="#myCarousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>
                        <a class="carousel-control right" href="#myCarousel" data-slide="next"><i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-3" style="border:3px solid #e18604; height:400px">
                <h4 class="margin-top-10">STATUS GUNUNG API</h4>
                <hr style="margin-top:0px; border-top:3px dashed #e18604">
                <div class="margin-bottom-5">
                    <h5 style="color:red"><b>AWAS</b></h5>
                    <table style="width:100%">
                        <tbody>
                            <?php $index = 0; ?>
                            @foreach($list_status_gunung as $status_gunung)
                            @if($index < 6)
                            @if($status_gunung->status == "AWAS")
                            <tr>
                                <td><b>{{ $status_gunung->gunung }}</b></td>
                                <td class="text-right"><span>{{ date('D, d M Y', strtotime($status_gunung->tanggal_status_gunung)) }}</span></td>
                            </tr>
                            <?php $index++; ?>
                            @endif
                            @endif
                            @endforeach
                        </tbody>
                    </table><hr style="margin-top:10px; margin-bottom:15px">
                </div>
                <div class="margin-bottom-5">
                    <h5 style="color:orange"><b>SIAGA</b></h5>
                    <table style="width:100%">
                        <tbody>
                            <?php $index = 0; ?>
                            @foreach($list_status_gunung as $status_gunung)
                            @if($index < 6)
                            @if($status_gunung->status == "SIAGA")
                            <tr>
                                <td><b>{{ $status_gunung->gunung }}</b></td>
                                <td class="text-right"><span>{{ date('D, d M Y', strtotime($status_gunung->tanggal_status_gunung)) }}</span></td>
                            </tr>
                            <?php $index++; ?>
                            @endif
                            @endif
                            @endforeach
                        </tbody>
                    </table><hr style="margin-top:10px; margin-bottom:15px">
                </div>
                <div>
                    <h5 style="color:gold"><b>WASPADA</b></h5>
                    <table style="width:100%">
                        <tbody>
                            <?php $index = 0; ?>
                            @foreach($list_status_gunung as $status_gunung)
                            @if($index < 6)
                            @if($status_gunung->status == "WASPADA")
                            <tr>
                                <td><b>{{ $status_gunung->gunung }}</b></td>
                                <td class="text-right"><span>{{ date('D, d M Y', strtotime($status_gunung->tanggal_status_gunung)) }}</span></td>
                            </tr>
                            <?php $index++; ?>
                            @endif
                            @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="col-md-9">
                <div class="row quote-v1">
                    <span class="margin-left-20">Info Kebencanaan</span>
                </div>
                @foreach($list_info as $info)
                <?php $lower_name = strtolower(str_replace(" ", "+", $info->nama_info)); ?>
                <div style="background-color: #FAFAFA; padding:15px 20px; margin-bottom:15px">
                    <a href="{{ url('/info-kebencanaan') .'/'. $lower_name }}" title="{{ $info->nama_info }}" data-toggle="tooltip" style="color:#4c4c4b">
                        <h3 class="no-top-space" style="margin-bottom:5px">{{ $info->nama_info }}</h3>
                        <span class="text-muted"><i class="fa fa-calendar"></i> {{ date('D, d M Y', strtotime($info->tanggal_info)) }}</span>
                        <section style="font-size: 12px; margin-top: 20px">{!! str_limit($info->konten, 300) !!}</section>
                    </a>
                    <div class="text-right">
                        <a class="btn-transparent" href="{{ url('/info-kebencanaan') .'/'. $lower_name }}">Selengkapnya</a>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="col-md-3">
                <div class="row quote-v1">
                    <span class="margin-left-20">Fasilitas</span>
                </div>
                <div class="row margin-top-10" style="font-size:13px;">
                    @foreach($list_fasilitas as $fasilitas)
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-4">
                                <?php
                                    if($fasilitas->foto_fasilitas)
                                        $url_foto = asset('upload/fasilitas') .'/'. $fasilitas->foto_fasilitas;
                                    else
                                        $url_foto = asset('img') .'/'. 'noimagefound.png';
                                ?>
                                <img alt="{{ strtolower($fasilitas->nama_fasilitas) }}" src="{{ $url_foto }}" class="img-thumbnail">
                            </div>
                            <div class="col-md-8">
                                <ul class="list-unstyled">
                                    <li>
                                        <b class="text-capitalize">{{ strtolower($fasilitas->nama_fasilitas) }}</b>
                                    </li>
                                    <li>
                                        <i class="fa fa-building"></i>&nbsp;
                                        <span>{{ $fasilitas->jenis_fasilitas }}</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-map-marker"></i>&nbsp;
                                        <span class="text-capitalize">{{ strtolower($fasilitas->alamat_fasilitas) }}</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-tag"></i>&nbsp;
                                        <span class="text-capitalize">{{ strtolower($fasilitas->gunung) }}</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <hr style="margin-top: 5px; margin-bottom:10px">
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('custom_js')
<script src="{{ asset('assets/corporate/plugins/owl.carousel/owl.carousel.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/corporate/pages/scripts/bs-carousel.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/corporate/scripts/layout.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        Layout.init();    
        Layout.initOWL();
    });
</script>
@endsection
