<!-- BEGIN VENDOR JS-->
<script src="{{ asset('assets/robust/app-assets/js/core/libraries/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/robust/app-assets/vendors/js/ui/tether.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/robust/app-assets/js/core/libraries/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/robust/app-assets/vendors/js/ui/perfect-scrollbar.jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/robust/app-assets/vendors/js/ui/unison.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/robust/app-assets/vendors/js/ui/blockUI.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/robust/app-assets/vendors/js/ui/jquery.matchHeight-min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/robust/app-assets/vendors/js/ui/screenfull.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/robust/app-assets/vendors/js/extensions/pace.min.js') }}" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->

<!-- BEGIN PAGE VENDOR JS-->
<script src="{{ asset('assets/robust/app-assets/vendors/js/charts/chart.min.js') }}" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->

<!-- BEGIN ROBUST JS-->
<script src="{{ asset('assets/robust/app-assets/js/core/app-menu.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/robust/app-assets/js/core/app.js') }}" type="text/javascript"></script>
<!-- END ROBUST JS-->

<!-- BEGIN COSTUM JS -->
<script type="text/javascript">
	function startLoader(){
		// $('.content-body').append(
		// 	'<div id="loader" class="loader display-block"><div class="loader-content font-medium-3"><i class="icon-spinner9 spinner font-medium-2"></i><span> Tunggu</span></div></div>'
		// );
		$('.content-body').append(
			'<div id="loader" class="loader display-block"><div class="loader-content font-medium-3"><img style="width:30%" src="http://localhost/sigapi/public/img/spinningwheel.gif" alt="loading"></div></div>'
		);
	};

	function endLoader(method){
		var counter = 0;
		var interval = setInterval(function () {
			if(counter == 1){
				clearInterval(interval);
				if(method == "lihat"){
					$('#modal-lihat').modal('show');
				}
				else if(method == "alert"){
					alert('server not responding...');
				}
				else if(method == "no connection"){
					new PNotify({
						title: 'Uups... tidak ada konektivitas',
						text: 'Harap periksa jaringan anda',
						type: 'warning',
						nonblock: {
							nonblock: true
						},
						delay: 2000,
						styling: 'bootstrap3'
					});
				}
				$('.loader').remove();
			}
			++counter;
		}, 1000);
	}

	function createInterval(){
	}

	function setIconTextArea(){
		$('.glyphicon-font').attr('class', 'icon-font2');
		$('.glyphicon-quote').attr('class', 'icon-quote-left');
		$('.glyphicon-list').attr('class', 'icon-list3');
		$('.glyphicon-th-list').attr('class', 'icon-th-list');
		$('.glyphicon-indent-right').attr('class', 'icon-outdent');
		$('.glyphicon-indent-left').attr('class', 'icon-indent');
		$('.glyphicon-share').attr('class', 'icon-share-square-o');
		$('.glyphicon-picture').attr('class', 'icon-image3');
	}

	function successMessage(message){
		new PNotify({
            title: 'Berhasil',
            text: message,
            type: 'success',
            nonblock: {
                nonblock: true
            },
            delay: 2000,
            styling: 'bootstrap3'
        });
	}

	function errorMessage(message){
		new PNotify({
            title: 'Gagal',
            text: message,
            type: 'error',
            nonblock: {
                nonblock: true
            },
            delay: 2000,
            styling: 'bootstrap3'
        });
	}

	function modal_delete(url){
		$('#id-delete').attr('action', url);
	    $('#modal-delete').modal('show');
	}
</script>
<!-- END COSTUM JS -->

@yield('custom_js')

<!-- Script for Arcgis JS -->
<script>
	function loadPointMap(data){
		$('#modal-lihat').modal('show');
		require([
		"esri/Map",
		"esri/views/MapView",
		"esri/Graphic",
		"esri/layers/FeatureLayer",
		"esri/widgets/Legend",
		"esri/widgets/ScaleBar",
		"esri/geometry/support/webMercatorUtils",
		"dojo/dom",
		"dojo/domReady!"
		], function(Map, MapView, Graphic, FeatureLayer, Legend, ScaleBar, webMercatorUtils, dom) {
			var map = new Map({
				basemap: "streets"
			});

			var view = new MapView({
				// center: [113.0209131, -8.0669649],
				center: data['center'],
				container: "frame-map",
				map: map,
				zoom: 11
			});

			var point = {
				type: "point",
				longitude: data['longitude'],
				latitude: data['latitude']
			};

			var markerSymbol = {
				type: "picture-marker",
				url: data['path_icon'],
				width: "32px",
  				height: "32px"
			};

			var renderer = {
				type: "simple",
				symbol: markerSymbol,
			}

			var fields = [
				{
					name: "ObjectID",
					alias: "ObjectID",
					type: "oid"
				},
				{
					name: "nama",
					alias: "Nama",
					type: "string"
				},
				{
					name: "alamat",
					alias: "Alamat",
					type: "string"
				},
				{
					name: "longitude",
					alias: "Longitude",
					type: "string"
				},
				{
					name: "latitude",
					alias: "Latitude",
					type: "string"
				},
				{
					name: "jenis_fasilitas",
					alias: "Jenis",
					type: "string"
				},
				{
					name: "gunung",
					alias: "Gunung",
					type: "string"
				},
			];

			var features =	[	
				{
					geometry: point,
					attributes: {
						ObjectID: 1,
						nama: data['nama'],
						alamat: data['alamat'],
						longitude: data['longitude'],
						latitude: data['latitude'],
						jenis_fasilitas: data['jenis_fasilitas'],
						gunung: data['gunung'],
					}
				}
			];

			/**
			var pointGraphic = new Graphic({
				geometry: point,
				symbol: markerSymbol,
				attributes: {
					ObjectID: 1,
					nama: data['nama'],
					alamat: data['alamat'],
					longitude: data['longitude'],
					latitude: data['latitude'],
					jenis_fasilitas: data['jenis_fasilitas'],
					gunung: data['gunung'],
				}
			});
			*/

			var layer = new FeatureLayer({
				fields: fields,
				objectIdField: "ObjectID",
				geometryType: "point",
				spatialReference: { wkid: 4326 },
				source: features,
				renderer: renderer
			});

			var legend = new Legend({
				view: view,
				layerInfos: [{
					layer: layer,
					title: data['jenis_fasilitas']
				}]
			});
			var scaleBar = new ScaleBar({
				view: view,
				unit: "metric"
			});

			/** view.graphics.add(pointGraphic); */
			map.add(layer);

			view.ui.add(legend, "top-right");
			view.ui.add(scaleBar, {
				position: "bottom-left"
			});
			
			view.on("pointer-move", function(evt){
				var point = view.toMap({
					x: evt.x,
					y: evt.y
				});
				var longitude = point.longitude;
				var latitude = point.latitude;
				dom.byId("info").innerHTML = longitude.toString().substr(0,8) + ", " + latitude.toString().substr(0,8);
			});

			view.then(function(){
				endLoader("lihat");
			}).otherwise(errorCallback);

			function errorCallback(){
				$('#modal-lihat').modal('hide');
				endLoader();
				alert('server not responding...');
			}
		});
	}

	function loadPolygonMap(data){
		$('#modal-lihat').modal('show');
		require([
			"esri/Map",
			"esri/views/MapView",
			"esri/layers/FeatureLayer",
			"esri/widgets/Legend",
			"esri/widgets/ScaleBar",
			"esri/symbols/SimpleFillSymbol",
			"esri/geometry/support/webMercatorUtils",
			"dojo/dom",
			"dojo/domReady!"
			], function(Map, MapView, FeatureLayer, Legend, ScaleBar, SimpleFillSymbol, webMercatorUtils, dom) {
			var map = new Map({
				basemap: "hybrid"
			});
			var view = new MapView({
				center: [113.0209131, -8.0669649],
				container: "frame-map",
				map: map,
				zoom: 10
			});

			/**
			var polygon = {
				type: "polygon",
				rings: data['coordinates']
			};
			*/

			/**
			var fillSymbol = {
				type: "simple-fill",
				color: [227, 139, 79, 0.8],
				outline: {
					color: [255, 255, 255],
					width: 1
				}
			};
			*/

			if(data['nama'] == "Rawan III"){
				var fillSymbol = SimpleFillSymbol({
					color: "red",
					outline: {
						color: [128, 128, 128, 0.5],
						width: "0.5px"
					}
				});
			} else if(data['nama'] == "Rawan II"){
				var fillSymbol = SimpleFillSymbol({
					color: "orange",
					outline: {
						color: [128, 128, 128, 0.5],
						width: "0.5px"
					}
				});
			} else {
				var fillSymbol = SimpleFillSymbol({
					color: "yellow",
					outline: {
						color: [128, 128, 128, 0.5],
						width: "0.5px"
					}
				});
			}

			var renderer = {
				type: "simple",
				symbol: fillSymbol,
			}

			var fields = [
				{
					name: "ObjectID",
					alias: "ObjectID",
					type: "oid"
				},
				{
					name: "nama",
					alias: "Nama",
					type: "string"
				},
				{
					name: "gunung",
					alias: "Gunung",
					type: "string"
				},
			];

			/**
			var features =	[	
				{
					geometry: polygon,
					attributes: {
						ObjectID: 1,
						nama: data['nama'],
						gunung: data['gunung'],
					}
				}
			];
			 */

			var layer = new FeatureLayer({
				// fields: fields,
				// objectIdField: "ObjectID",
				// geometryType: "polygon",
				// spatialReference: { wkid: 4326 },
				// source: features,
				url: data['file'],
				renderer: renderer,
				opacity: 0.6
			});
			
			var legend = new Legend({
				view: view,
				layerInfos: [{
					layer: layer,
					title: data['nama']
				}]
			});

			var scaleBar = new ScaleBar({
				view: view,
				unit: "metric"
			});

			map.add(layer);
			view.ui.add(legend, "top-right");
			view.ui.add(scaleBar, {
				position: "bottom-left"
			});
			
			view.on("pointer-move", function(evt){
				var point = view.toMap({
					x: evt.x,
					y: evt.y
				});
				var longitude = point.longitude;
				var latitude = point.latitude;
				dom.byId("info").innerHTML = longitude.toString().substr(0,8) + ", " + latitude.toString().substr(0,8);
			});
			view.then(function(){
				endLoader("lihat");
			}).otherwise(errorCallback);

			function errorCallback(){
				$('#modal-lihat').modal('hide');
				endLoader();
				alert('server not responding...');
			}
		});
	}

	function loadLineMap(data){
		$('#modal-lihat').modal('show');
		require([
			"esri/Map",
			"esri/views/MapView",
			"esri/layers/FeatureLayer",
			"esri/widgets/Legend",
			"esri/widgets/ScaleBar",
			"esri/symbols/SimpleLineSymbol",
			"esri/geometry/support/webMercatorUtils",
			"dojo/dom",
			"dojo/domReady!"
			], function(Map, MapView, FeatureLayer, Legend, ScaleBar, SimpleLineSymbol, webMercatorUtils, dom) {
			var map = new Map({
				basemap: "osm"
			});
			var view = new MapView({
				center: [113.0209131, -8.0669649],
				container: "frame-map",
				map: map,
				zoom: 10
			});

			var lineSymbol = SimpleLineSymbol({
				color: "blue",
				width: "2px",
				style: "short-dash-dot"
			});

			var renderer = {
				type: "simple",
				symbol: lineSymbol,
			}

			var layer = new FeatureLayer({
				url: data['file'],
				renderer: renderer,
				// opacity: 0.6
			});
			
			var legend = new Legend({
				view: view,
				layerInfos: [{
					layer: layer,
					title: "Jalur Evakuasi"
				}]
			});

			var scaleBar = new ScaleBar({
				view: view,
				unit: "metric"
			});

			map.add(layer);
			view.ui.add(legend, "top-right");
			view.ui.add(scaleBar, {
				position: "bottom-left"
			});
			
			view.on("pointer-move", function(evt){
				var point = view.toMap({
					x: evt.x,
					y: evt.y
				});
				var longitude = point.longitude;
				var latitude = point.latitude;
				dom.byId("info").innerHTML = longitude.toString().substr(0,8) + ", " + latitude.toString().substr(0,8);
			});
			view.then(function(){
				endLoader("lihat");
			}).otherwise(errorCallback);

			function errorCallback(){
				$('#modal-lihat').modal('hide');
				endLoader();
				alert('server not responding...');
			}
		});
	}
</script>
<!-- End -->