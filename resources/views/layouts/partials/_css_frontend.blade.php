<!-- Fonts START -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
<!-- Fonts END -->

<!-- Global styles START -->          
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
<link href="{{ asset('assets/corporate/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
<!-- Global styles END -->

<!-- Page level plugin styles START -->
<link href="{{ asset('assets/corporate/pages/css/animate.css') }}" rel="stylesheet">
<link href="{{ asset('assets/corporate/plugins/fancybox/source/jquery.fancybox.css') }}" rel="stylesheet">
<link href="{{ asset('assets/corporate/plugins/owl.carousel/assets/owl.carousel.css') }}" rel="stylesheet">
<!-- Page level plugin styles END -->

<!-- Theme styles START -->
<link href="{{ asset('assets/corporate/pages/css/components.css') }}" rel="stylesheet">
<link href="{{ asset('assets/corporate/pages/css/slider.css') }}" rel="stylesheet">
<link href="{{ asset('assets/corporate/css/style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/corporate/css/style-responsive.css') }}" rel="stylesheet">
<link href="{{ asset('assets/corporate/css/themes/orange.css') }}" rel="stylesheet" id="style-color">
<link href="{{ asset('assets/corporate/css/custom.css') }}" rel="stylesheet">
<!-- Theme styles END -->

<style>
	a:hover{
		text-decoration: none;
	}
	.main{
		min-height: 480px;
	}
</style>
@yield('custom_css')