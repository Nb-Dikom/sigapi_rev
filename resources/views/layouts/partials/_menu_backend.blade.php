<li class="nav-item {{ Request::is('dashboard') ? 'active':'' }}">
	<a href="{{ route('dashboard_page') }}"><i class="icon-home3"></i><span class="menu-title">Dashboard</span></a>
</li>
@if(Auth::user()->pegawai->jabatan->jabatan == "Admin" || Auth::user()->pegawai->jabatan->jabatan == "Pengolah Data Pra Bencana" || Auth::user()->pegawai->jabatan->jabatan == "Kepala Seksi Kesiapsiagaan"  || Auth::user()->pegawai->jabatan->jabatan == "Kepala Bidang Pencegahan dan Kesiapsiagaan" || Auth::user()->pegawai->jabatan->jabatan == "Kepala BPBD Jawa Timur")
@if(Auth::user()->pegawai->jabatan->jabatan == "Admin")
<li class="nav-item {{ Request::is('pegawai') ? 'active':'' }}">
	<a href="{{ route('pegawai_page') }}"><i class="icon-user-tie"></i><span class="menu-title">Pegawai</span></a>
</li>
@endif
@if(Auth::user()->pegawai->jabatan->jabatan == "Admin" || Auth::user()->pegawai->jabatan->jabatan == "Kepala BPBD Jawa Timur")
<li class="nav-item {{ Request::is('organisasi') ? 'active':'' }}">
	<a href="{{ route('organisasi_page') }}">
		<i class="icon-users3"></i><span class="menu-title">Organisasi </span>
	</a>
</li>
@endif
<li class="nav-item has-sub {{ Request::is('info') || Request::is('pengamatan') || Request::is('kawasan-rawan-bencana') || Request::is('fasilitas') || Request::is('jalur-evakuasi') || Request::is('status-gunung') ? 'open':'' }}">
	<a href="javascript:void(0)"><i class="icon-menu"></i><span class="menu-title">Mitigasi</span></a>
	<ul class="menu-content">
		<li class="nav-item {{ Request::is('info') ? 'active':'' }}">
			<a href="{{ route('info_page') }}"><i class="icon-book"></i><span class="menu-title">Info</span></a>
		</li>
		<li class="nav-item {{ Request::is('pengamatan') ? 'active':'' }}">
			<a href="{{ route('pengamatan_page') }}">
				<i class="icon-images2"></i><span class="menu-title">Pengamatan </span>
			</a>
		</li>
		<li class="nav-item {{ Request::is('kawasan-rawan-bencana') ? 'active':'' }}">
			<a href="{{ route('krb_page') }}">
				<i class="icon-stack"></i><span class="menu-title">Kawasan Rawan Bencana </span>
			</a>
		</li>
		<li class="nav-item {{ Request::is('fasilitas') ? 'active':'' }}">
			<a href="{{ route('fasilitas_page') }}">
				<i class="icon-office"></i><span class="menu-title">Fasilitas </span>
			</a>
		</li>
		<li class="nav-item {{ Request::is('status-gunung') ? 'active':'' }}">
			<a href="{{ route('status_gunung_page') }}">
				<i class="icon-warning2"></i><span class="menu-title">Status Gunung </span>
			</a>
		</li>
		<li class="nav-item {{ Request::is('jalur-evakuasi') ? 'active':'' }}">
			<a href="{{ route('jalur_evakuasi_page') }}">
				<i class="icon-android-walk"></i><span class="menu-title">Jalur Evakuasi </span>
			</a>
		</li>
	</ul>
</li>
@elseif(Auth::user()->pegawai->jabatan->jabatan == "Sub Bagian Umum dan Kepegawaian" ||
	Auth::user()->pegawai->jabatan->jabatan == "Sub Bagian Penanggulangan Bencana")
@if(Auth::user()->pegawai->jabatan->jabatan == "Sub Bagian Umum dan Kepegawaian")
<li class="nav-item {{ Request::is('organisasi') ? 'active':'' }}">
	<a href="{{ route('organisasi_page') }}">
		<i class="icon-users3"></i><span class="menu-title">Organisasi </span>
	</a>
</li>
@endif
<li class="nav-item {{ Request::is('info') ? 'active':'' }}">
	<a href="{{ route('info_page') }}"><i class="icon-book"></i><span class="menu-title">Info</span></a>
</li>
<li class="nav-item {{ Request::is('pengamatan') ? 'active':'' }}">
	<a href="{{ route('pengamatan_page') }}">
		<i class="icon-images2"></i><span class="menu-title">Pengamatan </span>
	</a>
</li>
<li class="nav-item {{ Request::is('fasilitas') ? 'active':'' }}">
	<a href="{{ route('fasilitas_page') }}">
		<i class="icon-office"></i><span class="menu-title">Fasilitas </span>
	</a>
</li>
<li class="nav-item {{ Request::is('status-gunung') ? 'active':'' }}">
	<a href="{{ route('status_gunung_page') }}">
		<i class="icon-warning2"></i><span class="menu-title">Status Gunung </span>
	</a>
</li>
<li class="nav-item {{ Request::is('peta') ? 'active':'' }}">
	<a href="{{ route('map_page') }}">
		<i class="icon-map5"></i><span class="menu-title">Peta</span>
	</a>
</li>
@endif