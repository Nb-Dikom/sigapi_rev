<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/robust/app-assets/css/bootstrap.css') }}">
<!-- font icons-->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/robust/app-assets/fonts/icomoon.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/robust/app-assets/vendors/css/extensions/pace.css') }}">		
<!-- END VENDOR CSS-->

<!-- BEGIN ROBUST CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/robust/app-assets/css/bootstrap-extended.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/robust/app-assets/css/app.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/robust/app-assets/css/colors.css') }}">
<!-- END ROBUST CSS-->


<!-- BEGIN Page Level CSS-->
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/robust/app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/robust/app-assets/css/core/menu/menu-types/vertical-overlay-menu.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/robust/app-assets/css/core/colors/palette-gradient.css') }}">
<!-- END Page Level CSS-->

<!-- BEGIN Custom CSS-->
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/robust/assets/css/style.css') }}">
<!-- END Custom CSS-->

@yield('custom_css')

<!-- Own CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">