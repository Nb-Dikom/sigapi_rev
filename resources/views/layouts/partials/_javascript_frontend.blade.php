<script src="{{ asset('assets/corporate/plugins/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/corporate/plugins/jquery-migrate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/corporate/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>      
<script src="{{ asset('assets/corporate/scripts/back-to-top.js') }}" type="text/javascript"></script>
@yield('custom_js')