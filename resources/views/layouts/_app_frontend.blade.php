<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/logo_bnpb.png') }}">

    <!-- Styles -->
    @include('layouts.partials._css_frontend')
    <title>{{ config('app.name') }}</title>
</head>

<body class="corporate">
    <div class="pre-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 additional-shop-info">
                    <ul class="list-unstyled list-inline">
                        <li><i class="fa fa-phone"></i><span>+62 821 6292 8618</span></li>
                        <li><i class="fa fa-envelope-o"></i><span>info@mail.com</span></li>
                    </ul>
                </div>
                <div class="col-md-6 col-sm-6 additional-nav">
                    <ul class="list-unstyled list-inline pull-right">
                        @if(Auth::user())
                        <li>{{ Auth::user()->organisasi->nama_organisasi }}</li>
                        <li>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Keluar</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                        @else
                        <li><a href="{{ route('form_login') }}">Masuk</a></li>
                        <li><a href="{{ route('form_registrasi') }}">Daftar</a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>        
    </div>
    <div class="header">
        <div class="container">
            <a class="site-logo" href="{{ url('/') }}"><img src="{{ asset('img/sigapi-logo.png') }}" alt="SIGAPI"></a>
            <a href="javascript:void(0);" class="mobi-toggler"><i class="fa fa-bars"></i></a>

            <div class="header-navigation pull-right font-transform-inherit">
                <ul>
                    <li class="{{ Request::is('/') ? 'active':'' }}"><a href="{{ url('/') }}">Beranda</a></li>
                    <li class="{{ Request::is('peta-bencana') ? 'active':'' }}"><a href="{{ url('peta-bencana') }}">Peta</a></li>
                    <li class="{{ Request::is('info-kebencanaan') ? 'active':'' }}"><a href="{{ url('info-kebencanaan') }}">Info Kebencanaan</a></li>
                    <li class="{{ Request::is('fasilitas-publik') ? 'active':'' }}"><a href="{{ url('fasilitas-publik') }}">Fasilitas</a></li>
                    <li class="{{ Request::is('pengamatan-gunung-api') ? 'active':'' }}"><a href="{{ url('pengamatan-gunung-api') }}">Pengamatan</a></li>
                    <li class="{{ Request::is('status-gunung-api') ? 'active':'' }}"><a href="{{ url('status-gunung-api') }}">Status Gunung</a></li>
                    @if(Auth::user())
                    @if(Auth::user()->level->level == "Organisasi")
                    <?php $lower_name = strtolower(str_replace(" ", "+", Auth::user()->organisasi->nama_organisasi)); ?>
                    <li class="{{ Request::is('akun') ? 'active':'' }}"><a href="{{ url('/akun?') .'organisasi='. $lower_name }}">Profil</a></li>
                    @endif
                    @endif
                </ul>
            </div>
        </div>
    </div>

    <div class="main">
        @yield('content')
    </div>

    <div class="footer">
      <div class="container">
        <div class="row">
          <div class="text-center">
            2018 © Diar Ichrom. ALL Rights Reserved. <a href="javascript:;">Privacy Policy</a> | <a href="javascript:;">Terms of Service</a>
          </div>
        </div>
      </div>
    </div>
    @include('layouts.partials._javascript_frontend')
</body>
</html>