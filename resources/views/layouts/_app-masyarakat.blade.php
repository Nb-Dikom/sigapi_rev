<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/logo_bnpb.png') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'SIGAPI_DEV') }}</title>

    <!-- Styles -->
    @include('layouts.partials._css')
    <style type="text/css">
        .header-navbar.navbar-semi-dark .navbar-header {
            background: white;
        }
        .nav-item.active {
            border-bottom: 2px solid #1D2B36;
        }
    </style>
</head>
<body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">
    <!-- navbar-fixed-top-->
    <nav class="header-navbar navbar navbar-with-menu navbar-fixed-top navbar-semi-dark navbar-shadow">
        <div class="navbar-wrapper">
            <div class="navbar-header">
                <ul class="nav navbar-nav">
                    <li class="nav-item">
                        <a href="{{ route('home_page') }}" class="navbar-brand nav-link">
                            <img alt="sigapi logo" src="{{ asset('img/sigapi-logo.png') }}" data-expand="{{ asset('assets/robust/app-assets/images/logo/robust-logo-light.png') }}" data-collapse="{{ asset('assets/robust/app-assets/images/logo/robust-logo-small.png') }}" class="brand-logo">
                        </a>
                    </li>
                    <li class="nav-item hidden-md-up float-xs-right">
                        <a data-toggle="collapse" data-target="#navbar-mobile" class="nav-link open-navbar-container"><i class="icon-ellipsis pe-2x icon-icon-rotate-right-right"></i></a>
                    </li>
                </ul>
            </div>
            <div class="navbar-container content container-fluid">
                <div id="navbar-mobile" class="collapse navbar-toggleable-sm">
                    <ul class="nav navbar-nav text-uppercase">
                        <li class="nav-item {{ Request::is('masyarakat') ? 'active':'' }}">
                            <a href="{{ route('home_page') }}" class="nav-link"><i class="icon-home22"></i> Beranda</a>
                        </li>
                        <li class="nav-item {{ Request::is('masyarakat/pengamatan') ? 'active':'' }}">
                            <a href="{{ route('observation_page') }}" class="nav-link"><i class="icon-binoculars"></i> Pengamatan</a>
                        </li>
                        <li class="nav-item {{ Request::is('masyarakat/peta') ? 'active':'' }}">
                            <a href="{{ route('peta_page') }}" class="nav-link"><i class="icon-map22"></i> Peta</a>
                        </li>
                        <li class="nav-item {{ Request::is('masyarakat/file') ? 'active':'' }}">
                            <a href="{{ route('shapefile_page') }}" class="nav-link"><i class="icon-file-zip"></i> File Peta</a>
                        </li>
                        <li class="nav-item {{ Request::is('masyarakat/data-prabencana') ? 'active':'' }}">
                            <a href="{{ route('masyarakat_prabencana_page') }}" class="nav-link"><i class="icon-database"></i> Data Pra Bencana</a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav float-xs-right">
                        @if(!Auth::user())
                        <li class="nav-item">
                            <a href="{{ route('form_login') }}" class="btn btn-outline-primary nav-link pointer square"><i class="icon-unlock2"></i>  Sign in</a>
                        </li>
                        @elseif(Auth::user())
                        <li class="dropdown dropdown-user nav-item">
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link dropdown-user-link">
                                <span class="avatar avatar-online">
                                    <img src="{{ asset('img/person.png') }}" alt="avatar"><i></i>
                                </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a href="{{ route('view_organisasi', $user_provider->organisasi->id_organisasi) }}" class="dropdown-item"><i class="icon-head"></i> Profil</a>
                                <div class="dropdown-divider"></div>
                                <a href="{{ route('logout') }}" class="dropdown-item"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <i class="icon-power3"></i> Keluar
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    
    <!-- Content -->
    <div id="content" style="min-height:540px">
        @yield('content')
    </div>
    

    <!-- Scripts -->
    @include('layouts.partials._javascript')

</body>
</html>
