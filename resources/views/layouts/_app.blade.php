<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/logo_bnpb.png') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'SIGAPI_DEV') }}</title>

    <!-- Styles -->
    @include('layouts.partials._css_backend')
</head>
<body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">
    <!-- navbar-fixed-top-->
    <nav class="header-navbar navbar navbar-with-menu navbar-fixed-top navbar-semi-dark navbar-shadow">
        <div class="navbar-wrapper">
            <div class="navbar-header">
                <ul class="nav navbar-nav">
                    <li class="nav-item mobile-menu hidden-md-up float-xs-left">
                        <a class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="icon-menu5 font-large-1"></i></a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('dashboard') }}" class="navbar-brand nav-link"><img alt="Logo SIGAPI"
                         src="{{ asset('img/sigapi-logo-light.png') }}"
                         data-expand="{{ asset('img/sigapi-logo-light.png') }}" 
                         data-collapse="{{ asset('img/sigapi-logo-small.png') }}" class="brand-logo"></a>
                    </li>
                    <li class="nav-item hidden-md-up float-xs-right">
                        <a data-toggle="collapse" data-target="#navbar-mobile" class="nav-link open-navbar-container"><i class="icon-ellipsis pe-2x icon-icon-rotate-right-right"></i></a>
                    </li>
                </ul>
            </div>
            <div class="navbar-container content container-fluid">
                <div id="navbar-mobile" class="collapse navbar-toggleable-sm">
                    <ul class="nav navbar-nav float-xs-right">
                        <li class="dropdown dropdown-user nav-item">
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link dropdown-user-link">
                                <span class="avatar avatar-online">
                                    @if($user_provider->pegawai->foto_pegawai)
                                        <img src="{{ asset('upload/pegawai/'.$user_provider->pegawai->foto_pegawai) }}" alt="avatar"><i></i>
                                    @else
                                        <img src="{{ asset('img/person.png') }}" alt="avatar"><i></i>
                                    @endif
                                </span>
                                <span class="user-name">
                                    @if($user_provider->level->level == 'Pegawai')
                                    {{ $user_provider->pegawai->nama_pegawai }}
                                    @else
                                    {{ $user_provider->organisasi->nama_organisasi }}
                                    @endif
                                </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <?php
                                    $lower_name = strtolower(str_replace(" ", "+", $user_provider->pegawai->nama_pegawai));
								?>
                                <a class="dropdown-item" href="{{ route('profile_page', $lower_name) }}"><i class="icon-head"></i> Ubah</a>
                                <div class="dropdown-divider"></div>
                                <a href="{{ route('logout') }}" class="dropdown-item"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <i class="icon-power3"></i> Keluar
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

    <!-- ////////////////////////////////////////////////////////////////////////////-->

    <!-- main menu-->
    <div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
        <!-- main menu header-->
        <div class="main-menu-header">
            <h4>
                @if($user_provider->level->level == 'Pegawai')
                {{ $user_provider->pegawai->nama_pegawai }}
                @else
                {{ $user_provider->organisasi->nama_organisasi }}
                @endif
            </h4>
            <span>
                @if($user_provider->level->level == 'Pegawai')
                {{ $user_provider->pegawai->jabatan->jabatan }}
                @else
                {{ $user_provider->organisasi->nama_organisasi }}
                @endif
            </span>
        </div>
        <!-- / main menu header-->

        <!-- main menu content-->
        <div class="main-menu-content">
            <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
                @include('layouts.partials._menu_backend')
            </ul>
        </div>
      <!-- /main menu content-->
    </div>
    <!-- / main menu-->
    
    <!-- Content -->
    <div id="content" style="min-height:540px">
        @yield('content')
    </div>
    

    <footer class="footer footer-static footer-light navbar-border">
        <p class="clearfix text-muted text-sm-center mb-0 px-2">
            <span class="float-md-left d-xs-block d-md-inline-block">Copyright &copy; 2017 <span class="text-bold-800 grey darken-2">Diar Ichrom</span>, All rights reserved. </span>
        </p>
    </footer>

    <!-- Scripts -->
    @include('layouts.partials._javascript_backend')

</body>
</html>
