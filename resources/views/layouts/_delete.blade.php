<div class="modal-dialog modal-sm" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title"><i class="icon-cogs"></i> Konfirmasi Hapus Data</h4>
		</div>
		<div class="modal-body">
			<div class="alert alert-danger" role="alert" style="font-size:11px">
				<span><b>Peringatan!</b></span> Data yang dihapus tidak bisa dikembalikan.
			</div>
			</br>
			<h5>Apakah anda ingin menghapus data ini?</h5>
		</div>
		<div class="modal-footer">
			<form id="id-delete" class="text-right" action="" method="POST">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
				<button class="btn btn-warning" data-dismiss="modal"><i class="icon-cross2"></i> Tidak</button>
				<button type="submit" class="btn btn-success"><i class="icon-check2"></i> Ya</button>
			</form>
		</div>
	</div>
</div>