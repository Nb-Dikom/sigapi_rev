@extends('layouts._app')
@section('custom_css')
<!-- bootstrap3-wysihtml5 css -->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/bootstrap3-wysihtml5/dist/bootstrap3-wysihtml5.min.css') }}">
<!-- plugin notify -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/pnotify.min.css') }}">
@endsection

@section('content')
<div class="app-content content container-fluid">
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-xs-12 mb-1">
				<h2 class="content-header-title">Info</h2>
			</div>
			<div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
				<div class="breadcrumb-wrapper col-xs-12">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{ url('/info') }}">Info</a></li>
						<li class="breadcrumb-item active"> Tambah Info</li>
					</ol>
				</div>
			</div>
		</div>
		<div class="content-body">
			<section>
				<div class="row">
					<div class="col-md-10 offset-md-1">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">Tambah Info</h4>
								<a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
								<div class="heading-elements">
									<ul class="list-inline mb-0">
										<li><a data-action="expand"><i class="icon-expand2"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="card-block">
								<form class="form" method="POST" action="{{ url('/info') }}" enctype="multipart/form-data">
									{!! csrf_field() !!}
									<div class="form-body">
										<div class="form-group {{ $errors->has('nama') ? ' has-error' : '' }}">
											<label for="nama">Judul</label>
											<div class="position-relative has-icon-left">
												<input type="text" id="nama" class="form-control" placeholder="masukkan judul info kebencanaan" name="nama"  required="" value="{{ old('nama') }}">
												<div class="form-control-position">
													<i class="icon-book"></i>
												</div>
											</div>
											@if($errors->has('nama'))
											<span class="help-block">
												<strong class="pink">{{ $errors->first('nama') }}</strong>
											</span>
											@endif
										</div>
										<div class="form-group">
											<label for="nama">Konten</label>
											<div class="position-relative">
												<textarea class="form-control" name="konten" style="height:500px"></textarea>
											</div>
										</div>
									</div>
									<div class="form-actions center">
										<a href="{{ url('/info-kebencanaan') }}" class="btn btn-warning mr-1"><i class="icon-cross2"></i> Batal</a>
										<button type="submit" class="btn btn-info"><i class="icon-check2"></i> Simpan</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection

@section('custom_js')
	<!-- bootstrap3-wysihtml5 -->
	<script src="{{ asset('assets/bootstrap3-wysihtml5/dist/bootstrap3-wysihtml5.min.js') }}"></script>
	<script src="{{ asset('assets/bootstrap3-wysihtml5/dist/bootstrap3-wysihtml5.all.min.js') }}"></script>
	<!-- plugin pnotify -->
	<script src="{{ asset('js/pnotify.min.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('textarea[name="konten"]').wysihtml5();
			setIconTextArea();
			
			if("{{ Session::has('error_message') }}"){
				errorMessage("{{ session('error_message') }}");
			}
			
			if("{{ $errors->first() }}"){
				errorMessage("data info kebencanaan tidak disimpan");
			}
		});
	</script>
@endsection