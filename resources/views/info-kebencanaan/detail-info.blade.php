@extends('layouts._app')
@section('content')
<div class="app-content content container-fluid">
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-xs-12 mb-1">
				<h2 class="content-header-title">Info</h2>
			</div>
			<div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
				<div class="breadcrumb-wrapper col-xs-12">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{ url('/info') }}">Info</a></li>
						<li class="breadcrumb-item active"> {{ $info->nama_info }}</li>
					</ol>
				</div>
			</div>
		</div>
		<div class="content-body">
			<section>
				<div class="row">
					<div class="col-md-10 offset-md-1">
						<div class="card">
							<div class="card-header" style="margin-bottom: 5px">
								<h4 class="card-title">{{ $info->nama_info }}</h4>
							</div>
							<div class="card-block pt-0">
                                <section>
                                    <div class="text-xs-right mb-2">
                                        <span class="text-muted"><i class="icon-calendar4"></i> {{ date('D, d M Y', strtotime($info->tanggal_info)) }}</span>
                                    </div>
                                    <p class="mt-2">{!! $info->konten !!}</p>
                                    <!-- <hr> -->
                                    <!-- <div class="text-sm-center">
                                        <a href="{{ url()->previous() }}" class="btn btn-default">Close</a>
                                    </div> -->
                                </section>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection