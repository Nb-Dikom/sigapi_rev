@extends('layouts._app_frontend')
@section('custom_css')
    <style>
        .red {
            color: red;
        }
        .orange{
            color: orange;
        }
        .yellow{
            color: yellow;
        }
    </style>
@endsection
@section('content')
<div class="container">
    <ul class="breadcrumb">
        <li><a href="{{ url('/') }}">Beranda</a></li>
        <li><a href="{{ url('/status-gunung-api') }}">Status Gunung Api</a></li>
        <li class="active">{{ $status_gunung->gunung  }}</li>
    </ul>
    <div class="row margin-bottom-40">
        <div class="col-md-12 col-sm-12">
            <div class="content-page">
                <div class="row">
                    <div class="col-md-9 col-sm-9 blog-item">
                        <ul class="blog-info">
                            <li><i class="fa fa-tag"></i> {{ $status_gunung->gunung }}</li>
                            <li><i class="fa fa-calendar"></i> {{ date('D, d M Y', strtotime($status_gunung->tanggal_status_gunung)) }}</li>
                        </ul>
                        <div class="text-center">
                            <?php
                                switch ($status_gunung->status) {
                                    case 'AWAS':
                                        $color = 'red';
                                        break;
                                    case 'SIAGA':
                                        $color = 'orange';
                                        break;
                                    default:
                                        $color = 'yellow';
                                        break;
                                }
                            ?>
                            <h3>Gunung {{ $status_gunung->gunung }}</h3><br>
                            <h4>Berada Pada Status</h4>
                            <h2 class="{{ $color }}">{{ $status_gunung->status }}</h2>
                            <hr style="margin-top: 30px">
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 blog-sidebar">
                        <h2 class="no-top-space">Status Gunung</h2>
                        <ul class="nav sidebar-categories margin-bottom-40">
                            <li><a href="{{ url('status-gunung-api') }}">Semua ({{ $jumlah }})</a></li>
                            <li><a href="{{ url('status-gunung-api') .'?status=awas' }}">Awas ({{$awas}})</a></li>
                            <li><a href="{{ url('status-gunung-api') .'?status=siaga' }}">Siaga ({{$siaga}})</a></li>
                            <li><a href="{{ url('status-gunung-api') .'?status=waspada' }}">Waspada ({{$waspada}})</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('custom_js')
<script src="{{ asset('assets/corporate/scripts/layout.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        Layout.init();    
    });
</script>
@endsection