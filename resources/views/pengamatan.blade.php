@extends('layouts._app_frontend')
@section('custom_css')
<link href="{{ asset('assets/corporate/plugins/fancybox/source/jquery.fancybox.css') }}" rel="stylesheet">
<link href="{{ asset('assets/corporate/pages/css/portfolio.css') }}" rel="stylesheet">

<style>
    a.btn-transparent {
        color: #FAFAFA;
        font-size: 13px;
        padding: 5px 13px;
        white-space: nowrap;
        text-decoration: none;
        border: solid 1px #FAFAFA;
        background: none;
    }
</style>
@endsection
@section('content')
<div class="container">
    <ul class="breadcrumb">
        <li><a href="{{ url('/') }}">Beranda</a></li>
        <li class="active">Pengamatan</li>
    </ul>
    <div class="row margin-bottom-40">
        <div class="col-md-12 col-sm-12">
            <div class="content-page">
                <div class="row">
                    <div class="col-md-9 col-sm-9">
                        <div class="row mix-grid thumbnails">
                            <?php
                                $param = null;
                                $string = null;
                                if(isset($_GET['tahun'])) {
                                    $param=$_GET['tahun'];
                                    $string = '&tahun='. $param;
                                }
                            ?>
                            @foreach($list_pengamatan as $pengamatan)
                            <?php $lower_name = strtolower(str_replace(" ", "+", $pengamatan->nama_pengamatan)); ?>
                            <div class="col-md-3 col-sm-4 mix" style="display: block; opacity: 1; ">
                                <div class="mix-inner">
                                    <img src="{{ asset('/upload/pengamatan') .'/'. $pengamatan->foto_pengamatan }}" alt="{{ $pengamatan->nama_pengamatan }}" class="img-responsive">
                                    <div class="mix-details text-center">
                                        <h4>{{ $pengamatan->nama_pengamatan }}</h4>
                                        <a class="mix-link" href="{{ url('/pengamatan-gunung-api') .'/'. $lower_name }}"><i class="fa fa-eye"></i></a>
                                        <a class="mix-preview fancybox-button" href="{{ url('/pengamatan/download') .'/'. $lower_name }}"><i class="fa fa-download"></i></a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <nav class="text-center" style="margin-top: 25px">
                            <ul class="pagination">
                                <li class="@if($list_pengamatan->previousPageUrl() == null) disabled @endif">
                                    <a href="@if($list_pengamatan->previousPageUrl() == null) javascript:void(0); @else {{ $list_pengamatan->previousPageUrl() .$string }} @endif">
                                        <i class="fa fa-chevron-left"></i>
                                    </a>
                                </li>
                                @for($i = 1; $i <= ($list_pengamatan->lastPage()); $i++)
                                <li class="@if($list_pengamatan->currentPage() == $i) active @endif"><a href="@if($list_pengamatan->currentPage() == $i) javascript:void(0); @else {{ $list_pengamatan->url($i) .$string  }} @endif">{{ $i }}</a></li>
                                @endfor
                                <li class="@if($list_pengamatan->nextPageUrl() == null) disabled @endif">
                                    <a href="@if($list_pengamatan->nextPageUrl() == null) javascript:void(0); @else {{ $list_pengamatan->nextPageUrl() .$string  }} @endif">
                                        <i class="fa fa-chevron-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <div class="col-md-3 col-sm-3 blog-sidebar">
                        <h2 class="no-top-space">Tahun</h2>
                        <ul class="nav sidebar-categories margin-bottom-40">
                            <li class="@if(!$param) active @endif"><a href="{{ url('pengamatan-gunung-api') }}">Semua ({{ $jumlah }})</a></li>
                            @foreach($list_tahun as $tahun)
                            <li class="@if($param == $tahun['tahun']) active @endif"><a href="{{ url('pengamatan-gunung-api') .'?tahun='. $tahun['tahun'] }}">{{ $tahun["tahun"] .' ('. $tahun["total"].')' }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('custom_js')
<script src="{{ asset('assets/corporate/plugins/fancybox/source/jquery.fancybox.pack.js') }}" type="text/javascript"></script><!-- pop up -->
<script src="{{ asset('assets/corporate/plugins/jquery-mixitup/jquery.mixitup.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/corporate/scripts/layout.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/portfolio.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        Layout.init();    
    });
</script>
@endsection