@extends('layouts._app_frontend')
@section('custom_css')
<style>
    .pink {
        color: red;
    }
</style>
@endsection
@section('content')
<div class="container">
    <ul class="breadcrumb">
        <li><a href="{{ url('/') }}">Beranda</a></li>
        <li class="active">Registrasi</li>
    </ul>
    <div class="content-form-page">
        <div class="row">
            <div class="col-md-7 col-sm-7">
                @if(Session::has('success_message'))
                <h3>Sukses !</h3>
                <p>Terima anda telah mendaftar, kami akan memproses akun anda</p>
                @else
                <h3>Form Pendaftaran</h3>
                <form class="form-horizontal" method="POST" action="{{ route('simpan_organisasi') }}" enctype="multipart/form-data" role="form">
                    {!! csrf_field() !!}
                    <fieldset>
                        <legend>Organisasi</legend>
                        <div class="form-group">
                            <label for="organisasi" class="col-lg-4 control-label">Organisasi <span class="require">*</span></label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" id="organisasi" name="organisasi" value="{{ old('organisasi') }}">
                                @if($errors->has('organisasi'))
                                <span class="help-block">
                                    <strong class="pink">{{ $errors->first('organisasi') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="file" class="col-lg-4 control-label">Surat Keterangan Terdaftar Organisasi <span class="require">*</span></label>
                            <div class="col-lg-8">
                                <input type="file" id="file" name="file">
                                @if($errors->has('file'))
                                <span class="help-block">
                                    <strong class="pink">{{ $errors->first('file') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>Detail Organisasi</legend>
                        <div class="form-group">
                            <label for="alamat" class="col-lg-4 control-label">Alamat <span class="require">*</span></label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" id="alamat" name="alamat" value="{{ old('alamat') }}">
                                @if($errors->has('alamat'))
                                <span class="help-block">
                                    <strong class="pink">{{ $errors->first('alamat') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-lg-4 control-label">Email <span class="require">*</span></label>
                            <div class="col-lg-8">
                                <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">
                                @if($errors->has('email'))
                                <span class="help-block">
                                    <strong class="pink">{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="no_telp" class="col-lg-4 control-label">Nomor Telepon <span class="require">*</span></label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" id="no_telp" name="no_telp" value="{{ old('no_telp') }}">
                                @if($errors->has('no_telp'))
                                <span class="help-block">
                                    <strong class="pink">{{ $errors->first('no_telp') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="handphone" class="col-lg-4 control-label">Handphone</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" id="handphone" name="handphone" value="{{ old('handphone') }}">
                                @if($errors->has('handphone'))
                                <span class="help-block">
                                    <strong class="pink">{{ $errors->first('handphone') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fax" class="col-lg-4 control-label">Fax</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" id="fax" name="fax" value="{{ old('fax') }}">
                                @if($errors->has('fax'))
                                <span class="help-block">
                                    <strong class="pink">{{ $errors->first('fax') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="kode_pos" class="col-lg-4 control-label">Kode Pos</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" id="kode_pos" name="kode_pos" value="{{ old('kode_pos') }}">
                                @if($errors->has('kode_pos'))
                                <span class="help-block">
                                    <strong class="pink">{{ $errors->first('kode_pos') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="web" class="col-lg-4 control-label">Web</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" id="web" name="web" value="{{ old('web') }}">
                                @if($errors->has('web'))
                                <span class="help-block">
                                    <strong class="pink">{{ $errors->first('web') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </fieldset>
                    <div class="row text-center">
                        <div class="padding-left-0 padding-top-20">                        
                            <a class="btn btn-default" href="{{ url('/') }}">Batal</a>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
                @endif
            </div>
            <div class="col-md-4 col-sm-4 pull-right">
                <div class="form-info">
                    <h2><em>Informasi</em> Penting</h2>
                    <p>Apabila anda sudah mendaftar, cek email yang anda daftarkan.</p>
                    <p>Untuk informasi lebih lanjut, anda dapat menghubungi info@mail.com</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('custom_js')
<script src="{{ asset('assets/corporate/scripts/layout.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        Layout.init();    
    });
</script>
@endsection