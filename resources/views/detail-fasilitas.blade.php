@extends('layouts._app_frontend')
@section('custom_css')
<!-- ArcGis css -->
<link rel="stylesheet" href="https://js.arcgis.com/4.5/esri/css/main.css">
@endsection
@section('content')
<div class="container">
    <ul class="breadcrumb">
        <li><a href="{{ url('/') }}">Beranda</a></li>
        <li><a href="{{ url('/fasilitas-publik') }}">Fasilitas</a></li>
        <li class="active">{{ $fasilitas->nama_fasilitas  }}</li>
    </ul>
    <div class="row margin-bottom-40">
        <div class="col-md-12 col-sm-12">
            <div class="content-page">
                <div class="row">
                    <div class="col-md-9 col-sm-9 blog-item">
                        <div class="row text-center">
                            <div class="col-md-12 col-sm-12 margin-bottom-10">
                                <div id="frame-map" style="height: 420px"></div>
                                <span id="info" name="info" style="position:absolute; right:25px; bottom:25px; color:#000; z-index:50;"></span>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <table class="table table-bordered table-striped text-left">
                                    <thead>
                                        <tr>
                                            <td style="width:8%"></td>
                                            <td></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-bold-600">Fasilitas</td>
                                            <td><span class="text-bold-700" name="nama">{{ $fasilitas->nama_fasilitas }}</span></td>
                                        </tr>
                                        <tr>
                                            <td class="text-bold-600">Alamat</td>
                                            <td><span class="text-bold-700" name="alamat">{{ $fasilitas->alamat_fasilitas }}</span></td>
                                        </tr>
                                        <tr>
                                            <td class="text-bold-600">Longitude</td>
                                            <td><span class="text-bold-700" name="longitude">{{ $fasilitas->longitude }}</span></td>
                                        </tr>
                                        <tr>
                                            <td class="text-bold-600">Latitude</td>
                                            <td><span class="text-bold-700" name="latitude">{{ $fasilitas->latitude }}</span></td>
                                        </tr>
                                        <tr>
                                            <td class="text-bold-600">Jenis</td>
                                            <td><span class="text-bold-700" name="jenis">{{ $fasilitas->jenis_fasilitas }}</span></td>
                                        </tr>
                                        <tr>
                                            <td class="text-bold-600">Gunung</td>
                                            <td><span class="text-bold-700" name="gunung">{{ $fasilitas->gunung }}</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                @if($fasilitas->foto_fasilitas)
                                <img name="foto" class="img-responsive" src="{{ asset('/upload/fasilitas' .'/'. $fasilitas->foto_fasilitas) }}" alt="{{ $fasilitas->nama_fasilitas }}">
                                @else
                                <img name="foto" class="img-responsive" src="{{ asset('img/noimagefound.png') }}" alt="No Image">
                                @endif
                            </div>
                        </div>
                        <ul class="blog-info">
                            <li><i class="fa fa-user"></i> By: {{ $fasilitas->nama_pegawai }}</li>
                            <li><i class="fa fa-building"></i> {{ $fasilitas->jenis_fasilitas }}</li>
                            <li><i class="fa fa-tag"></i> {{ $fasilitas->gunung }}</li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-3 blog-sidebar">
                        <h2 class="no-top-space">Kategori</h2>
                        <ul class="nav sidebar-categories margin-bottom-40">
                            @foreach($kategori as $jenis_fasilitas)
                            @if($jenis_fasilitas['total'] !== 0)
                            <li>
                                <a href="{{ url('/fasilitas-publik?kategori') .'='.$jenis_fasilitas['alt'] }}">{{ $jenis_fasilitas['jenis_fasilitas'] .' ('. $jenis_fasilitas['total'] .')' }}</a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                        <a class="btn btn-primary" href="{{ url('/masuk') }}" style="color:white">Unduh Data</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('custom_js')
<script src="{{ asset('assets/corporate/scripts/layout.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        Layout.init();    
    });
</script>
<!-- ArcGis js -->
<script src="https://js.arcgis.com/4.5/"></script>
<script type="text/javascript">
    $(document).ready(function(){
        var basePath = "{{ url('/') }}";
        var data = {
            center: [
                parseFloat("{{ $fasilitas->longitude }}"),
                parseFloat("{{ $fasilitas->latitude }}"),
            ],
            nama: "{{ $fasilitas->nama_fasilitas }}",
            alamat: "{{ $fasilitas->alamat_fasilitas }}",
            longitude: parseFloat("{{ $fasilitas->longitude }}"),
            latitude: parseFloat("{{ $fasilitas->latitude }}"),
            jenis_fasilitas: "{{ $fasilitas->jenis_fasilitas }}",
            gunung: "{{ $fasilitas->gunung }}",
            path_icon: basePath + "/img/icon" +"/" + "{{ $fasilitas->jenis_fasilitas }}" +".png",
        }
        $.ajax({
            url: 'https://js.arcgis.com/4.5',
            type: 'GET',
            success: function() {
                processPointMap(data);
            },
            error: function() {
                errorMessage("no connection");
            }
        });
        
        if("{{ Session::has('error_message') }}"){
            errorMessage("{{ session('error_message') }}");
        }
    });
</script>
<script>
    function processPointMap(data){
        require([
        "esri/Map",
        "esri/views/MapView",
        "esri/Graphic",
        "esri/layers/FeatureLayer",
        "esri/widgets/Legend",
        "esri/widgets/ScaleBar",
        "esri/geometry/support/webMercatorUtils",
        "dojo/dom",
        "dojo/domReady!"
        ], function(Map, MapView, Graphic, FeatureLayer, Legend, ScaleBar, webMercatorUtils, dom) {
            var map = new Map({
                basemap: "streets"
            });

            var view = new MapView({
                center: data['center'],
                container: "frame-map",
                map: map,
                zoom: 11
            });

            var point = {
                type: "point",
                longitude: data['longitude'],
                latitude: data['latitude']
            };

            var markerSymbol = {
                type: "picture-marker",
                url: data['path_icon'],
                width: "32px",
                height: "32px"
            };

            var renderer = {
                type: "simple",
                symbol: markerSymbol,
            }

            var fields = [
                {
                    name: "ObjectID",
                    alias: "ObjectID",
                    type: "oid"
                },
                {
                    name: "nama",
                    alias: "Nama",
                    type: "string"
                },
                {
                    name: "alamat",
                    alias: "Alamat",
                    type: "string"
                },
                {
                    name: "longitude",
                    alias: "Longitude",
                    type: "string"
                },
                {
                    name: "latitude",
                    alias: "Latitude",
                    type: "string"
                },
                {
                    name: "jenis_fasilitas",
                    alias: "Jenis",
                    type: "string"
                },
                {
                    name: "gunung",
                    alias: "Gunung",
                    type: "string"
                },
            ];

            var features =	[	
                {
                    geometry: point,
                    attributes: {
                        ObjectID: 1,
                        nama: data['nama'],
                        alamat: data['alamat'],
                        longitude: data['longitude'],
                        latitude: data['latitude'],
                        jenis_fasilitas: data['jenis_fasilitas'],
                        gunung: data['gunung'],
                    }
                }
            ];

            var layer = new FeatureLayer({
                fields: fields,
                objectIdField: "ObjectID",
                geometryType: "point",
                spatialReference: { wkid: 4326 },
                source: features,
                renderer: renderer
            });

            var legend = new Legend({
                view: view,
                layerInfos: [{
                    layer: layer,
                    title: data['jenis_fasilitas']
                }]
            });
            var scaleBar = new ScaleBar({
                view: view,
                unit: "metric"
            });

            map.add(layer);

            view.ui.add(legend, "top-right");
            view.ui.add(scaleBar, {
                position: "bottom-left"
            });
            
            view.on("pointer-move", function(evt){
                var point = view.toMap({
                    x: evt.x,
                    y: evt.y
                });
                var longitude = point.longitude;
                var latitude = point.latitude;
                dom.byId("info").innerHTML = longitude.toString().substr(0,8) + ", " + latitude.toString().substr(0,8);
            });

            view.then(function(){
                //
            }).otherwise(errorCallback);

            function errorCallback(){
                errorMessage('server not responding...');
            }
        });
    }
</script>
@endsection