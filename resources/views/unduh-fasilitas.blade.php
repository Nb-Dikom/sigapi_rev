<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

        <title>Fasilitas</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                    <div class="text-center">
                        <img src="{{ asset('img/logo_bnpb.png') }}" alt="Logo" style="height:100px;">
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                    <div class="text-center">
                        <h4 class="text-uppercase" style="margin-bottom:20px">Badan Nasional Penanggulangan Bencana</h4>
                        <h4>Sistem Informasi Mitigasi</h4>
                        <h4>Bencana Gunung Api</h4>
                    </div>
                </div>
            </div>
            <hr style="border:2px solid black">
            <div class="row">
                <div class="text-center" style="margin-bottom:20px">
                    <h3>Fasilitas</h3>
                </div>
                <ul class="list-unstyled">
                    @foreach($data as $key => $val)
                        <li>{{ $key }}</li>
                        <table class="table table-bordered" style="font-size:11px">
                            <thead>
                                <tr>
                                    <th class="text-center">No.</th>
                                    <th>Nama</th>
                                    <th>Alamat</th>
                                    <th>Gunung</th>
                                    <th>Latitude</th>
                                    <th>Longitude</th>
                                    <th>Foto</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $numbering = 1; @endphp
                                @foreach($val as $fasilitas)
                                <tr>
                                    <td class="text-center">{{ $numbering }}</td>
                                    <td>{{ $fasilitas['nama'] }}</td>
                                    <td>{{ $fasilitas['alamat'] }}</td>
                                    <td>{{ $fasilitas['gunung'] }}</td>
                                    <td>{{ $fasilitas['latitude'] }}</td>
                                    <td>{{ $fasilitas['longitude'] }}</td>
                                    <td class="text-center">
                                        @if($fasilitas['img'])
                                        <img style="width:50px" src="{{ asset('upload/fasilitas') .'/'. $fasilitas['img'] }}" alt="foto">
                                        @else
                                        <img style="width:50px" src="{{ asset('img/noimagefound.png') }}" alt="foto">
                                        @endif
                                    </td>
                                </tr>
                                @php $numbering++; @endphp
                                @endforeach
                            </tbody>
                        </table>
                    @endforeach
                </ul>
            </div>
        </div>
    </body>
</html>