@extends('layouts._app')
@section('custom_css')
<!-- plugin notify -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/pnotify.min.css') }}">
@endsection

@section('content')
<div class="app-content content container-fluid">
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-xs-12 mb-1">
				<h2 class="content-header-title">Kawasan Rawan Bencana</h2>
			</div>
			<div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
				<div class="breadcrumb-wrapper col-xs-12">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{ route('krb_page') }}">Kawasan Rawan Bencana</a></li>
						<li class="breadcrumb-item active"> Tambah Kawasan Rawan Bencana</li>
					</ol>
				</div>
			</div>
		</div>
		<div class="content-body">
			<section>
				<div class="row">
					<div class="col-md-6 offset-md-3">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">Tambah Kawasan Rawan Bencana</h4>
								<a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
								<div class="heading-elements">
									<ul class="list-inline mb-0">
										<li><a data-action="expand"><i class="icon-expand2"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="card-block">
								<form class="form" method="POST" action="{{ route('save_krb') }}" enctype="multipart/form-data">
									{!! csrf_field() !!}
									<div class="form-body">
										<div class="form-group {{ $errors->has('jenis_krb') ? 'has-error' : '' }}">
											<label for="jenis_krb">Kawasan</label>
											<select id="jenis_krb" name="jenis_krb" class="form-control">
												<option value="none" selected="" disabled="">Pilih Kawasan</option>
												@foreach($collection['jenis_krb'] as $jenis_krb)
												@if(old('jenis_krb') == null)
												<option value="{{ $jenis_krb->id_jenis_krb}}">{{ $jenis_krb->jenis_krb }}</option>
												@else
												@if($jenis_krb->id_jenis_krb == old('jenis_krb'))
	                                                <option selected value="{{ $jenis_krb->id_jenis_krb }}">{{ $jenis_krb->jenis_krb }}</option>
	                                            @else
	                                                <option value="{{ $jenis_krb->id_jenis_krb }}">{{ $jenis_krb->jenis_krb }}</option>
	                                            @endif
												@endif
												@endforeach
											</select>
											@if($errors->has('jenis_krb'))
											<span class="help-block">
												<strong class="pink">{{ $errors->first('jenis_krb') }}</strong>
											</span>
											@endif
										</div>
										<div class="form-group">
											<label for="gunung">Gunung</label>
											<select id="gunung" name="gunung" class="form-control">
												<option value="none" selected="" disabled="">Pilih Gunung</option>
												@foreach($collection['gunung'] as $gunung)
												@if(old('gunung') == null)
												<option value="{{ $gunung->gunung}}">{{ $gunung->gunung }}</option>
												@else
												@if($gunung->gunung == old('gunung'))
													<option selected value="{{ $gunung->gunung }}">{{ $gunung->gunung }}</option>
												@else
													<option value="{{ $gunung->gunung }}">{{ $gunung->gunung }}</option>
												@endif
												@endif
												@endforeach
											</select>
										</div>
                                        <div class="form-group {{ $errors->has('json_file') ? 'has-error' : '' }}">
											<label for="json_file">File</label>
											<select id="json_file" name="json_file" class="form-control">
												<option value="none" selected="" disabled="">Pilih File</option>
												@foreach($collection['file_krb'] as $json_file)
												@if(old('json_file') == null)
												<option value="{{ $json_file['url']}}">{{ $json_file['name'] ." | ". $json_file['tags'] }}</option>
												@else
												@if($json_file['url'] == old('json_file'))
	                                                <option selected value="{{ $json_file['url'] }}">{{ $json_file['name'] ." | ". $json_file['tags'] }}</option>
	                                            @else
	                                                <option value="{{ $json_file['url'] }}">{{ $json_file['name'] ." | ". $json_file['tags'] }}</option>
	                                            @endif
												@endif
												@endforeach
											</select>
											@if($errors->has('jenis_krb'))
											<span class="help-block">
												<strong class="pink">{{ $errors->first('jenis_krb') }}</strong>
											</span>
											@endif
										</div>
									</div>
									<div class="form-actions center">
										<a href="{{ route('krb_page') }}" class="btn btn-warning mr-1"><i class="icon-cross2"></i> Batal</a>
										<button type="submit" class="btn btn-info"><i class="icon-check2"></i> Simpan</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection

@section('custom_js')
	<!-- plugin pnotify -->
	<script src="{{ asset('js/pnotify.min.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function(){		
			if("{{ Session::has('error_message') }}"){
				errorMessage("{{ session('error_message') }}");
			}

			if("{{ $errors->first() }}"){
				errorMessage("data kawasan rawan bencana tidak disimpan");
			}
		});
	</script>
@endsection