@extends('layouts._app')
@section('custom_css')
<!-- plugin notify -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/pnotify.min.css') }}">
@endsection

@section('content')
<div class="app-content content container-fluid">
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-xs-12 mb-1">
				<h2 class="content-header-title">Kawasan Rawan Bencana</h2>
			</div>
			<div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
				<div class="breadcrumb-wrapper col-xs-12">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{ route('krb_page') }}">Kawasan Rawan Bencana</a></li>
						<li class="breadcrumb-item active"> Ubah Kawasan Rawan Bencana</li>
					</ol>
				</div>
			</div>
		</div>
		<div class="content-body">
			<section>
				<div class="row">
					<div class="col-md-6 offset-md-3">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">Ubah Kawasan Rawan Bencana</h4>
								<a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
								<div class="heading-elements">
									<ul class="list-inline mb-0">
										<li><a data-action="expand"><i class="icon-expand2"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="card-block">
								<form class="form" method="POST" action="{{ route('update_krb', $collection['krb']->lower_name) }}" enctype="multipart/form-data">
									{!! csrf_field() !!}
									{{ method_field('PATCH') }}
									<div class="form-body">
										<div class="form-group">
											<label for="jenis_krb">Jenis Krb</label>
											<select id="jenis_krb" name="jenis_krb" class="form-control">
												<option value="none" selected="" disabled="">Pilih Jenis Krb</option>
												@foreach ($collection['jenis_krb'] as $jenis_krb)
												@if(old('jenis_krb') == null)
													@if($jenis_krb->jenis_krb == $collection['krb']->jenis_krb)
														<option selected value="{{ $jenis_krb->jenis_krb }}">{{ $jenis_krb->jenis_krb }}</option>
													@else
														<option value="{{ $jenis_krb->jenis_krb }}">{{ $jenis_krb->jenis_krb }}</option>
													@endif
												@else
													@if($jenis_krb->jenis_krb == old('jenis_krb'))
														<option selected value="{{ $jenis_krb->jenis_krb }}">{{ $jenis_krb->jenis_krb }}</option>
													@else
														<option value="{{ $jenis_krb->jenis_krb }}">{{ $jenis_krb->jenis_krb }}</option>
													@endif
												@endif
												@endforeach
											</select>
											@if($errors->has('jenis_krb'))
											<span class="help-block">
												<strong class="pink">{{ $errors->first('jenis_krb') }}</strong>
											</span>
											@endif
										</div>
										<div class="form-group">
											<label for="gunung">Gunung</label>
											<select id="gunung" name="gunung" class="form-control">
												<option value="none" selected="" disabled="">Pilih Gunung</option>
												@foreach ($collection['gunung'] as $gunung)
												@if(old('gunung') == null)
													@if($gunung->gunung == $collection['krb']->gunung)
														<option selected value="{{ $gunung->gunung }}">{{ $gunung->gunung }}</option>
													@else
														<option value="{{ $gunung->gunung }}">{{ $gunung->gunung }}</option>
													@endif
												@else
													@if($gunung->gunung == old('gunung'))
														<option selected value="{{ $gunung->gunung }}">{{ $gunung->gunung }}</option>
													@else
														<option value="{{ $gunung->gunung }}">{{ $gunung->gunung }}</option>
													@endif
												@endif
												@endforeach
											</select>
											@if($errors->has('gunung'))
											<span class="help-block">
												<strong class="pink">{{ $errors->first('gunung') }}</strong>
											</span>
											@endif
										</div>
                                        <div class="form-group {{ $errors->has('json_file') ? 'has-error' : '' }}">
											<label for="json_file">File</label>
											<select id="json_file" name="json_file" class="form-control">
												@foreach ($collection['file_krb'] as $json_file)
		                                        @if(old('json_file') == null)
		                                            @if($json_file['url'] == $collection['krb']->file_krb)
		                                                <option selected value="{{ $json_file['url'] }}">{{ $json_file['name'] ." | ". $json_file['tags'] }}</option>
		                                            @else
		                                                <option value="{{ $json_file['url'] }}">{{ $json_file['name'] ." | ". $json_file['tags'] }}</option>
		                                            @endif
		                                        @else
		                                            @if($json_file['url'] == old('json_file'))
		                                                <option selected value="{{ $json_file['url'] }}">{{ $json_file['name'] ." | ". $json_file['tags'] }}</option>
		                                            @else
		                                                <option value="{{ $json_file['url'] }}">{{ $json_file['name'] ." | ". $json_file['tags'] }}</option>
		                                            @endif
		                                        @endif
		                                    	@endforeach
											</select>
											@if($errors->has('json_file'))
											<span class="help-block">
												<strong class="pink">{{ $errors->first('json_file') }}</strong>
											</span>
											@endif
										</div>
									</div>
									<div class="form-actions center">
										<a href="{{ route('krb_page') }}" class="btn btn-warning mr-1"><i class="icon-cross2"></i> Batal</a>
										<button type="submit" class="btn btn-info"><i class="icon-check2"></i> Simpan</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection

@section('custom_js')
	<!-- plugin pnotify -->
	<script src="{{ asset('js/pnotify.min.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function(){		
			if("{{ Session::has('error_message') }}"){
				errorMessage("{{ session('error_message') }}");
			}
			
			if("{{ $errors->first() }}"){
				errorMessage("data kawasan rawan bencana tidak disimpan");
			}
		});
	</script>
@endsection