@extends('layouts._app_frontend')
@section('custom_css')
<style>
    a.btn-transparent {
        color: #7c858e;
        font-size: 15px;
        padding: 6px 14px;
        white-space: nowrap;
        text-decoration: none;
        border: solid 1px #7c858e;
        background: none;
    }
</style>
@endsection
@section('content')
<div class="container">
    <ul class="breadcrumb">
        <li><a href="{{ url('/') }}">Beranda</a></li>
        <li class="active">Info Kebencanaan</li>
    </ul>
    <div class="row margin-bottom-40">
        <div class="col-md-12 col-sm-12">
            <div class="content-page">
                <div class="row">
                    <div class="col-md-9 col-sm-9 blog-posts">
                        <?php
                            $index = 0;
                            $param = null;
                            $string = null;
                            $total = count($list_info);
                            if(isset($_GET['tahun'])) {
                                $param=$_GET['tahun'];
                                $string = '&tahun='. $param;
                            }
                        ?>
                        @foreach($list_info as $info)
                        <?php 
                            $lower_name = strtolower(str_replace(" ", "+", $info->nama_info));
                            $index++;
                        ?>
                        <div>
                            <h3><a href="{{ url('/info-kebencanaan') .'/'.$lower_name }}">{{ $info->nama_info }}</a></h3>
                            <ul class="blog-info">
                                <li><i class="fa fa-calendar"></i> {{ date('D, d M Y', strtotime($info->tanggal_info)) }}</li>
                            </ul>
                            <section class="margin-bottom-30" style="font-size:12px">{!! str_limit($info->konten, 300) !!}</section>
                            <a class="btn-transparent" href="{{ url('/info-kebencanaan') .'/'.$lower_name }}" class="more">Selengkapnya<i class="icon-angle-right"></i></a>
                        </div>
                        @if($index !== $total)
                        <hr class="blog-post-sep" style="margin:14px">
                        @endif
                        @endforeach
                        <nav class="text-center" style="margin-top: 25px">
                            <ul class="pagination">
                                <li class="@if($list_info->previousPageUrl() == null) disabled @endif">
                                    <a href="@if($list_info->previousPageUrl() == null) javascript:void(0); @else {{ $list_info->previousPageUrl() .$string }} @endif">
                                        <i class="fa fa-chevron-left"></i>
                                    </a>
                                </li>
                                @for($i = 1; $i <= ($list_info->lastPage()); $i++)
                                <li class="@if($list_info->currentPage() == $i) active @endif"><a href="@if($list_info->currentPage() == $i) javascript:void(0); @else {{ $list_info->url($i) .$string }} @endif">{{ $i }}</a></li>
                                @endfor
                                <li class="@if($list_info->nextPageUrl() == null) disabled @endif">
                                    <a href="@if($list_info->nextPageUrl() == null) javascript:void(0); @else {{ $list_info->nextPageUrl() .$string }} @endif">
                                        <i class="fa fa-chevron-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <div class="col-md-3 col-sm-3 blog-sidebar">
                        <h2 class="no-top-space">Tahun</h2>
                        <ul class="nav sidebar-categories margin-bottom-40">
                            <li class="@if(!$param) active @endif"><a href="{{ url('info-kebencanaan') }}">Semua ({{ $jumlah }})</a></li>
                            @foreach($list_tahun as $tahun)
                            <li class="@if($param == $tahun['tahun']) active @endif"><a href="{{ url('info-kebencanaan') .'?tahun='. $tahun['tahun'] }}">{{ $tahun["tahun"] .' ('. $tahun["total"].')' }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('custom_js')
<script src="{{ asset('assets/corporate/scripts/layout.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        Layout.init();    
    });
</script>
@endsection