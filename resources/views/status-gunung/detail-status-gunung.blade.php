@extends('layouts._app')
@section('content')
<div class="app-content content container-fluid">
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-xs-12 mb-1">
				<h2 class="content-header-title">Status Gunung</h2>
			</div>
			<div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
				<div class="breadcrumb-wrapper col-xs-12">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{ route('status_gunung_page') }}">Status Gunung</a></li>
						<li class="breadcrumb-item active"> {{ $status_gunung->gunung }}</li>
					</ol>
				</div>
			</div>
		</div>
		<div class="content-body">
			<section>
				<div class="row">
					<div class="col-md-10 offset-md-1">
						<div class="card">
							<div class="card-header" style="margin-bottom: 5px">
								<h4 class="card-title">{{ $status_gunung->gunung }}</h4>
							</div>
							<div class="card-block pt-0">
                                <section class="col-md-6 offset-md-3">
                                    <?php
                                        switch ($status_gunung->status) {
                                            case 'AWAS':
                                                $alert = "alert-danger";
                                                break;
                                            case 'SIAGA':
                                                $alert = "alert-warning";
                                                break;
                                            case 'WASPADA':
                                                $alert = "alert-teal";
                                                break;
                                            default:
                                                $alert = "alert-teal";
                                                break;
                                        }
                                    ?>
                                    <div class="alert {{ $alert }} text-sm-center" role="alert">
                                        <b>Pada Tanggal:</b>
                                        <h4>{{ date('D, d M Y', strtotime($status_gunung->tanggal_status_gunung)) }}</h4>
                                        <h5 class="mt-2 mb-0">status:</h5>
                                        <h3><b>{{ $status_gunung->status }}</b></h3>
                                    </div>
                                    <!-- <div class="text-sm-center">
                                        <a href="{{ url()->previous() }}" class="btn btn-default">Close</a>
                                    </div> -->
                                </section>
							</div>
						</div>
					<status_gunung>
				</div>
		</div>
	</div>
</div>
@endsection