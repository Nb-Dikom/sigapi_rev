@extends('layouts._app')
@section('custom_css')
<!-- Datatables css -->
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">

<!-- plugin notify -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/pnotify.min.css') }}">
@endsection

@section('content')
<div class="app-content content container-fluid">
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-xs-12 mb-1">
				<h2 class="content-header-title">Status Gunung</h2>
			</div>
			<div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
				<div class="breadcrumb-wrapper col-xs-12">
					<ol class="breadcrumb">
						<li class="breadcrumb-item active">Status Gunung</li>
					</ol>
				</div>
			</div>
		</div>
		<div class="content-body">
			<section>
				<div class="row">
					@if($jabatan !== "Sub Bagian Umum dan Kepegawaian" && $jabatan !== "Sub Bagian Penanggulangan Bencana")
					<div class="col-xs-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">Status Gunung</h4>
							</div>
							<div class="card-block">
								<div style="margin-bottom:15px">
									<a href="{{ route('form_create_status_gunung') }}" class="btn btn-success" title="Tambah Data" data-toggle="tooltip" data-placement="bottom" data-trigger="hover">Tambah <i class="icon-warning2"></i></a>
								</div>
								<div class="table-responsive">
									<table class="table table-hover table-striped table-file">
										<thead>
											<tr>
												<th style="width:8%">No.</th>
												<th>Status</th>
												<th>Gunung</th>
												<th>Verifikasi</th>
												@if($jabatan !== "Kepala Seksi Kesiapsiagaan")
												<th>Validasi</th>
												@endif
												<th>Tanggal</th>
												<th>Oleh</th>
												<th class="text-xs-center" style="width:15%">Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php $index=0;?>
									    	@foreach($list_status_gunung as $status_gunung)
									    	<?php
												$lower_name = strtolower($status_gunung->id_status_gunung . "+" .$status_gunung->id_status);
												$index++;
											?>
											<tr>
				                                <th scope="row">{{ $index }}.</th>
				                                <td>{{ $status_gunung->status }}</td>
				                                <td>{{ $status_gunung->gunung }}</td>
				                                <td>
				                                	<span name="status-verifikasi-{{ $index }}" @if($status_gunung->verifikasi == 'Pending') class="tag tag-warning"
		                                			@elseif($status_gunung->verifikasi == 'Tidak Lengkap') class="tag tag-danger"
		                                			@elseif($status_gunung->verifikasi == 'Lengkap') class="tag tag-success" @endif >
				                                	{{ $status_gunung->verifikasi }}</span>
				                                </td>
												@if($jabatan !== "Kepala Seksi Kesiapsiagaan")
				                                <td>
				                                	<span name="status-validasi-{{ $index }}" @if($status_gunung->validasi == 'Pending') class="tag tag-warning"
		                                			@elseif($status_gunung->validasi == 'Tidak Valid') class="tag tag-danger"
		                                			@elseif($status_gunung->validasi == 'Valid') class="tag tag-success" @endif >
				                                	{{ $status_gunung->validasi }}</span>
				                                </td>
												@endif
				                                <td>{{ date('D, d M Y', strtotime($status_gunung->tanggal_status_gunung)) }}</td>
				                                <td>{{ $status_gunung->nama_pegawai }}</td>
				                                <td class="text-xs-center">
													<ul class="list-inline">
														<li>
						                                	<button index="{{ $index }}" value="{{ $lower_name }}" class="btn btn-sm btn-info" name="lihat" title="Lihat Data" data-toggle="tooltip" data-placement="bottom" data-trigger="hover"><i class="icon-eye6"></i></button>
														</li>
														<li>
						                                	<a href="{{ route('form_edit_status_gunung', $lower_name) }}" class="btn btn-sm btn-warning" name="edit" title="Ubah Data" data-toggle="tooltip" data-placement="bottom" data-trigger="hover"><i class="icon-edit2"></i></a>
														</li>
														<li>
						                                	<button value="{{ $lower_name }}" class="btn btn-sm btn-danger" name="hapus" title="Hapus Data" data-toggle="tooltip" data-placement="bottom" data-trigger="hover"><i class="icon-android-delete"></i></button>
														</li>
													</ul>
				                                </td>
				                            </tr>
				                            @endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					@else
					<div class="col-xs-12">
						<div class="card mb-1">
							<div class="card-header">
								<h4 class="card-title">Status Gunung</h4>
							</div>
							<div class="card-body">
								<div class="card-block">
									<table class="table table-hover table-striped  table-file">
										<thead>
											<tr>
												<th style="width:8%">No.</th>
												<th>Gunung</th>
												<th>Status</th>
												<th>Tanggal</th>
											</tr>
										</thead>
										<tbody>
											<?php $index=0;?>
											@foreach($list_status_gunung as $status_gunung)
											@if($status_gunung->validasi == "Valid")
											<?php
												$lower_name = strtolower($status_gunung->id_status_gunung . "+" .$status_gunung->id_status);
												$index++;
											?>
											<tr>
												<th scope="row">{{ $index }}.</th>
												<td><a href="{{ route('view_status_gunung', $lower_name) }}">{{ $status_gunung->gunung }}</a></td>
												<td>{{ $status_gunung->status }}</td>
												<td>{{ date('D, d M Y', strtotime($status_gunung->tanggal_status_gunung)) }}</td>
											</tr>
											@endif
											@endforeach
											@foreach($list_status_gunung as $status_gunung)
											@if(!$status_gunung->status)
											<?php
												$index++;
											?>
											<tr>
												<th scope="row">{{ $index }}.</th>
												<td>{{ $status_gunung->gunung }}</td>
												<td>NORMAL</td>
												<td>-</td>
											</tr>
											@endif
											@endforeach
										</tbody>
									</table>
									
								</div>
							</div>
						</div>
					</div>
					@endif
				</div>
			</section>
			@if($jabatan !== "Sub Bagian Umum dan Kepegawaian" && $jabatan !== "Sub Bagian Penanggulangan Bencana")
			<div id="modal-lihat" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
		        <div class="modal-dialog modal-md" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title"><i class="icon-warning"></i> Status Gunung</h4>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-md-12">
									<span>Status: </span><span name="status"></span> <br>
									<span>Gunung: </span><span name="gunung"></span> <br>
									<span class="text-muted">Tanggal: </span><span name="tanggal"></span>
									@if($jabatan !== "Pengolah Data Pra Bencana")
									<div class="text-sm-center mt-5">
										@if($jabatan !== "Kepala Bidang Pencegahan dan Kesiapsiagaan")
										<ul class="list-inline verified">
											<li>
												<button index="" value="" class="btn btn-danger" name="tidak_lengkap" title="Tidak Lengkap" data-toggle="tooltip" data-placement="bottom" data-trigger="hover"><i class="icon-cross2"></i> Tidak Lengkap</button>
											</li>
											<li>
												<button index="" value="" class="btn btn-success" name="lengkap" title="Lengkap" data-toggle="tooltip" data-placement="bottom" data-trigger="hover"><i class="icon-check2"></i> Lengkap</button>
											</li>
										</ul>
										@endif
										@if($jabatan !== "Kepala Seksi Kesiapsiagaan")
										<ul class="list-inline validated">
											<li>
												<button index="" value="" class="btn btn-danger" name="tidak_valid" title="Tidak Valid" data-toggle="tooltip" data-placement="bottom" data-trigger="hover"><i class="icon-cross2"></i> Tidak Valid</button>
											</li>
											<li>
												<button index="" value="" class="btn btn-success" name="valid" title="Valid" data-toggle="tooltip" data-placement="bottom" data-trigger="hover"><i class="icon-check2"></i> Valid</button>
											</li>
										</ul>
										@endif
									</div>
									@endif
									<hr>
									<div class="text-sm-right">
										<button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
		    </div>
			<!-- Modal Delete -->
			<div class="modal fade text-xs-left" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
			@include('layouts._delete')
			@endif
			</div>
		</div>
	</div>
</div>
@endsection

@section('custom_js')
<!-- Datatables js  -->
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<!-- plugin pnotify -->
<script src="{{ asset('js/pnotify.min.js') }}"></script>
<!-- moment -->
<script src="{{ asset('assets/moment/moment.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		@if($jabatan !== "Sub Bagian Umum dan Kepegawaian" && $jabatan !== "Sub Bagian Penanggulangan Bencana")
		$('.table-file').DataTable();

		$('.table-responsive').on('click', 'button[name="lihat"]', function(){
			var id = $(this).attr('value');
			var index = $(this).attr('index');
			$.ajax({
                url: '{{ url("/status-gunung") }}'+'/'+id,
                type: 'GET',
                beforeSend: function () {
                    //
                    startLoader()
                },
                success: function(response){
					$('span[name="status"]').html('');
					$('span[name="gunung"]').html('');
            		$('span[name="tanggal"]').html('');

            		$('span[name="status"]').html(response.status);
            		$('span[name="gunung"]').html(response.gunung);
            		$('span[name="tanggal"]').html(moment(response.tanggal_status_gunung).format('DD MMMM YYYY'));
					switch (response.verifikasi) {
						case "Pending":
							$('.verified').show();
							$('.validated').hide();
							$('button[name="tidak_lengkap"]').val(response.lower_name);
							$('button[name="tidak_lengkap"]').attr("index", index);
							$('button[name="lengkap"]').val(response.lower_name);
							$('button[name="lengkap"]').attr("index", index);
							break;
						case "Tidak Lengkap":
							$('.verified').hide();
							$('.validated').hide();
							break;
					
						default:
							$('.verified').hide();
							if(response.validasi != "Pending"){ $('.validated').hide(); }
							else
							{
								$('.validated').show();
								$('button[name="tidak_valid"]').val(response.lower_name);
								$('button[name="tidak_valid"]').attr("index", index);
								$('button[name="valid"]').val(response.lower_name);
								$('button[name="valid"]').attr("index", index);
							};
							break;
					}
					
					endLoader("lihat");
                },
                error: function () {
                	endLoader("alert");
                }
            });
		});
		@if($jabatan !== "Pengolah Data Pra Bencana")
		@if($jabatan !== "Kepala Bidang Pencegahan dan Kesiapsiagaan")
		$('#modal-lihat').on('click', 'button[name="lengkap"]', function(){
			var index = $(this).attr('index');
        	$.ajaxSetup({
                headers:{
                    'X-CSRF-TOKEN': $('input[name="_token"]').val()
                }
            });
            $.ajax({
                url: "{{ route('verified_status_gunung') }}",
                type: 'PATCH',
                data: {'name': $(this).attr('value')},
                beforeSend: function () {
                    startLoader()
                },
                success: function(response){
					status = response.data.verifikasi;
					if(status == "Lengkap")
					{
                		$('span[name="status-verifikasi-'+index+'"]').attr('class', 'tag tag-success');
					}
                	$('span[name="status-verifikasi-'+index+'"]').html(status);
					$('#modal-lihat').modal('hide');
                	endLoader();
                },
                error: function () {
                	endLoader("alert");
                }
            });
        });
		$('#modal-lihat').on('click', 'button[name="tidak_lengkap"]', function(){
			var index = $(this).attr('index');
        	$.ajaxSetup({
                headers:{
                    'X-CSRF-TOKEN': $('input[name="_token"]').val()
                }
            });
            $.ajax({
                url: "{{ route('unverified_status_gunung') }}",
                type: 'PATCH',
                data: {'name': $(this).attr('value')},
                beforeSend: function () {
                    startLoader()
                },
                success: function(response){
                	status = response.data.verifikasi;
					if(status == "Tidak Lengkap")
					{
                		$('span[name="status-verifikasi-'+index+'"]').attr('class', 'tag tag-danger');
					}
                	$('span[name="status-verifikasi-'+index+'"]').html(status);
					$('#modal-lihat').modal('hide');
                	endLoader();
                },
                error: function () {
                	endLoader("alert");
                }
            });
        });
		@endif
		@if($jabatan !== "Kepala Seksi Kesiapsiagaan")
		$('#modal-lihat').on('click', 'button[name="valid"]', function(){
			var index = $(this).attr('index');
        	$.ajaxSetup({
                headers:{
                    'X-CSRF-TOKEN': $('input[name="_token"]').val()
                }
            });
            $.ajax({
                url: "{{ route('validated_status_gunung') }}",
                type: 'PATCH',
                data: {'name': $(this).attr('value')},
                beforeSend: function () {
                    startLoader()
                },
                success: function(response){
					status = response.data.validasi;
					if(status == "Valid")
					{
                		$('span[name="status-validasi-'+index+'"]').attr('class', 'tag tag-success');
					}
                	$('span[name="status-validasi-'+index+'"]').html(status);
					$('#modal-lihat').modal('hide');
                	endLoader();
                },
                error: function () {
                	endLoader("alert");
                }
            });
        });
		$('#modal-lihat').on('click', 'button[name="tidak_valid"]', function(){
			var index = $(this).attr('index');
        	$.ajaxSetup({
                headers:{
                    'X-CSRF-TOKEN': $('input[name="_token"]').val()
                }
            });
            $.ajax({
                url: "{{ route('unvalidated_status_gunung') }}",
                type: 'PATCH',
                data: {'name': $(this).attr('value')},
                beforeSend: function () {
                    startLoader()
                },
                success: function(response){
                	status = response.data.validasi;
					if(status == "Tidak Valid")
					{
                		$('span[name="status-validasi-'+index+'"]').attr('class', 'tag tag-danger');
					}
                	$('span[name="status-validasi-'+index+'"]').html(status);
					$('#modal-lihat').modal('hide');
                	endLoader();
                },
                error: function () {
                	endLoader("alert");
                }
            });
        });
		@endif
		@endif
		$('.table-responsive').on('click', 'button[name="hapus"]', function(){
			var id = $(this).attr('value');

	        modal_delete("{{ url('/status_gunung') }}" +'/'+id);
		});
		@endif

		if("{{ Session::has('success_message') }}"){
			successMessage("{{ session('success_message') }}");
		}
		if("{{ Session::has('error_message') }}"){
			errorMessage("{{ session('error_message') }}");
		}
	});
</script>
@endsection
