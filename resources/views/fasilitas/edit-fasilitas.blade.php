@extends('layouts._app')
@section('custom_css')
<!-- plugin notify -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/pnotify.min.css') }}">
@endsection

@section('content')
<div class="app-content content container-fluid">
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-xs-12 mb-1">
				<h2 class="content-header-title">Fasilitas</h2>
			</div>
			<div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
				<div class="breadcrumb-wrapper col-xs-12">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{ url('/fasilitas') }}">Fasilitas</a></li>
						<li class="breadcrumb-item active"> Ubah Fasilitas</li>
					</ol>
				</div>
			</div>
		</div>
		<div class="content-body">
			<section>
				<div class="row">
					<div class="col-md-6 offset-md-3">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">Ubah Fasilitas</h4>
								<a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
								<div class="heading-elements">
									<ul class="list-inline mb-0">
										<li><a data-action="expand"><i class="icon-expand2"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="card-block">
                                <form class="form" method="POST" action="{{ route('update_fasilitas', $collection['fasilitas']->lower_name) }}" enctype="multipart/form-data">
									{!! csrf_field() !!}
									{{ method_field('PATCH') }}
									<div class="form-body">
                                        <div class="form-group {{ $errors->has('nama') ? 'has-error' : '' }}">
											<label for="nama">Nama</label>
											<div class="position-relative has-icon-left">
												<input type="text" id="nama" class="form-control" placeholder="masukkan nama" name="nama"  required="" 
                                                @if(old('nama') == null) value="{{ $collection['fasilitas']->nama_fasilitas }}"
												@else value="{{ old('nama') }}" @endif >
												<div class="form-control-position">
													<i class="icon-office"></i>
												</div>
											</div>
											@if($errors->has('nama'))
											<span class="help-block">
												<strong class="pink">{{ $errors->first('nama') }}</strong>
											</span>
											@endif
										</div>
                                        <div class="form-group {{ $errors->has('alamat') ? 'has-error' : '' }}">
                                            <label for="alamat">Alamat</label>
                                            <div class="position-relative has-icon-left">
                                                <input type="text" id="alamat" class="form-control" placeholder="masukkan alamat" name="alamat"  required="" 
                                                @if(old('alamat') == null) value="{{ $collection['fasilitas']->alamat_fasilitas }}"
												@else value="{{ old('alamat') }}" @endif >
                                                <div class="form-control-position">
                                                    <i class="icon-location4"></i>
                                                </div>
                                            </div>
                                            @if($errors->has('alamat'))
                                            <span class="help-block">
                                                <strong class="pink">{{ $errors->first('alamat') }}</strong>
                                            </span>
                                            @endif
										</div>
										<div class="form-group {{ $errors->has('foto') ? 'has-error' : '' }}">
											<label>Foto</label><br>
											<label id="foto" class="file center-block">
												<input type="file" id="foto" name="foto">
												<span class="file-custom"></span>
											</label>
											@if($errors->has('foto'))
											<span class="help-block">
												<strong class="pink">{{ $errors->first('foto') }}</strong>
											</span>
											@endif
										</div>
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <div class="form-group {{ $errors->has('longitude') ? 'has-error' : '' }}">
                                                    <label for="longitude">Longitude</label>
                                                    <div class="position-relative has-icon-left">
                                                        <input type="text" id="longitude" class="form-control" placeholder="longitude" name="longitude"  required="" 
                                                        @if(old('longitude') == null) value="{{ $collection['fasilitas']->longitude }}"
												        @else value="{{ old('longitude') }}" @endif >
                                                        <div class="form-control-position">
                                                            <i class="icon-location-2"></i>
                                                        </div>
                                                    </div>
                                                    @if($errors->has('longitude'))
                                                    <span class="help-block">
                                                        <strong class="pink">{{ $errors->first('longitude') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <div class="form-group {{ $errors->has('latitude') ? 'has-error' : '' }}">
                                                    <label for="latitude">Latitude</label>
                                                    <div class="position-relative has-icon-left">
                                                        <input type="text" id="latitude" class="form-control" placeholder="latitude" name="latitude"  required="" 
                                                        @if(old('latitude') == null) value="{{ $collection['fasilitas']->latitude }}"
												        @else value="{{ old('latitude') }}" @endif >
                                                        <div class="form-control-position">
                                                            <i class="icon-location-2"></i>
                                                        </div>
                                                    </div>
                                                    @if($errors->has('latitude'))
                                                    <span class="help-block">
                                                        <strong class="pink">{{ $errors->first('latitude') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group {{ $errors->has('jenis_fasilitas') ? 'has-error' : '' }}">
											<label for="jenis_fasilitas">Jenis Fasilitas</label>
											<select id="jenis_fasilitas" name="jenis_fasilitas" class="form-control">
												<option value="none" selected="" disabled="">Pilih Jenis Fasilitas</option>
												@foreach ($collection['jenis_fasilitas'] as $jenis_fasilitas)
												@if(old('jenis_fasilitas') == null)
													@if($jenis_fasilitas->jenis_fasilitas == $collection['fasilitas']->jenis_fasilitas)
														<option selected value="{{ $jenis_fasilitas->jenis_fasilitas }}">{{ $jenis_fasilitas->jenis_fasilitas }}</option>
													@else
														<option value="{{ $jenis_fasilitas->jenis_fasilitas }}">{{ $jenis_fasilitas->jenis_fasilitas }}</option>
													@endif
												@else
													@if($jenis_fasilitas->jenis_fasilitas == old('jenis_fasilitas'))
														<option selected value="{{ $jenis_fasilitas->jenis_fasilitas }}">{{ $jenis_fasilitas->jenis_fasilitas }}</option>
													@else
														<option value="{{ $jenis_fasilitas->jenis_fasilitas }}">{{ $jenis_fasilitas->jenis_fasilitas }}</option>
													@endif
												@endif
												@endforeach
											</select>
											@if($errors->has('jenis_fasilitas'))
											<span class="help-block">
												<strong class="pink">{{ $errors->first('jenis_fasilitas') }}</strong>
											</span>
											@endif
										</div>
										<div class="form-group">
											<label for="gunung">Gunung</label>
											<select id="gunung" name="gunung" class="form-control">
												<option value="none" selected="" disabled="">Pilih Gunung</option>
												@foreach ($collection['gunung'] as $gunung)
												@if(old('gunung') == null)
													@if($gunung->gunung == $collection['fasilitas']->gunung)
														<option selected value="{{ $gunung->gunung }}">{{ $gunung->gunung }}</option>
													@else
														<option value="{{ $gunung->gunung }}">{{ $gunung->gunung }}</option>
													@endif
												@else
													@if($gunung->gunung == old('gunung'))
														<option selected value="{{ $gunung->gunung }}">{{ $gunung->gunung }}</option>
													@else
														<option value="{{ $gunung->gunung }}">{{ $gunung->gunung }}</option>
													@endif
												@endif
												@endforeach
											</select>
											@if($errors->has('gunung'))
											<span class="help-block">
												<strong class="pink">{{ $errors->first('gunung') }}</strong>
											</span>
											@endif
										</div>
									</div>
									<div class="form-actions center">
										<a href="{{ url('/fasilitas') }}" class="btn btn-warning mr-1"><i class="icon-cross2"></i> Batal</a>
										<button type="submit" class="btn btn-info"><i class="icon-check2"></i> Simpan</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection

@section('custom_js')
	<!-- plugin pnotify -->
	<script src="{{ asset('js/pnotify.min.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function(){
            $('input[name="longitude"]').keydown(function (e) {
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                        (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                        (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                        (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                        (e.keyCode >= 35 && e.keyCode <= 39)) {
                    return;
                }
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
            $('input[name="latitude"]').keydown(function (e) {
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                        (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                        (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                        (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                        (e.keyCode >= 35 && e.keyCode <= 39)) {
                    return;
                }
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });

			if("{{ Session::has('error_message') }}"){
				errorMessage("{{ session('error_message') }}");
			}
			
			if("{{ $errors->first() }}"){
				errorMessage("data fasilitas tidak disimpan");
			}
		});
	</script>
@endsection