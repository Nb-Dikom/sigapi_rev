@extends('layouts._app')
@section('custom_css')
<!-- Datatables css -->
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">

<!-- plugin notify -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/pnotify.min.css') }}">

<!-- ArcGis css -->
<link rel="stylesheet" href="https://js.arcgis.com/4.5/esri/css/main.css">
@endsection

@section('content')
<div class="app-content content container-fluid">
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-xs-12 mb-1">
				<h2 class="content-header-title">Fasilitas</h2>
			</div>
			<div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
				<div class="breadcrumb-wrapper col-xs-12">
					<ol class="breadcrumb">
						<li class="breadcrumb-item active">Fasilitas</li>
					</ol>
				</div>
			</div>
		</div>
		<div class="content-body">
			<section>
				<div class="row">
					@if($jabatan !== "Sub Bagian Umum dan Kepegawaian" && $jabatan !== "Sub Bagian Penanggulangan Bencana")
					<div class="col-xs-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">Fasilitas</h4>
							</div>
							<div class="card-block">
								<div style="margin-bottom:15px">
									<a href="{{ route('form_create_fasilitas') }}" class="btn btn-success" title="Tambah Data" data-toggle="tooltip" data-placement="bottom" data-trigger="hover">Tambah <i class="icon-office"></i></a>
								</div>
								<div class="table-responsive">
									<table class="table table-hover table-striped table-file">
										<thead>
											<tr>
												<th style="width:8%">No.</th>
												<th>Nama</th>
												<th>Jenis</th>
												<th>Gunung</th>
												<th>Verifikasi</th>
												@if($jabatan !== "Kepala Seksi Kesiapsiagaan")
												<th>Validasi</th>
												@endif
												<th>Tanggal</th>
												<th>Oleh</th>
												<th class="text-xs-center" style="width:20%">Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php $index=0;?>
									    	@foreach($list_fasilitas as $fasilitas)
									    	<?php
												$lower_name = strtolower(str_replace(" ", "+", $fasilitas->nama_fasilitas));
												$index++;
											?>
											<tr>
				                                <th scope="row">{{ $index }}.</th>
				                                <td>{{ $fasilitas->nama_fasilitas }}</td>
				                                <td>{{ $fasilitas->jenis_fasilitas }}</td>
				                                <td>{{ $fasilitas->gunung }}</td>
				                                <td>
				                                	<span name="status-verifikasi-{{ $index }}" @if($fasilitas->verifikasi == 'Pending') class="tag tag-warning"
		                                			@elseif($fasilitas->verifikasi == 'Tidak Lengkap') class="tag tag-danger"
		                                			@elseif($fasilitas->verifikasi == 'Lengkap') class="tag tag-success" @endif >
				                                	{{ $fasilitas->verifikasi }}</span>
				                                </td>
												@if($jabatan !== "Kepala Seksi Kesiapsiagaan")
				                                <td>
				                                	<span name="status-validasi-{{ $index }}" @if($fasilitas->validasi == 'Pending') class="tag tag-warning"
		                                			@elseif($fasilitas->validasi == 'Tidak Valid') class="tag tag-danger"
		                                			@elseif($fasilitas->validasi == 'Valid') class="tag tag-success" @endif >
				                                	{{ $fasilitas->validasi }}</span>
				                                </td>
												@endif
				                                <td>{{ date('D, d M Y', strtotime($fasilitas->tanggal_pengamatan)) }}</td>
				                                <td>{{ $fasilitas->nama_pegawai }}</td>
				                                <td class="text-xs-center">
													<ul class="list-inline">
														<li>
						                                	<button index="{{ $index }}" value="{{ $lower_name }}" class="btn btn-sm btn-info" name="lihat" title="Lihat Data" data-toggle="tooltip" data-placement="bottom" data-trigger="hover"><i class="icon-eye6"></i></button>
														</li>
														<li>
						                                	<a href="{{ route('form_edit_fasilitas', $lower_name) }}" class="btn btn-sm btn-warning" name="edit" title="Ubah Data" data-toggle="tooltip" data-placement="bottom" data-trigger="hover"><i class="icon-edit2"></i></a>
														</li>
														<li>
						                                	<button value="{{ $lower_name }}" class="btn btn-sm btn-danger" name="hapus" title="Hapus Data" data-toggle="tooltip" data-placement="bottom" data-trigger="hover"><i class="icon-android-delete"></i></button>
														</li>
													</ul>
				                                </td>
				                            </tr>
				                            @endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					@else
					<div class="col-xs-12">
						<div class="card mb-1">
							<div class="card-header">
								<h4 class="card-title">Fasilitas <a class="btn btn-info btn-sm float-right" href="{{ url('fasilitas').'/download' }}" target="_blank" style="color:white"><i class="icon-download"></i></a></h4>
							</div>
						</div>
					</div>
					@foreach($list_fasilitas as $fasilitas)
					<?php $lower_name = strtolower(str_replace(" ", "+", $fasilitas->nama_fasilitas)); ?>
					<div class="col-md-6 col-xs-12">
						<div class="card match-height">
							<div class="card-body">
								<div class="card-block pb-0">
									<div class="row">
										<a href="{{ route('view_fasilitas', $lower_name) }}" style="color: #374149">
											<div class="col-md-4 col-xs-12">
												<?php
													if($fasilitas->foto_fasilitas)
														$url_foto = asset('upload/fasilitas') .'/'. $fasilitas->foto_fasilitas;
													else
														$url_foto = asset('img') .'/'. 'noimagefound.png';
												?>
												<img class="img-thumbnail" src="{{ $url_foto }}" alt="{{ $fasilitas->nama_fasilitas }}">
											</div>
											<div class="col-md-8 col-xs-12">
												<ul class="list-unstyled">
													<li class="mb-1">
														<h4 class="text-capitalize">{{ strtolower($fasilitas->nama_fasilitas) }}</h4>
													</li>
													<li>
														<i class="icon-office"></i>&nbsp;
														<span>{{ $fasilitas->jenis_fasilitas }}</span>
													</li>
													<li>
														<i class="icon-map-marker"></i>&nbsp;
														<span class="text-capitalize">{{ strtolower($fasilitas->alamat_fasilitas) }}</span>
													</li>
													<li>
														<i class="icon-tag"></i>&nbsp;
														<span class="text-capitalize">{{ strtolower($fasilitas->gunung) }}</span>
													</li>
												</ul>
											</div>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					@endforeach
					<div class="col-md-12">
						<div class="text-xs-center">
							<ul class="pagination">
								<li class="page-item @if($list_fasilitas->previousPageUrl() == null) disabled @endif">
									<a class="page-link" href="@if($list_fasilitas->previousPageUrl() == null) javascript:void(0); @else {{ $list_fasilitas->previousPageUrl() }} @endif" aria-label="Previous">
										<span aria-hidden="true">« Prev</span>
										<span class="sr-only">Previous</span>
									</a>
								</li>
								@for($i = 1; $i <= ($list_fasilitas->lastPage()); $i++)
								<li class="page-item @if($list_fasilitas->currentPage() == $i) active @endif"><a class="page-link" href="@if($list_fasilitas->currentPage() == $i) javascript:void(0); @else {{ $list_fasilitas->url($i) }} @endif">{{ $i }}</a></li>
								@endfor
								<li class="page-item @if($list_fasilitas->nextPageUrl() == null) disabled @endif">
									<a class="page-link" href="@if($list_fasilitas->nextPageUrl() == null) javascript:void(0); @else {{ $list_fasilitas->nextPageUrl() }} @endif" aria-label="Next">
									<span aria-hidden="true">Next »</span>
										<span class="sr-only">Next</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
					@endif
				</div>
			</section>
			@if($jabatan !== "Sub Bagian Umum dan Kepegawaian" && $jabatan !== "Sub Bagian Penanggulangan Bencana")
			<div id="modal-lihat" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
		        <div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title"><i class="icon-office"></i> Fasilitas</h4>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-md-6 col-sm-6">
									<div id="frame-map" style="height: 520px"></div>
									<span id="info" name="info" style="position:absolute; right:25px; bottom:25px; color:#000; z-index:50;"></span>
								</div>
								<div class="col-md-6 col-sm-6 text-xs-center mb-3">
									<img name="foto" class="card-img-top img-fluid" src="" alt="">
									<table class="table table-bordered table-striped text-xs-left">
										<thead>
											<tr>
												<td style="width:8%"></td>
												<td></td>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="text-bold-600">Nama</td>
												<td><span class="text-bold-700" name="nama"></span></td>
											</tr>
											<tr>
												<td class="text-bold-600">Alamat</td>
												<td><span class="text-bold-700" name="alamat"></span></td>
											</tr>
											<tr>
												<td class="text-bold-600">Longitude</td>
												<td><span class="text-bold-700" name="longitude"></span></td>
											</tr>
											<tr>
												<td class="text-bold-600">Latitude</td>
												<td><span class="text-bold-700" name="latitude"></span></td>
											</tr>
											<tr>
												<td class="text-bold-600">Jenis</td>
												<td><span class="text-bold-700" name="jenis"></span></td>
											</tr>
											<tr>
												<td class="text-bold-600">Gunung</td>
												<td><span class="text-bold-700" name="gunung"></span></td>
											</tr>
										</tbody>
									</table>
								</div>
								@if($jabatan !== "Pengolah Data Pra Bencana")
								<div class="col-md-12">
									<div class="text-sm-center mt-5">
										@if($jabatan !== "Kepala Bidang Pencegahan dan Kesiapsiagaan")
										<ul class="list-inline verified">
											<li>
												<button index="" value="" class="btn btn-danger" name="tidak_lengkap" title="Tidak Lengkap" data-toggle="tooltip" data-placement="bottom" data-trigger="hover"><i class="icon-cross2"></i> Tidak Lengkap</button>
											</li>
											<li>
												<button index="" value="" class="btn btn-success" name="lengkap" title="Lengkap" data-toggle="tooltip" data-placement="bottom" data-trigger="hover"><i class="icon-check2"></i> Lengkap</button>
											</li>
										</ul>
										@endif
										@if($jabatan !== "Kepala Seksi Kesiapsiagaan")
										<ul class="list-inline validated">
											<li>
												<button index="" value="" class="btn btn-danger" name="tidak_valid" title="Tidak Valid" data-toggle="tooltip" data-placement="bottom" data-trigger="hover"><i class="icon-cross2"></i> Tidak Valid</button>
											</li>
											<li>
												<button index="" value="" class="btn btn-success" name="valid" title="Valid" data-toggle="tooltip" data-placement="bottom" data-trigger="hover"><i class="icon-check2"></i> Valid</button>
											</li>
										</ul>
										@endif
									</div>
									@endif
									<hr>
									<div class="text-sm-right">
										<button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
		    </div>
			<!-- Modal Delete -->
			<div class="modal fade text-xs-left" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
			@include('layouts._delete')
			</div>
			@endif
		</div>
	</div>
</div>
@endsection

@section('custom_js')
<!-- Datatables js  -->
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<!-- plugin pnotify -->
<script src="{{ asset('js/pnotify.min.js') }}"></script>
<!-- ArcGis js -->
<script src="https://js.arcgis.com/4.5/"></script>
<script type="text/javascript">
	$(document).ready(function(){
		@if($jabatan !== "Sub Bagian Umum dan Kepegawaian" && $jabatan !== "Sub Bagian Penanggulangan Bencana")
		$('.table-file').DataTable();

		var basePath = "{{ url('/') }}";
		$('.table-responsive').on('click', 'button[name="lihat"]', function(){
			var id = $(this).attr('value');
			var index = $(this).attr('index');
			$('#frame-map').html('');
			$.ajax({
                url: '{{ url("/fasilitas") }}'+'/'+id,
                type: 'GET',
                beforeSend: function () {
                    //
                    startLoader()
                },
                success: function(response){
                	$('span[name="nama"]').html("");
					$('span[name="alamat"]').html("");
					$('span[name="longitude"]').html("");
					$('span[name="latitude"]').html("");
					$('span[name="jenis"]').html("");
					$('span[name="gunung"]').html("");

					if(response.foto_fasilitas){
						$('img[name="foto"]').attr('src', "{{ asset('upload/fasilitas') }}"+'/'+response.foto_fasilitas);
						$('img[name="foto"]').attr('alt', response.nama_fasilitas);
					}
					else{
						$('img[name="foto"]').attr('src', "{{ asset('img/noimagefound.png') }}");
						$('img[name="foto"]').attr('alt', 'No Image');
					}

                	$('img[name="foto"]').html(response.foto_fasilitas);
                	$('span[name="nama"]').html(response.nama_fasilitas);
					$('span[name="alamat"]').html(response.alamat_fasilitas);
					$('span[name="longitude"]').html(response.longitude);
					$('span[name="latitude"]').html(response.latitude);
					$('span[name="jenis"]').html(response.jenis_fasilitas);
					$('span[name="gunung"]').html(response.gunung);
					switch (response.verifikasi) {
						case "Pending":
							$('.verified').show();
							$('.validated').hide();
							$('button[name="tidak_lengkap"]').val(response.lower_name);
							$('button[name="tidak_lengkap"]').attr("index", index);
							$('button[name="lengkap"]').val(response.lower_name);
							$('button[name="lengkap"]').attr("index", index);
							break;
						case "Tidak Lengkap":
							$('.verified').hide();
							$('.validated').hide();
							break;
					
						default:
							$('.verified').hide();
							if(response.validasi != "Pending"){ $('.validated').hide(); }
							else
							{
								$('.validated').show();
								$('button[name="tidak_valid"]').val(response.lower_name);
								$('button[name="tidak_valid"]').attr("index", index);
								$('button[name="valid"]').val(response.lower_name);
								$('button[name="valid"]').attr("index", index);
							};
							break;
					}
					data = {
						center: [
							parseFloat(response.longitude),
							parseFloat(response.latitude),
						],
						nama: response.nama_fasilitas,
						alamat: response.alamat_fasilitas,
						longitude: parseFloat(response.longitude),
						latitude: parseFloat(response.latitude),
						jenis_fasilitas: response.jenis_fasilitas,
						gunung: response.gunung,
						path_icon: basePath + "/img/icon" +"/" + response.jenis_fasilitas +".png",
					}
					$.ajax({
						url: 'https://js.arcgis.com/4.5',
						type: 'GET',
						success: function() {
							loadPointMap(data);
						},
						error: function() {
							endLoader("no connection");
						}
					});
					endLoader("lihat");
                },
                error: function (error) {
                	endLoader("alert");
                }
            });
		});
		@if($jabatan !== "Pengolah Data Pra Bencana")
		@if($jabatan !== "Kepala Bidang Pencegahan dan Kesiapsiagaan")
		$('#modal-lihat').on('click', 'button[name="lengkap"]', function(){
			var index = $(this).attr('index');
        	$.ajaxSetup({
                headers:{
                    'X-CSRF-TOKEN': $('input[name="_token"]').val()
                }
            });
            $.ajax({
                url: "{{ route('verified_fasilitas') }}",
                type: 'PATCH',
                data: {'name': $(this).attr('value')},
                beforeSend: function () {
                    startLoader()
                },
                success: function(response){
					status = response.data.verifikasi;
					if(status == "Lengkap")
					{
                		$('span[name="status-verifikasi-'+index+'"]').attr('class', 'tag tag-success');
					}
                	$('span[name="status-verifikasi-'+index+'"]').html(status);
					$('#modal-lihat').modal('hide');
                	endLoader();
                },
                error: function () {
                	endLoader("alert");
                }
            });
        });
		$('#modal-lihat').on('click', 'button[name="tidak_lengkap"]', function(){
			var index = $(this).attr('index');
        	$.ajaxSetup({
                headers:{
                    'X-CSRF-TOKEN': $('input[name="_token"]').val()
                }
            });
            $.ajax({
                url: "{{ route('unverified_fasilitas') }}",
                type: 'PATCH',
                data: {'name': $(this).attr('value')},
                beforeSend: function () {
                    startLoader()
                },
                success: function(response){
                	status = response.data.verifikasi;
					if(status == "Tidak Lengkap")
					{
                		$('span[name="status-verifikasi-'+index+'"]').attr('class', 'tag tag-danger');
					}
                	$('span[name="status-verifikasi-'+index+'"]').html(status);
					$('#modal-lihat').modal('hide');
                	endLoader();
                },
                error: function () {
                	endLoader("alert");
                }
            });
        });
		@endif
		@if($jabatan !== "Kepala Seksi Kesiapsiagaan")
		$('#modal-lihat').on('click', 'button[name="valid"]', function(){
			var index = $(this).attr('index');
        	$.ajaxSetup({
                headers:{
                    'X-CSRF-TOKEN': $('input[name="_token"]').val()
                }
            });
            $.ajax({
                url: "{{ route('validated_fasilitas') }}",
                type: 'PATCH',
                data: {'name': $(this).attr('value')},
                beforeSend: function () {
                    startLoader()
                },
                success: function(response){
					status = response.data.validasi;
					if(status == "Valid")
					{
                		$('span[name="status-validasi-'+index+'"]').attr('class', 'tag tag-success');
					}
                	$('span[name="status-validasi-'+index+'"]').html(status);
					$('#modal-lihat').modal('hide');
                	endLoader();
                },
                error: function () {
                	endLoader("alert");
                }
            });
        });
		$('#modal-lihat').on('click', 'button[name="tidak_valid"]', function(){
			var index = $(this).attr('index');
        	$.ajaxSetup({
                headers:{
                    'X-CSRF-TOKEN': $('input[name="_token"]').val()
                }
            });
            $.ajax({
                url: "{{ route('unvalidated_fasilitas') }}",
                type: 'PATCH',
                data: {'name': $(this).attr('value')},
                beforeSend: function () {
                    startLoader()
                },
                success: function(response){
                	status = response.data.validasi;
					if(status == "Tidak Valid")
					{
                		$('span[name="status-validasi-'+index+'"]').attr('class', 'tag tag-danger');
					}
                	$('span[name="status-validasi-'+index+'"]').html(status);
					$('#modal-lihat').modal('hide');
                	endLoader();
                },
                error: function () {
                	endLoader("alert");
                }
            });
        });
		@endif
		@endif
		$('.table-responsive').on('click', 'button[name="hapus"]', function(){
			var id = $(this).attr('value');

	        modal_delete("{{ url('/fasilitas') }}" +'/'+id);
		});
		@endif

		if("{{ Session::has('success_message') }}"){
			successMessage("{{ session('success_message') }}");
		}
		if("{{ Session::has('error_message') }}"){
			errorMessage("{{ session('error_message') }}");
		}
	});
</script>
@endsection
