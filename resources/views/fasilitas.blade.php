@extends('layouts._app_frontend')
@section('custom_css')
<style>
    a.btn-transparent {
        color: #7c858e;
        font-size: 15px;
        padding: 6px 14px;
        white-space: nowrap;
        text-decoration: none;
        border: solid 1px #7c858e;
        background: none;
    }
    .list-unstyled span {
        font-size: 14px !important;
    }
</style>
@endsection
@section('content')
<div class="container">
    <ul class="breadcrumb">
        <li><a href="{{ url('/') }}">Beranda</a></li>
        <li class="active">Fasilitas</li>
    </ul>
    <div class="row margin-bottom-40">
        <div class="col-md-12 col-sm-12">
            <div class="content-page">
                <div class="row">
                    <div class="col-md-9 col-sm-9 blog-posts">
                        <?php
                            $index = 0;
                            $total = count($list_fasilitas);
                            $param = null;
                            $string = null;
                            if(isset($_GET['kategori'])) {
                                $param=$_GET['kategori'];
                                $string = '&kategori='. $param;
                            }
                        ?>
                        @foreach($list_fasilitas as $fasilitas)
                        <?php 
                            $lower_name = strtolower(str_replace(" ", "+", $fasilitas->nama_fasilitas));
                            $index++;
                        ?>
                        <div class="row">
                            <div class="col-md-3">
                                <?php
                                    if($fasilitas->foto_fasilitas)
                                        $url_foto = asset('upload/fasilitas') .'/'. $fasilitas->foto_fasilitas;
                                    else
                                        $url_foto = asset('img') .'/'. 'noimagefound.png';
                                ?>
                                <img alt="{{ strtolower($fasilitas->nama_fasilitas) }}" src="{{ $url_foto }}" class="img-thumbnail">
                            </div>
                            <div class="col-md-9">
                                <ul class="list-unstyled">
                                    <li class="margin-bottom-15">
                                        <h3 class="text-capitalize">{{ strtolower($fasilitas->nama_fasilitas) }}</h3>
                                    </li>
                                    <li>
                                        <i class="fa fa-building"></i>&nbsp;
                                        <span>{{ $fasilitas->jenis_fasilitas }}</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-map-marker"></i>&nbsp;
                                        <span class="text-capitalize">{{ strtolower($fasilitas->alamat_fasilitas) }}</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-tag"></i>&nbsp;
                                        <span class="text-capitalize">{{ strtolower($fasilitas->gunung) }}</span>
                                    </li>
                                </ul>
                                <a class="btn-transparent" href="{{ url('/fasilitas-publik') .'/'.$lower_name }}" class="more">Lihat <i class="icon-angle-right"></i></a>
                            </div>
                        </div>
                        @if($index !== $total)
                        <hr class="blog-post-sep" style="margin:14px">
                        @endif
                        @endforeach
                        <nav class="text-center" style="margin-top: 25px">
                            <ul class="pagination">
                                <li class="@if($list_fasilitas->previousPageUrl() == null) disabled @endif">
                                    <a href="@if($list_fasilitas->previousPageUrl() == null) javascript:void(0); @else {{ $list_fasilitas->previousPageUrl() .$string }} @endif">
                                        <i class="fa fa-chevron-left"></i>
                                    </a>
                                </li>
                                @for($i = 1; $i <= ($list_fasilitas->lastPage()); $i++)
                                <li class="@if($list_fasilitas->currentPage() == $i) active @endif"><a href="@if($list_fasilitas->currentPage() == $i) javascript:void(0); @else {{ $list_fasilitas->url($i) .$string }} @endif">{{ $i }}</a></li>
                                @endfor
                                <li class="@if($list_fasilitas->nextPageUrl() == null) disabled @endif">
                                    <a href="@if($list_fasilitas->nextPageUrl() == null) javascript:void(0); @else {{ $list_fasilitas->nextPageUrl() .$string }} @endif">
                                        <i class="fa fa-chevron-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <div class="col-md-3 col-sm-3 blog-sidebar">
                        <h2 class="no-top-space">Kategori</h2>
                        <ul class="nav sidebar-categories margin-bottom-40">
                            <li class="@if(!$param) active @endif"><a href="{{ url('fasilitas-publik') }}">Semua ({{ $jumlah }})</a></li>
                            @foreach($kategori as $jenis_fasilitas)
                            @if($jenis_fasilitas['total'] !== 0)
                            <li class="@if($param == $jenis_fasilitas['alt']) active @endif">
                                <a href="{{ url('/fasilitas-publik?kategori') .'='.$jenis_fasilitas['alt'] }}">{{ $jenis_fasilitas['jenis_fasilitas'] .' ('. $jenis_fasilitas['total'] .')' }}</a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                        @php
                        $url = url()->full();
                        $download = url('fasilitas').'/download';
                        $explode_url = explode("?", $url);
                        if(count($explode_url) > 1) $download = url('fasilitas').'/download?'.$explode_url[1];
                        @endphp
                        <a class="btn btn-primary" href="{{ $download }}" target="_blank" style="color:white">Unduh Data</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('custom_js')
<script src="{{ asset('assets/corporate/scripts/layout.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        Layout.init();    
    });
</script>
@endsection