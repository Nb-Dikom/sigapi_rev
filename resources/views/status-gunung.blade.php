@extends('layouts._app_frontend')
@section('custom_css')
<style>
    td {
        padding-top: 5px;
        width: 30%;
    }
</style>
@endsection
@section('content')
<div class="container">
    <ul class="breadcrumb">
        <li><a href="{{ url('/') }}">Beranda</a></li>
        <li class="active">Status Gunung</li>
    </ul>
    <div class="row margin-bottom-40">
        <div class="col-md-12 col-sm-12">
            <div class="content-page">
                <div class="row">
                    <?php
                        $param = null;
                        if(isset($_GET['status'])) {
                            $param=$_GET['status'];
                        }
                    ?>
                    <div class="col-md-9 col-sm-9 blog-posts">
                        <div class="margin-bottom-5 @if($param) @if($param == 'awas') @else hidden  @endif @endif">
                            <h5 style="color:red"><b>AWAS</b></h5>
                            <table style="width:100%">
                                <tbody>
                                    <?php $total_awas=0; ?>
                                    @foreach($list_status_gunung as $status_gunung)
                                    @if($status_gunung->status == "AWAS")
                                    <?php $lower_name = strtolower($status_gunung->id_status_gunung . "+" .$status_gunung->id_status)?>
                                    <tr>
                                        <td><b><a href="{{ url('status-gunung-api') .'/'. $lower_name }}">{{ $status_gunung->gunung }}</a></b></td>
                                        <td><span>{{ date('D, d M Y', strtotime($status_gunung->tanggal_status_gunung)) }}</span></td>
                                    </tr>
                                    <?php $total_awas++; ?>
                                    @endif
                                    @endforeach
                                </tbody>
                            </table><hr style="margin-top:10px; margin-bottom:15px">
                        </div>
                        <div class="margin-bottom-5 @if($param) @if($param == 'siaga') @else hidden  @endif @endif">
                            <h5 style="color:orange"><b>SIAGA</b></h5>
                            <table style="width:100%">
                                <tbody>
                                    <?php $total_siaga=0; ?>
                                    @foreach($list_status_gunung as $status_gunung)
                                    @if($status_gunung->status == "SIAGA")
                                    <?php $lower_name = strtolower($status_gunung->id_status_gunung . "+" .$status_gunung->id_status)?>
                                    <tr>
                                        <td><b><a href="{{ url('status-gunung-api') .'/'. $lower_name }}">{{ $status_gunung->gunung }}</a></b></td>
                                        <td><span>{{ date('D, d M Y', strtotime($status_gunung->tanggal_status_gunung)) }}</span></td>
                                    </tr>
                                    <?php $total_siaga++; ?>
                                    @endif
                                    @endforeach
                                </tbody>
                            </table><hr style="margin-top:10px; margin-bottom:15px">
                        </div>
                        <div class="@if($param) @if($param == 'waspada') @else hidden  @endif @endif">
                            <h5 style="color:gold"><b>WASPADA</b></h5>
                            <table style="width:100%">
                                <tbody>
                                    <?php $total_waspada=0; ?>
                                    @foreach($list_status_gunung as $status_gunung)
                                    @if($status_gunung->status == "WASPADA")
                                    <?php $lower_name = strtolower($status_gunung->id_status_gunung . "+" .$status_gunung->id_status)?>
                                    <tr>
                                        <td><b><a href="{{ url('status-gunung-api') .'/'. $lower_name }}">{{ $status_gunung->gunung }}</a></b></td>
                                        <td><span>{{ date('D, d M Y', strtotime($status_gunung->tanggal_status_gunung)) }}</span></td>
                                    </tr>
                                    <?php $total_waspada++; ?>
                                    @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 blog-sidebar">
                        <h2 class="no-top-space">Status Gunung</h2>
                        <ul class="nav sidebar-categories margin-bottom-40">
                            <li class="@if(!$param) active @endif"><a href="{{ url('status-gunung-api') }}">Semua ({{ $jumlah }})</a></li>
                            <li class="@if($param == 'awas') active @endif"><a href="{{ url('status-gunung-api') .'?status=awas' }}">Awas ({{$total_awas}})</a></li>
                            <li class="@if($param == 'siaga') active @endif"><a href="{{ url('status-gunung-api') .'?status=siaga' }}">Siaga ({{$total_siaga}})</a></li>
                            <li class="@if($param == 'waspada') active @endif"><a href="{{ url('status-gunung-api') .'?status=waspada' }}">Waspada ({{$total_waspada}})</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('custom_js')
<script src="{{ asset('assets/corporate/scripts/layout.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        Layout.init();    
    });
</script>
@endsection