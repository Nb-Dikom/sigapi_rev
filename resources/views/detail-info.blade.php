@extends('layouts._app_frontend')
@section('custom_css')

@endsection
@section('content')
<div class="container">
    <ul class="breadcrumb">
        <li><a href="{{ url('/') }}">Beranda</a></li>
        <li><a href="{{ url('/info-kebencanaan') }}">Info Kebencanaan</a></li>
        <li class="active">{{ $info->nama_info  }}</li>
    </ul>
    <div class="row margin-bottom-40">
        <div class="col-md-12 col-sm-12">
            <div class="content-page">
                <div class="row">
                        <?php
                            $index = 0;
                            $param = null;
                            $string = null;
                            if(isset($_GET['tahun'])) {
                                $param=$_GET['tahun'];
                                $string = '&tahun='. $param;
                            }
                        ?>
                    <div class="col-md-9 col-sm-9 blog-item">
                        <h2>{{ $info->nama_info }}</h2>
                        <section>{!! $info->konten !!}</section>
                        <ul class="blog-info">
                            <li><i class="fa fa-user"></i> By: {{ $info->nama_pegawai }}</li>
                            <li><i class="fa fa-calendar"></i> {{ date('D, d M Y', strtotime($info->tanggal_info)) }}</li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-3 blog-sidebar">
                        <h2 class="no-top-space">Tahun</h2>
                        <ul class="nav sidebar-categories margin-bottom-40">
                            <li class="@if(!$param) active @endif"><a href="{{ url('info-kebencanaan') }}">Semua ({{ $jumlah }})</a></li>
                            @foreach($list_tahun as $tahun)
                            <li class="@if($param == $tahun['tahun']) active @endif"><a href="{{ url('info-kebencanaan') .'?tahun='. $tahun['tahun'] }}">{{ $tahun["tahun"] .' ('. $tahun["total"].')' }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('custom_js')
<script src="{{ asset('assets/corporate/scripts/layout.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        Layout.init();    
    });
</script>
@endsection