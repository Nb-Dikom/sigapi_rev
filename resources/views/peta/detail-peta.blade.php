@extends('layouts._app')
@section('custom_css')
<!-- plugin notify -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/pnotify.min.css') }}">

<!-- ArcGis css -->
<link rel="stylesheet" href="https://js.arcgis.com/4.6/esri/css/main.css">
@endsection

@section('content')
<div class="app-content content container-fluid">
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-xs-12 mb-1">
				<h2 class="content-header-title">Peta</h2>
			</div>
			<div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
				<div class="breadcrumb-wrapper col-xs-12">
					<ol class="breadcrumb">
						<li class="breadcrumb-item active">Peta</li>
					</ol>
				</div>
			</div>
		</div>
		<div class="content-body">
			<section>
				<div class="row">
					<div class="col-xs-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">Peta</h4>
							</div>
							<div class="card-block">
								<div id="frame-map" style="height: 580px; width:100%"></div>
								<span id="info" name="info" style="position:absolute; right:25px; bottom:25px; color:#000; z-index:50;"></span>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection

@section('custom_js')
<!-- plugin pnotify -->
<script src="{{ asset('js/pnotify.min.js') }}"></script>
<!-- ArcGis js -->
<script src="https://js.arcgis.com/4.6/"></script>
<script type="text/javascript">
    $(document).ready(function(){
		if("{{ Session::has('success_message') }}"){
			successMessage("{{ session('success_message') }}");
		}
		if("{{ Session::has('error_message') }}"){
			errorMessage("{{ session('error_message') }}");
		}

		$.ajax({
			url: 'https://js.arcgis.com/4.6',
			type: 'GET',
			success: function() {
				$.ajax({
					url: "{{ $url }}",
					type: "GET",

					success: function(result){
						var features_point = [];
						var features_geometry = [];
						$.each(result.data['point'], function(key, value){
							features_point[key] = value;
						});
						$.each(result.data['geometry'], function(key, value){
							features_geometry[value.jenis_krb] = value;
						});
						loadMap(features_point, "{{ $type }}", features_geometry);
					},
					error: function (xhr, message) {
						errorMessage(message);
					}
				});
			},
			error: function(xhr, message) {
				errorMessage("No Connection");
			}
		});
    });
</script>
<script>
	var basePath = "{{ asset('/img/icon') .'/' }}"
    function loadMap(point, type, geometry){
        require([
		"esri/Map",
		"esri/views/MapView",
		"esri/Graphic",
		"esri/layers/FeatureLayer",
		"esri/widgets/Legend",
		"esri/widgets/ScaleBar",
        "esri/renderers/UniqueValueRenderer",
		"esri/geometry/support/webMercatorUtils",
		"esri/layers/GroupLayer",
		"esri/widgets/LayerList",
		"esri/widgets/BasemapGallery",
		"esri/widgets/Home",
		"esri/widgets/Compass",
		"esri/widgets/Expand",
		"esri/widgets/Directions",
		"dojo/dom",
		"dojo/domReady!"
		], function(
			Map, MapView, Graphic, FeatureLayer, Legend, ScaleBar, UniqueValueRenderer, 
			webMercatorUtils, GroupLayer, LayerList, BasemapGallery, Home, Compass,
			Expand, 
			Directions,
			 dom) {
			var indexRenderer = new UniqueValueRenderer({
                field: "jenis_fasilitas",
                uniqueValueInfos: [{
                    value: "Kantor Polisi",
                    symbol: {
						type: "picture-marker",
						url: basePath + 'Kantor Polisi.png',
						width: "24px",
						height: "24px"
					}
                },{
                    value: "Kantor Pemadam Kebakaran",
                    symbol: {
						type: "picture-marker",
						url: basePath + 'Kantor Pemadam Kebakaran.png',
						width: "24px",
						height: "24px"
					}
                },{
                    value: "Kantor Pemerintahan",
                    symbol: {
						type: "picture-marker",
						url: basePath + 'Kantor Pemerintahan.png',
						width: "24px",
						height: "24px"
					}
                },{
                    value: "Pertokoan",
                    symbol: {
						type: "picture-marker",
						url: basePath + 'Pertokoan.png',
						width: "24px",
						height: "24px"
					}
                },{
                    value: "Pusat Perbelanjaan",
                    symbol: {
						type: "picture-marker",
						url: basePath + 'Pusat Perbelanjaan.png',
						width: "24px",
						height: "24px"
					}
                },{
                    value: "Pasar Tradisional",
                    symbol: {
						type: "picture-marker",
						url: basePath + 'Pasar Tradisional.png',
						width: "24px",
						height: "24px"
					}
                },{
                    value: "Tempat Rekreasi (Buatan dan Alami di Pegunungan)",
                    symbol: {
						type: "picture-marker",
						url: basePath + 'Tempat Rekreasi (Buatan dan Alami di Pegunungan).png',
						width: "24px",
						height: "24px"
					}
                },{
                    value: "Terminal Bis",
                    symbol: {
						type: "picture-marker",
						url: basePath + 'Terminal Bis.png',
						width: "24px",
						height: "24px"
					}
                },{
                    value: "Stasiun Kereta Api",
                    symbol: {
						type: "picture-marker",
						url: basePath + 'Stasiun Kereta Api.png',
						width: "24px",
						height: "24px"
					}
                },{
                    value: "Posko (Rupusdalops)",
                    symbol: {
						type: "picture-marker",
						url: basePath + 'Posko (Rupusdalops).png',
						width: "24px",
						height: "24px"
					}
                },{
                    value: "Rumah Sakit",
                    symbol: {
						type: "picture-marker",
						url: basePath + 'Rumah Sakit.png',
						width: "24px",
						height: "24px"
					}
                },{
                    value: "Puskesmas",
                    symbol: {
						type: "picture-marker",
						url: basePath + 'Puskesmas.png',
						width: "24px",
						height: "24px"
					}
                }]
            });

			var fieldsPoint = [
				{
					name: "ObjectID",
					alias: "ObjectID",
					type: "oid"
				},
				{
					name: "nama_fasilitas",
					alias: "Nama",
					type: "string"
				},
				{
					name: "alamat_fasilitas",
					alias: "Alamat",
					type: "string"
				},
				{
					name: "jenis_fasilitas",
					alias: "Jenis",
					type: "string"
				},
				{
					name: "gunung",
					alias: "Gunung",
					type: "string"
				},
				{
					name: "longitude",
					alias: "Longitude",
					type: "string"
				},
				{
					name: "latitude",
					alias: "Latitude",
					type: "string"
				},
			];

			var templatePopupPoint = {
				title: "{nama_fasilitas}",
				content: [{
					type: "fields",
					fieldInfos: [{
						fieldName: "nama_fasilitas"
						}, {
						fieldName: "alamat_fasilitas"
						}, {
						fieldName: "jenis_fasilitas"
						}, {
						fieldName: "gunung"
					}],
				},{
					type: "text",
					text: '<a href="http://localhost/sigapi_rev/public/masyarakat/peta/rute?longitude='+'{longitude}'+'&latitude='+'{latitude}'+'">Dapatkan Rute</a>'
				}]
			}

			var layersPoint = [];
			if(point['JF-01']){
				var polisiLayer = new FeatureLayer({
					fields: fieldsPoint,
					objectIdField: "ObjectID",
					geometryType: "point",
					spatialReference: { wkid: 4326 },
					source: point['JF-01'],
					renderer: indexRenderer,
					// renderer: {
					// 	type: "simple",
					// 	symbol: {
                    //         type: "picture-marker",
                    //         url: basePath + 'Kantor Polisi.png',
                    //         width: "24px",
                    //         height: "24px"
                    //     }
					// },
                    visible: false,
					title: 'Kantor Polisi',
					popupTemplate: templatePopupPoint
				});
				layersPoint.push(polisiLayer);
			}

			if(point['JF-02']){
				var damkarLayer = new FeatureLayer({
					fields: fieldsPoint,
					objectIdField: "ObjectID",
					geometryType: "point",
					spatialReference: { wkid: 4326 },
					source: point['JF-02'],
					renderer: indexRenderer,
					visible: false,
					title: 'Kantor Pemadam Kebakaran',
					popupTemplate: templatePopupPoint
				});
				layersPoint.push(damkarLayer);
			}

			if(point['JF-03']){
				var pemerintahanLayer = new FeatureLayer({
					fields: fieldsPoint,
					objectIdField: "ObjectID",
					geometryType: "point",
					spatialReference: { wkid: 4326 },
					source: point['JF-03'],
					renderer: indexRenderer,
					visible: false,
					title: 'Kantor Pemerintahan',
					popupTemplate: templatePopupPoint
				});
				layersPoint.push(pemerintahanLayer);
			}
			
			if(point['JF-04']){
				var situsLayer = new FeatureLayer({
					fields: fieldsPoint,
					objectIdField: "ObjectID",
					geometryType: "point",
					spatialReference: { wkid: 4326 },
					source: point['JF-04'],
					renderer: indexRenderer,
					visible: false,
					title: 'Pertokoan',
					popupTemplate: templatePopupPoint
				});
				layersPoint.push(situsLayer);
			}

			if(point['JF-05']){
				var perbelanjaanLayer = new FeatureLayer({
					fields: fieldsPoint,
					objectIdField: "ObjectID",
					geometryType: "point",
					spatialReference: { wkid: 4326 },
					source: point['JF-05'],
					renderer: indexRenderer,
					visible: false,
					title: 'Pusat Perbelanjaan',
					popupTemplate: templatePopupPoint
				});
				layersPoint.push(perbelanjaanLayer);
			}

			if(point['JF-06']){
				var tradisionalLayer = new FeatureLayer({
					fields: fieldsPoint,
					objectIdField: "ObjectID",
					geometryType: "point",
					spatialReference: { wkid: 4326 },
					source: point['JF-06'],
					renderer: indexRenderer,
					visible: false,
					title: 'Pasar Tradisional',
					popupTemplate: templatePopupPoint
				});
				layersPoint.push(tradisionalLayer);
			}

			if(point['JF-07']){
				var rekreasiLayer = new FeatureLayer({
					fields: fieldsPoint,
					objectIdField: "ObjectID",
					geometryType: "point",
					spatialReference: { wkid: 4326 },
					source: point['JF-07'],
					renderer: indexRenderer,
					visible: false,
					title: 'Tempat Rekreasi (Buatan dan Alami di Pegunungan)',
					popupTemplate: templatePopupPoint
				});
				layersPoint.push(rekreasiLayer);
			}

			if(point['JF-08']){
				var terminalLayer = new FeatureLayer({
					fields: fieldsPoint,
					objectIdField: "ObjectID",
					geometryType: "point",
					spatialReference: { wkid: 4326 },
					source: point['JF-08'],
					renderer: indexRenderer,
					visible: false,
					title: 'Terminal Bis',
					popupTemplate: templatePopupPoint
				});
				layersPoint.push(terminalLayer);
			}

			if(point['JF-09']){
				var stasiunLayer = new FeatureLayer({
					fields: fieldsPoint,
					objectIdField: "ObjectID",
					geometryType: "point",
					spatialReference: { wkid: 4326 },
					source: point['JF-09'],
					renderer: indexRenderer,
					visible: false,
					title: 'Stasiun Kereta Api',
					popupTemplate: templatePopupPoint
				});
				layersPoint.push(stasiunLayer);
			}

			if(point['JF-10']){
				var poskoLayer = new FeatureLayer({
					fields: fieldsPoint,
					objectIdField: "ObjectID",
					geometryType: "point",
					spatialReference: { wkid: 4326 },
					source: point['JF-10'],
					renderer: indexRenderer,
					visible: false,
					title: 'Posko (Rupusdalops)',
					popupTemplate: templatePopupPoint
				});
				layersPoint.push(poskoLayer);
			}
			
			if(point['JF-11']){
				var rumkitLayer = new FeatureLayer({
					fields: fieldsPoint,
					objectIdField: "ObjectID",
					geometryType: "point",
					spatialReference: { wkid: 4326 },
					source: point['JF-11'],
					renderer: indexRenderer,
					visible: false,
					title: 'Rumah Sakit',
					popupTemplate: templatePopupPoint
				});
				layersPoint.push(rumkitLayer);
			}
			
			if(point['JF-12']){
				var puskesmasLayer = new FeatureLayer({
					fields: fieldsPoint,
					objectIdField: "ObjectID",
					geometryType: "point",
					spatialReference: { wkid: 4326 },
					source: point['JF-12'],
					renderer: indexRenderer,
					title: 'Puskesmas',
					popupTemplate: templatePopupPoint
				});
				layersPoint.push(puskesmasLayer);
			}

			var pointGroupLayer = new GroupLayer({
				title: "Fasilitas",
				visible: false,
				layers: layersPoint
			});

			if(type == "krb"){
				var layersPolygon = [];
				var fieldsPolygon = [
					{
						name: "ObjectID",
						alias: "ObjectID",
						type: "oid"
					},
					{
						name: "jenis_rawan",
						alias: "Jenis",
						type: "string"
					},
					{
						name: "gunung",
						alias: "Gunung",
						type: "string"
					},
				];

				var templatePopupPolygon = {
					title: "{jenis_rawan}",
					content: [{
						type: "fields",
						fieldInfos: [{
							fieldName: "jenis_rawan"
							}, {
							fieldName: "gunung"
						}]
					}]
				}
				// if(polygon['rawan_i']){
				if(geometry['Rawan I']){
					var layerRawanI = new FeatureLayer({
						// fields: fieldsPolygon,
						// objectIdField: "ObjectID",
						// geometryType: "polygon",
						// spatialReference: { wkid: 4326 },
						// source: polygon['rawan_i'],
						url: geometry['Rawan I'].file_krb,
						renderer: {
							type: "simple",
							symbol: {
								type: "simple-fill",
								color: "yellow",
								outline: {
									color: [128, 128, 128, 0.5],
									width: "0.5px"
								}
							}
						},
						opacity: 0.6,
						title: 'Rawan I',
						// popupTemplate: templatePopupPolygon
					});
					layersPolygon.push(layerRawanI);
				}

				// if(polygon['rawan_ii']){
				if(geometry['Rawan II']){
					var layerRawanII = new FeatureLayer({
						url: geometry['Rawan II'].file_krb,
						renderer: {
							type: "simple",
							symbol: {
								type: "simple-fill",
								color: "orange",
								outline: {
									color: [128, 128, 128, 0.5],
									width: "0.5px"
								}
							}
						},
						opacity: 0.6,
						title: 'Rawan II',
						// popupTemplate: templatePopupPolygon
					});
					layersPolygon.push(layerRawanII);
				}

				// if(polygon['rawan_iii']){
				if(geometry['Rawan III']){
					var layerRawanIII = new FeatureLayer({
						url: geometry['Rawan III'].file_krb,
						renderer: {
							type: "simple",
							symbol: {
								type: "simple-fill",
								color: "red",
								outline: {
									color: [128, 128, 128, 0.5],
									width: "0.5px"
								}
							}
						},
						opacity: 0.6,
						title: 'Rawan III',
						// popupTemplate: templatePopupPolygon
					});
					layersPolygon.push(layerRawanIII);
				}

				var polygonGroupLayer = new GroupLayer({
					title: "Daerah Rawan",
					visible: true,
					layers: layersPolygon
				});

				var layerContent = [polygonGroupLayer, pointGroupLayer];
			}
			
			var map = new Map({
				// basemap: "streets",
				basemap: "hybrid",
				layers: layerContent
			});
			// map.add(puskesmasLayer);
			var view = new MapView({
				center: [113.0002131, -8.1569649],
				container: "frame-map",
				map: map,
				zoom: 10,
				popup: {
					dockEnabled: true,
					dockOptions: {
						buttonEnabled: false,
						breakpoint: false,
						position: "top-left"
					}
				}
			});
			var home = new Home({
				view: view
			});
			var compass = new Compass({
				view: view
			});
			var scaleBar = new ScaleBar({
				view: view,
				unit: "metric"
			});
			var legend = new Legend({
				view: view,
			});
			
			var layerList = new LayerList({
				view: view,
				container: document.createElement("div")
			});
			var basemapGallery = new BasemapGallery({
				view: view,
				container: document.createElement("div")
			});
			var directions = new Directions({
				view: view,
				routeServiceUrl: "http://utility.arcgis.com/usrsvcs/servers/4ea1d05bc5314c37a3f253164797023d/rest/services/World/Route/NAServer/Route_World",
				container: document.createElement("div")
			});

			view.ui.add(legend, "bottom-right");
			view.ui.add(scaleBar, {
				position: "bottom-left"
			});
			view.ui.add(home, "top-left");
			view.ui.add(compass, "top-left");
			
			var layerListExpand = new Expand({
				view: view,
				content: layerList.domNode,
				expandIconClass: "esri-icon-layers"
			});
			var directionsExpand = new Expand({
				view: view,
				content: directions.domNode,
				expandIconClass: "esri-icon-directions"
			});
			var baseMapExpand = new Expand({
				view: view,
				content: basemapGallery.domNode,
				expandIconClass: "esri-icon-basemap"
			});
			var legendExpand = new Expand({
				view: view,
				content: legend.domNode,
				expandIconClass: "esri-icon-collection"
			});

			view.ui.add(layerListExpand, "top-right");
			view.ui.add(directionsExpand, "top-right");
			view.ui.add(baseMapExpand, "top-right");
			view.ui.add(legendExpand, "top-right");
			view.on("pointer-move", function(evt){
				var point = view.toMap({
					x: evt.x,
					y: evt.y
				});
				var longitude = point.longitude;
				var latitude = point.latitude;
				dom.byId("info").innerHTML = longitude.toString().substr(0,8) + ", " + latitude.toString().substr(0,8);
			});
		});
    }
</script>
@endsection
