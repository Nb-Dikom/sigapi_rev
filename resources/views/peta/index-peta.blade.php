@extends('layouts._app')
@section('content')
<div class="app-content content container-fluid">
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-xs-12 mb-1">
				<h2 class="content-header-title">Peta</h2>
			</div>
			<div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
				<div class="breadcrumb-wrapper col-xs-12">
					<ol class="breadcrumb">
						<li class="breadcrumb-item active">Peta</li>
					</ol>
				</div>
			</div>
		</div>
		<div class="content-body">
			<section>
				<div class="row">
					<div class="col-xs-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">Peta</h4>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-12">
						<div class="card bg-deep-orange">
							<div class="card-body">
								<div class="card-block">
									<div class="media">
										<div class="media-body white text-xs-left">
											<h3>Kawasan</h3>
											<h3>Rawan Bencana</h3>
											<a name="lihat" id="lihat" class="btn btn-secondary" href="{{ route('view_map', 'krb') }}" role="button">Lihat</a>
										</div>
										<div class="media-right media-middle">
											<i class="icon-stack white font-large-2 float-xs-right"></i>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-12">
						<div class="card bg-cyan">
							<div class="card-body">
								<div class="card-block">
									<div class="media">
										<div class="media-body white text-xs-left">
											<h3>Jalur</h3>
											<h3>Evakuasi</h3>
											<a name="lihat" id="lihat" class="btn btn-secondary" href="{{ route('view_map', 'jalur-e
											vakuasi') }}" role="button">Lihat</a>
										</div>
										<div class="media-right media-middle">
											<i class="icon-android-walk white font-large-2 float-xs-right"></i>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection