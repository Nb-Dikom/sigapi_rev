@extends('layouts._app-masyarakat')
@section('content')
    <div class="col-lg-12 col-md-12 col-xs-12">
        <div class="mt-2 mb-2">
            <div class="row">
                <div class="col-md-10 offset-md-1 col-xs-10 offset-xs-1 box-shadow-2 p-0">
                    <div class="card border-grey border-lighten-3 m-0">
                        <div class="card-header no-border">
                            <h3 class="card-title line-on-side text-muted text-xs-center pt-1"><span><i class="icon-user"></i> Profil</span></h3>
                            <div class="text-xs-right mt-1">
                                <a href="{{ route('form_edit_organisasi', $organisasi->id_organisasi) }}" class="btn btn-success" title="Ubah Profil" data-toggle="tooltip" data-placement="bottom" data-trigger="hover"><i class="icon-user-check"></i> Ubah</a>
                            </div>
                        </div>
                        <div class="card-body mt-0">
                            <div class="card-block pt-0">
                                <div class="row">
                                    <div class="col-md-10 offset-md-1">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <!-- Info User -->
                                                <div class="mb-3">
                                                    <div class="text-xs-left">
                                                        <h4 class="form-section"><span><i class="icon-head"></i> Akun</span></h4>
                                                        <hr>
                                                    </div>
                                                    <table>
                                                        <tbody>
                                                            <tr>
                                                                <td><i class="icon-user-tie"></i> <span class="text-bold-600">Username</span></td>
                                                                <td class="pl-3 pr-1">:</td>
                                                                <td>{{ $organisasi->user->username }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td><i class="icon-lock"></i> <span class="text-bold-600">Kata Sandi</span></td>
                                                                <td class="pl-3 pr-1">:</td>
                                                                <td>*****</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div>
                                                    <!-- Info Organisasi -->
                                                    <div class="text-xs-right">
                                                        <!-- <h4 class="form-section"><span><i class="icon-paper"></i> Organisasi</span></h4> -->
                                                        <hr>
                                                    </div>
                                                    <table>
                                                        <tbody>
                                                            <tr>
                                                                <td><i class="icon-office"></i> <span class="text-bold-600">Organisasi</span></td>
                                                                <td class="pl-3 pr-1">:</td>
                                                                <td>{{ $organisasi->nama_organisasi }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td><i class="icon-location2"></i> <span class="text-bold-600">Alamat</span></td>
                                                                <td class="pl-3 pr-1">:</td>
                                                                <td>{{ $organisasi->alamat_organisasi }}<td>
                                                            </tr>
                                                            <tr>
                                                                <td><i class="icon-phone"></i> <span class="text-bold-600">No.Telp</span></td>
                                                                <td class="pl-3 pr-1">:</td>
                                                                <td>{{ $organisasi->no_telp_organisasi }}<td>
                                                            </tr>
                                                            <tr>
                                                                <td><i class="icon-telephone"></i> <span class="text-bold-600">Fax</span></td>
                                                                <td class="pl-3 pr-1">:</td>
                                                                <td>{{ $organisasi->fax }}<td>
                                                            </tr>
                                                            <tr>
                                                                <td><i class="icon-mobile-phone"></i> <span class="text-bold-600">Handphone</span></td>
                                                                <td class="pl-3 pr-1">:</td>
                                                                <td>{{ $organisasi->handphone_organisasi }}<td>
                                                            </tr>
                                                            <tr>
                                                                <td><i class="icon-email22"></i> <span class="text-bold-600">Kode Pos</span></td>
                                                                <td class="pl-3 pr-1">:</td>
                                                                <td>{{ $organisasi->kode_pos }}<td>
                                                            </tr>
                                                            <tr>
                                                                <td><i class="icon-email2"></i> <span class="text-bold-600">Email</span></td>
                                                                <td class="pl-3 pr-1">:</td>
                                                                <td>{{ $organisasi->email_organisasi }}<td>
                                                            </tr>
                                                            <tr>
                                                                <td><i class="icon-earth2"></i> <span class="text-bold-600">Web</span></td>
                                                                <td class="pl-3 pr-1">:</td>
                                                                <td>{{ $organisasi->web }}<td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="text-xs-left">
                                                    <h4 class="form-section"><span><i class="icon-image2"></i> Sertifikat Keterangan Terdaftar</span></h4>
                                                    <hr>
                                                </div>
                                                @if($organisasi->foto_sertifikat)
                                                <figure>
                                                    <img src="{{ asset('upload/sertifikat/'.$organisasi->foto_sertifikat) }}" class="img-thumbnail img-fluid">
                                                </figure>
                                                @else
                                                <figure>
                                                    <img src="{{ asset('img/noimagefound.png') }}" class="img-thumbnail img-fluid">
                                                </figure>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection