<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/logo_bnpb.png') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'SIGAPI_DEV') }}</title>

    <!-- Styles -->
    @include('layouts.partials._css')
</head>
<body data-open="click" data-menu="vertical-menu" data-col="1-column" class="vertical-layout vertical-menu 1-column  blank-page blank-page">
    <div class="app-content content container-fluid">
        <div class="content-wrapper">
            <div class="content-header row"></div>
            <div class="content-body">
                <section class=" mt-1">
                    <div class="col-md-8 offset-md-2 col-xs-10 offset-xs-1 box-shadow-2 p-0">
                        <div class="card border-grey border-lighten-3">
                            <div class="card-header no-border">
                                <div class="card-title text-xs-center">
                                    <div class="p-1"><img src="{{ asset('img/sigapi-logo.png') }}" alt="branding logo"></div>
                                </div>
                                <h6 class="card-subtitle line-on-side text-muted text-xs-center font-small-3 pt-2"><span>Sign Up with SIGAPI</span></h6>
                            </div>
                            <div class="card-body collapse in">
                                <div class="card-block">
                                    <form class="form-horizontal" method="POST" action="{{ route('save_organisasi') }}" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="col-md-10 offset-md-1">
                                            <div class="form-body">
                                                <div class="form-group {{ $errors->has('organisasi') ? 'has-error' : '' }}">
                                                    <label for="organisasi">Nama Organisasi</label>
                                                    <div class="position-relative has-icon-left">
                                                        <input type="text" id="organisasi" class="form-control square" placeholder="nama organisasi" name="organisasi" required="" value="{{ old('organisasi') }}" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Masukkan nama organisasi">
                                                        <div class="form-control-position">
                                                            <i class="icon-office"></i>
                                                        </div>
                                                    </div>
                                                    @if($errors->has('organisasi'))
                                                    <span class="help-block">
                                                        <strong class="pink">{{ $errors->first('organisasi') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group {{ $errors->has('alamat') ? 'has-error' : '' }}">
                                                                    <label for="alamat">Alamat</label>
                                                                    <div class="position-relative has-icon-left">
                                                                        <input type="text" id="alamat" class="form-control square" placeholder="alamat" name="alamat" required="" value="{{ old('alamat') }}" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Masukkan alamat dari organisasi">
                                                                        <div class="form-control-position">
                                                                            <i class="icon-location2"></i>
                                                                        </div>
                                                                    </div>
                                                                    @if($errors->has('alamat'))
                                                                    <span class="help-block">
                                                                        <strong class="pink">{{ $errors->first('alamat') }}</strong>
                                                                    </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group {{ $errors->has('file') ? 'has-error' : '' }}">
                                                                    <label>Unggah File SKT: </label>
                                                                    <label id="file" class="file center-block">
                                                                        <input type="file" id="file" name="file">
                                                                        <span class="file-custom"></span>
                                                                    </label>
                                                                    <div>
                                                                        @if($errors->has('file'))
                                                                        <span class="help-block">
                                                                            <strong class="pink">{{ $errors->first('file') }}</strong>
                                                                        </span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group {{ $errors->has('no_telp') ? 'has-error' : '' }}">
                                                                    <label for="no_telp">No.Telp</label>
                                                                    <div class="position-relative has-icon-left">
                                                                        <input type="text" id="no_telp" class="form-control square" placeholder="nomor telepon" name="no_telp" required="" value="{{ old('no_telp') }}" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Masukkan nomor telepon dari organisasi">
                                                                        <div class="form-control-position">
                                                                            <i class="icon-phone"></i>
                                                                        </div>
                                                                    </div>
                                                                    @if($errors->has('no_telp'))
                                                                    <span class="help-block">
                                                                        <strong class="pink">{{ $errors->first('no_telp') }}</strong>
                                                                    </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group {{ $errors->has('fax') ? 'has-error' : '' }}">
                                                                    <label for="fax">Fax</label>
                                                                    <div class="position-relative has-icon-left">
                                                                        <input type="text" id="fax" class="form-control square" placeholder="nomor fax" name="fax" value="{{ old('fax') }}" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Masukkan nomor fax dari organisasi">
                                                                        <div class="form-control-position">
                                                                            <i class="icon-telephone"></i>
                                                                        </div>
                                                                    </div>
                                                                    @if($errors->has('fax'))
                                                                    <span class="help-block">
                                                                        <strong class="pink">{{ $errors->first('fax') }}</strong>
                                                                    </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group {{ $errors->has('handphone') ? 'has-error' : '' }}">
                                                                    <label for="handphone">Handphone</label>
                                                                    <div class="position-relative has-icon-left">
                                                                        <input type="text" id="handphone" class="form-control square" placeholder="nomor handphone" name="handphone" value="{{ old('handphone') }}" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Masukkan nomor handphone dari organisasi">
                                                                        <div class="form-control-position">
                                                                            <i class="icon-mobile-phone"></i>
                                                                        </div>
                                                                    </div>
                                                                    @if($errors->has('handphone'))
                                                                    <span class="help-block">
                                                                        <strong class="pink">{{ $errors->first('handphone') }}</strong>
                                                                    </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group {{ $errors->has('kode_pos') ? 'has-error' : '' }}">
                                                                    <label for="kode_pos">Kode Pos</label>
                                                                    <div class="position-relative has-icon-left">
                                                                        <input type="text" id="kode_pos" class="form-control square" placeholder="kode pos" name="kode_pos" value="{{ old('kode_pos') }}" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Masukkan nomor kode pos dari organisasi">
                                                                        <div class="form-control-position">
                                                                            <i class="icon-email22"></i>
                                                                        </div>
                                                                    </div>
                                                                    @if($errors->has('kode_pos'))
                                                                    <span class="help-block">
                                                                        <strong class="pink">{{ $errors->first('kode_pos') }}</strong>
                                                                    </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                                                    <label for="email">Email</label>
                                                                    <div class="position-relative has-icon-left">
                                                                        <input type="text" id="email" class="form-control square" placeholder="email" name="email" required="" value="{{ old('email') }}" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Masukkan email dari organisasi">
                                                                        <div class="form-control-position">
                                                                            <i class="icon-email2"></i>
                                                                        </div>
                                                                    </div>
                                                                    @if($errors->has('email'))
                                                                    <span class="help-block">
                                                                        <strong class="pink">{{ $errors->first('email') }}</strong>
                                                                    </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group {{ $errors->has('web') ? 'has-error' : '' }}">
                                                                    <label for="web">Web</label>
                                                                    <div class="position-relative has-icon-left">
                                                                        <input type="text" id="web" class="form-control square" placeholder="web" name="web" value="{{ old('web') }}" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Masukkan alamat web dari organisasi">
                                                                        <div class="form-control-position">
                                                                            <i class="icon-earth2"></i>
                                                                        </div>
                                                                    </div>
                                                                    @if($errors->has('web'))
                                                                    <span class="help-block">
                                                                        <strong class="pink">{{ $errors->first('web') }}</strong>
                                                                    </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-primary btn-block"><i class="icon-floppy-disk"></i> Simpan</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="text-sm-center">
                                    <span>Sudah punya akun? <a href="{{ route('form_login') }}"><i>Sign In</i></a></span>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div>
                                    <p class="clearfix text-muted text-sm-center mb-0 px-2">
                                        <span>Copyright &copy; 2017 <span class="text-bold-800 grey darken-2">Diar Ichrom</span>, All rights reserved. </span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    

    <!-- Scripts -->
    @include('layouts.partials._javascript')
    <script type="text/javascript">
        $(document).ready(function(){
            $('input[name="no_telp"]').keydown(function (e) {
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                        (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                        (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                        (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                        (e.keyCode >= 35 && e.keyCode <= 39)) {
                    return;
                }
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
            $('input[name="fax"]').keydown(function (e) {
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                        (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                        (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                        (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                        (e.keyCode >= 35 && e.keyCode <= 39)) {
                    return;
                }
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
            $('input[name="handphone"]').keydown(function (e) {
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                        (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                        (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                        (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                        (e.keyCode >= 35 && e.keyCode <= 39)) {
                    return;
                }
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
            $('input[name="kode_pos"]').keydown(function (e) {
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                        (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                        (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                        (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                        (e.keyCode >= 35 && e.keyCode <= 39)) {
                    return;
                }
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });

            var idProv = $('select[name="provinsi"]').val();
            var idKab = $('select[name="kabupaten"]').val();
            if(idProv != null){
                $.ajax({
                    type: "GET",
                    url: "{{url('api/prov')}}/" + idProv,
                    timeout: 600000,
                    beforeSend: function() {
                        //
                        startLoader();
                    },
                    success: function(data) {
                        endLoader();
                        var string = '';
                        for(var i=0; i<data.length; i++){
                            string = string + '@if(old("kabupaten") == null)'+
                                '<option value="'+data[i].id_kabupaten+'">'+data[i].kabupaten+'</option>'+
                            '@else'+
                                '@if('+data[i].id_kabupaten+'== old("kabupaten"))'+
                                    '<option selected value="'+data[i].id_kabupaten+'">'+data[i].kabupaten+'</option>'+
                                '@else'+
                                    '<option value="'+data[i].id_kabupaten+'">'+data[i].kabupaten+'</option> @endif @endif'
                        }
                        $('select[name="kabupaten"]').html(
                            '<option value="0" @if(old('kabupaten') == null) selected="" @endif disabled="">Pilih Kabupaten/Kota</option>'+string
                        );
                    },
                    error: function() {
                        endLoader();
                        alert('something error, contact your dev');
                    }
                });
            }

            $('select[name="provinsi"]').on('change', function(){
				var id = $(this).val();
				if(id != 0){
					$.ajax({
		                type: "GET",
		                url: "{{url('api/prov')}}/" + id,
		                timeout: 600000,
		                beforeSend: function() {
		                	//
		                	startLoader();
		                },
		                success: function(data) {
		                	endLoader();
                            var string = '';
                            for(var i=0; i<data.length; i++){
                                string = string + '@if(old("kabupaten") == null)'+
                                    '<option value="'+data[i].id_kabupaten+'">'+data[i].kabupaten+'</option>'+
                                '@else'+
                                    '@if('+data[i].id_kabupaten+'== old("kabupaten"))'+
                                        '<option selected value="'+data[i].id_kabupaten+'">'+data[i].kabupaten+'</option>'+
                                    '@else'+
                                        '<option value="'+data[i].id_kabupaten+'">'+data[i].kabupaten+'</option> @endif @endif'
                            }
                            $('select[name="kabupaten"]').html(
                                '<option value="0" @if(old('kabupaten') == null) selected="" @endif disabled="">Pilih Kabupaten/Kota</option>'+string
                            );
		                },
		                error: function() {
		                	endLoader();
		                    alert('something error, contact your dev');
		                }
		            });
				}
				else{
					$('input[name="provinsi"]').attr('value', '');
				}
			});
        });
    </script>
    
</body>
</html>
