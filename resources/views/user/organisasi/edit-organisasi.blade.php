@extends('layouts._app')
@section('custom_css')
<!-- bootstrap3-wysihtml5 css -->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/bootstrap3-wysihtml5/dist/bootstrap3-wysihtml5.min.css') }}">
<!-- plugin notify -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/pnotify.min.css') }}">
@endsection

@section('content')
<div class="app-content content container-fluid">
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-xs-12 mb-1">
				<h2 class="content-header-title">Organisasi</h2>
			</div>
			<div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
				<div class="breadcrumb-wrapper col-xs-12">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{ url('/organisasi') }}">Organisasi</a></li>
						<li class="breadcrumb-item active"> Ubah Organisasi</li>
					</ol>
				</div>
			</div>
		</div>
		<div class="content-body">
			<section>
				<div class="row">
					<div class="col-md-10 offset-md-1">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">Ubah Organisasi</h4>
								<a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
								<div class="heading-elements">
									<ul class="list-inline mb-0">
										<li><a data-action="expand"><i class="icon-expand2"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="card-block">
                                <form class="form-horizontal" method="POST" action="{{ route('update_organisasi', $organisasi->lower_name) }}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    {{ method_field('PATCH') }}
                                    <div class="row">
                                        <div class="form-body">
                                            <div class="col-md-6">
                                                <!-- Info User -->
                                                <h4 class="form-section">&nbsp;<span><i class="icon-head"></i> Data Akun</span></h4>
                                                <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
                                                    <label for="username">Username</label>
                                                    <div class="position-relative has-icon-left">
                                                        <input type="text" id="username" class="form-control" placeholder="masukkan username" name="username"  disabled="" 
                                                        @if(old('username') == null) value="{{ $organisasi->user->username }}"
                                                        @else value="{{ old('username') }}" @endif >
                                                        <div class="form-control-position">
                                                            <i class="icon-head"></i>
                                                        </div>
                                                    </div>
                                                    @if($errors->has('username'))
                                                    <span class="help-block">
                                                        <strong class="pink">{{ $errors->first('username') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                                                    <label for="password">Kata Sandi</label>
                                                    <div class="position-relative has-icon-left">
                                                        <input type="password" id="password" class="form-control" placeholder="masukkan kata sandi" name="password">
                                                        <div class="form-control-position">
                                                            <i class="icon-lock4"></i>
                                                        </div>
                                                    </div>
                                                    @if($errors->has('password'))
                                                    <span class="help-block">
                                                        <strong class="pink">{{ $errors->first('password') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <label for="password_confirmation">Konfirmasi Kata Sandi</label>
                                                    <input type="password" id="password_confirmation" class="form-control" placeholder="konfirmasi kata sandi" name="password_confirmation">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <!-- Personal Data -->
                                                <h4 class="form-section" >&nbsp;<span><i class="icon-paper"></i> Data Organisasi</span></h4>
                                                <div class="form-group {{ $errors->has('organisasi') ? 'has-error' : '' }}">
                                                    <label for="organisasi">Nama Organisasi*</label>
                                                    <div class="position-relative has-icon-left">
                                                        <input type="text" id="organisasi" class="form-control square" placeholder="nama organisasi" name="organisasi" required="" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Masukkan nama organisasi" @if(old('organisasi') == null) value="{{ $organisasi->nama_organisasi }}" @else value="{{ old('organisasi') }}" @endif >
                                                        <div class="form-control-position">
                                                            <i class="icon-office"></i>
                                                        </div>
                                                    </div>
                                                    @if($errors->has('organisasi'))
                                                    <span class="help-block">
                                                        <strong class="pink">{{ $errors->first('organisasi') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="form-group {{ $errors->has('alamat') ? 'has-error' : '' }}">
                                                    <label for="alamat">Alamat</label>
                                                    <div class="position-relative has-icon-left">
                                                        <input type="text" id="alamat" class="form-control square" placeholder="alamat" name="alamat" required="" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Masukkan alamat dari organisasi" @if(old('alamat') == null) value="{{ $organisasi->alamat_organisasi }}" @else value="{{ old('alamat') }}" @endif >
                                                        <div class="form-control-position">
                                                            <i class="icon-location2"></i>
                                                        </div>
                                                    </div>
                                                    @if($errors->has('alamat'))
                                                    <span class="help-block">
                                                        <strong class="pink">{{ $errors->first('alamat') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6 form-group {{ $errors->has('file') ? 'has-error' : '' }}">
                                                        <label>Unggah File SKT: </label>
                                                        <label id="file" class="file center-block">
                                                            <input type="file" id="file" name="file">
                                                            <span class="file-custom"></span>
                                                        </label>
                                                        <div>
                                                            @if($errors->has('file'))
                                                            <span class="help-block">
                                                                <strong class="pink">{{ $errors->first('file') }}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group {{ $errors->has('no_telp') ? 'has-error' : '' }}">
                                                                    <label for="no_telp">No.Telp</label>
                                                                    <div class="position-relative has-icon-left">
                                                                        <input type="text" id="no_telp" class="form-control square" placeholder="nomor telepon" name="no_telp" required=""  data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Masukkan nomor telepon dari organisasi" @if(old('no_telp') == null) value="{{ $organisasi->no_telp_organisasi }}"
                                                                            @else value="{{ old('no_telp') }}" @endif >
                                                                        <div class="form-control-position">
                                                                            <i class="icon-phone"></i>
                                                                        </div>
                                                                    </div>
                                                                    @if($errors->has('no_telp'))
                                                                    <span class="help-block">
                                                                        <strong class="pink">{{ $errors->first('no_telp') }}</strong>
                                                                    </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group {{ $errors->has('fax') ? 'has-error' : '' }}">
                                                                    <label for="fax">Fax</label>
                                                                    <div class="position-relative has-icon-left">
                                                                        <input type="text" id="fax" class="form-control square" placeholder="nomor fax" name="fax" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Masukkan nomor fax dari organisasi" @if(old('fax') == null) value="{{ $organisasi->fax }}" @else value="{{ old('fax') }}" @endif >
                                                                        <div class="form-control-position">
                                                                            <i class="icon-telephone"></i>
                                                                        </div>
                                                                    </div>
                                                                    @if($errors->has('fax'))
                                                                    <span class="help-block">
                                                                        <strong class="pink">{{ $errors->first('fax') }}</strong>
                                                                    </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group {{ $errors->has('handphone') ? 'has-error' : '' }}">
                                                                    <label for="handphone">Handphone</label>
                                                                    <div class="position-relative has-icon-left">
                                                                        <input type="text" id="handphone" class="form-control square" placeholder="nomor handphone" name="handphone" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Masukkan nomor handphone dari organisasi" @if(old('handphone') == null) value="{{ $organisasi->handphone_organisasi }}" @else value="{{ old('handphone') }}" @endif >
                                                                        <div class="form-control-position">
                                                                            <i class="icon-mobile-phone"></i>
                                                                        </div>
                                                                    </div>
                                                                    @if($errors->has('handphone'))
                                                                    <span class="help-block">
                                                                        <strong class="pink">{{ $errors->first('handphone') }}</strong>
                                                                    </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group {{ $errors->has('kode_pos') ? 'has-error' : '' }}">
                                                                    <label for="kode_pos">Kode Pos</label>
                                                                    <div class="position-relative has-icon-left">
                                                                        <input type="text" id="kode_pos" class="form-control square" placeholder="kode pos" name="kode_pos" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Masukkan nomor kode pos dari organisasi" @if(old('kode_pos') == null) value="{{ $organisasi->kode_pos }}" @else value="{{ old('kode_pos') }}" @endif >
                                                                        <div class="form-control-position">
                                                                            <i class="icon-email22"></i>
                                                                        </div>
                                                                    </div>
                                                                    @if($errors->has('kode_pos'))
                                                                    <span class="help-block">
                                                                        <strong class="pink">{{ $errors->first('kode_pos') }}</strong>
                                                                    </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                                                    <label for="email">Email</label>
                                                                    <div class="position-relative has-icon-left">
                                                                        <input type="text" id="email" class="form-control square" placeholder="email" name="email" required="" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Masukkan email dari organisasi" @if(old('email') == null) value="{{ $organisasi->email_organisasi }}" @else value="{{ old('email') }}" @endif >
                                                                        <div class="form-control-position">
                                                                            <i class="icon-email2"></i>
                                                                        </div>
                                                                    </div>
                                                                    @if($errors->has('email'))
                                                                    <span class="help-block">
                                                                        <strong class="pink">{{ $errors->first('email') }}</strong>
                                                                    </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group {{ $errors->has('web') ? 'has-error' : '' }}">
                                                                    <label for="web">Web</label>
                                                                    <div class="position-relative has-icon-left">
                                                                        <input type="text" id="web" class="form-control square" placeholder="web" name="web" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Masukkan alamat web dari organisasi" @if(old('web') == null) value="{{ $organisasi->web }}" @else value="{{ old('web') }}" @endif >
                                                                        <div class="form-control-position">
                                                                            <i class="icon-earth2"></i>
                                                                        </div>
                                                                    </div>
                                                                    @if($errors->has('web'))
                                                                    <span class="help-block">
                                                                        <strong class="pink">{{ $errors->first('web') }}</strong>
                                                                    </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="col-md-12">
                                            <div class="text-xs-center mt-2">
                                                <a href="{{ route('organisasi_page' ) }}" class="btn btn-warning mr-1"><i class="icon-cross2"></i> Batal</a>
                                                <button type="submit" class="btn btn-info"><i class="icon-check"></i> Simpan</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection

<!-- Scripts -->
@section('custom_js')
<!-- plugin pnotify -->
<script src="{{ asset('js/pnotify.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('input[name="no_telp"]').keydown(function (e) {
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                    (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                    (e.keyCode >= 35 && e.keyCode <= 39)) {
                return;
            }
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
        $('input[name="fax"]').keydown(function (e) {
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                    (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                    (e.keyCode >= 35 && e.keyCode <= 39)) {
                return;
            }
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
        $('input[name="handphone"]').keydown(function (e) {
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                    (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                    (e.keyCode >= 35 && e.keyCode <= 39)) {
                return;
            }
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
        $('input[name="kode_pos"]').keydown(function (e) {
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                    (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                    (e.keyCode >= 35 && e.keyCode <= 39)) {
                return;
            }
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

        if("{{ Session::has('error_message') }}"){
            alert("{{ session('error_message') }}");
        }

        if("{{ Session::has('success_message') }}"){
            alert("{{ session('success_message') }}");
        }

        if("{{ $errors->first() }}"){
			errorMessage("data pegawai tidak disimpan");
		}
    });
</script>
@endsection