@extends('layouts._app_frontend')
@section('custom_css')
<style>
    .pink {
        color: red;
    }

    .sidebar-menu a.active{
        color: #e18604
    }
</style>
@endsection
@section('content')
<div class="container">
    <ul class="breadcrumb">
        <li><a href="{{ url('/') }}">Beranda</a></li>
        <li class="active">Profil</li>
    </ul>
    <div class="row margin-bottom-40">
        <div class="sidebar col-md-3 col-sm-3">
            <ul class="list-group margin-bottom-25 sidebar-menu">
                <li class="list-group-item clearfix"><a class="active" href="{{ url('/akun?') .'organisasi='. $organisasi->lower_name }}"><i class="fa fa-angle-right"></i> Profil</a></li>
            </ul>
        </div>
        <div class="col-md-9 col-sm-9">
            <div class="content-form-page">
                <div class="row">
                    <div class="col-md-8 col-sm-8">
                        <form class="form-horizontal" role="form">
                            <fieldset>
                                <legend>Akun</legend>
                                <div class="form-group">
                                    <label for="username" class="col-lg-4 control-label">Username</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="username" name="username" @if(old('username') == null) value="{{ $organisasi->user->username }}" @else value="{{ old('username') }}" @endif disabled>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <legend>Organisasi</legend>
                                <div class="form-group">
                                    <label for="organisasi" class="col-lg-4 control-label">Organisasi <span class="require"></span></label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="organisasi" name="organisasi" @if(old('organisasi') == null) value="{{ $organisasi->nama_organisasi }}" @else value="{{ old('organisasi') }}" @endif disabled >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="file" class="col-lg-4 control-label">Surat Keterangan Terdaftar Organisasi <span class="require"></span></label>
                                    <div class="col-lg-8">
                                    @if($organisasi->foto_sertifikat)
                                        <img src="{{ asset('upload/organisasi').'/'.$organisasi->foto_sertifikat }}" alt="sertifikat">
                                    @else
                                        <img src="{{ asset('img/noimagefound.png') }}" alt="no image">
                                    @endif
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <legend>Detail Organisasi</legend>
                                <div class="form-group">
                                    <label for="alamat" class="col-lg-4 control-label">Alamat <span class="require"></span></label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="alamat" name="alamat" @if(old('alamat') == null) value="{{ $organisasi->alamat_organisasi }}" @else value="{{ old('alamat') }}" @endif disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-lg-4 control-label">Email <span class="require"></span></label>
                                    <div class="col-lg-8">
                                        <input type="email" class="form-control" id="email" name="email" @if(old('email') == null) value="{{ $organisasi->email_organisasi }}" @else value="{{ old('email') }}" @endif  disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="no_telp" class="col-lg-4 control-label">Nomor Telepon <span class="require"></span></label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="no_telp" name="no_telp" @if(old('no_telp') == null) value="{{ $organisasi->no_telp_organisasi }}" @else value="{{ old('no_telp') }}" @endif  disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="handphone" class="col-lg-4 control-label">Handphone</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="handphone" name="handphone" @if(old('handphone') == null) value="{{ $organisasi->handphone_organisasi }}" @else value="{{ old('handphone') }}" @endif  disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="fax" class="col-lg-4 control-label">Fax</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="fax" name="fax" @if(old('fax') == null) value="{{ $organisasi->fax }}" @else value="{{ old('fax') }}" @endif  disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="kode_pos" class="col-lg-4 control-label">Kode Pos</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="kode_pos" name="kode_pos" @if(old('kode_pos') == null) value="{{ $organisasi->kode_pos }}" @else value="{{ old('kode_pos') }}" @endif  disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="web" class="col-lg-4 control-label">Web</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="web" name="web" @if(old('web') == null) value="{{ $organisasi->web }}" @else value="{{ old('web') }}" @endif  disabled>
                                    </div>
                                </div>
                            </fieldset>
                            <div class="row text-center">
                                <div class="padding-left-0 padding-top-20">
                                    <?php $lower_name = strtolower(str_replace(" ", "+", Auth::user()->organisasi->nama_organisasi)); ?>
                                    <a class="btn btn-default" href="{{ url('/edit-akun?') .'organisasi='. $lower_name }}">Ubah</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('custom_js')
<script src="{{ asset('assets/corporate/scripts/layout.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        Layout.init();    
    });
</script>
@endsection