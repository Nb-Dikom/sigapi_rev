@extends('layouts._app_frontend')
@section('custom_css')
<style>
    .pink {
        color: red;
    }

    .sidebar-menu a.active{
        color: #e18604
    }
</style>
@endsection
@section('content')
<?php $lower_name = strtolower(str_replace(" ", "+", Auth::user()->organisasi->nama_organisasi)); ?>
<div class="container">
    <ul class="breadcrumb">
        <li><a href="{{ url('/') }}">Beranda</a></li>
        <li><a href="{{ url('/akun?') .'organisasi='. $lower_name }}">Profil</a></li>
        <li class="active">Ubah Profil</li>
    </ul>
    <div class="row margin-bottom-40">
        <div class="sidebar col-md-3 col-sm-3">
            <ul class="list-group margin-bottom-25 sidebar-menu">
                <li class="list-group-item clearfix"><a class="active" href="{{ url('/akun?') .'organisasi='. $organisasi->lower_name }}"><i class="fa fa-angle-right"></i> Profil</a></li>
            </ul>
        </div>
        <div class="col-md-9 col-sm-9">
            <div class="content-form-page">
                <div class="row">
                    <div class="col-md-8 col-sm-8">
                        <h3>Form Ubah Profil</h3>
                        <form class="form-horizontal" method="POST" action="{{ route('update_organisasi', $organisasi->lower_name) }}" enctype="multipart/form-data" role="form">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}
                            <fieldset>
                                <legend>Akun</legend>
                                <div class="form-group">
                                    <label for="username" class="col-lg-4 control-label">Username</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="username" name="username" disabled="" @if(old('username') == null) value="{{ $organisasi->user->username }}" @else value="{{ old('username') }}" @endif >
                                        @if($errors->has('username'))
                                        <span class="help-block">
                                            <strong class="pink">{{ $errors->first('username') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password" class="col-lg-4 control-label">Kata Sandi</label>
                                    <div class="col-lg-8">
                                        <input type="password" class="form-control" id="password" name="password">
                                        @if($errors->has('password'))
                                        <span class="help-block">
                                            <strong class="pink">{{ $errors->first('password') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password_confirmation" class="col-lg-4 control-label">Konfirmasi Kata Sandi</label>
                                    <div class="col-lg-8">
                                        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation">
                                        @if($errors->has('password_confirmation'))
                                        <span class="help-block">
                                            <strong class="pink">{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <legend>Organisasi</legend>
                                <div class="form-group">
                                    <label for="organisasi" class="col-lg-4 control-label">Organisasi <span class="require">*</span></label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="organisasi" name="organisasi" @if(old('organisasi') == null) value="{{ $organisasi->nama_organisasi }}" @else value="{{ old('organisasi') }}" @endif >
                                        @if($errors->has('organisasi'))
                                        <span class="help-block">
                                            <strong class="pink">{{ $errors->first('organisasi') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="file" class="col-lg-4 control-label">Surat Keterangan Terdaftar Organisasi <span class="require">*</span></label>
                                    <div class="col-lg-8">
                                        <input type="file" id="file" name="file">
                                        @if($errors->has('file'))
                                        <span class="help-block">
                                            <strong class="pink">{{ $errors->first('file') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <legend>Detail Organisasi</legend>
                                <div class="form-group">
                                    <label for="alamat" class="col-lg-4 control-label">Alamat <span class="require">*</span></label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="alamat" name="alamat" @if(old('alamat') == null) value="{{ $organisasi->alamat_organisasi }}" @else value="{{ old('alamat') }}" @endif >
                                        @if($errors->has('alamat'))
                                        <span class="help-block">
                                            <strong class="pink">{{ $errors->first('alamat') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-lg-4 control-label">Email <span class="require">*</span></label>
                                    <div class="col-lg-8">
                                        <input type="email" class="form-control" id="email" name="email" @if(old('email') == null) value="{{ $organisasi->email_organisasi }}" @else value="{{ old('email') }}" @endif >
                                        @if($errors->has('email'))
                                        <span class="help-block">
                                            <strong class="pink">{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="no_telp" class="col-lg-4 control-label">Nomor Telepon <span class="require">*</span></label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="no_telp" name="no_telp" @if(old('no_telp') == null) value="{{ $organisasi->no_telp_organisasi }}" @else value="{{ old('no_telp') }}" @endif >
                                        @if($errors->has('no_telp'))
                                        <span class="help-block">
                                            <strong class="pink">{{ $errors->first('no_telp') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="handphone" class="col-lg-4 control-label">Handphone</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="handphone" name="handphone" @if(old('handphone') == null) value="{{ $organisasi->handphone_organisasi }}" @else value="{{ old('handphone') }}" @endif >
                                        @if($errors->has('handphone'))
                                        <span class="help-block">
                                            <strong class="pink">{{ $errors->first('handphone') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="fax" class="col-lg-4 control-label">Fax</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="fax" name="fax" @if(old('fax') == null) value="{{ $organisasi->fax }}" @else value="{{ old('fax') }}" @endif >
                                        @if($errors->has('fax'))
                                        <span class="help-block">
                                            <strong class="pink">{{ $errors->first('fax') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="kode_pos" class="col-lg-4 control-label">Kode Pos</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="kode_pos" name="kode_pos" @if(old('kode_pos') == null) value="{{ $organisasi->kode_pos }}" @else value="{{ old('kode_pos') }}" @endif >
                                        @if($errors->has('kode_pos'))
                                        <span class="help-block">
                                            <strong class="pink">{{ $errors->first('kode_pos') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="web" class="col-lg-4 control-label">Web</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="web" name="web" @if(old('web') == null) value="{{ $organisasi->web }}" @else value="{{ old('web') }}" @endif >
                                        @if($errors->has('web'))
                                        <span class="help-block">
                                            <strong class="pink">{{ $errors->first('web') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </fieldset>
                            <div class="row text-center">
                                <div class="padding-left-0 padding-top-20">   
                                    <a class="btn btn-default" href="{{ url('/akun?') .'organisasi='. $lower_name }}">Batal</a>
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('custom_js')
<script src="{{ asset('assets/corporate/scripts/layout.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        Layout.init();    
    });
</script>
@endsection