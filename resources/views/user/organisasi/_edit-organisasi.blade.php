@extends('layouts._app')
@section('custom_css')
<!-- plugin notify -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/pnotify.min.css') }}">
@endsection

@section('content')
<div class="app-content content container-fluid">
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-xs-12 mb-1">
				<h2 class="content-header-title">Organisasi</h2>
			</div>
			<div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
				<div class="breadcrumb-wrapper col-xs-12">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{ url('/organisasi') }}">Organisasi</a></li>
						<li class="breadcrumb-item active"> Ubah Organisasi</li>
					</ol>
				</div>
			</div>
		</div>
		<div class="content-body">
			<section>
				<div class="row">
					<div class="col-md-6 offset-md-3">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">Ubah Organisasi</h4>
								<a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
								<div class="heading-elements">
									<ul class="list-inline mb-0">
										<li><a data-action="expand"><i class="icon-expand2"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="card-block">
								<form class="form" method="POST" action="{{ route('update_organisasi', $organisasi->id_organisasi) }}" enctype="multipart/form-data">
									{!! csrf_field() !!}
									{{ method_field('PATCH') }}
									<div class="form-body">
										<!-- Info User -->
										<h4 class="form-section">&nbsp;<span style="float:right;"><i class="icon-head"></i> Info User</span></h4>
										<div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
											<label for="username">Username</label>
											<div class="position-relative has-icon-left">
												<input type="text" id="username" class="form-control" placeholder="masukkan username" name="username"  required="" 
												@if(old('username') == null) value="{{ $organisasi->user->username }}"
												@else value="{{ old('username') }}" @endif >
												<div class="form-control-position">
													<i class="icon-head"></i>
												</div>
											</div>
											@if($errors->has('username'))
											<span class="help-block">
												<strong class="pink">{{ $errors->first('username') }}</strong>
											</span>
											@endif
										</div>
										<div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
											<label for="password">Kata Sandi</label>
											<div class="position-relative has-icon-left">
												<input type="password" id="password" class="form-control" placeholder="masukkan kata sandi" name="password">
												<div class="form-control-position">
													<i class="icon-lock4"></i>
												</div>
											</div>
											@if($errors->has('password'))
											<span class="help-block">
												<strong class="pink">{{ $errors->first('password') }}</strong>
											</span>
											@endif
										</div>
										<div class="form-group">
											<label for="password_confirmation">Konfirmasi Kata Sandi</label>
											<input type="password" id="password_confirmation" class="form-control" placeholder="konfirmasi kata sandi" name="password_confirmation">
										</div>

										<!-- Personal Data -->
										<h4 class="form-section" style="margin-top:3em;">&nbsp;<span style="float:right;"><i class="icon-paper"></i> Data Pribadi</span></h4>
										<div class="form-group {{ $errors->has('nama') ? 'has-error' : '' }}">
											<label for="nama">Nama Lengkap</label>
											<div class="position-relative has-icon-left">
												<input type="text" id="nama" class="form-control" placeholder="masukkan nama lengkap" name="nama"  required="" 
												@if(old('nama') == null) value="{{ $organisasi->nama }}"
												@else value="{{ old('nama') }}" @endif >
												<div class="form-control-position">
													<i class="icon-head"></i>
												</div>
											</div>
											@if($errors->has('nama'))
											<span class="help-block">
												<strong class="pink">{{ $errors->first('nama') }}</strong>
											</span>
											@endif
										</div>
										<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
											<label for="email">Email</label>
											<div class="position-relative has-icon-left">
												<input type="text" id="email" class="form-control" placeholder="masukkan email" name="email"  required="" 
												@if(old('email') == null) value="{{ $organisasi->email }}"
												@else value="{{ old('email') }}" @endif >
												<div class="form-control-position">
													<i class="icon-mail6"></i>
												</div>
											</div>
											@if($errors->has('email'))
											<span class="help-block">
												<strong class="pink">{{ $errors->first('email') }}</strong>
											</span>
											@endif
										</div>
										<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
											<label for="nomor">Nomor Telepon</label>
											<div class="position-relative has-icon-left">
												<input type="text" id="nomor" class="form-control" placeholder="masukkan nomor telepon" name="nomor"  required="" 
												@if(old('nomor') == null) value="{{ $organisasi->no_telp }}"
												@else value="{{ old('nomor') }}" @endif >
												<div class="form-control-position">
													<i class="icon-android-call"></i>
												</div>
											</div>
											@if($errors->has('nomor'))
											<span class="help-block">
												<strong class="pink">{{ $errors->first('nomor') }}</strong>
											</span>
											@endif
										</div>
										<div class="form-group {{ $errors->has('instansi') ? 'has-error' : '' }}">
											<label for="instansi">Instansi</label>
											<div class="position-relative has-icon-left">
												<input type="text" id="instansi" class="form-control" placeholder="masukkan instansi" name="instansi"  required="" 
												@if(old('instansi') == null) value="{{ $organisasi->instansi }}"
												@else value="{{ old('instansi') }}" @endif >
												<div class="form-control-position">
													<i class="icon-office"></i>
												</div>
											</div>
											@if($errors->has('instansi'))
											<span class="help-block">
												<strong class="pink">{{ $errors->first('instansi') }}</strong>
											</span>
											@endif
										</div>
										<div class="form-group {{ $errors->has('alamat') ? 'has-error' : '' }}">
											<label for="alamat">Alamat</label>
											<div class="position-relative has-icon-left">
												<input type="text" id="alamat" class="form-control" placeholder="masukkan alamat lengkap" name="alamat"  required="" 
												@if(old('alamat') == null) value="{{ $organisasi->alamat }}"
												@else value="{{ old('alamat') }}" @endif >
												<div class="form-control-position">
													<i class="icon-android-home"></i>
												</div>
											</div>
											@if($errors->has('alamat'))
											<span class="help-block">
												<strong class="pink">{{ $errors->first('alamat') }}</strong>
											</span>
											@endif
										</div>
									</div>
									<div class="form-actions center">
										<a href="{{ route('organisasi_page') }}" class="btn btn-warning mr-1"><i class="icon-cross2"></i> Batal</a>
										<button type="submit" class="btn btn-info"><i class="icon-check2"></i> Simpan</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection

@section('custom_js')
	<!-- plugin pnotify -->
	<script src="{{ asset('js/pnotify.min.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function(){          
			if("{{ Session::has('error_message') }}"){
				errorMessage("{{ session('error_message') }}");
			}
		});
	</script>
@endsection