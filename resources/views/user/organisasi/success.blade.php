<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/logo_bnpb.png') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'SIGAPI_DEV') }}</title>

    <!-- Styles -->
    @include('layouts.partials._css')
    <style type="text/css">
        .header-navbar.navbar-semi-dark .navbar-header {
            /*background: white;*/
        }
        .nav-item.active {
            border-bottom: 2px solid #1D2B36;
        }
    </style>
</head>
<body data-open="click" data-menu="vertical-menu" data-col="1-column" class="vertical-layout vertical-menu 1-column bg-maintenance-image blank-page blank-page">
    <div class="app-content content container-fluid">
        <div class="content-wrapper">
            <div class="content-header row"></div>
            <div class="content-body">
                <section class="flexbox-container">
                    <div class="col-md-4 offset-md-4 col-xs-10 offset-xs-1">
                        <div class="card border-grey border-lighten-3 px-1 py-1 box-shadow-3">
                            <div class="card-block">
                                <span class="card-title text-xs-center">
                                    <img src="{{ asset('assets/robust/app-assets/images/logo/robust-logo-dark-big.png') }}" class="img-fluid mx-auto d-block pt-2" width="250" alt="logo">
                                </span>
                            </div>
                            <div class="card-block text-xs-center">
                                <h3>Terima kasih telah mendaftar</h3>
                                <p>Harap tunggu proses persetujuan.<br> Cek pesan pada email anda.</p>
                                <div class="row py-2">
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <a href="{{ route('home_page') }}" class="btn btn-primary btn-block font-small-3"><i class="icon-home22"></i> Beranda</a>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <a href="{{ route('form_login') }}" class="btn btn-danger btn-block font-small-3"><i class="icon-unlock2"></i>  Login</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <!-- Scripts -->
    @include('layouts.partials._javascript')

</body>
</html>
