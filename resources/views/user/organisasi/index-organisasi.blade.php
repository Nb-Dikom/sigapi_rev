
@extends('layouts._app')
@section('custom_css')
<!-- Datatables css -->
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
<!-- plugin notify -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/pnotify.min.css') }}">
@endsection

@section('content')
<div class="app-content content container-fluid">
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-xs-12 mb-1">
				<h2 class="content-header-title">Organisasi</h2>
			</div>
			<div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
				<div class="breadcrumb-wrapper col-xs-12">
					<ol class="breadcrumb">
						<li class="breadcrumb-item active">Organisasi</li>
					</ol>
				</div>
			</div>
		</div>
		<div class="content-body">
			<section>
				<div class="row">
					<div class="col-xs-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">Organisasi</h4>
							</div>
							<div class="card-block">
								<div style="margin-bottom:15px">
									<a href="{{ route('form_create_organisasi') }}" class="btn btn-success" title="Tambah Data" data-toggle="tooltip" data-placement="bottom" data-trigger="hover">Tambah <i class="icon-users3"></i></a>
								</div>
								<div class="table-responsive">
									<table class="table table-hover table-striped table-organisasi">
										<thead>
											<tr>
												<th style="width:8%">No.</th>
												<th>Organisasi</th>
												<th>Email</th>
												<th>No Telp</th>
												<th class="text-sm-center">Status</th>
												<th class="text-xs-center">Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php $index=0;?>
									    	@foreach($list_organisasi as $organisasi)
									    	<?php
												$lower_name = strtolower(str_replace(" ", "+", $organisasi->nama_organisasi));
												$index++;
											?>
											<tr>
				                                <th scope="row">{{ $index }}.</th>
				                                <td>{{ $organisasi->nama_organisasi }}</td>
				                                <td>{{ $organisasi->email_organisasi }}</td>
				                                <td>{{ $organisasi->no_telp_organisasi }}</td>
				                                <td class="text-sm-center">
				                                	<span name="status-{{ $index }}" @if($organisasi->status_organisasi == 'Pending') class="tag tag-warning"
		                                			@elseif($organisasi->status_organisasi == 'Tidak Setuju') class="tag tag-danger"
		                                			@elseif($organisasi->status_organisasi == 'Setuju') class="tag tag-success" @endif >
				                                	{{ $organisasi->status_organisasi }}</span>
				                                </td>
				                                <td class="text-xs-center">
													<ul class="list-inline">
														<li>
															<button index="{{ $index }}" value="{{ $lower_name }}" class="btn btn-sm btn-info" name="lihat" title="Lihat Data" data-toggle="tooltip" data-placement="bottom" data-trigger="hover"><i class="icon-eye6"></i></button>
														</li>
														<li>
															<a href="{{ route('form_edit_organisasi', $lower_name) }}" class="btn btn-sm btn-warning" name="edit" title="Ubah Data" data-toggle="tooltip" data-placement="bottom" data-trigger="hover"><i class="icon-edit2"></i></a>
														</li>
														<li>
				                                			<button value="{{ $lower_name }}" class="btn btn-sm btn-danger" name="hapus" title="Hapus Data" data-toggle="tooltip" data-placement="bottom" data-trigger="hover"><i class="icon-android-delete"></i></button>
														</li>
													</ul>
				                                </td>
				                            </tr>
				                            @endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<div id="modal-lihat" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
		        <div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title"><i class="icon-user"></i> <span name="nama_organisasi"></span></h4>
						</div>
						<div class="modal-body">
							<div class="row">
                                <div class="col-md-6 mt-1">
                                	<div class="text-xs-left">
                                        <h4 class="form-section"><span><i class="icon-file"></i> Profil Organisasi</span></h4>
                                        <hr>
                                    </div>
                                    <div class="pl-2">
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td><span class="text-bold-600">Alamat</span></td>
                                                    <td class="pl-3 pr-1">:</td>
                                                    <td><span name="alamat"></span><td>
                                                </tr>
                                                <tr>
                                                    <td><span class="text-bold-600">No.Telp</span></td>
                                                    <td class="pl-3 pr-1">:</td>
                                                    <td><span name="no_telp"></span><td>
                                                </tr>
                                                <tr>
                                                    <td><span class="text-bold-600">Fax</span></td>
                                                    <td class="pl-3 pr-1">:</td>
                                                    <td><span name="fax"></span><td>
                                                </tr>
                                                <tr>
                                                    <td><span class="text-bold-600">Handphone</span></td>
                                                    <td class="pl-3 pr-1">:</td>
                                                    <td><span name="handphone"></span><td>
                                                </tr>
                                                <tr>
                                                    <td><span class="text-bold-600">Kode Pos</span></td>
                                                    <td class="pl-3 pr-1">:</td>
                                                    <td><span name="kode_pos"></span><td>
                                                </tr>
                                                <tr>
                                                    <td><span class="text-bold-600">Email</span></td>
                                                    <td class="pl-3 pr-1">:</td>
                                                    <td><span name="email"></span><td>
                                                </tr>
                                                <tr>
                                                    <td><span class="text-bold-600">Web</span></td>
                                                    <td class="pl-3 pr-1">:</td>
                                                    <td><span name="web"></span><td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-6 mt-1">
                                    <div class="text-xs-left">
                                        <h4 class="form-section"><span><i class="icon-image2"></i> Sertifikat Keterangan Terdaftar</span></h4>
                                        <hr>
                                    </div>
                                    <figure>
                                        <img name="sertifikat" src="" class="img-thumbnail img-fluid">
                                    </figure>
                                </div>
	                            <div class="col-md-6 offset-md-3">
	                            	<div class="mt-2">
										<div name="status" class="alert alert-warning text-sm-center" role="alert" style="font-size:11px">
											Status Akun organisasi: <span><b name="status"></b></span>
										</div>
	                            	</div>
	                            </div>
								<div class="col-md-12">
									<div class="text-sm-center mt-5">
										<ul class="list-inline validated">
											<li>
												<button index="" value="" class="btn btn-danger" name="tidak_valid" title="Tidak Setuju" data-toggle="tooltip" data-placement="bottom" data-trigger="hover"><i class="icon-cross2"></i> Tidak Setuju</button>
											</li>
											<li>
												<button index="" value="" class="btn btn-success" name="valid" title="Setuju" data-toggle="tooltip" data-placement="bottom" data-trigger="hover"><i class="icon-check2"></i> Setuju</button>
											</li>
										</ul>
									</div>
									<hr>
									<div class="text-sm-right">
										<button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
									</div>
								</div>
                            </div>
						</div>
					</div>
				</div>
		    </div>
			<!-- Modal Delete -->
			<div class="modal fade text-xs-left" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
			@include('layouts._delete')
			</div>
		</div>
	</div>
</div>
@endsection

@section('custom_js')
<!-- Datatables js  -->
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<!-- plugin pnotify -->
<script src="{{ asset('js/pnotify.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.table-organisasi').DataTable();
		$('.table-responsive').on('click', 'button[name="lihat"]', function(){
			var id = $(this).attr('value');
			var index = $(this).attr('index');
			$.ajax({
                url: '{{ url("/organisasi") }}'+'/'+id,
                type: 'GET',
                beforeSend: function () {
                    //
                    startLoader()
                },
                success: function(response){
                	endLoader();
            		$('span[name="nama_organisasi"]').html('');
            		$('span[name="alamat"]').html('');
            		$('span[name="no_telp"]').html('');
            		$('span[name="fax"]').html('');
            		$('span[name="handphone"]').html('');
            		$('span[name="kode_pos"]').html('');
            		$('span[name="email"]').html('');
            		$('span[name="web"]').html('');
                	$('div[name="status"]').attr('class', '');
                	$('b[name="status"]').html('');

                	$('span[name="nama_organisasi"]').html(response.nama_organisasi);
            		$('span[name="alamat"]').html(response.alamat_organisasi);
            		$('span[name="no_telp"]').html(response.no_telp_organisasi);
            		$('span[name="fax"]').html(response.fax);
            		$('span[name="handphone"]').html(response.handphone_organisasi);
            		$('span[name="kode_pos"]').html(response.kode_pos);
            		$('span[name="email"]').html(response.email_organisasi);
            		$('span[name="web"]').html(response.web);
            		if(response.foto_sertifikat !== null){
            			$('img[name="sertifikat"]').attr('src', "{{ asset('upload/sertifikat/') }}"+"/"+response.foto_sertifikat);
            		}
            		else {
            			$('img[name="sertifikat"]').attr('src', "{{ asset('img/noimagefound.png') }}");
            		}
                	$('b[name="status"]').html(response.status_organisasi);
					switch (response.status_organisasi) {
						case "Pending":
								$('div[name="status"]').attr('class', 'alert alert-warning text-sm-center');
								$('.validated').show();
								$('button[name="tidak_valid"]').val(response.lower_name);
								$('button[name="tidak_valid"]').attr("index", index);
								$('button[name="valid"]').val(response.lower_name);
								$('button[name="valid"]').attr("index", index);
							break;
						case "Tidak Setuju":
							$('div[name="status"]').attr('class', 'alert alert-danger text-sm-center');
							$('.validated').hide();
							break;
						default:
                			$('div[name="status"]').attr('class', 'alert alert-success text-sm-center');
							$('.validated').hide();
							break;
					}

                    $('#modal-lihat').modal('show');
                },
                error: function () {
                	endLoader();
                	alert('server not responding...');
                }
            });
		});

		$('#modal-lihat').on('click', 'button[name="valid"]', function(){
			var index = $(this).attr('index');
        	$.ajaxSetup({
                headers:{
                    'X-CSRF-TOKEN': $('input[name="_token"]').val()
                }
            });
            $.ajax({
                url: "{{ route('verified_organisasi') }}",
                type: 'PATCH',
                data: {'name': $(this).attr('value')},
                beforeSend: function () {
                    startLoader()
                },
                success: function(response){
					status = response.data.status_organisasi;
					if(status == "Setuju")
					{
                		$('span[name="status-'+index+'"]').attr('class', 'tag tag-success');
					}
                	$('span[name="status-'+index+'"]').html(status);
					$('#modal-lihat').modal('hide');
                	endLoader();
                },
                error: function () {
                	endLoader("alert");
                }
            });
        });

		$('#modal-lihat').on('click', 'button[name="tidak_valid"]', function(){
			var index = $(this).attr('index');
        	$.ajaxSetup({
                headers:{
                    'X-CSRF-TOKEN': $('input[name="_token"]').val()
                }
            });
            $.ajax({
                url: "{{ route('unverified_organisasi') }}",
                type: 'PATCH',
                data: {'name': $(this).attr('value')},
                beforeSend: function () {
                    startLoader()
                },
                success: function(response){
                	status = response.data.status_organisasi;
					if(status == "Tidak Setuju")
					{
                		$('span[name="status-'+index+'"]').attr('class', 'tag tag-danger');
					}
                	$('span[name="status-'+index+'"]').html(status);
					$('#modal-lihat').modal('hide');
                	endLoader();
                },
                error: function () {
                	endLoader("alert");
                }
            });
        });

		$('.table-responsive').on('click', 'button[name="hapus"]', function(){
			var id = $(this).attr('value');
			modal_delete("{{ url('/organisasi') }}" +'/'+id);
		});
		if("{{ Session::has('success_message') }}"){
			successMessage("{{ session('success_message') }}");
		}
		if("{{ Session::has('error_message') }}"){
			errorMessage("{{ session('error_message') }}");
		}
	});
</script>
@endsection
