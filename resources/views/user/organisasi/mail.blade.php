<div style="background:#f5f5f5; margin:0px">
    <div style="margin-left:auto; margin-right:auto; width:50%;">
            <!-- <div style="text-align:center; padding-top:5px; padding-bottom:10px">
                <a href="#" style="color:#00bcd4; font-family:'Roboto',Arial,sans-serif; font-weight:300; font-size:12px; line-height:16px">View this email in your browser</a>
            </div> -->
            <div style="background:white;">
                <div style="background:#1D2B36; padding:20px 40px">
                    <img src="{{ $message->embed(asset('img/sigapi-logo-light.png')) }}" alt="SIGAPI">
                </div>
                <div style="padding:20px 40px 0px 40px">
                    <div style="margin-top:10px; font-family:'Roboto',Arial,sans-serif; text-align:center;">
                        <span style="font-weight:100; font-size:24px; line-height:30px; color:black">Hi, {{ $nama }}</span>
                        <p style="font-weight:300; font-size:15px; line-height:21px; color:black">Terima Kasih telah melakukan pendaftaran SIGAPI</p>
                    </div>
                    <div style="font-family:'Roboto',Arial,sans-serif;">
                        <span style="font-weight:300; font-size:14px; line-height:21px; color:black">Berikut merupakan informasi akun anda :</span>
                        <table style="margin-left:-5px;">
                            <tbody>
                                <tr>
                                    <td style="font-weight:300; font-size:13px; line-height:21px; color:black">Username</td>
                                    <td style="font-weight:300; font-size:13px; line-height:21px; color:black">:</td>
                                    <td style="font-weight:300; font-size:13px; line-height:21px; color:black">{{ $username }}</td>
                                </tr>
                                <tr>
                                    <td style="font-weight:300; font-size:13px; line-height:21px; color:black">Kata Sandi</td>
                                    <td style="font-weight:300; font-size:13px; line-height:21px; color:black">:</td>
                                    <td style="font-weight:300; font-size:13px; line-height:21px; color:black">{{ $kata_sandi }}</td>
                                </tr>
                            </tbody>
                        </table>
                        <div style="font-weight:500; font-size:14px; line-height:150%; margin-top:30px; text-align:center; padding-bottom:30px">
                            <a style="background:#00bcd4; padding:10px 20px; color:white; text-decoration:none; border-radius: 3px" href="{{ route('form_login') }}">Sign in</a>
                        </div>
                        <p style="font-weight:300; font-size:14px; line-height:21px; text-align:center; color:black">Jika terdapat permasalahan akun, anda dapat menghubungi <a href="mailto:diar.ichrom@mhs.uinjkt.ac.id" style="color:#00bcd4;">diar.ichrom@mhs.uinjkt.ac.id</a></p>
                    </div>
                </div>
                <div style="background:#1D2B36; padding-top:20px; padding-bottom:10px; text-align:center">
                    <img src="{{ $message->embed(asset('img/sigapi-logo-small.png')) }}" alt="SIGAPI">
                    <div style="color:white; font-weight:300; font-size:12px; line-height:20px; margin-top:10px">
                        <span >Copyright &copy; 2017 <span>Diar Ichrom</span>, All rights reserved. </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>