@extends('layouts._app')
@section('custom_css')
<!-- plugin notify -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/pnotify.min.css') }}">
@endsection

@section('content')
<div class="app-content content container-fluid">
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-xs-12 mb-1">
				<h2 class="content-header-title">Pegawai</h2>
			</div>
			<div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
				<div class="breadcrumb-wrapper col-xs-12">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{ url('/pegawai') }}">Pegawai</a></li>
						<li class="breadcrumb-item active"> Ubah Pegawai</li>
					</ol>
				</div>
			</div>
		</div>
		<div class="content-body">
			<section>
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">Ubah Pegawai</h4>
							</div>
							<div class="card-block">
								<form class="form" method="POST" action="{{ route('update_pegawai', $pegawai->lower_name) }}" enctype="multipart/form-data">
									{!! csrf_field() !!}
									{{ method_field('PATCH') }}
									<div class="form-body">
										<div class="row">
											<div class="col-md-6">
												<!-- Info User -->
												<h4 class="form-section">&nbsp;<span><i class="icon-head"></i> Info User</span></h4>
												<div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
													<label for="nama_lengkap">Username</label>
													<div class="position-relative has-icon-left">
														<input type="text" id="username" class="form-control" placeholder="masukkan username" name="username"  required="" 
														@if(old('username') == null) value="{{ $pegawai->username }}"
														@else value="{{ old('username') }}" @endif disabled>
														<div class="form-control-position">
															<i class="icon-head"></i>
														</div>
													</div>
													@if($errors->has('username'))
													<span class="help-block">
														<strong class="pink">{{ $errors->first('username') }}</strong>
													</span>
													@endif
												</div>
												<div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
													<label for="password">Kata Sandi</label>
													<div class="position-relative has-icon-left">
														<input type="password" id="password" class="form-control" placeholder="masukkan kata sandi" name="password">
														<div class="form-control-position">
															<i class="icon-lock4"></i>
														</div>
													</div>
													@if($errors->has('password'))
													<span class="help-block">
														<strong class="pink">{{ $errors->first('password') }}</strong>
													</span>
													@endif
												</div>
												<div class="form-group">
													<label for="password_confirmation">Konfirmasi Kata Sandi</label>
													<input type="password" id="password_confirmation" class="form-control" placeholder="konfirmasi kata sandi" name="password_confirmation">
												</div>
											</div>
											<div class="col-md-6">
												<!-- Personal Data -->
												<h4 class="form-section">&nbsp;<span><i class="icon-paper"></i> Data Pribadi</span></h4>
												<div class="form-group {{ $errors->has('nama') ? 'has-error' : '' }}">
													<label for="nama">Nama Lengkap</label>
													<div class="position-relative has-icon-left">
														<input type="text" id="nama" class="form-control" placeholder="masukkan nama lengkap" name="nama"  required="" 
														@if(old('nama') == null) value="{{ $pegawai->nama_pegawai }}"
														@else value="{{ old('nama') }}" @endif >
														<div class="form-control-position">
															<i class="icon-head"></i>
														</div>
													</div>
													@if($errors->has('nama'))
													<span class="help-block">
														<strong class="pink">{{ $errors->first('nama') }}</strong>
													</span>
													@endif
												</div>
												<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
													<label for="email">Email</label>
													<div class="position-relative has-icon-left">
														<input type="text" id="email" class="form-control" placeholder="masukkan email" name="email"  required="" 
														@if(old('email') == null) value="{{ $pegawai->email_pegawai }}"
														@else value="{{ old('email') }}" @endif >
														<div class="form-control-position">
															<i class="icon-mail6"></i>
														</div>
													</div>
													@if($errors->has('email'))
													<span class="help-block">
														<strong class="pink">{{ $errors->first('email') }}</strong>
													</span>
													@endif
												</div>
												<div class="form-group {{ $errors->has('photo') ? 'has-error' : '' }}">
													<label>Foto Profil</label><br>
													<label id="photo" class="file center-block">
														<input type="file" id="photo" name="photo">
														<span class="file-custom"></span>
													</label>
												</div>
												@if($errors->has('photo'))
												<span class="help-block">
													<strong class="pink">{{ $errors->first('photo') }}</strong>
												</span>
												@endif
												<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
													<label for="nomor">Nomor Telepon</label>
													<div class="position-relative has-icon-left">
														<input type="text" id="nomor" class="form-control" placeholder="masukkan nomor telepon" name="nomor"  required="" 
														@if(old('nomor') == null) value="{{ $pegawai->no_telp_pegawai }}"
														@else value="{{ old('nomor') }}" @endif >
														<div class="form-control-position">
															<i class="icon-android-call"></i>
														</div>
													</div>
													@if($errors->has('nomor'))
													<span class="help-block">
														<strong class="pink">{{ $errors->first('nomor') }}</strong>
													</span>
													@endif
												</div>
												<div class="form-group {{ $errors->has('instansi') ? 'has-error' : '' }}">
													<label for="instansi">Instansi</label>
													<div class="position-relative has-icon-left">
														<input type="text" id="instansi" class="form-control" placeholder="masukkan instansi" name="instansi"  required="" 
														@if(old('instansi') == null) value="{{ $pegawai->instansi_pegawai }}"
														@else value="{{ old('instansi') }}" @endif >
														<div class="form-control-position">
															<i class="icon-office"></i>
														</div>
													</div>
													@if($errors->has('instansi'))
													<span class="help-block">
														<strong class="pink">{{ $errors->first('instansi') }}</strong>
													</span>
													@endif
												</div>
												<div class="form-group {{ $errors->has('jabatan') ? 'has-error' : '' }}">
													<label for="jabatan">Jabatan</label>
													<select id="jabatan" name="jabatan" class="form-control">
														<option value="none" selected="" disabled="">Pilih Jabatan</option>		
														@foreach ($list_jabatan as $jabatan)
														@if(old('jabatan') == null)
															@if($jabatan->jabatan == $pegawai->jabatan)
																<option selected value="{{ $jabatan->jabatan }}">{{ $jabatan->jabatan }}</option>
															@else
																<option value="{{ $jabatan->jabatan }}">{{ $jabatan->jabatan }}</option>
															@endif
														@else
															@if($jabatan->jabatan == old('jabatan'))
																<option selected value="{{ $jabatan->jabatan }}">{{ $jabatan->jabatan }}</option>
															@else
																<option value="{{ $jabatan->jabatan }}">{{ $jabatan->jabatan }}</option>
															@endif
														@endif
														@endforeach
													</select>
													@if($errors->has('jabatan'))
													<span class="help-block">
														<strong class="pink">{{ $errors->first('jabatan') }}</strong>
													</span>
													@endif
												</div>
											</div>
										</div>
									</div>
									<div class="form-actions center">
										<a href="{{ route('pegawai_page') }}" class="btn btn-warning mr-1"><i class="icon-cross2"></i> Batal</a>
										<button type="submit" class="btn btn-info"><i class="icon-check2"></i> Simpan</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection

@section('custom_js')
	<!-- plugin pnotify -->
	<script src="{{ asset('js/pnotify.min.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function(){
	        $('input[name="nomor"]').keydown(function (e) {
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                        (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                        (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                        (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                        (e.keyCode >= 35 && e.keyCode <= 39)) {
                    return;
                }
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
            
			if("{{ Session::has('error_message') }}"){
				errorMessage("{{ session('error_message') }}");
			}
			if("{{ $errors->first() }}"){
				errorMessage("data pegawai tidak disimpan");
			}
		});
	</script>
@endsection