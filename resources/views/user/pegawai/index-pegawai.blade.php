@extends('layouts._app')
@section('custom_css')
<!-- Datatables css -->
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">

<!-- plugin notify -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/pnotify.min.css') }}">
@endsection

@section('content')
<div class="app-content content container-fluid">
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-xs-12 mb-1">
				<h2 class="content-header-title">Pegawai</h2>
			</div>
			<div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
				<div class="breadcrumb-wrapper col-xs-12">
					<ol class="breadcrumb">
						<li class="breadcrumb-item active">Pegawai</li>
					</ol>
				</div>
			</div>
		</div>
		<div class="content-body">
			<section>
				<div class="row">
					<div class="col-xs-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">Pegawai</h4>
							</div>
							<div class="card-block">
								<div style="margin-bottom:15px">
									<a href="{{ route('form_create_pegawai') }}" class="btn btn-success" title="Tambah Data" data-toggle="tooltip" data-placement="bottom" data-trigger="hover">Tambah <i class="icon-person-add"></i></a>
								</div>
								<div class="table-responsive">
									<table class="table table-hover table-striped table-pegawai">
										<thead>
											<tr>
												<th style="width:8%">No.</th>
												<th style="width:15%">Username</th>
												<th>Nama</th>
												<th>Email</th>
												<th class="text-xs-right">Nomor Telp</th>
												<th>Jabatan</th>
												<th class="text-xs-center" style="width:12%">Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php $index=0;?>
									    	@foreach($list_pegawai as $pegawai)
											<?php
												$lower_name = strtolower(str_replace(" ", "+", $pegawai->nama_pegawai));
												$index++;
											?>
											<tr>
				                                <th scope="row">{{ $index }}.</th>
				                                <td>{{ $pegawai->username }}</td>
				                                <td>{{ $pegawai->nama_pegawai }}</td>
				                                <td>{{ $pegawai->email_pegawai }}</td>
				                                <td class="text-xs-right">{{ $pegawai->no_telp_pegawai }}</td>
				                                <td>{{ $pegawai->jabatan }}</td>
				                                <td class="text-xs-center">
													<ul class="list-inline">
														<li>
						                                	<a href="{{ route('form_edit_pegawai', $lower_name) }}" class="btn btn-sm btn-warning" name="edit" title="Ubah Data" data-toggle="tooltip" data-placement="bottom" data-trigger="hover"><i class="icon-edit2"></i></a>
														</li>
														<li>
						                                	<button value="{{ $lower_name }}" class="btn btn-sm btn-danger" name="hapus" title="Hapus Data" data-toggle="tooltip" data-placement="bottom" data-trigger="hover"><i class="icon-android-delete"></i></button>
														</li>
													</ul>
				                                </td>
				                            </tr>
				                            @endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- Modal Delete -->
			<div class="modal fade text-xs-left" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
			@include('layouts._delete')
			</div>
		</div>
	</div>
</div>
@endsection

@section('custom_js')
<!-- Datatables js  -->
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<!-- plugin pnotify -->
<script src="{{ asset('js/pnotify.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.table-pegawai').DataTable();
		$('.table-responsive').on('click', 'button[name="hapus"]', function(){
			var id = $(this).attr('value');

			modal_delete("{{ url('/pegawai') }}" +'/'+id);
		});
		if("{{ Session::has('success_message') }}"){
			successMessage("{{ session('success_message') }}");
		}
		if("{{ Session::has('error_message') }}"){
			errorMessage("{{ session('error_message') }}");
		}
	});
</script>
@endsection
