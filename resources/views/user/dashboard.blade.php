@extends('layouts._app')
@section('custom_css')
<!-- plugin notify -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/pnotify.min.css') }}">
@endsection
@section('content')
<div class="app-content content container-fluid">
	<div class="content-wrapper">
		<div class="content-body">
			<section>
				<div class="row">
					<div class="col-lg-12">
						<div class="card" style="min-height: 480px">
							<div class="card-header">
								<h4 class="card-title">Dashboard</h4>
							</div>
							<div class="card-body">
						        <div class="card-block">
						            <div class="card-text">
						                <p>
						                	<h3><span class="icon-display"></span><strong> Sistem Informasi Spasial Mitigasi Bencana Gunung Api</strong></h3><br>
						                    <h4>Selamat Datang,</h4>
											@if($data['level'] == "Pegawai")
						                    <h4>~ {{ $data['nama'] }}</h4>
											@endif
						                </p>
						            </div>
						        </div>
						    </div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection

@section('custom_js')
<!-- plugin pnotify -->
<script src="{{ asset('js/pnotify.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		if("{{ Session::has('success_message') }}"){
			successMessage("{{ session('success_message') }}");
		}
		if("{{ Session::has('error_message') }}"){
			errorMessage("{{ session('error_message') }}");
		}
	});
</script>
@endsection