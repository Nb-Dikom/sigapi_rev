<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>RESEARCH</title>

    <!-- ArcGis css -->
<link rel="stylesheet" href="https://js.arcgis.com/4.6/esri/css/main.css">
<style>
    .frame-map
    {
        width: 100%;
        height: 520px;
        border: 1px solid #A9BF04;
        margin-bottom: 5px;
    }
</style>
</head>
<body>
<div id="frame-map" style="height: 580px; width:100%"></div>
<span id="info" name="info" style="position:absolute; left:25px; bottom:15%; color:#000; z-index:50;"></span>
<script src="{{ asset('assets/robust/app-assets/js/core/libraries/jquery.min.js') }}" type="text/javascript"></script>
<script src="https://js.arcgis.com/4.6/"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        loadMap();

        function loadMap(data){
            require([
            "esri/Map",
            "esri/views/MapView",
            "esri/layers/FeatureLayer",
            "esri/geometry/Point",
            "esri/geometry/SpatialReference",
            "esri/tasks/GeometryService",
            "esri/tasks/support/DistanceParameters",
            "dojo/dom",
            "dojo/domReady!"
            ], function(
                Map, MapView,
                FeatureLayer,
                Point, SpatialReference,
                GeometryService, DistanceParameters,
                dom) {

                var firstLayer = new FeatureLayer({
                    fields: [{
                        name: "ObjectID",
                        alias: "ObjectID",
                        type: "oid"
                    },{
                        name: "nama",
                        alias: "Nama",
                        type: "string"
                    }],
                    objectIdField: "ObjectID",
                    geometryType: "point",
                    spatialReference: { wkid: 4326 },
                    source: [{
                        geometry: {
                            type: "point",
                            longitude: 106.7500186,
                            latitude: -6.3198743
                        },
                        attributes:{
                            "ObjectID": 0,
                            "nama": "Point 0"
                        }
                    }],
                    renderer: {
                        type: "simple",
                        symbol: {
                            type: "simple-marker",
                            size: 6,
                            color: "blue"
                        }
                    }
                });
                console.log(firstLayer.source);

                var secondLayer = new FeatureLayer({
                    fields: [{
                        name: "ObjectID",
                        alias: "ObjectID",
                        type: "oid"
                    },{
                        name: "nama",
                        alias: "Nama",
                        type: "string"
                    }],
                    objectIdField: "ObjectID",
                    geometryType: "point",
                    spatialReference: { wkid: 4326 },
                    source: [{
                        geometry: {
                            type: "point",
                            longitude: 106.7514703,
                            latitude: -6.316743
                        },
                        attributes:{
                            "ObjectID": 0,
                            "nama": "Point 0"
                        }
                    }],
                    renderer: {
                        type: "simple",
                        symbol: {
                            type: "simple-marker",
                            size: 6,
                            color: "orange"
                        }
                    }
                });

                var map = new Map({
                    basemap: "osm",
                });
                map.add(firstLayer);
                map.add(secondLayer);

                var view = new MapView({
                    center: [106.7500186,-6.3198743],
                    container: "frame-map",
                    map: map,
                    zoom: 15,
                });

                var start = new Point(106.7500186, -6.3198743, new SpatialReference({ "wkid": 4326 }))
                var end = new Point(106.7514703, -6.316743, new SpatialReference({ "wkid": 4326 }))

                distanceParam = new DistanceParameters({
                    geodesic: true,
                    distanceUnit: "meters",
                    geometry1: start,
                    geometry2: end
                });


                // geoService = new GeometryService({
                //     url: "https://sampleserver6.arcgisonline.com/arcgis/rest/services/Utilities/Geometry/GeometryServer"
                // });

                // geoService.distance(distanceParam).then(function(result){
                //     console.log(result);
                // });

                // view.when(function(){
                //     $('#frame-loader').remove();
                // }, function(errback){
                //     alert(errback);
                // });

            });
        }
    });
</script>
</body>
</html>
