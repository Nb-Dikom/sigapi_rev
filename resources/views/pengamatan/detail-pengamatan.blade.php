@extends('layouts._app')
@section('content')
<div class="app-content content container-fluid">
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-xs-12 mb-1">
				<h2 class="content-header-title">Pengamatan</h2>
			</div>
			<div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
				<div class="breadcrumb-wrapper col-xs-12">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{ route('pengamatan_page') }}">Pengamatan</a></li>
						<li class="breadcrumb-item active"> {{ $pengamatan->nama_pengamatan }}</li>
					</ol>
				</div>
			</div>
		</div>
		<div class="content-body">
			<section>
				<div class="row">
					<div class="col-md-10 offset-md-1">
						<div class="card">
							<div class="card-header" style="margin-bottom: 5px">
								<h4 class="card-title">{{ $pengamatan->nama_pengamatan }}</h4>
							</div>
							<div class="card-block pt-0">
                                <section class="text-xs-center">
									<?php
										$lower_name = strtolower(str_replace(" ", "+", $pengamatan->nama_pengamatan));
									?>
                                    <div class="text-xs-right mb-2">
                                        <span class="text-muted mr-1"><i class="icon-calendar4"></i> {{ date('D, d M Y', strtotime($pengamatan->tanggal_pengamatan)) }}</span>
										<a class="btn btn-sm btn-default" href="{{ url('/pengamatan/download') .'/'. $lower_name }}"><i class="icon-download"></i></a>
                                    </div>
                                    <img src="{{ asset('upload/pengamatan').'/'.$pengamatan->foto_pengamatan }}" alt="{{ 'Image'. $pengamatan->nama_pengamatan }}" class="img-responsive">
                                    <!-- <hr> -->
                                    <!-- <a href="{{ url()->previous() }}" class="btn btn-default">Close</a> -->
                                </section>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection