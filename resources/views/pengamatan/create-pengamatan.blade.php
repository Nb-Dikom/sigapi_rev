@extends('layouts._app')
@section('custom_css')
<!-- plugin notify -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/pnotify.min.css') }}">
<!-- bootstrap-daterangepicker css -->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/datepicker/daterangepicker.css') }}">
@endsection

@section('content')
<div class="app-content content container-fluid">
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-xs-12 mb-1">
				<h2 class="content-header-title">Pengamatan</h2>
			</div>
			<div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
				<div class="breadcrumb-wrapper col-xs-12">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{ route('pengamatan_page') }}">Pengamatan</a></li>
						<li class="breadcrumb-item active"> Tambah Pengamatan</li>
					</ol>
				</div>
			</div>
		</div>
		<div class="content-body">
			<section>
				<div class="row">
					<div class="col-md-6 offset-md-3">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">Tambah Pengamatan</h4>
								<a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
								<div class="heading-elements">
									<ul class="list-inline mb-0">
										<li><a data-action="expand"><i class="icon-expand2"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="card-block">
								<form class="form" method="POST" action="{{ route('save_pengamatan') }}" enctype="multipart/form-data">
									{!! csrf_field() !!}
									<div class="form-body">
										<div class="form-group {{ $errors->has('nama') ? ' has-error' : '' }}">
											<label for="nama">Nama Pengamatan</label>
											<div class="position-relative has-icon-left">
												<input type="text" id="nama" class="form-control" placeholder="masukkan nama pengamatan" name="nama"  required="" value="{{ old('nama') }}">
												<div class="form-control-position">
													<i class="icon-image"></i>
												</div>
											</div>
											@if($errors->has('nama'))
											<span class="help-block">
												<strong class="pink">{{ $errors->first('nama') }}</strong>
											</span>
											@endif
										</div>
										<div class="form-group">
											<label for="gunung">Gunung</label>
											<select id="gunung" name="gunung" class="form-control">
												<option value="none" selected="" disabled="">Pilih Gunung</option>
												@foreach($list_gunung as $gunung)
												@if(old('gunung') == null)
												<option value="{{ $gunung->gunung}}">{{ $gunung->gunung }}</option>
												@else
												@if($gunung->gunung == old('gunung'))
													<option selected value="{{ $gunung->gunung }}">{{ $gunung->gunung }}</option>
												@else
													<option value="{{ $gunung->gunung }}">{{ $gunung->gunung }}</option>
												@endif
												@endif
												@endforeach
											</select>
										</div>
										<div class="form-group {{ $errors->has('file') ? 'has-error' : '' }}">
											<label>File Pengamatan</label>
											<label id="file" class="file center-block">
												<input type="file" id="file" name="file">
												<span class="file-custom"></span>
											</label>
										</div>
										@if($errors->has('file'))
										<span class="help-block">
											<strong class="pink">{{ $errors->first('file') }}</strong>
										</span>
										@endif
										<div class="form-group {{ $errors->has('tanggal') ? 'has-error' : '' }}">
											<label>Tanggal Pengamatan</label>
											<div class="position-relative has-icon-left">
												<input type="text" id="tanggal" class="form-control" name="tanggal" placeholder="masukkan tanggal pengamatan"  required="" value="{{old('tanggal')}}" required="">
												<div class="form-control-position">
													<i class="icon-calendar"></i>
												</div>
											</div>
										</div>
										@if($errors->has('tanggal'))
										<span class="help-block">
											<strong class="pink">{{ $errors->first('tanggal') }}</strong>
										</span>
										@endif
									</div>
									<div class="form-actions center">
										<a href="{{ route('pengamatan_page') }}" class="btn btn-warning mr-1"><i class="icon-cross2"></i> Batal</a>
										<button type="submit" class="btn btn-info"><i class="icon-check2"></i> Simpan</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection

@section('custom_js')
	<!-- plugin pnotify -->
	<script src="{{ asset('js/pnotify.min.js') }}"></script>
	<!-- bootstrap-daterangepicker -->
	<script src="{{ asset('assets/moment/moment.min.js') }}"></script>
	<script src="{{ asset('assets/datepicker/daterangepicker.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('input[name="tanggal"]').daterangepicker({
			        singleDatePicker: true,
			        calender_style: "picker_3",
                    format: 'DD/MM/YYYY'
			    }, 
			    function(start, end, label) {
			        console.log("You are choose ");
			});
			if("{{ Session::has('error_message') }}"){
				errorMessage("{{ session('error_message') }}");
			}
			if("{{ $errors->first() }}"){
				errorMessage("data pengamatan tidak disimpan");
			}
		});
	</script>
@endsection