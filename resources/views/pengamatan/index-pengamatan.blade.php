@extends('layouts._app')
@section('custom_css')
<!-- Datatables css -->
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">

<!-- plugin notify -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/pnotify.min.css') }}">
@endsection

@section('content')
<div class="app-content content container-fluid">
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-6 col-xs-12 mb-1">
				<h2 class="content-header-title">Pengamatan</h2>
			</div>
			<div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
				<div class="breadcrumb-wrapper col-xs-12">
					<ol class="breadcrumb">
						<li class="breadcrumb-item active">Pengamatan</li>
					</ol>
				</div>
			</div>
		</div>
		<div class="content-body">
			<section>
				<div class="row">
					@if($jabatan !== "Sub Bagian Umum dan Kepegawaian" && $jabatan !== "Sub Bagian Penanggulangan Bencana")
					<div class="col-xs-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">Pengamatan</h4>
							</div>
							<div class="card-block">
								<div style="margin-bottom:15px">
									<a href="{{ route('form_create_pengamatan') }}" class="btn btn-success" title="Tambah Data" data-toggle="tooltip" data-placement="bottom" data-trigger="hover">Tambah <i class="icon-images2"></i></a>
								</div>
								<div class="table-responsive">
									<table class="table table-hover table-striped table-file">
										<thead>
											<tr>
												<th style="width:8%">No.</th>
												<th>Nama</th>
												<th>Gunung</th>
												<th>Verifikasi</th>
												@if($jabatan !== "Kepala Seksi Kesiapsiagaan")
												<th>Validasi</th>
												@endif
												<th>Tanggal</th>
												<th>Oleh</th>
												<th class="text-xs-center" style="width:15%">Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php $index=0;?>
									    	@foreach($list_pengamatan as $pengamatan)
									    	<?php
												$lower_name = strtolower(str_replace(" ", "+", $pengamatan->nama_pengamatan));
												$index++;
											?>
											<tr>
				                                <th scope="row">{{ $index }}.</th>
				                                <td>{{ $pengamatan->nama_pengamatan }}</td>
				                                <td>{{ $pengamatan->gunung }}</td>
				                                <td>
				                                	<span name="status-verifikasi-{{ $index }}" @if($pengamatan->verifikasi == 'Pending') class="tag tag-warning"
		                                			@elseif($pengamatan->verifikasi == 'Tidak Lengkap') class="tag tag-danger"
		                                			@elseif($pengamatan->verifikasi == 'Lengkap') class="tag tag-success" @endif >
				                                	{{ $pengamatan->verifikasi }}</span>
				                                </td>
												@if($jabatan !== "Kepala Seksi Kesiapsiagaan")
				                                <td>
				                                	<span name="status-validasi-{{ $index }}" @if($pengamatan->validasi == 'Pending') class="tag tag-warning"
		                                			@elseif($pengamatan->validasi == 'Tidak Valid') class="tag tag-danger"
		                                			@elseif($pengamatan->validasi == 'Valid') class="tag tag-success" @endif >
				                                	{{ $pengamatan->validasi }}</span>
				                                </td>
												@endif
				                                <td>{{ date('D, d M Y', strtotime($pengamatan->tanggal_pengamatan)) }}</td>
				                                <td>{{ $pengamatan->nama_pegawai }}</td>
				                                <td class="text-xs-center">
													<ul class="list-inline">
														<li>
						                                	<button index="{{ $index }}" value="{{ $lower_name }}" class="btn btn-sm btn-info" name="lihat" title="Lihat Data" data-toggle="tooltip" data-placement="bottom" data-trigger="hover"><i class="icon-eye6"></i></button>
														</li>
														<li>
						                                	<a href="{{ route('form_edit_pengamatan', $lower_name) }}" class="btn btn-sm btn-warning" name="edit" title="Ubah Data" data-toggle="tooltip" data-placement="bottom" data-trigger="hover"><i class="icon-edit2"></i></a>
														</li>
														<li>
						                                	<button value="{{ $lower_name }}" class="btn btn-sm btn-danger" name="hapus" title="Hapus Data" data-toggle="tooltip" data-placement="bottom" data-trigger="hover"><i class="icon-android-delete"></i></button>
														</li>
													</ul>
				                                </td>
				                            </tr>
				                            @endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					@else
					<div class="col-xs-12">
						<div class="card mb-1">
							<div class="card-header">
								<h4 class="card-title">Pengamatan</h4>
							</div>
						</div>
					</div>
					@foreach($list_pengamatan as $pengamatan)
					<?php
							$lower_name = strtolower(str_replace(" ", "+", $pengamatan->nama_pengamatan));
					?>
					<div class="col-lg-3 col-md-6 col-xs-12">
						<div class="card match-height">
							<div class="card-body">
								<div class="card-block text-xs-center pb-0">
									<a href="{{ route('view_pengamatan', $lower_name) }}">
										<img class="img-thumbnail img-fluid" src="{{ asset('upload/pengamatan') .'/'. $pengamatan->foto_pengamatan }}" alt="{{ $pengamatan->nama_pengamatan }}"/>
									</a>
									<ul class="list-unstyled text-xs-left mt-1">
										<li>
											<span class="text-muted font-small-3"><i class="icon-calendar4"></i> {{ date('D, d M Y', strtotime($pengamatan->tanggal_pengamatan)) }}</span>
										</li>
										<li>
											<span class="text-muted font-small-3"><i class="icon-tag"></i> {{ $pengamatan->gunung }}</span>
										</li>
										<li>
											<a class="btn btn-default btn-sm" href="{{ url('/pengamatan/download') .'/'. $lower_name }}"><i class="icon-download"></i> Unduh</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					@endforeach
					<div class="col-md-12">
						<div class="text-xs-center">
							<ul class="pagination">
								<li class="page-item @if($list_pengamatan->previousPageUrl() == null) disabled @endif">
									<a class="page-link" href="@if($list_pengamatan->previousPageUrl() == null) javascript:void(0); @else {{ $list_pengamatan->previousPageUrl() }} @endif" aria-label="Previous">
										<span aria-hidden="true">« Prev</span>
										<span class="sr-only">Previous</span>
									</a>
								</li>
								@for($i = 1; $i <= ($list_pengamatan->lastPage()); $i++)
								<li class="page-item @if($list_pengamatan->currentPage() == $i) active @endif"><a class="page-link" href="@if($list_pengamatan->currentPage() == $i) javascript:void(0); @else {{ $list_pengamatan->url($i) }} @endif">{{ $i }}</a></li>
								@endfor
								<li class="page-item @if($list_pengamatan->nextPageUrl() == null) disabled @endif">
									<a class="page-link" href="@if($list_pengamatan->nextPageUrl() == null) javascript:void(0); @else {{ $list_pengamatan->nextPageUrl() }} @endif" aria-label="Next">
									<span aria-hidden="true">Next »</span>
										<span class="sr-only">Next</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
					@endif
				</div>
			</section>
			@if($jabatan !== "Sub Bagian Umum dan Kepegawaian" && $jabatan !== "Sub Bagian Penanggulangan Bencana")
			<div id="modal-lihat" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
		        <div class="modal-dialog modal-md" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title">
								<i class="icon-binoculars"></i> <span name="nama"></span>
							</h4>
							<span class="text-muted">Tanggal pengamatan: </span><span name="tanggal"></span>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-md-12">
									<img name="file" class="card-img-top img-fluid" src="" alt="">
									@if($jabatan !== "Pengolah Data Pra Bencana")
									<div class="text-sm-center mt-5">
										@if($jabatan !== "Kepala Bidang Pencegahan dan Kesiapsiagaan")
										<ul class="list-inline verified">
											<li>
												<button index="" value="" class="btn btn-danger" name="tidak_lengkap" title="Tidak Lengkap" data-toggle="tooltip" data-placement="bottom" data-trigger="hover"><i class="icon-cross2"></i> Tidak Lengkap</button>
											</li>
											<li>
												<button index="" value="" class="btn btn-success" name="lengkap" title="Lengkap" data-toggle="tooltip" data-placement="bottom" data-trigger="hover"><i class="icon-check2"></i> Lengkap</button>
											</li>
										</ul>
										@endif
										@if($jabatan !== "Kepala Seksi Kesiapsiagaan")
										<ul class="list-inline validated">
											<li>
												<button index="" value="" class="btn btn-danger" name="tidak_valid" title="Tidak Valid" data-toggle="tooltip" data-placement="bottom" data-trigger="hover"><i class="icon-cross2"></i> Tidak Valid</button>
											</li>
											<li>
												<button index="" value="" class="btn btn-success" name="valid" title="Valid" data-toggle="tooltip" data-placement="bottom" data-trigger="hover"><i class="icon-check2"></i> Valid</button>
											</li>
										</ul>
										@endif
									</div>
									@endif
									<hr>
									<div class="text-sm-right">
										<button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
		    </div>
			<!-- Modal Delete -->
			<div class="modal fade text-xs-left" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
			@include('layouts._delete')
			</div>
			@endif
		</div>
	</div>
</div>
@endsection

@section('custom_js')
<!-- Datatables js  -->
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<!-- plugin pnotify -->
<script src="{{ asset('js/pnotify.min.js') }}"></script>
<!-- moment -->
<script src="{{ asset('assets/moment/moment.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		@if($jabatan !== "Sub Bagian Umum dan Kepegawaian" && $jabatan !== "Sub Bagian Penanggulangan Bencana")
		$('.table-file').DataTable();

		$('.table-responsive').on('click', 'button[name="lihat"]', function(){
			var id = $(this).attr('value');
			var index = $(this).attr('index');
			$.ajax({
                url: '{{ url("/pengamatan") }}'+'/'+id,
                type: 'GET',
                beforeSend: function () {
                    //
                    startLoader()
                },
                success: function(response){
					$('span[name="nama"]').html('');
            		$('img[name="file"]').attr('src', '');
            		$('img[name="file"]').attr('alt', '');
            		$('span[name="tanggal"]').html('');

            		$('span[name="nama"]').html(response.nama_pengamatan);
            		$('img[name="file"]').attr('src', "{{ asset('upload/pengamatan') }}"+'/'+response.foto_pengamatan);
            		$('img[name="file"]').attr('alt', response.nama_pengamatan);
            		$('span[name="tanggal"]').html(moment(response.tanggal_pengamatan).format('DD MMMM YYYY'));
					switch (response.verifikasi) {
						case "Pending":
							$('.verified').show();
							$('.validated').hide();
							$('button[name="tidak_lengkap"]').val(response.lower_name);
							$('button[name="tidak_lengkap"]').attr("index", index);
							$('button[name="lengkap"]').val(response.lower_name);
							$('button[name="lengkap"]').attr("index", index);
							break;
						case "Tidak Lengkap":
							$('.verified').hide();
							$('.validated').hide();
							break;
					
						default:
							$('.verified').hide();
							if(response.validasi != "Pending"){ $('.validated').hide(); }
							else
							{
								$('.validated').show();
								$('button[name="tidak_valid"]').val(response.lower_name);
								$('button[name="tidak_valid"]').attr("index", index);
								$('button[name="valid"]').val(response.lower_name);
								$('button[name="valid"]').attr("index", index);
							};
							break;
					}
					
					endLoader("lihat");
                },
                error: function () {
                	endLoader("alert");
                }
            });
		});
		@if($jabatan !== "Pengolah Data Pra Bencana")
		@if($jabatan !== "Kepala Bidang Pencegahan dan Kesiapsiagaan")
		$('#modal-lihat').on('click', 'button[name="lengkap"]', function(){
			var index = $(this).attr('index');
        	$.ajaxSetup({
                headers:{
                    'X-CSRF-TOKEN': $('input[name="_token"]').val()
                }
            });
            $.ajax({
                url: "{{ route('verified_pengamatan') }}",
                type: 'PATCH',
                data: {'name': $(this).attr('value')},
                beforeSend: function () {
                    startLoader()
                },
                success: function(response){
					status = response.data.verifikasi;
					if(status == "Lengkap")
					{
                		$('span[name="status-verifikasi-'+index+'"]').attr('class', 'tag tag-success');
					}
                	$('span[name="status-verifikasi-'+index+'"]').html(status);
					$('#modal-lihat').modal('hide');
                	endLoader();
                },
                error: function () {
                	endLoader("alert");
                }
            });
        });
		$('#modal-lihat').on('click', 'button[name="tidak_lengkap"]', function(){
			var index = $(this).attr('index');
        	$.ajaxSetup({
                headers:{
                    'X-CSRF-TOKEN': $('input[name="_token"]').val()
                }
            });
            $.ajax({
                url: "{{ route('unverified_pengamatan') }}",
                type: 'PATCH',
                data: {'name': $(this).attr('value')},
                beforeSend: function () {
                    startLoader()
                },
                success: function(response){
                	status = response.data.verifikasi;
					if(status == "Tidak Lengkap")
					{
                		$('span[name="status-verifikasi-'+index+'"]').attr('class', 'tag tag-danger');
					}
                	$('span[name="status-verifikasi-'+index+'"]').html(status);
					$('#modal-lihat').modal('hide');
                	endLoader();
                },
                error: function () {
                	endLoader("alert");
                }
            });
        });
		@endif
		@if($jabatan !== "Kepala Seksi Kesiapsiagaan")
		$('#modal-lihat').on('click', 'button[name="valid"]', function(){
			var index = $(this).attr('index');
        	$.ajaxSetup({
                headers:{
                    'X-CSRF-TOKEN': $('input[name="_token"]').val()
                }
            });
            $.ajax({
                url: "{{ route('validated_pengamatan') }}",
                type: 'PATCH',
                data: {'name': $(this).attr('value')},
                beforeSend: function () {
                    startLoader()
                },
                success: function(response){
					status = response.data.validasi;
					if(status == "Valid")
					{
                		$('span[name="status-validasi-'+index+'"]').attr('class', 'tag tag-success');
					}
                	$('span[name="status-validasi-'+index+'"]').html(status);
					$('#modal-lihat').modal('hide');
                	endLoader();
                },
                error: function () {
                	endLoader("alert");
                }
            });
        });
		$('#modal-lihat').on('click', 'button[name="tidak_valid"]', function(){
			var index = $(this).attr('index');
        	$.ajaxSetup({
                headers:{
                    'X-CSRF-TOKEN': $('input[name="_token"]').val()
                }
            });
            $.ajax({
                url: "{{ route('unvalidated_pengamatan') }}",
                type: 'PATCH',
                data: {'name': $(this).attr('value')},
                beforeSend: function () {
                    startLoader()
                },
                success: function(response){
                	status = response.data.validasi;
					if(status == "Tidak Valid")
					{
                		$('span[name="status-validasi-'+index+'"]').attr('class', 'tag tag-danger');
					}
                	$('span[name="status-validasi-'+index+'"]').html(status);
					$('#modal-lihat').modal('hide');
                	endLoader();
                },
                error: function () {
                	endLoader("alert");
                }
            });
        });
		@endif
		@endif
		$('.table-responsive').on('click', 'button[name="hapus"]', function(){
			var id = $(this).attr('value');

	        modal_delete("{{ url('/pengamatan') }}" +'/'+id);
		});
		@endif

		if("{{ Session::has('success_message') }}"){
			successMessage("{{ session('success_message') }}");
		}
		if("{{ Session::has('error_message') }}"){
			errorMessage("{{ session('error_message') }}");
		}
	});
</script>
@endsection
