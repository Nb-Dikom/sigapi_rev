@extends('layouts._app_frontend')
@section('custom_css')
<style>
    a.btn-transparent {
        color: #7c858e;
        font-size: 15px;
        padding: 6px 14px;
        white-space: nowrap;
        text-decoration: none;
        border: solid 1px #7c858e;
        background: none;
    }
</style>
@endsection
@section('content')
<div class="container">
    <ul class="breadcrumb">
        <li><a href="{{ url('/') }}">Beranda</a></li>
        <li class="active">Peta</li>
    </ul>
    <div class="row margin-bottom-40">
        <div class="col-md-12 col-sm-12">
            <div class="content-page">
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <div class="pricing">
                            <div class="pricing-head">
                                <h3>Kawasan<br>Rawan Bencana</h3>
                                <h4><i class="fa fa-exclamation-triangle" style="font-size:52px !important"></i></h4>
                            </div>
                            <div class="pricing-footer">
                                <a class="btn btn-primary" href="{{ url('/peta-bencana/krb') }}" style="margin-top:20px">Lihat</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <div class="pricing">
                            <div class="pricing-head">
                                <h3>Jalur<br> Evakuasi</h3>
                                <h4><i class="fa fa-map-signs" style="font-size:52px !important"></i></h4>
                            </div>
                            <div class="pricing-footer">
                                <a class="btn btn-primary" href="{{ url('/peta-bencana/jalur-evakuasi') }}" style="margin-top:20px">Lihat</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('custom_js')
<script src="{{ asset('assets/corporate/scripts/layout.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        Layout.init();    
    });
</script>
@endsection