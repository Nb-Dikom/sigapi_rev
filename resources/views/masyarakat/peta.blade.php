@extends('layouts._app-masyarakat')
@section('custom_css')
<!-- ArcGis css -->
<link rel="stylesheet" href="https://js.arcgis.com/4.6/esri/css/main.css">
@endsection
@section('content')
    <div class="col-lg-12 col-md-12 col-xs-12">
		<div class="mt-1">
			<a class="col-cyan" href="{{ route('peta_page') }}">Arcgis Online</a> |
			<a class="col-cyan" href="{{ route('mapguide_page') }}">Mapguide</a>
		</div>
        <div class="remove row pr-3 pl-3 mt-3">
            <div class="col-md-4 offset-md-4 col-xs-10 offset-xs-1">
                <div class="card border-grey border-lighten-3 px-1 py-1 box-shadow-3">
                    <div class="card-block">
                        <span class="card-title text-xs-center">
                            <img src="{{ asset('img/sigapi-logo-big.png') }}" class="img-fluid mx-auto d-block" width="200" alt="Logo SIGAPI">
                        </span>
                    </div>
                    <div class="card-block text-xs-center">
                        <div><img style="width:40%" src="{{ asset('img/loader-gear.gif') }}" alt="loading"></div>
                        <div class="no-connectivy display-hidden">
                            <h4>Uups... tidak ada konektivitas</h4>
                            <p>Harap periksa jaringan anda</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="content-map" class="display-hidden">
            <div id="frame-map" style="height: 580px; width:100%"></div>
            <span id="info" name="info" style="position:absolute; right:25px; bottom:25px; color:#000; z-index:50;"></span>
        </div>
    </div>
@endsection
@section('custom_js')
<!-- Google Maps API -->
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCuLC7TFn55BT4zrUsXy3V714WlzEs034c"></script> -->
<!-- ArcGis js -->
<script src="https://js.arcgis.com/4.6/"></script>
<script type="text/javascript">
    $(document).ready(function(){       
		$.ajax({
			url: 'http://www.google.com',
			type: 'GET',
			success: function() {
				$.ajax({
					url: "{{ route('load_peta_page') }}",
					type: "GET",

					success: function(result){
						$('.remove').remove();
						$('#content-map').removeClass('display-hidden');

						var features_point = [];
						var features_polygon = [];
						$.each(result['point'], function(key, value){
							features_point[key] = value;
						});
						$.each(result['polygon'], function(key, value){
							features_polygon[value.jenis_krb] = value;
						});
						loadMap(features_point, features_polygon);
					},
					error: function () {
						$('.no-connectivy').removeClass('display-hidden');
					}
				});
			},
			error: function() {
				$('.no-connectivy').removeClass('display-hidden');
			}
		});
    });
</script>
<script>
    function loadMap(point, polygon){
        require([
		"esri/Map",
		"esri/views/MapView",
		"esri/Graphic",
		"esri/layers/FeatureLayer",
		"esri/widgets/Legend",
		"esri/widgets/ScaleBar",
        "esri/renderers/UniqueValueRenderer",
		"esri/geometry/support/webMercatorUtils",
		"esri/layers/GroupLayer",
		"esri/widgets/LayerList",
		"esri/widgets/BasemapGallery",
		"esri/widgets/Home",
		"esri/widgets/Compass",
		"esri/widgets/Expand",
		"esri/widgets/Directions",
		"dojo/dom",
		"dojo/domReady!"
		], function(
			Map, MapView, Graphic, FeatureLayer, Legend, ScaleBar, UniqueValueRenderer, 
			webMercatorUtils, GroupLayer, LayerList, BasemapGallery, Home, Compass,
			Expand, 
			Directions,
			 dom) {
			var indexRenderer = new UniqueValueRenderer({
                field: "jenis_fasilitas",
                uniqueValueInfos: [{
                    value: "Kantor Polisi",
                    symbol: {
						type: "picture-marker",
						url: 'http://localhost/sigapi_rev/public/img/icon/1.png',
						width: "24px",
						height: "24px"
					}
                },{
                    value: "Kantor Pemadam Kebakaran",
                    symbol: {
						type: "picture-marker",
						url: 'http://localhost/sigapi_rev/public/img/icon/2.png',
						width: "24px",
						height: "24px"
					}
                },{
                    value: "Kantor Pemerintahan",
                    symbol: {
						type: "picture-marker",
						url: 'http://localhost/sigapi_rev/public/img/icon/3.png',
						width: "24px",
						height: "24px"
					}
                },{
                    value: "Pertokoan",
                    symbol: {
						type: "picture-marker",
						url: 'http://localhost/sigapi_rev/public/img/icon/4.png',
						width: "24px",
						height: "24px"
					}
                },{
                    value: "Pusat Perbelanjaan",
                    symbol: {
						type: "picture-marker",
						url: 'http://localhost/sigapi_rev/public/img/icon/5.png',
						width: "24px",
						height: "24px"
					}
                },{
                    value: "Pasar Tradisional",
                    symbol: {
						type: "picture-marker",
						url: 'http://localhost/sigapi_rev/public/img/icon/6.png',
						width: "24px",
						height: "24px"
					}
                },{
                    value: "Tempat Rekreasi (Buatan dan Alami di Pegunungan)",
                    symbol: {
						type: "picture-marker",
						url: 'http://localhost/sigapi_rev/public/img/icon/7.png',
						width: "24px",
						height: "24px"
					}
                },{
                    value: "Terminal Bis",
                    symbol: {
						type: "picture-marker",
						url: 'http://localhost/sigapi_rev/public/img/icon/8.png',
						width: "24px",
						height: "24px"
					}
                },{
                    value: "Stasiun Kereta Api",
                    symbol: {
						type: "picture-marker",
						url: 'http://localhost/sigapi_rev/public/img/icon/9.png',
						width: "24px",
						height: "24px"
					}
                },{
                    value: "Posko (Rupusdalops)",
                    symbol: {
						type: "picture-marker",
						url: 'http://localhost/sigapi_rev/public/img/icon/10.png',
						width: "24px",
						height: "24px"
					}
                },{
                    value: "Rumah Sakit",
                    symbol: {
						type: "picture-marker",
						url: 'http://localhost/sigapi_rev/public/img/icon/11.png',
						width: "24px",
						height: "24px"
					}
                },{
                    value: "Puskesmas",
                    symbol: {
						type: "picture-marker",
						url: 'http://localhost/sigapi_rev/public/img/icon/12.png',
						width: "24px",
						height: "24px"
					}
                }]
            });

			var fieldsPoint = [
				{
					name: "ObjectID",
					alias: "ObjectID",
					type: "oid"
				},
				{
					name: "nama_fasilitas",
					alias: "Nama",
					type: "string"
				},
				{
					name: "alamat_fasilitas",
					alias: "Alamat",
					type: "string"
				},
				{
					name: "jenis_fasilitas",
					alias: "Jenis",
					type: "string"
				},
				{
					name: "gunung",
					alias: "Gunung",
					type: "string"
				},
				{
					name: "longitude",
					alias: "Longitude",
					type: "string"
				},
				{
					name: "latitude",
					alias: "Latitude",
					type: "string"
				},
			];

			var fieldsPolygon = [
				{
					name: "ObjectID",
					alias: "ObjectID",
					type: "oid"
				},
				{
					name: "jenis_rawan",
					alias: "Jenis",
					type: "string"
				},
				{
					name: "gunung",
					alias: "Gunung",
					type: "string"
				},
			];

			var templatePopupPoint = {
				title: "{nama_fasilitas}",
				content: [{
					type: "fields",
					fieldInfos: [{
						fieldName: "nama_fasilitas"
						}, {
						fieldName: "alamat_fasilitas"
						}, {
						fieldName: "jenis_fasilitas"
						}, {
						fieldName: "gunung"
					}],
				},{
					type: "text",
					text: '<a href="http://localhost/sigapi_rev/public/masyarakat/peta/rute?longitude='+'{longitude}'+'&latitude='+'{latitude}'+'">Dapatkan Rute</a>'
				}]
			}

			var templatePopupPolygon = {
				title: "{jenis_rawan}",
				content: [{
					type: "fields",
					fieldInfos: [{
						fieldName: "jenis_rawan"
						}, {
						fieldName: "gunung"
					}]
				}]
			}

			var layersPoint = [];
			var layersPolygon = [];
			if(point['1']){
				var polisiLayer = new FeatureLayer({
					fields: fieldsPoint,
					objectIdField: "ObjectID",
					geometryType: "point",
					spatialReference: { wkid: 4326 },
					source: point['1'],
					renderer: {
						type: "simple",
						symbol: {
                            type: "picture-marker",
                            url: 'http://localhost/sigapi_rev/public/img/icon/1.png',
                            width: "24px",
                            height: "24px"
                        }
					},
                    visible: false,
					title: 'Kantor Polisi',
					popupTemplate: templatePopupPoint
				});
				layersPoint.push(polisiLayer);
			}

			if(point['2']){
				var damkarLayer = new FeatureLayer({
					fields: fieldsPoint,
					objectIdField: "ObjectID",
					geometryType: "point",
					spatialReference: { wkid: 4326 },
					source: point['2'],
					renderer: {
						type: "simple",
						symbol: {
                            type: "picture-marker",
                            url: 'http://localhost/sigapi_rev/public/img/icon/2.png',
                            width: "24px",
                            height: "24px"
                        }
					},
					visible: false,
					title: 'Kantor Pemadam Kebakaran',
					popupTemplate: templatePopupPoint
				});
				layersPoint.push(damkarLayer);
			}

			if(point['3']){
				var pemerintahanLayer = new FeatureLayer({
					fields: fieldsPoint,
					objectIdField: "ObjectID",
					geometryType: "point",
					spatialReference: { wkid: 4326 },
					source: point['3'],
					renderer: {
						type: "simple",
						symbol: {
                            type: "picture-marker",
                            url: 'http://localhost/sigapi_rev/public/img/icon/3.png',
                            width: "24px",
                            height: "24px"
                        }
					},
					visible: false,
					title: 'Kantor Pemerintahan',
					popupTemplate: templatePopupPoint
				});
				layersPoint.push(pemerintahanLayer);
			}
			
			if(point['4']){
				var situsLayer = new FeatureLayer({
					fields: fieldsPoint,
					objectIdField: "ObjectID",
					geometryType: "point",
					spatialReference: { wkid: 4326 },
					source: point['4'],
					renderer: {
						type: "simple",
						symbol: {
                            type: "picture-marker",
                            url: 'http://localhost/sigapi_rev/public/img/icon/4.png',
                            width: "24px",
                            height: "24px"
                        }
					},
					visible: false,
					title: 'Pertokoan',
					popupTemplate: templatePopupPoint
				});
				layersPoint.push(situsLayer);
			}

			if(point['5']){
				var perbelanjaanLayer = new FeatureLayer({
					fields: fieldsPoint,
					objectIdField: "ObjectID",
					geometryType: "point",
					spatialReference: { wkid: 4326 },
					source: point['5'],
					renderer: {
						type: "simple",
						symbol: {
                            type: "picture-marker",
                            url: 'http://localhost/sigapi_rev/public/img/icon/5.png',
                            width: "24px",
                            height: "24px"
                        }
					},
					visible: false,
					title: 'Pusat Perbelanjaan',
					popupTemplate: templatePopupPoint
				});
				layersPoint.push(perbelanjaanLayer);
			}

			if(point['6']){
				var tradisionalLayer = new FeatureLayer({
					fields: fieldsPoint,
					objectIdField: "ObjectID",
					geometryType: "point",
					spatialReference: { wkid: 4326 },
					source: point['6'],
					renderer: {
						type: "simple",
						symbol: {
                            type: "picture-marker",
                            url: 'http://localhost/sigapi_rev/public/img/icon/6.png',
                            width: "24px",
                            height: "24px"
                        }
					},
					visible: false,
					title: 'Pasar Tradisional',
					popupTemplate: templatePopupPoint
				});
				layersPoint.push(tradisionalLayer);
			}

			if(point['7']){
				var rekreasiLayer = new FeatureLayer({
					fields: fieldsPoint,
					objectIdField: "ObjectID",
					geometryType: "point",
					spatialReference: { wkid: 4326 },
					source: point['7'],
					renderer: {
						type: "simple",
						symbol: {
                            type: "picture-marker",
                            url: 'http://localhost/sigapi_rev/public/img/icon/7.png',
                            width: "24px",
                            height: "24px"
                        }
					},
					visible: false,
					title: 'Tempat Rekreasi (Buatan dan Alami di Pegunungan)',
					popupTemplate: templatePopupPoint
				});
				layersPoint.push(rekreasiLayer);
			}

			if(point['8']){
				var terminalLayer = new FeatureLayer({
					fields: fieldsPoint,
					objectIdField: "ObjectID",
					geometryType: "point",
					spatialReference: { wkid: 4326 },
					source: point['8'],
					renderer: {
						type: "simple",
						symbol: {
                            type: "picture-marker",
                            url: 'http://localhost/sigapi_rev/public/img/icon/8.png',
                            width: "24px",
                            height: "24px"
                        }
					},
					visible: false,
					title: 'Terminal Bis',
					popupTemplate: templatePopupPoint
				});
				layersPoint.push(terminalLayer);
			}

			if(point['9']){
				var stasiunLayer = new FeatureLayer({
					fields: fieldsPoint,
					objectIdField: "ObjectID",
					geometryType: "point",
					spatialReference: { wkid: 4326 },
					source: point['9'],
					renderer: {
						type: "simple",
						symbol: {
                            type: "picture-marker",
                            url: 'http://localhost/sigapi_rev/public/img/icon/9.png',
                            width: "24px",
                            height: "24px"
                        }
					},
					visible: false,
					title: 'Stasiun Kereta Api',
					popupTemplate: templatePopupPoint
				});
				layersPoint.push(stasiunLayer);
			}

			if(point['10']){
				var poskoLayer = new FeatureLayer({
					fields: fieldsPoint,
					objectIdField: "ObjectID",
					geometryType: "point",
					spatialReference: { wkid: 4326 },
					source: point['10'],
					renderer: {
						type: "simple",
						symbol: {
                            type: "picture-marker",
                            url: 'http://localhost/sigapi_rev/public/img/icon/10.png',
                            width: "24px",
                            height: "24px"
                        }
					},
					visible: false,
					title: 'Posko (Rupusdalops)',
					popupTemplate: templatePopupPoint
				});
				layersPoint.push(poskoLayer);
			}
			
			if(point['11']){
				var rumkitLayer = new FeatureLayer({
					fields: fieldsPoint,
					objectIdField: "ObjectID",
					geometryType: "point",
					spatialReference: { wkid: 4326 },
					source: point['11'],
					renderer: {
						type: "simple",
						symbol: {
                            type: "picture-marker",
                            url: 'http://localhost/sigapi_rev/public/img/icon/11.png',
                            width: "24px",
                            height: "24px"
                        }
					},
					visible: false,
					title: 'Rumah Sakit',
					popupTemplate: templatePopupPoint
				});
				layersPoint.push(rumkitLayer);
			}
			
			if(point['12']){
				var puskesmasLayer = new FeatureLayer({
					fields: fieldsPoint,
					objectIdField: "ObjectID",
					geometryType: "point",
					spatialReference: { wkid: 4326 },
					source: point['12'],
					renderer: {
						type: "simple",
						symbol: {
                            type: "picture-marker",
                            url: 'http://localhost/sigapi_rev/public/img/icon/12.png',
                            width: "24px",
                            height: "24px"
                        }
					},
					title: 'Puskesmas',
					popupTemplate: templatePopupPoint
				});
				layersPoint.push(puskesmasLayer);
			}

            // if(polygon['rawan_i']){
            if(polygon['Rawan I']){
                var layerRawanI = new FeatureLayer({
                    // fields: fieldsPolygon,
                    // objectIdField: "ObjectID",
                    // geometryType: "polygon",
                    // spatialReference: { wkid: 4326 },
                    // source: polygon['rawan_i'],
					url: polygon['Rawan I'].file_krb,
                    renderer: {
                        type: "simple",
                        symbol: {
                            type: "simple-fill",
                            color: "yellow",
                            outline: {
                                color: [128, 128, 128, 0.5],
                                width: "0.5px"
                            }
                        }
                    },
                    opacity: 0.6,
                    title: 'Rawan I',
					// popupTemplate: templatePopupPolygon
                });
                layersPolygon.push(layerRawanI);
            }

            // if(polygon['rawan_ii']){
            if(polygon['Rawan II']){
                var layerRawanII = new FeatureLayer({
                    // fields: fieldsPolygon,
                    // objectIdField: "ObjectID",
                    // geometryType: "polygon",
                    // spatialReference: { wkid: 4326 },
                    // source: polygon['rawan_ii'],
					url: polygon['Rawan II'].file_krb,
                    renderer: {
                        type: "simple",
                        symbol: {
                            type: "simple-fill",
                            color: "orange",
                            outline: {
                                color: [128, 128, 128, 0.5],
                                width: "0.5px"
                            }
                        }
                    },
                    opacity: 0.6,
                    title: 'Rawan II',
					// popupTemplate: templatePopupPolygon
                });
                layersPolygon.push(layerRawanII);
            }

            // if(polygon['rawan_iii']){
            if(polygon['Rawan III']){
                var layerRawanIII = new FeatureLayer({
                    // fields: fieldsPolygon,
                    // objectIdField: "ObjectID",
                    // geometryType: "polygon",
                    // spatialReference: { wkid: 4326 },
                    // source: polygon['rawan_iii'],
					url: polygon['Rawan III'].file_krb,
                    renderer: {
                        type: "simple",
                        symbol: {
                            type: "simple-fill",
                            color: "red",
                            outline: {
                                color: [128, 128, 128, 0.5],
                                width: "0.5px"
                            }
                        }
                    },
                    opacity: 0.6,
                    title: 'Rawan III',
					// popupTemplate: templatePopupPolygon
                });
                layersPolygon.push(layerRawanIII);
            }
			
			var pointGroupLayer = new GroupLayer({
				title: "Fasilitas",
				visible: false,
				layers: layersPoint
			});

			var polygonGroupLayer = new GroupLayer({
				title: "Daerah Rawan",
				visible: true,
				layers: layersPolygon
			});
			
			var map = new Map({
				// basemap: "streets",
				basemap: "hybrid",
				layers: [polygonGroupLayer, pointGroupLayer]
			});
			// map.add(puskesmasLayer);
			var view = new MapView({
				center: [113.0209131, -8.0669649],
				container: "frame-map",
				map: map,
				zoom: 11,
				popup: {
					dockEnabled: true,
					dockOptions: {
						buttonEnabled: false,
						breakpoint: false,
						position: "top-left"
					}
				}
			});
			var home = new Home({
				view: view
			});
			var compass = new Compass({
				view: view
			});
			var scaleBar = new ScaleBar({
				view: view,
				unit: "metric"
			});
			var legend = new Legend({
				view: view,
			});
			
			var layerList = new LayerList({
				view: view,
				container: document.createElement("div")
			});
			var basemapGallery = new BasemapGallery({
				view: view,
				container: document.createElement("div")
			});
			var directions = new Directions({
				view: view,
				routeServiceUrl: "http://utility.arcgis.com/usrsvcs/servers/4ea1d05bc5314c37a3f253164797023d/rest/services/World/Route/NAServer/Route_World",
				container: document.createElement("div")
			});

			view.ui.add(legend, "bottom-right");
			view.ui.add(scaleBar, {
				position: "bottom-left"
			});
			view.ui.add(home, "top-left");
			view.ui.add(compass, "top-left");
			
			var layerListExpand = new Expand({
				view: view,
				content: layerList.domNode,
				expandIconClass: "esri-icon-layers"
			});
			var directionsExpand = new Expand({
				view: view,
				content: directions.domNode,
				expandIconClass: "esri-icon-directions"
			});
			var baseMapExpand = new Expand({
				view: view,
				content: basemapGallery.domNode,
				expandIconClass: "esri-icon-basemap"
			});
			var legendExpand = new Expand({
				view: view,
				content: legend.domNode,
				expandIconClass: "esri-icon-collection"
			});

			view.ui.add(layerListExpand, "top-right");
			view.ui.add(directionsExpand, "top-right");
			view.ui.add(baseMapExpand, "top-right");
			view.ui.add(legendExpand, "top-right");
			view.on("pointer-move", function(evt){
				var point = view.toMap({
					x: evt.x,
					y: evt.y
				});
				var longitude = point.longitude;
				var latitude = point.latitude;
				dom.byId("info").innerHTML = longitude.toString().substr(0,8) + ", " + latitude.toString().substr(0,8);
			});
		});
    }
</script>
@endsection