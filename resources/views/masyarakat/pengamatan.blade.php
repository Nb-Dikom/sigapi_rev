@extends('layouts._app-masyarakat')
@section('content')
    <div class="col-lg-12 col-md-12 col-xs-12">
        <div class="mt-2">
            <div class="row">
                @foreach($pengamatan_collection as $pengamatan)
                <div class="col-lg-3 col-md-6 col-xs-12">
                    <a name="lihat" value="{{ $pengamatan->id_pengamatan }}" class="cursor-pointer">
                        <div class="card match-height">
                            <div class="card-body">
                                <div class="card-block">
                                    <figure>
                                        <img src="{{ asset('upload/pengamatan/'.$pengamatan->foto_pengamatan) }}" class="img-thumbnail img-fluid">
                                    </figure>
                                    <div class=" text-xs-right mb-0 mt-0">
                                        <span class="text-muted font-small-2">{{ date('D, d M Y', strtotime($pengamatan->tanggal_pengamatan)) }}</span>
                                    </div>
                                    <span style="color:black">{{ $pengamatan->nama_pengamatan }}</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
        <div id="modal-lihat" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">
                            <i class="icon-binoculars"></i> <span name="nama"></span>
                        </h4>
                        <span class="text-muted">Tanggal Pengamatan: </span><span name="tanggal"></span>
                    </div>
                    <div class="modal-body">
                        <img name="file" class="card-img-top img-fluid" src="" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('custom_js')
<!-- Moment -->
<script src="{{ asset('assets/moment/moment.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.row').on('click', 'a[name="lihat"]', function(){
            var id = $(this).attr('value');
            $.ajax({
                url: '{{ url("/masyarakat/pengamatan") }}'+'/'+id,
                type: 'GET',
                beforeSend: function () {
                    //
                    startLoader()
                },
                success: function(data){
                    endLoader();
                    $('span[name="nama"]').html('');
                    $('img[name="file"]').attr('src', '');
                    $('img[name="file"]').attr('alt', '');
                    $('span[name="tanggal"]').html('');

                    $('span[name="nama"]').html(data.nama_pengamatan);
                    $('img[name="file"]').attr('src', "{{ asset('upload/pengamatan') }}"+'/'+data.foto_pengamatan);
                    $('img[name="file"]').attr('alt', data.nama_pengamatan+' img');
                    $('span[name="tanggal"]').html(moment(data.tanggal_pengamatan).format('DD MMMM YYYY'));
                    
                    $('#modal-lihat').modal('show');
                },
                error: function () {
                    endLoader();
                    alert('server not responding...');
                }
            });
        });
    });
</script>
@endsection