@extends('layouts._app-masyarakat')
@section('content')
    <div class="col-lg-12 col-md-12 col-xs-12">
        <div class="mt-2">
            <div class="row">
                @foreach($info_collection as $info)
                <div class="col-xl-4 col-md-6 col-sm-12">
                    <div class="card match-height">
                        <div class="card-body">
                            <div class="card-block">
                                <h4 class="card-title mb-0">{{ $info->nama_info }}</h4>
                                <span class="text-muted font-small-2">{{ date('D, d M Y', strtotime($info->tanggal_info)) }}</span>
                                <div class="overflow-hidden mt-1 mb-1" style="height:85px">
                                    <section class="card-text">{!! $info->konten !!}</section>
                                </div>
                                <div class=" text-xs-right mb-0">
                                    <a href="{{ route('detail_info_page', $info->id_info) }}" class="btn btn-outline-info">Selengkapnya</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection