@extends('layouts._app-masyarakat')
@section('custom_css')
<!-- ArcGis css -->
<link rel="stylesheet" href="https://js.arcgis.com/4.6/esri/css/main.css">
@endsection
@section('content')
<div class="mt-2">
    <div id="frame-map" style="height: 580px; width:100%"></div>
    <span id="info" name="info" style="position:absolute; left:15px; bottom:75px; color:#000; z-index:50;"></span>
</div>
@endsection

@section('custom_js')
<script src="https://js.arcgis.com/4.6/"></script>
<script type="text/javascript">
	$(document).ready(function(){
        response = {
            'longitude': "{{ $data['longitude'] }}",
            'latitude': "{{ $data['latitude'] }}",
        }
        loadMap(response);
    });
</script>
<script>
    function loadMap(data){
        require([
            "esri/Map",
            "esri/views/MapView",
            "esri/Graphic",
            "esri/layers/GraphicsLayer",
            "esri/tasks/RouteTask",
            "esri/tasks/support/RouteParameters",
            "esri/tasks/support/FeatureSet",
            "esri/widgets/Legend",
            "esri/widgets/ScaleBar",
            "esri/geometry/support/webMercatorUtils",
            "dojo/dom",
            "dojo/domReady!"
            ], function(Map, MapView, Graphic, GraphicsLayer, RouteTask, RouteParameters,
                FeatureSet, Legend, ScaleBar, webMercatorUtils, dom) {
                
            var map = new Map({
                basemap: "streets-navigation-vector"
            });
            var view = new MapView({
                center: [data['longitude'], data['latitude']],
                container: "frame-map",
                map: map,
                zoom: 6
            });
            var startSymbol = {
				type: "picture-marker",
				url: "http://localhost/sigapi_rev/public/img/icon/icon_start.png",
				width: "32px",
  				height: "32px"
			};
            var endSymbol = {
				type: "picture-marker",
				url: "http://localhost/sigapi_rev/public/img/icon/icon_end.png",
				width: "32px",
  				height: "32px"
			};
            var routeSymbol = {
                type: "simple-line",
                color: [0, 0, 255, 0.5],
                width: 5
            };
            var routeTask = new RouteTask({
                url: "http://utility.arcgis.com/usrsvcs/servers/4ea1d05bc5314c37a3f253164797023d/rest/services/World/Route/NAServer/Route_World"
            });
            view.then(function(){
                initFunc();
                console.log('done');
            }).otherwise(errorCallback);
            view.on("pointer-move", function(evt){
                var point = view.toMap({
                    x: evt.x,
                    y: evt.y
                });
                var longitude = point.longitude;
                var latitude = point.latitude;
                dom.byId("info").innerHTML = longitude.toString().substr(0,8) + ", " + latitude.toString().substr(0,8);
            });
            var scaleBar = new ScaleBar({
                view: view,
                unit: "metric"
            });
            view.ui.add(scaleBar, {
                position: "bottom-left"
            });
            function initFunc() {
                if( navigator.geolocation ) {
                    navigator.geolocation.getCurrentPosition(function(result){
                        var routeLyr = new GraphicsLayer();
                        var routeParams = new RouteParameters({
                            stops: new FeatureSet(),
                            directionsLengthUnits: 'kilometers',
                            returnDirections: true,
                            findBestSequence: true,
                            outSpatialReference: {
                                wkid: 4326
                            },
                            restrictionAttributes: ["Preferred for Pedestrians"]
                            // travelMode: {"attributeParameterValues":
                            //     [{
                            //         "parameterName":"Restriction Usage","attributeName":"Avoid Private Roads","value":"AVOID_MEDIUM"
                            //     },{
                            //         "parameterName":"Restriction Usage","attributeName":"Walking","value":"PROHIBITED"
                            //     },{
                            //         "parameterName":"Restriction Usage","attributeName":"Preferred for Pedestrians","value":"PREFER_LOW"
                            //     },{
                            //         "parameterName":"Walking Speed (km/h)","attributeName":"WalkTime","value":5
                            //     },{
                            //         "parameterName":"Restriction Usage","attributeName":"Avoid Roads Unsuitable for Pedestrians","value":"AVOID_HIGH"
                            //     }],
                            //         "description":"Follows paths and roads that allow pedestrian traffic and finds solutions that optimize travel time. The walking speed is set to 5 kilometers per hour.","impedanceAttributeName":"WalkTime","simplificationToleranceUnits":"esriMeters","uturnAtJunctions":"esriNFSBAllowBacktrack","restrictionAttributeNames":["Avoid Private Roads","Avoid Roads Unsuitable for Pedestrians","Preferred for Pedestrians","Walking"],"useHierarchy":false,"simplificationTolerance":2,"timeAttributeName":"WalkTime","distanceAttributeName":"Kilometers","type":"WALK","id":"caFAgoThrvUpkFBW","name":"Walking Time"
                            //     }
                        });
                        var start = new Graphic({
                            geometry: {
                                type: "point",
                                longitude: result.coords.longitude,
                                latitude: result.coords.latitude
                            },
                            symbol: startSymbol
                        });
                        var end = new Graphic({
                            geometry: {
                                type: "point",
                                longitude: data['longitude'],
                                latitude: data['latitude']
                            },
                            symbol: endSymbol
                        });
                        view.center = [result.coords.longitude, result.coords.latitude];
                        view.zoom = 14;
                        routeLyr.add(start);
                        routeLyr.add(end);
                        map.layers.add(routeLyr);

                        routeParams.stops.features.push(start, end);
                        routeTask.solve(routeParams).then(function(response){
                            var routeResult = response.routeResults[0].route;
                            var direction = response.routeResults[0].directions;
                            console.log(direction.totalLength);
                            routeResult.symbol = routeSymbol;
                            routeLyr.add(routeResult);
                        });
                    });
                } else {
                    console.log("Browser doesn't support Geolocation. Visit http://caniuse.com to see browser support for the Geolocation API.");
                }
            }
            function errorCallback(){
                alert('server not responding...');
            }
        });
    }
</script>
@endsection