@extends('layouts._app-masyarakat')
@section('content')
    <div class="col-lg-12 col-md-12 col-xs-12">
        <div class="breadcrumbs-left breadcrumbs-top mt-1">
            <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home_page') }}">Beranda</a></li>
                    <li class="breadcrumb-item active"> Detail Info</li>
                    <li class="breadcrumb-item active"> {{ $info->nama_info }}</li>
                </ol>
            </div>
        </div>
        <div>
            <div class="row pr-3 pl-3">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="card mt-2">
                        <div class="card-body">
                            <div class="card-block">
                                <h3 class="card-title mb-0">{{ $info->nama_info }}</h3>
                                <span class="text-muted font-medium-1">{{ date('D, d M Y', strtotime($info->tanggal_info)) }}</span>
                                <section class="card-text mt-2">{!! $info->konten !!}</section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection