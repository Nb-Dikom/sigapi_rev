<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/logo_bnpb.png') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'SIGAPI_DEV') }}</title>

    <!-- Styles -->
    @include('layouts.partials._css_backend')
</head>
<body data-open="click" data-menu="vertical-menu" data-col="1-column" class="vertical-layout vertical-menu 1-column  blank-page blank-page">
    <div class="app-content content container-fluid">
        <div class="content-wrapper">
            <div class="content-header row"></div>
            <div class="content-body">
                <section class="flexbox-container">
                    <div class="col-md-4 offset-md-4 col-xs-10 offset-xs-1  box-shadow-2 p-0">
                        <div class="card border-grey border-lighten-3 m-0">
                            <div class="card-header no-border">
                                <div class="card-title text-xs-center">
                                    <div class="p-1"><img src="{{ asset('img/sigapi-logo.png') }}" alt="Logo SIGAPI"></div>
                                </div>
                                <h6 class="card-subtitle line-on-side text-muted text-xs-center font-small-3 pt-2"><span>Sign In SIGAPI</span></h6>
                            </div>
                            <div class="card-body collapse in">
                                <div class="card-block">
                                    <form class="form-horizontal form-simple" method="POST" action="{{ route('login') }}">
                                        {{ csrf_field() }}
                                        <div class="form-group position-relative has-icon-left">
                                            <input name="username" type="text" class="form-control form-control-lg input-lg" id="username" placeholder="Masukkan username" required value="{{ old('username') }}">
                                            <div class="form-control-position"><i class="icon-head"></i></div>
                                        </div>
                                        <div class="form-group position-relative has-icon-left">
                                            <input name="password" type="password" class="form-control form-control-lg input-lg" id="password" placeholder="Masukkan kata sandi" required>
                                            <div class="form-control-position"><i class="icon-key3"></i></div>
                                        </div>
                                        @if ($errors->has('username'))
                                            <span class="help-block">
                                                <strong class="pink">{{ $errors->first('username') }}</strong>
                                            </span>
                                        @endif
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong class="pink">{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                        <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="icon-unlock2"></i> Masuk</button>
                                    </form>
                                    <span>Belum punya akun? <a href="{{ route('form_registrasi') }}" class="text-info"><i>Daftar</i></a></span>
                                    <div class="col-xs-12 col-sm-6 col-md-6 offset-xs-3 offset-sm-3 offset-md-3 mt-3">
                                        <a href="{{ url('/') }}" class="btn btn-outline-primary btn-block font-small-3"><i class="icon-home22"></i> Beranda</a>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="">
                                    <p class="clearfix text-muted text-sm-center mb-0 px-2">
                                        <span>Copyright &copy; 2018 <span class="text-bold-800 grey darken-2">Diar Ichrom</span>, All rights reserved. </span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    

    <!-- Scripts -->
    @include('layouts.partials._javascript_backend')
    </script>
</body>
</html>
