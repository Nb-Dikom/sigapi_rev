<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/profil/{id}', function ($id) {
	$provinsi = \App\Models\ProfilDaerah::find($id);
	return response()->json($provinsi->kabupaten->provinsi, 200);
});
Route::get('/prov/{provinsi}', function (\App\Models\Provinsi $provinsi) {
	return response()->json($provinsi->kabupaten, 200);
});

// Mobile route
Route::get('/status-gunung', 'StatusGunungController@list_status_gunung');
Route::get('/status-gunung/{status}', 'StatusGunungController@list_status_gunung_by_name');
Route::get('/info', 'InfoKebencanaanController@list_info');
Route::get('/pengamatan', 'PengamatanController@list_pengamatan');
Route::get('/fasilitas', 'FasilitasController@list_fasilitas');
Route::get('/peta/{peta}', 'PetaController@list_peta');
