<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('research', function(){
	return view ('research');
});

Route::get('/shapefile', function(){
	return "shapefile";
})->name('shapefile_page');

Route::get('/mapguide', function(){
	return "map guide";
})->name('mapguide_page');

Route::get('/', function(){
	$c_info = new \App\Models\Info();
	$c_fasilitas = new \App\Models\Fasilitas();
	$c_pengamatan = new \App\Models\Pengamatan();
	$c_status_gn = new \App\Models\StatusGunung();

	$data['list_info'] = $c_info->get_validated()->take(2)->get();
	$data['list_fasilitas'] = $c_fasilitas->get_validated()->take(4)->get();
	$data['list_pengamatan'] = $c_pengamatan->get_validated()->take(3)->get();
	$data['list_status_gunung'] = $c_status_gn->get_valid_status()->take(18)->get();

	return view ('beranda')->with($data);
});

Route::get('/info-kebencanaan', 'InfoKebencanaanController@info_page');
Route::get('/info-kebencanaan/{info_kebencanaan}', 'InfoKebencanaanController@view_info');

Route::get('/peta-bencana', 'PetaController@peta_page');
Route::get('/peta-bencana/rute', 'PetaController@route');
Route::get('/peta-bencana/{jenis}', 'PetaController@view_peta');

Route::get('/pengamatan-gunung-api', 'PengamatanController@pengamatan_page');
Route::get('/pengamatan-gunung-api/download/{name}', 'PengamatanController@download');
Route::get('/pengamatan-gunung-api/{pengamatan_gunung_api}', 'PengamatanController@view_pengamatan');

Route::get('/fasilitas-publik', 'FasilitasController@fasilitas_page');
Route::get('/fasilitas-publik/download/{fasilitas_publik}', 'FasilitasController@download');
Route::get('/fasilitas-publik/{fasilitas_publik}', 'FasilitasController@view_fasilitas');

Route::get('/status-gunung-api', 'StatusGunungController@status_gunung_page');
Route::get('/status-gunung-api/{status_gunung}', 'StatusGunungController@view_status_gunung');

Route::get('/daftar', 'OrganisasiController@registrasi')->name('form_registrasi');
Route::post('/simpan-organisasi', 'OrganisasiController@store_organisasi')->name('simpan_organisasi');

Route::get('/login', function(){ return redirect()->route('form_login'); });
Route::get('/masuk', 'Auth\LoginController@show_login')->name('form_login');
Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::post('/sign-out', 'Auth\LoginController@logout')->name('logout');

Route::get('/peta/load/{peta}', 'PetaController@load_peta')->name('load_map');

Route::group(['middleware' => 'auth'], function (){
	Route::get('/dashboard', function(){
		$data = [
            'nama' => Auth::user()->pegawai->nama_pegawai,
            'level' => Auth::user()->level->level,
        ];
        return view('user.dashboard', compact('data'));
	})->name('dashboard_page');
	Route::get('/profil/{profil}', 'PegawaiController@edit_profile')->name('profile_page');
	Route::patch('/pegawai/{pegawai}', 'PegawaiController@update_pegawai')->name('update_pegawai');

	Route::group(['middleware' => 'role:Admin'], function (){
		Route::get('/pegawai', 'PegawaiController@index_pegawai')->name('pegawai_page');
		Route::post('/pegawai', 'PegawaiController@store_pegawai')->name('save_pegawai');
		Route::get('/pegawai/create', 'PegawaiController@create_pegawai')->name('form_create_pegawai');
		Route::delete('/pegawai/{pegawai}', 'PegawaiController@destroy_pegawai')->name('delete_pegawai');
		Route::get('/pegawai/{pegawai}/edit', 'PegawaiController@edit_pegawai')->name('form_edit_pegawai');
	});

	Route::group(['middleware' => 'role:Admin,Kepala Seksi Kesiapsiagaan,Kepala BPBD Jawa Timur'], function (){
		Route::patch('/info/verified', 'InfoKebencanaanController@verified')->name('verified_info');
		Route::patch('/info/unverified', 'InfoKebencanaanController@unverified')->name('unverified_info');

		Route::patch('/pengamatan/verified', 'PengamatanController@verified')->name('verified_pengamatan');
		Route::patch('/pengamatan/unverified', 'PengamatanController@unverified')->name('unverified_pengamatan');

		Route::patch('/kawasan-rawan-bencana/verified', 'KrbController@verified')->name('verified_krb');
		Route::patch('/kawasan-rawan-bencana/unverified', 'KrbController@unverified')->name('unverified_krb');

		Route::patch('/fasilitas/verified', 'FasilitasController@verified')->name('verified_fasilitas');
		Route::patch('/fasilitas/unverified', 'FasilitasController@unverified')->name('unverified_fasilitas');

		Route::patch('/status-gunung/verified', 'StatusGunungController@verified')->name('verified_status_gunung');
		Route::patch('/status-gunung/unverified', 'StatusGunungController@unverified')->name('unverified_status_gunung');

		Route::patch('/jalur-evakuasi/verified', 'JalurEvakuasiController@verified')->name('verified_jalur_evakuasi');
		Route::patch('/jalur-evakuasi/unverified', 'JalurEvakuasiController@unverified')->name('unverified_jalur_evakuasi');
		
	});

	Route::group(['middleware' => 'role:Admin,Kepala Bidang Pencegahan dan Kesiapsiagaan,Kepala BPBD Jawa Timur'], function (){
		Route::patch('/info/validated', 'InfoKebencanaanController@validated')->name('validated_info');
		Route::patch('/info/unvalidated', 'InfoKebencanaanController@unvalidated')->name('unvalidated_info');

		Route::patch('/pengamatan/validated', 'PengamatanController@validated')->name('validated_pengamatan');
		Route::patch('/pengamatan/unvalidated', 'PengamatanController@unvalidated')->name('unvalidated_pengamatan');

		Route::patch('/kawasan-rawan-bencana/validated', 'KrbController@validated')->name('validated_krb');
		Route::patch('/kawasan-rawan-bencana/unvalidated', 'KrbController@unvalidated')->name('unvalidated_krb');

		Route::patch('/fasilitas/validated', 'FasilitasController@validated')->name('validated_fasilitas');
		Route::patch('/fasilitas/unvalidated', 'FasilitasController@unvalidated')->name('unvalidated_fasilitas');
		
		Route::patch('/status-gunung/validated', 'StatusGunungController@validated')->name('validated_status_gunung');
		Route::patch('/status-gunung/unvalidated', 'StatusGunungController@unvalidated')->name('unvalidated_status_gunung');
		
		Route::patch('/jalur-evakuasi/validated', 'JalurEvakuasiController@validated')->name('validated_jalur_evakuasi');
		Route::patch('/jalur-evakuasi/unvalidated', 'JalurEvakuasiController@unvalidated')->name('unvalidated_jalur_evakuasi');

	});

	Route::group(['middleware' => 'role:Sub Bagian Umum dan Kepegawaian,Sub Bagian Penanggulangan Bencana'], function (){
		Route::get('/peta', 'PetaController@index_peta')->name('map_page');
		Route::get('/peta/{peta}', 'PetaController@show_peta')->name('view_map');

		Route::get('/fasilitas/download', 'FasilitasController@download');
		Route::get('/pengamatan/download/{pengamatan}', 'PengamatanController@download');
	});

	Route::group(['middleware' => 'role:Admin,Pengolah Data Pra Bencana,Kepala Seksi Kesiapsiagaan,Kepala Bidang Pencegahan dan Kesiapsiagaan,Kepala BPBD Jawa Timur'], function (){
		Route::post('/info', 'InfoKebencanaanController@store_info')->name('save_info');
		Route::get('/info/create', 'InfoKebencanaanController@create_info')->name('form_create_info');
		Route::patch('/info/{info}', 'InfoKebencanaanController@update_info')->name('update_info');
		Route::delete('/info/{info}', 'InfoKebencanaanController@destroy_info')->name('delete_info');
		Route::get('/info/{info}/edit', 'InfoKebencanaanController@edit_info')->name('form_edit_info');
	
		Route::post('/pengamatan', 'PengamatanController@store_pengamatan')->name('save_pengamatan');
		Route::get('/pengamatan/create', 'PengamatanController@create_pengamatan')->name('form_create_pengamatan');
		Route::patch('/pengamatan/{pengamatan}', 'PengamatanController@update_pengamatan')->name('update_pengamatan');
		Route::delete('/pengamatan/{pengamatan}', 'PengamatanController@destroy_pengamatan')->name('delete_pengamatan');
		Route::get('/pengamatan/{pengamatan}/edit', 'PengamatanController@edit_pengamatan')->name('form_edit_pengamatan');
	
		Route::post('/kawasan-rawan-bencana', 'KrbController@store_krb')->name('save_krb');
		Route::get('/kawasan-rawan-bencana/create', 'KrbController@create_krb')->name('form_create_krb');
		Route::patch('/kawasan-rawan-bencana/{krb}', 'KrbController@update_krb')->name('update_krb');
		Route::delete('/kawasan-rawan-bencana/{krb}', 'KrbController@destroy_krb')->name('delete_krb');
		Route::get('/kawasan-rawan-bencana/{krb}/edit', 'KrbController@edit_krb')->name('form_edit_krb');
	
		Route::post('/fasilitas', 'FasilitasController@store_fasilitas')->name('save_fasilitas');
		Route::get('/fasilitas/create', 'FasilitasController@create_fasilitas')->name('form_create_fasilitas');
		Route::patch('/fasilitas/{fasilitas}', 'FasilitasController@update_fasilitas')->name('update_fasilitas');
		Route::delete('/fasilitas/{fasilitas}', 'FasilitasController@destroy_fasilitas')->name('delete_fasilitas');
		Route::get('/fasilitas/{fasilitas}/edit', 'FasilitasController@edit_fasilitas')->name('form_edit_fasilitas');
	
		Route::post('/status-gunung', 'StatusGunungController@store_status_gunung')->name('save_status_gunung');
		Route::get('/status-gunung/create', 'StatusGunungController@create_status_gunung')->name('form_create_status_gunung');
		Route::patch('/status-gunung/{status_gunung}', 'StatusGunungController@update_status_gunung')->name('update_status_gunung');
		Route::delete('/status-gunung/{status_gunung}', 'StatusGunungController@destroy_status_gunung')->name('delete_status_gunung');
		Route::get('/status-gunung/{status_gunung}/edit', 'StatusGunungController@edit_status_gunung')->name('form_edit_status_gunung');
	
		Route::post('/jalur-evakuasi', 'JalurEvakuasiController@store_jalur_evakuasi')->name('save_jalur_evakuasi');
		Route::get('/jalur-evakuasi/create', 'JalurEvakuasiController@create_jalur_evakuasi')->name('form_create_jalur_evakuasi');
		Route::patch('/jalur-evakuasi/{jalur_evakuasi}', 'JalurEvakuasiController@update_jalur_evakuasi')->name('update_jalur_evakuasi');
		Route::delete('/jalur-evakuasi/{jalur_evakuasi}', 'JalurEvakuasiController@destroy_jalur_evakuasi')->name('delete_jalur_evakuasi');
		Route::get('/jalur-evakuasi/{jalur_evakuasi}/edit', 'JalurEvakuasiController@edit_jalur_evakuasi')->name('form_edit_jalur_evakuasi');
	});

	Route::group(['middleware' => 'role:Admin,Sub Bagian Umum dan Kepegawaian,Pengolah Data Pra Bencana,Kepala Seksi Kesiapsiagaan,Kepala Bidang Pencegahan dan Kesiapsiagaan,Kepala BPBD Jawa Timur,Sub Bagian Penanggulangan Bencana'], function (){
		Route::get('/info', 'InfoKebencanaanController@index_info')->name('info_page');
		Route::get('/info/{info}', 'InfoKebencanaanController@show_info')->name('view_info');
	
		Route::get('/pengamatan', 'PengamatanController@index_pengamatan')->name('pengamatan_page');
		Route::get('/pengamatan/{pengamatan}', 'PengamatanController@show_pengamatan')->name('view_pengamatan');
	
		Route::get('/kawasan-rawan-bencana', 'KrbController@index_krb')->name('krb_page');
		Route::get('/kawasan-rawan-bencana/{krb}', 'KrbController@show_krb')->name('view_krb');
	
		Route::get('/fasilitas', 'FasilitasController@index_fasilitas')->name('fasilitas_page');
		Route::get('/fasilitas/{fasilitas}', 'FasilitasController@show_fasilitas')->name('view_fasilitas');
	
		Route::get('/status-gunung', 'StatusGunungController@index_status_gunung')->name('status_gunung_page');
		Route::get('/status-gunung/{status_gunung}', 'StatusGunungController@show_status_gunung')->name('view_status_gunung');
	
		Route::get('/jalur-evakuasi', 'JalurEvakuasiController@index_jalur_evakuasi')->name('jalur_evakuasi_page');
		Route::get('/jalur-evakuasi/{jalur_evakuasi}', 'JalurEvakuasiController@show_jalur_evakuasi')->name('view_jalur_evakuasi');
	});
	
	Route::group(['middleware' => 'role:Admin,Sub Bagian Umum dan Kepegawaian,Kepala BPBD Jawa Timur'], function (){
		Route::get('/organisasi', 'OrganisasiController@index_organisasi')->name('organisasi_page');
		Route::post('/organisasi', 'OrganisasiController@store_organisasi')->name('save_organisasi');
		Route::get('/organisasi/create', 'OrganisasiController@create_organisasi')->name('form_create_organisasi');
		Route::patch('/organisasi/verified', 'OrganisasiController@verified')->name('verified_organisasi');
		Route::patch('/organisasi/unverified', 'OrganisasiController@unverified')->name('unverified_organisasi');
		Route::get('/organisasi/{organisasi}', 'OrganisasiController@show_organisasi')->name('view_organisasi');
		Route::patch('/organisasi/{organisasi}', 'OrganisasiController@update_organisasi')->name('update_organisasi');
		Route::delete('/organisasi/{organisasi}', 'OrganisasiController@destroy_organisasi')->name('delete_organisasi');
		Route::get('/organisasi/{organisasi}/edit', 'OrganisasiController@edit_organisasi')->name('form_edit_organisasi');
	});

	Route::group(['middleware' => 'role:Organisasi'], function (){
		Route::get('/akun', 'OrganisasiController@show_by_organisasi');
		Route::get('/edit-akun', 'OrganisasiController@edit_by_organisasi');
	});
});
