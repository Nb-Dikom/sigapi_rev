<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class StatusGunung extends Model
{
    #----------------------------------#
    #
    # Setting attribut for model
    # @status gunung
    protected $table = 'status_gunung';
    protected $primaryKey = 'id_status_gunung';
    protected $fillable = [
        'id_status_gunung', 'id_status', 'id_gunung',
        'id_verifikasi', 'id_validasi', 'id_pegawai',
        'tanggal_status_gunung'

    ];

    public $hidden = ['id_status_gunung'];
    public $timestamps = false;
    public $dates = ['tanggal_status_gunung'];
    public $incrementing = false;

    #----------------------------------#
    #
    # Set function for model
    #
    function get_all()
    {
        return $this->join('status', 'status_gunung.id_status', '=', 'status.id_status')
        ->join('gunung', 'status_gunung.id_gunung', '=', 'gunung.id_gunung')
        ->join('verifikasi', 'status_gunung.id_verifikasi', '=', 'verifikasi.id_verifikasi')
        ->join('validasi', 'status_gunung.id_validasi', '=', 'validasi.id_validasi')
        ->join('pegawai', 'status_gunung.id_pegawai', '=', 'pegawai.id_pegawai')
        ->select([
            'status_gunung.id_status_gunung', 'status_gunung.tanggal_status_gunung',
            'status.id_status', 'status.status',
            'gunung.gunung',
            'verifikasi.verifikasi',
            'validasi.validasi',
            'pegawai.nama_pegawai'
        ])
        ->orderBy('status_gunung.tanggal_status_gunung', 'desc')
        ->orderBy('validasi.id_validasi', 'asc')
        ->orderBy('verifikasi.id_verifikasi', 'asc')
        ->orderBy('status_gunung.id_status_gunung', 'desc')
        ->get();
    }

    function sort_desc_verified()
    {
        return $this->join('status', 'status_gunung.id_status', '=', 'status.id_status')
        ->join('gunung', 'status_gunung.id_gunung', '=', 'gunung.id_gunung')
        ->join('verifikasi', 'status_gunung.id_verifikasi', '=', 'verifikasi.id_verifikasi')
        ->join('validasi', 'status_gunung.id_validasi', '=', 'validasi.id_validasi')
        ->join('pegawai', 'status_gunung.id_pegawai', '=', 'pegawai.id_pegawai')
        ->select([
            'status_gunung.id_status_gunung', 'status_gunung.tanggal_status_gunung',
            'status.id_status', 'status.status',
            'gunung.gunung',
            'verifikasi.verifikasi',
            'validasi.validasi',
            'pegawai.nama_pegawai'
        ])
        ->orderBy('status_gunung.tanggal_status_gunung', 'desc')
        ->orderBy('verifikasi.id_verifikasi', 'desc')
        ->orderBy('validasi.id_validasi', 'asc')
        ->orderBy('status_gunung.id_status_gunung', 'desc')
        ->get();
    }

    function sort_desc_validated()
    {
        return $this->join('status', 'status_gunung.id_status', '=', 'status.id_status')
        ->join('gunung', 'status_gunung.id_gunung', '=', 'gunung.id_gunung')
        ->join('verifikasi', 'status_gunung.id_verifikasi', '=', 'verifikasi.id_verifikasi')
        ->join('validasi', 'status_gunung.id_validasi', '=', 'validasi.id_validasi')
        ->join('pegawai', 'status_gunung.id_pegawai', '=', 'pegawai.id_pegawai')
        ->select([
            'status_gunung.id_status_gunung', 'status_gunung.tanggal_status_gunung',
            'status.id_status', 'status.status',
            'gunung.gunung',
            'verifikasi.verifikasi',
            'validasi.validasi',
            'pegawai.nama_pegawai'
        ])
        ->orderBy('status_gunung.tanggal_status_gunung', 'desc')
        ->orderBy('validasi.id_validasi', 'desc')
        ->orderBy('verifikasi.id_verifikasi', 'desc')
        ->orderBy('status_gunung.id_status_gunung', 'desc')
        ->get();
    }

    function get_validated()
    {
        return DB::table('gunung')
            ->leftJoin('status_gunung', 'gunung.id_gunung', '=', 'status_gunung.id_gunung')
            ->leftJoin('status', 'status_gunung.id_status', '=', 'status.id_status')
            ->leftJoin('verifikasi', 'status_gunung.id_verifikasi', '=', 'verifikasi.id_verifikasi')
            ->leftJoin('validasi', 'status_gunung.id_validasi', '=', 'validasi.id_validasi')
            ->leftJoin('pegawai', 'status_gunung.id_pegawai', '=', 'pegawai.id_pegawai')
            ->select([
                'status_gunung.id_status_gunung', 'status_gunung.tanggal_status_gunung',
                'status.id_status', 'status.status',
                'gunung.gunung',
                'verifikasi.verifikasi',
                'validasi.validasi',
                'pegawai.nama_pegawai'
            ])
            ->orderBy('status.id_status', 'asc')
            ->orderBy('status_gunung.tanggal_status_gunung', 'desc')
            ->orderBy('gunung.id_gunung', 'asc')
            ->get();
    }

    function get_valid_status()
    {
        return $this->join('status', 'status_gunung.id_status', '=', 'status.id_status')
        ->join('gunung', 'status_gunung.id_gunung', '=', 'gunung.id_gunung')
        ->join('verifikasi', 'status_gunung.id_verifikasi', '=', 'verifikasi.id_verifikasi')
        ->join('validasi', 'status_gunung.id_validasi', '=', 'validasi.id_validasi')
        ->join('pegawai', 'status_gunung.id_pegawai', '=', 'pegawai.id_pegawai')
        ->where('verifikasi.verifikasi', 'Lengkap')
        ->where('validasi.validasi', 'Valid')
        ->select([
            'status_gunung.id_status_gunung', 'status_gunung.tanggal_status_gunung',
            'status.id_status', 'status.status',
            'gunung.gunung',
            'pegawai.nama_pegawai'
        ])
        ->orderBy('status.id_status', 'asc')
        ->orderBy('status_gunung.tanggal_status_gunung', 'desc');
    }

    function get_valid_status_by_status($status)
    {
        return $this->join('status', 'status_gunung.id_status', '=', 'status.id_status')
        ->join('gunung', 'status_gunung.id_gunung', '=', 'gunung.id_gunung')
        ->join('verifikasi', 'status_gunung.id_verifikasi', '=', 'verifikasi.id_verifikasi')
        ->join('validasi', 'status_gunung.id_validasi', '=', 'validasi.id_validasi')
        ->join('pegawai', 'status_gunung.id_pegawai', '=', 'pegawai.id_pegawai')
        ->where('verifikasi.verifikasi', 'Lengkap')
        ->where('validasi.validasi', 'Valid')
        ->where('status.status', $status)
        ->select([
            'status_gunung.id_status_gunung', 'status_gunung.tanggal_status_gunung',
            'status.id_status', 'status.status',
            'gunung.gunung',
            'pegawai.nama_pegawai'
        ])
        ->orderBy('status.id_status', 'asc')
        ->orderBy('status_gunung.tanggal_status_gunung', 'desc');
    }

    function get_by_lower_name($name)
    {
        $string = explode("+", $name);
        $id = $string[0];

        return $this->join('status', 'status_gunung.id_status', '=', 'status.id_status')
        ->join('gunung', 'status_gunung.id_gunung', '=', 'gunung.id_gunung')
        ->join('verifikasi', 'status_gunung.id_verifikasi', '=', 'verifikasi.id_verifikasi')
        ->join('validasi', 'status_gunung.id_validasi', '=', 'validasi.id_validasi')
        ->where('id_status_gunung', $id)
        ->select([
            'status_gunung.id_status_gunung', 'status_gunung.tanggal_status_gunung',
            'status.status',
            'gunung.gunung',
            'verifikasi.verifikasi',
            'validasi.validasi'
        ])->first();
    }

    function get_latest()
    {
        $latest = $this->orderBy('id_status_gunung', 'desc')->first();
        return $latest;
    }

    #=========================
    public function getTanggalStatusGunungAttribute($tanggal_status_gunung){
        return $this->asDateTime($tanggal_status_gunung)->format('D, d M Y');
    }
}
