<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    #----------------------------------#
    #
    # Setting attribut for model
    # @status
    protected $table = 'status';
    protected $primaryKey = 'id_status';

    // public $hidden = ['id_status'];
    public $timestamps = false;
    public $incrementing = false;

    // #----------------------------------#
    #
    # Set function for model
    #
    function get_by_name($name)
    {
        return $this->where('status.status', $name)
            ->first();
    }
}
