<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JalurEvakuasi extends Model
{
    #----------------------------------#
    #
    # Setting attribut for model
    # @kawasan rawan bencana
    protected $table = 'jalur_evakuasi';
    protected $primaryKey = 'id_jalur_evakuasi';
    protected $fillable = [
        'id_jalur_evakuasi', 'id_gunung',
        'id_verifikasi', 'id_validasi', 'id_pegawai',
        'file_jalur_evakuasi', 'tanggal_jalur_evakuasi'

    ];

    public $hidden = ['id_jalur_evakuasi'];
    public $timestamps = false;
    public $dates = ['tanggal_jalur_evakuasi'];
    public $incrementing = false;

    #----------------------------------#
    #
    # Adding relation for another model
    #
    public function gunung(){
        return $this->belongsTo('App\Models\Gunung', 'id_gunung');
    }

    public function verifikasi(){
        return $this->belongsTo('App\Models\Verifikasi', 'id_verifikasi');
    }

    public function validasi(){
        return $this->belongsTo('App\Models\Validasi', 'id_validasi');
    }

    public function pegawai(){
        return $this->belongsTo('App\Models\Pegawai', 'id_pegawai');
    }

    #----------------------------------#
    #
    # Set function for model
    #
    function get_all()
    {
        return $this->join('gunung', 'jalur_evakuasi.id_gunung', '=', 'gunung.id_gunung')
        ->join('verifikasi', 'jalur_evakuasi.id_verifikasi', '=', 'verifikasi.id_verifikasi')
        ->join('validasi', 'jalur_evakuasi.id_validasi', '=', 'validasi.id_validasi')
        ->join('pegawai', 'jalur_evakuasi.id_pegawai', '=', 'pegawai.id_pegawai')
        ->select([
            'jalur_evakuasi.id_jalur_evakuasi', 'jalur_evakuasi.file_jalur_evakuasi', 'jalur_evakuasi.tanggal_jalur_evakuasi',
            'gunung.id_gunung','gunung.gunung',
            'verifikasi.verifikasi',
            'validasi.validasi',
            'pegawai.nama_pegawai'
        ])
        ->orderBy('validasi.id_validasi', 'asc')
        ->orderBy('verifikasi.id_verifikasi', 'asc')
        ->orderBy('jalur_evakuasi.id_jalur_evakuasi', 'desc')
        ->get();
    }

    function sort_desc_verified()
    {
        return $this->join('gunung', 'jalur_evakuasi.id_gunung', '=', 'gunung.id_gunung')
        ->join('verifikasi', 'jalur_evakuasi.id_verifikasi', '=', 'verifikasi.id_verifikasi')
        ->join('validasi', 'jalur_evakuasi.id_validasi', '=', 'validasi.id_validasi')
        ->join('pegawai', 'jalur_evakuasi.id_pegawai', '=', 'pegawai.id_pegawai')
        ->select([
            'jalur_evakuasi.id_jalur_evakuasi', 'jalur_evakuasi.file_jalur_evakuasi', 'jalur_evakuasi.tanggal_jalur_evakuasi',
            'gunung.gunung',
            'verifikasi.verifikasi',
            'validasi.validasi',
            'pegawai.nama_pegawai'
        ])
        ->orderBy('verifikasi.id_verifikasi', 'desc')
        ->orderBy('validasi.id_validasi', 'asc')
        ->orderBy('jalur_evakuasi.id_jalur_evakuasi', 'desc')
        ->get();
    }

    function sort_desc_validated()
    {
        return $this->join('gunung', 'jalur_evakuasi.id_gunung', '=', 'gunung.id_gunung')
        ->join('verifikasi', 'jalur_evakuasi.id_verifikasi', '=', 'verifikasi.id_verifikasi')
        ->join('validasi', 'jalur_evakuasi.id_validasi', '=', 'validasi.id_validasi')
        ->join('pegawai', 'jalur_evakuasi.id_pegawai', '=', 'pegawai.id_pegawai')
        ->select([
            'jalur_evakuasi.id_jalur_evakuasi', 'jalur_evakuasi.file_jalur_evakuasi', 'jalur_evakuasi.tanggal_jalur_evakuasi',
            'gunung.gunung',
            'verifikasi.verifikasi',
            'validasi.validasi',
            'pegawai.nama_pegawai'
        ])
        ->orderBy('validasi.id_validasi', 'desc')
        ->orderBy('verifikasi.id_verifikasi', 'desc')
        ->orderBy('jalur_evakuasi.id_jalur_evakuasi', 'desc')
        ->get();
    }

    function get_validated_by_name($gunung)
    {
        return $this->join('gunung', 'jalur_evakuasi.id_gunung', '=', 'gunung.id_gunung')
        ->join('verifikasi', 'jalur_evakuasi.id_verifikasi', '=', 'verifikasi.id_verifikasi')
        ->join('validasi', 'jalur_evakuasi.id_validasi', '=', 'validasi.id_validasi')
        ->where('gunung.gunung', $gunung)
        ->where('verifikasi.verifikasi', 'Lengkap')
        ->where('validasi.validasi', 'Valid')
        ->select([
            'jalur_evakuasi.id_jalur_evakuasi', 'jalur_evakuasi.file_jalur_evakuasi', 'jalur_evakuasi.tanggal_jalur_evakuasi',
            'gunung.gunung',
        ]);
    }

    function get_by_lower_name($name)
    {
        $string = explode("+", $name);
        $id = $string[0];

        return $this->join('gunung', 'jalur_evakuasi.id_gunung', '=', 'gunung.id_gunung')
        ->join('verifikasi', 'jalur_evakuasi.id_verifikasi', '=', 'verifikasi.id_verifikasi')
        ->join('validasi', 'jalur_evakuasi.id_validasi', '=', 'validasi.id_validasi')
        ->where('id_jalur_evakuasi', $id)
        ->select([
            'jalur_evakuasi.id_jalur_evakuasi', 'jalur_evakuasi.file_jalur_evakuasi', 'jalur_evakuasi.tanggal_jalur_evakuasi',
            'gunung.gunung',
            'verifikasi.verifikasi',
            'validasi.validasi'
        ])->first();
    }

    function get_latest()
    {
        $latest = $this->orderBy('id_jalur_evakuasi', 'desc')->first();
        return $latest;
    }
}
