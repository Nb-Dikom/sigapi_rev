<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class KirimSertifikat extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        //
        $this->data = $data;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('user.organisasi.mail')->with([
            'nama' => $this->data['nama'],
            'username' => $this->data['username'],
            'kata_sandi' => $this->data['kata_sandi']
        ])
        ->subject('Terima kasih telah mendaftar!');
    }
}
