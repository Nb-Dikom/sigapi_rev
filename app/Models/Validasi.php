<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Validasi extends Model
{
    #----------------------------------#
    #
    # Setting attribut for model
    # @validasi
    protected $table = 'validasi';
    protected $primaryKey = 'id_validasi';

    public $incrementing = false;
    public $hidden = ['id_validasi'];

    #----------------------------------#
    #
    # Adding relation for another model
    #
    //

    #----------------------------------#
    #
    # Set function for model
    #
    function get_by_name($name)
    {
        return $this->where('validasi', $name)->first();
    }
}
