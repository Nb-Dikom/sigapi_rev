<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StatusOrganisasi extends Model
{
    //
    protected $table = 'status_organisasi';
    protected $primaryKey = 'id_status_organisasi';

    public $hidden = ['id_status_organisasi'];
    public $incrementing = false;
    
    #----------------------------------#
    #
    # Set function for model
    #
    function get_by_name($name)
    {
        return $this->where('status_organisasi.status_organisasi', $name)
            ->first();
    }
}
