<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Organisasi extends Model
{
    #----------------------------------#
    #
    # Setting attribut for model
    # @organisasi
    protected $table = 'organisasi';
    protected $primaryKey = 'id_organisasi';
    protected $fillable = [
        'id_user', 'id_status_organisasi',
        'nama_organisasi', 'alamat_organisasi',
        'foto_sertifikat', 'no_telp_organisasi',
        'handphone_organisasi', 'email_organisasi',
        'fax', 'kode_pos', 'web'
    ];

    public $timestamps = false;
    public $incrementing = false;
    public $hidden = ['id_organisasi'];

    #----------------------------------#
    #
    # Adding relation for another model
    #
    public function user(){
    	return $this->belongsTo('App\Models\User', 'id_user');
    }

    public function status_organisasi(){
        return $this->belongsTo('App\Models\StatusOrganisasi', 'id_status_organisasi');
    }

    #----------------------------------#
    #
    # Set function for model
    #
    function get_all()
    {
        return $this->join('status_organisasi', 'organisasi.id_status_organisasi', '=', 'status_organisasi.id_status_organisasi')
            ->orderBy('status_organisasi.status_organisasi', 'asc')
            ->orderBy('id_organisasi', 'desc')
            ->get();
    }

    function get_by_lower_name($name)
    {
        $sure_name = str_replace("+", " ", $name);

        return $this->join('user', 'organisasi.id_user', '=', 'user.id_user')
        ->join('status_organisasi', 'organisasi.id_status_organisasi', '=', 'status_organisasi.id_status_organisasi')
        ->where('nama_organisasi', $sure_name)
        ->select([
            'user.id_user', 'user.username',
            'organisasi.id_organisasi', 'organisasi.nama_organisasi', 'organisasi.alamat_organisasi', 'organisasi.alamat_organisasi',
            'organisasi.foto_sertifikat', 'organisasi.no_telp_organisasi', 'organisasi.handphone_organisasi', 'organisasi.email_organisasi',
            'organisasi.fax', 'organisasi.kode_pos', 'organisasi.web',
            'status_organisasi.status_organisasi'
        ])->first();
    }

    function get_latest()
    {
        $latest = $this->orderBy('id_organisasi', 'desc')->first();
        return $latest;
    }
}
