<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Krb extends Model
{
    #----------------------------------#
    #
    # Setting attribut for model
    # @kawasan rawan bencana
    protected $table = 'krb';
    protected $primaryKey = 'id_krb';
    protected $fillable = [
        'id_krb', 'id_jenis_krb', 'id_gunung',
        'id_verifikasi', 'id_validasi', 'id_pegawai',
        'file_krb', 'tanggal_krb'

    ];

    public $hidden = ['id_krb'];
    public $timestamps = false;
    public $dates = ['tanggal_krb'];
    public $incrementing = false;

    #----------------------------------#
    #
    # Adding relation for another model
    #
    public function jenis_krb(){
        return $this->belongsTo('App\Models\JenisKrb', 'id_jenis_krb');
    }

    public function gunung(){
        return $this->belongsTo('App\Models\Gunung', 'id_gunung');
    }

    public function verifikasi(){
        return $this->belongsTo('App\Models\Verifikasi', 'id_verifikasi');
    }

    public function validasi(){
        return $this->belongsTo('App\Models\Validasi', 'id_validasi');
    }

    public function pegawai(){
        return $this->belongsTo('App\Models\Pegawai', 'id_pegawai');
    }

    #----------------------------------#
    #
    # Set function for model
    #
    function get_all()
    {
        return $this->join('jenis_krb', 'krb.id_jenis_krb', '=', 'jenis_krb.id_jenis_krb')
        ->join('gunung', 'krb.id_gunung', '=', 'gunung.id_gunung')
        ->join('verifikasi', 'krb.id_verifikasi', '=', 'verifikasi.id_verifikasi')
        ->join('validasi', 'krb.id_validasi', '=', 'validasi.id_validasi')
        ->join('pegawai', 'krb.id_pegawai', '=', 'pegawai.id_pegawai')
        ->select([
            'krb.id_krb', 'krb.file_krb', 'krb.tanggal_krb',
            'jenis_krb.id_jenis_krb', 'jenis_krb.jenis_krb',
            'gunung.gunung',
            'verifikasi.verifikasi',
            'validasi.validasi',
            'pegawai.nama_pegawai'
        ])
        ->orderBy('validasi.id_validasi', 'asc')
        ->orderBy('verifikasi.id_verifikasi', 'asc')
        ->orderBy('krb.id_krb', 'desc')
        ->get();
    }

    function sort_desc_verified()
    {
        return $this->join('jenis_krb', 'krb.id_jenis_krb', '=', 'jenis_krb.id_jenis_krb')
        ->join('gunung', 'krb.id_gunung', '=', 'gunung.id_gunung')
        ->join('verifikasi', 'krb.id_verifikasi', '=', 'verifikasi.id_verifikasi')
        ->join('validasi', 'krb.id_validasi', '=', 'validasi.id_validasi')
        ->join('pegawai', 'krb.id_pegawai', '=', 'pegawai.id_pegawai')
        ->select([
            'krb.id_krb', 'krb.file_krb', 'krb.tanggal_krb',
            'jenis_krb.id_jenis_krb', 'jenis_krb.jenis_krb',
            'gunung.gunung',
            'verifikasi.verifikasi',
            'validasi.validasi',
            'pegawai.nama_pegawai'
        ])
        ->orderBy('verifikasi.id_verifikasi', 'desc')
        ->orderBy('validasi.id_validasi', 'asc')
        ->orderBy('krb.id_krb', 'desc')
        ->get();
    }

    function sort_desc_validated()
    {
        return $this->join('jenis_krb', 'krb.id_jenis_krb', '=', 'jenis_krb.id_jenis_krb')
        ->join('gunung', 'krb.id_gunung', '=', 'gunung.id_gunung')
        ->join('verifikasi', 'krb.id_verifikasi', '=', 'verifikasi.id_verifikasi')
        ->join('validasi', 'krb.id_validasi', '=', 'validasi.id_validasi')
        ->join('pegawai', 'krb.id_pegawai', '=', 'pegawai.id_pegawai')
        ->select([
            'krb.id_krb', 'krb.file_krb', 'krb.tanggal_krb',
            'jenis_krb.id_jenis_krb', 'jenis_krb.jenis_krb',
            'gunung.gunung',
            'verifikasi.verifikasi',
            'validasi.validasi',
            'pegawai.nama_pegawai'
        ])
        ->orderBy('validasi.id_validasi', 'desc')
        ->orderBy('verifikasi.id_verifikasi', 'desc')
        ->orderBy('krb.id_krb', 'desc')
        ->get();
    }

    function get_validated_by_name($gunung)
    {
        return $this->join('jenis_krb', 'krb.id_jenis_krb', '=', 'jenis_krb.id_jenis_krb')
        ->join('gunung', 'krb.id_gunung', '=', 'gunung.id_gunung')
        ->join('verifikasi', 'krb.id_verifikasi', '=', 'verifikasi.id_verifikasi')
        ->join('validasi', 'krb.id_validasi', '=', 'validasi.id_validasi')
        ->where('gunung.gunung', $gunung)
        ->where('verifikasi.verifikasi', 'Lengkap')
        ->where('validasi.validasi', 'Valid')
        ->select([
            'krb.id_krb', 'krb.file_krb', 'krb.tanggal_krb',
            'jenis_krb.id_jenis_krb', 'jenis_krb.jenis_krb',
            'gunung.gunung',
        ]);
    }

    function get_by_lower_name($name)
    {
        $string = explode("+", $name);
        $id = $string[0];

        return $this->join('jenis_krb', 'krb.id_jenis_krb', '=', 'jenis_krb.id_jenis_krb')
        ->join('gunung', 'krb.id_gunung', '=', 'gunung.id_gunung')
        ->join('verifikasi', 'krb.id_verifikasi', '=', 'verifikasi.id_verifikasi')
        ->join('validasi', 'krb.id_validasi', '=', 'validasi.id_validasi')
        ->where('id_krb', $id)
        ->select([
            'krb.id_krb', 'krb.file_krb', 'krb.tanggal_krb',
            'jenis_krb.id_jenis_krb', 'jenis_krb.jenis_krb',
            'gunung.gunung',
            'verifikasi.verifikasi',
            'validasi.validasi'
        ])->first();
    }

    function get_latest()
    {
        $latest = $this->orderBy('id_krb', 'desc')->first();
        return $latest;
    }
}
