<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pengamatan extends Model
{
    #----------------------------------#
    #
    # Setting attribut for model
    # @pengamatan
    protected $table = 'pengamatan';
    protected $primaryKey = 'id_pengamatan';
    protected $fillable = [
        'id_pengamatan', 'id_gunung', 'id_verifikasi', 'id_validasi', 'id_pegawai',
        'nama_pengamatan', 'foto_pengamatan', 'tanggal_pengamatan'
    ];
    protected $hidden = ['id_pengamatan'];

    public $timestamps = false;
    public $dates = ['tanggal_pengamatan'];
    public $incrementing = false;

    #----------------------------------#
    #
    # Adding relation for another model
    #
    public function gunung(){
        return $this->belongsTo('App\Models\Gunung', 'id_gunung');
    }

    public function verifikasi(){
        return $this->belongsTo('App\Models\Verifikasi', 'id_verifikasi');
    }

    public function validasi(){
        return $this->belongsTo('App\Models\Validasi', 'id_validasi');
    }

    public function pegawai(){
    	return $this->belongsTo('App\Models\Pegawai', 'id_pegawai');
    }

    #----------------------------------#
    #
    # Set function for model
    #
    function get_all()
    {
        return $this->join('gunung', 'pengamatan.id_gunung', '=', 'gunung.id_gunung')
        ->join('verifikasi', 'pengamatan.id_verifikasi', '=', 'verifikasi.id_verifikasi')
        ->join('validasi', 'pengamatan.id_validasi', '=', 'validasi.id_validasi')
        ->join('pegawai', 'pengamatan.id_pegawai', '=', 'pegawai.id_pegawai')
        ->select([
            'pengamatan.id_pengamatan', 'pengamatan.nama_pengamatan', 'pengamatan.tanggal_pengamatan',
            'gunung.gunung',
            'verifikasi.verifikasi',
            'validasi.validasi',
            'pegawai.nama_pegawai'
        ])
        ->orderBy('validasi.id_validasi', 'asc')
        ->orderBy('verifikasi.id_verifikasi', 'asc')
        ->orderBy('pengamatan.id_pengamatan', 'desc')
        ->get();
    }

    function sort_desc_verified()
    {
        return $this->join('gunung', 'pengamatan.id_gunung', '=', 'gunung.id_gunung')
        ->join('verifikasi', 'pengamatan.id_verifikasi', '=', 'verifikasi.id_verifikasi')
        ->join('validasi', 'pengamatan.id_validasi', '=', 'validasi.id_validasi')
        ->join('pegawai', 'pengamatan.id_pegawai', '=', 'pegawai.id_pegawai')
        ->select([
            'pengamatan.id_pengamatan', 'pengamatan.nama_pengamatan', 'pengamatan.tanggal_pengamatan',
            'gunung.gunung',
            'verifikasi.verifikasi',
            'validasi.validasi',
            'pegawai.nama_pegawai'
        ])
        ->orderBy('verifikasi.id_verifikasi', 'desc')
        ->orderBy('validasi.id_validasi', 'asc')
        ->orderBy('pengamatan.id_pengamatan', 'desc')
        ->get();
    }

    function sort_desc_validated()
    {
        return $this->join('gunung', 'pengamatan.id_gunung', '=', 'gunung.id_gunung')
        ->join('verifikasi', 'pengamatan.id_verifikasi', '=', 'verifikasi.id_verifikasi')
        ->join('validasi', 'pengamatan.id_validasi', '=', 'validasi.id_validasi')
        ->join('pegawai', 'pengamatan.id_pegawai', '=', 'pegawai.id_pegawai')
        ->select([
            'pengamatan.id_pengamatan', 'pengamatan.nama_pengamatan', 'pengamatan.tanggal_pengamatan',
            'gunung.gunung',
            'verifikasi.verifikasi',
            'validasi.validasi',
            'pegawai.nama_pegawai'
        ])
        ->orderBy('validasi.id_validasi', 'desc')
        ->orderBy('verifikasi.id_verifikasi', 'desc')
        ->orderBy('pengamatan.id_pengamatan', 'desc')
        ->get();
    }

    function get_validated()
    {
        return $this->join('gunung', 'pengamatan.id_gunung', '=', 'gunung.id_gunung')
        ->join('verifikasi', 'pengamatan.id_verifikasi', '=', 'verifikasi.id_verifikasi')
        ->join('validasi', 'pengamatan.id_validasi', '=', 'validasi.id_validasi')
        ->join('pegawai', 'pengamatan.id_pegawai', '=', 'pegawai.id_pegawai')
        ->where('verifikasi.verifikasi', 'Lengkap')
        ->where('validasi.validasi', 'Valid')
        ->select([
            'pengamatan.id_pengamatan', 'pengamatan.nama_pengamatan', 'pengamatan.foto_pengamatan', 'pengamatan.tanggal_pengamatan',
            'gunung.gunung',
            'pegawai.nama_pegawai'
        ])
        ->orderBy('pengamatan.tanggal_pengamatan', 'desc');
    }

    function get_by_lower_name($name)
    {
        $sure_name = str_replace("+", " ", $name);

        return $this->join('gunung', 'pengamatan.id_gunung', '=', 'gunung.id_gunung')
        ->join('verifikasi', 'pengamatan.id_verifikasi', '=', 'verifikasi.id_verifikasi')
        ->join('validasi', 'pengamatan.id_validasi', '=', 'validasi.id_validasi')
        ->join('pegawai', 'pengamatan.id_pegawai', '=', 'pegawai.id_pegawai')
        ->where('nama_pengamatan', $sure_name)
        ->select([
            'pengamatan.id_pengamatan', 'pengamatan.nama_pengamatan', 'pengamatan.foto_pengamatan', 'pengamatan.tanggal_pengamatan',
            'gunung.gunung',
            'verifikasi.verifikasi',
            'validasi.validasi',
            'pegawai.nama_pegawai'
        ])->first();
    }

    function get_latest()
    {
        $latest = $this->orderBy('id_pengamatan', 'desc')->first();
        return $latest;
    }

    function get_pengamatan_api()
    {
        return $this->join('gunung', 'pengamatan.id_gunung', '=', 'gunung.id_gunung')
        ->join('verifikasi', 'pengamatan.id_verifikasi', '=', 'verifikasi.id_verifikasi')
        ->join('validasi', 'pengamatan.id_validasi', '=', 'validasi.id_validasi')
        ->where('verifikasi.verifikasi', 'Lengkap')
        ->where('validasi.validasi', 'Valid')
        ->select([
            'pengamatan.nama_pengamatan', 'pengamatan.foto_pengamatan', 'pengamatan.tanggal_pengamatan',
            'gunung.gunung'
        ])->get();
    }

    #==========================
    function getTanggalPengamatanAttribute($tanggal_pengamatan){
        return $this->asDateTime($tanggal_pengamatan)->format('D, d M Y');
    }
}
