<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JenisFasilitas extends Model
{
    #----------------------------------#
    #
    # Setting attribut for model
    # @jenis fasilitas
    protected $table = 'jenis_fasilitas';
    protected $primaryKey = 'id_jenis_fasilitas';

    public $hidden = ['id_gunung'];
    public $incrementing = false;

    #----------------------------------#
    #
    # Set function for model
    #
    function get_by_name($name)
    {
        return $this->where('jenis_fasilitas.jenis_fasilitas', $name);
    }
}
