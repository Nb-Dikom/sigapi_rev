<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fasilitas extends Model
{
    #----------------------------------#
    #
    # Setting attribut for model
    # @fasilitas
    protected $table = 'fasilitas';
    protected $primaryKey = 'id_fasilitas';
    protected $fillable = [
        'id_fasilitas', 'id_jenis_fasilitas', 'id_gunung',
        'id_verifikasi', 'id_validasi', 'id_pegawai',
        'nama_fasilitas', 'alamat_fasilitas', 'foto_fasilitas',
        'longitude', 'latitude',
        'tanggal_fasilitas'
    ];

    public $hidden = ['id_fasilitas'];
    public $timestamps = false;
    public $dates = ['tanggal_fasilitas'];
    public $incrementing = false;

    #----------------------------------#
    #
    # Adding relation for another model
    #
    public function jenis_fasilitas(){
        return $this->belongsTo('App\Models\JenisFasilitas', 'id_jenis_fasilitas');
    }

    public function gunung(){
        return $this->belongsTo('App\Models\Gunung', 'id_gunung');
    }

    public function verifikasi(){
        return $this->belongsTo('App\Models\Verifikasi', 'id_verifikasi');
    }

    public function validasi(){
        return $this->belongsTo('App\Models\Validasi', 'id_validasi');
    }
    
    public function pegawai(){
    	return $this->belongsTo('App\Models\Pegawai', 'id_pegawai');
    }

    #----------------------------------#
    #
    # Set function for model
    #
    function get_all()
    {
        return $this->join('jenis_fasilitas', 'fasilitas.id_jenis_fasilitas', '=', 'jenis_fasilitas.id_jenis_fasilitas')
        ->join('gunung', 'fasilitas.id_gunung', '=', 'gunung.id_gunung')
        ->join('verifikasi', 'fasilitas.id_verifikasi', '=', 'verifikasi.id_verifikasi')
        ->join('validasi', 'fasilitas.id_validasi', '=', 'validasi.id_validasi')
        ->join('pegawai', 'fasilitas.id_pegawai', '=', 'pegawai.id_pegawai')
        ->select([
            'fasilitas.id_fasilitas', 'fasilitas.nama_fasilitas', 'fasilitas.alamat_fasilitas',
            'fasilitas.longitude', 'fasilitas.latitude', 'fasilitas.tanggal_fasilitas',
            'jenis_fasilitas.jenis_fasilitas',
            'gunung.gunung',
            'verifikasi.verifikasi',
            'validasi.validasi',
            'pegawai.nama_pegawai'
        ])
        ->orderBy('validasi.id_validasi', 'asc')
        ->orderBy('verifikasi.id_verifikasi', 'asc')
        ->orderBy('fasilitas.id_fasilitas', 'desc')
        ->get();
    }

    function sort_desc_verified()
    {
        return $this->join('jenis_fasilitas', 'fasilitas.id_jenis_fasilitas', '=', 'jenis_fasilitas.id_jenis_fasilitas')
        ->join('gunung', 'fasilitas.id_gunung', '=', 'gunung.id_gunung')
        ->join('verifikasi', 'fasilitas.id_verifikasi', '=', 'verifikasi.id_verifikasi')
        ->join('validasi', 'fasilitas.id_validasi', '=', 'validasi.id_validasi')
        ->join('pegawai', 'fasilitas.id_pegawai', '=', 'pegawai.id_pegawai')
        ->select([
            'fasilitas.id_fasilitas', 'fasilitas.nama_fasilitas', 'fasilitas.alamat_fasilitas',
            'fasilitas.longitude', 'fasilitas.latitude', 'fasilitas.tanggal_fasilitas',
            'jenis_fasilitas.jenis_fasilitas',
            'gunung.gunung',
            'verifikasi.verifikasi',
            'validasi.validasi',
            'pegawai.nama_pegawai'
        ])
        ->orderBy('verifikasi.id_verifikasi', 'desc')
        ->orderBy('validasi.id_validasi', 'asc')
        ->orderBy('fasilitas.id_fasilitas', 'desc')
        ->get();
    }

    function sort_desc_validated()
    {
        return $this->join('jenis_fasilitas', 'fasilitas.id_jenis_fasilitas', '=', 'jenis_fasilitas.id_jenis_fasilitas')
        ->join('gunung', 'fasilitas.id_gunung', '=', 'gunung.id_gunung')
        ->join('verifikasi', 'fasilitas.id_verifikasi', '=', 'verifikasi.id_verifikasi')
        ->join('validasi', 'fasilitas.id_validasi', '=', 'validasi.id_validasi')
        ->join('pegawai', 'fasilitas.id_pegawai', '=', 'pegawai.id_pegawai')
        ->select([
            'fasilitas.id_fasilitas', 'fasilitas.nama_fasilitas', 'fasilitas.alamat_fasilitas',
            'fasilitas.longitude', 'fasilitas.latitude', 'fasilitas.foto_fasilitas', 'fasilitas.tanggal_fasilitas',
            'jenis_fasilitas.jenis_fasilitas',
            'gunung.gunung',
            'verifikasi.verifikasi',
            'validasi.validasi',
            'pegawai.nama_pegawai'
        ])
        ->orderBy('validasi.id_validasi', 'desc')
        ->orderBy('verifikasi.id_verifikasi', 'desc')
        ->orderBy('fasilitas.id_fasilitas', 'desc')
        ->get();
    }

    function get_validated()
    {
        return $this->join('jenis_fasilitas', 'fasilitas.id_jenis_fasilitas', '=', 'jenis_fasilitas.id_jenis_fasilitas')
        ->join('gunung', 'fasilitas.id_gunung', '=', 'gunung.id_gunung')
        ->join('verifikasi', 'fasilitas.id_verifikasi', '=', 'verifikasi.id_verifikasi')
        ->join('validasi', 'fasilitas.id_validasi', '=', 'validasi.id_validasi')
        ->join('pegawai', 'fasilitas.id_pegawai', '=', 'pegawai.id_pegawai')
        ->where('verifikasi.verifikasi', 'Lengkap')
        ->where('validasi.validasi', 'Valid')
        ->select([
            'fasilitas.id_fasilitas', 'fasilitas.nama_fasilitas', 'fasilitas.alamat_fasilitas',
            'fasilitas.longitude', 'fasilitas.latitude', 'fasilitas.foto_fasilitas', 'fasilitas.tanggal_fasilitas',
            'jenis_fasilitas.jenis_fasilitas',
            'gunung.gunung',
            'pegawai.nama_pegawai'
        ])
        ->orderBy('fasilitas.id_fasilitas', 'desc');
    }

    function get_by_category_validated($category)
    {
        return $this->join('jenis_fasilitas', 'fasilitas.id_jenis_fasilitas', '=', 'jenis_fasilitas.id_jenis_fasilitas')
        ->join('gunung', 'fasilitas.id_gunung', '=', 'gunung.id_gunung')
        ->join('verifikasi', 'fasilitas.id_verifikasi', '=', 'verifikasi.id_verifikasi')
        ->join('validasi', 'fasilitas.id_validasi', '=', 'validasi.id_validasi')
        ->join('pegawai', 'fasilitas.id_pegawai', '=', 'pegawai.id_pegawai')
        ->where('verifikasi.verifikasi', 'Lengkap')
        ->where('validasi.validasi', 'Valid')
        ->where('jenis_fasilitas.jenis_fasilitas', $category)
        ->select([
            'fasilitas.id_fasilitas', 'fasilitas.nama_fasilitas', 'fasilitas.alamat_fasilitas',
            'fasilitas.longitude', 'fasilitas.latitude', 'fasilitas.foto_fasilitas', 'fasilitas.tanggal_fasilitas',
            'jenis_fasilitas.jenis_fasilitas',
            'gunung.gunung',
            'pegawai.nama_pegawai'
        ])
        ->orderBy('fasilitas.id_fasilitas', 'desc');
    }

    function get_by_jenis($jenis)
    {
        return $this->join('jenis_fasilitas', 'fasilitas.id_jenis_fasilitas', '=', 'jenis_fasilitas.id_jenis_fasilitas')
        ->join('verifikasi', 'fasilitas.id_verifikasi', '=', 'verifikasi.id_verifikasi')
        ->join('validasi', 'fasilitas.id_validasi', '=', 'validasi.id_validasi')
        ->where('verifikasi.verifikasi', 'Lengkap')
        ->where('validasi.validasi', 'Valid')
        ->where('jenis_fasilitas', $jenis)
        ->select([
            'fasilitas.id_fasilitas', 'fasilitas.nama_fasilitas', 'fasilitas.alamat_fasilitas',
            'fasilitas.longitude', 'fasilitas.latitude', 'fasilitas.foto_fasilitas', 'fasilitas.tanggal_fasilitas',
            'jenis_fasilitas.jenis_fasilitas',
        ])
        ->orderBy('fasilitas.id_fasilitas', 'desc');
    }

    function get_by_lower_name($name)
    {
        $sure_name = str_replace("+", " ", $name);

        return $this->join('jenis_fasilitas', 'fasilitas.id_jenis_fasilitas', '=', 'jenis_fasilitas.id_jenis_fasilitas')
        ->join('gunung', 'fasilitas.id_gunung', '=', 'gunung.id_gunung')
        ->join('verifikasi', 'fasilitas.id_verifikasi', '=', 'verifikasi.id_verifikasi')
        ->join('validasi', 'fasilitas.id_validasi', '=', 'validasi.id_validasi')
        ->join('pegawai', 'fasilitas.id_pegawai', '=', 'pegawai.id_pegawai')
        ->where('nama_fasilitas', $sure_name)
        ->select([
            'fasilitas.id_fasilitas', 'fasilitas.nama_fasilitas', 'fasilitas.alamat_fasilitas',
            'fasilitas.longitude', 'fasilitas.latitude', 'fasilitas.foto_fasilitas', 'fasilitas.tanggal_fasilitas',
            'jenis_fasilitas.jenis_fasilitas',
            'gunung.gunung',
            'verifikasi.verifikasi',
            'validasi.validasi',
            'pegawai.nama_pegawai'
        ])->first();
    }

    function get_latest()
    {
        $latest = $this->orderBy('id_fasilitas', 'desc')->first();
        return $latest;
    }

    #=========================
    public function getTanggalFasilitasAttribute($tanggal_fasilitas){
        return $this->asDateTime($tanggal_fasilitas)->format('D, d M Y');
    }
}
