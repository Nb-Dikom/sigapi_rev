<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Jabatan extends Model
{
    #----------------------------------#
    #
    # Setting attribut for model
    # @jabatan
    protected $table = 'jabatan';
    protected $primaryKey = 'id_jabatan';
    protected $hidden = ['id_jabatan'];

    public $incrementing = false;
    
    #----------------------------------#
    #
    # Adding relation for another model
    #
    public function pegawai(){
    	return $this->hasOne('App\Models\Pegawai', 'id_jabatan');
    }

    #----------------------------------#
    #
    # Set function for model
    #
    function get_by_name($name)
    {
        return $this->where('jabatan', $name)
        ->first();
    }
}
