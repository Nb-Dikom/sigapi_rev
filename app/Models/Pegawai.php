<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    #----------------------------------#
    #
    # Setting attribut for model
    # @pegawai
    protected $table = 'pegawai';
    protected $primaryKey = 'id_pegawai';
    protected $fillable = [
        'id_pegawai', 'id_jabatan', 'id_user',
        'nama_pegawai', 'email_pegawai',
        'no_telp_pegawai', 'instansi_pegawai', 'foto_pegawai'
    ];
    protected $hidden = ['id_pegawai'];

    public $timestamps = false;
    public $incrementing = false;

    #----------------------------------#
    #
    # Adding relation for another model
    #
    function user()
    {
    	return $this->belongsTo('App\Models\User', 'id_user');
    }

    function jabatan()
    {
        return $this->belongsTo('App\Models\Jabatan', 'id_jabatan');
    }

    #----------------------------------#
    #
    # Set function for model
    #
    function get_all()
    {
        return $this->join('user', 'pegawai.id_user', '=', 'user.id_user')
        ->join('jabatan', 'pegawai.id_jabatan', '=', 'jabatan.id_jabatan')
        ->select([
            'pegawai.nama_pegawai', 'pegawai.email_pegawai', 'pegawai.no_telp_pegawai',
            'user.id_user', 'user.username',
            'jabatan.jabatan',
        ])->orderBy('user.id_user', 'asc')
        ->get();
    }

    function get_by_lower_name($name)
    {
        $sure_name = str_replace("+", " ", $name);

        return $this->join('user', 'pegawai.id_user', '=', 'user.id_user')
        ->join('jabatan', 'pegawai.id_jabatan', '=', 'jabatan.id_jabatan')
        ->where('nama_pegawai', $sure_name)
        ->select([
            'pegawai.id_pegawai', 'pegawai.nama_pegawai', 'pegawai.email_pegawai', 'pegawai.no_telp_pegawai',
            'pegawai.instansi_pegawai', 'pegawai.foto_pegawai',
            'user.id_user', 'user.username',
            'jabatan.jabatan',
        ])->first();
    }

    function get_latest()
    {
        $last = $this->orderBy('id_pegawai', 'desc')->first();
        return $last;
    }
}
