<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Info extends Model
{
    #----------------------------------#
    #
    # Setting attribut for model
    # @info
    protected $table = 'info';
    protected $primaryKey = 'id_info';
    protected $fillable = [
        'id_info', 'id_verifikasi', 'id_validasi', 'id_pegawai',
        'nama_info', 'konten',
        'tanggal_info'
    ];
    protected $hidden = ['id_info'];

    public $timestamps = false;
    protected $dates = ['tanggal_info'];
    public $incrementing = false;

    #----------------------------------#
    #
    # Adding relation for another model
    #
    public function pegawai(){
    	return $this->belongsTo('App\Models\Pegawai', 'id_pegawai');
    }

    #----------------------------------#
    #
    # Set function for model
    #
    function get_all()
    {
        return $this->join('verifikasi', 'info.id_verifikasi', '=', 'verifikasi.id_verifikasi')
        ->join('validasi', 'info.id_validasi', '=', 'validasi.id_validasi')
        ->join('pegawai', 'info.id_pegawai', '=', 'pegawai.id_pegawai')
        ->select([
            'info.nama_info', 'info.konten', 'info.tanggal_info',
            'verifikasi.verifikasi',
            'validasi.validasi',
            'pegawai.nama_pegawai'
        ])
        ->orderBy('validasi.id_validasi', 'asc')
        ->orderBy('verifikasi.id_verifikasi', 'asc')
        ->orderBy('info.id_info', 'desc')
        ->get();
    }

    function sort_desc_verified()
    {
        return $this->join('verifikasi', 'info.id_verifikasi', '=', 'verifikasi.id_verifikasi')
        ->join('validasi', 'info.id_validasi', '=', 'validasi.id_validasi')
        ->join('pegawai', 'info.id_pegawai', '=', 'pegawai.id_pegawai')
        ->select([
            'info.nama_info', 'info.konten', 'info.tanggal_info',
            'verifikasi.verifikasi',
            'validasi.validasi',
            'pegawai.nama_pegawai'
        ])
        ->orderBy('verifikasi.id_verifikasi', 'desc')
        ->orderBy('validasi.id_validasi', 'asc')
        ->orderBy('info.id_info', 'desc')
        ->get();
    }

    function sort_desc_validated()
    {
        return $this->join('verifikasi', 'info.id_verifikasi', '=', 'verifikasi.id_verifikasi')
        ->join('validasi', 'info.id_validasi', '=', 'validasi.id_validasi')
        ->join('pegawai', 'info.id_pegawai', '=', 'pegawai.id_pegawai')
        ->select([
            'info.nama_info', 'info.konten', 'info.tanggal_info',
            'verifikasi.verifikasi',
            'validasi.validasi',
            'pegawai.nama_pegawai'
        ])
        ->orderBy('validasi.id_validasi', 'desc')
        ->orderBy('verifikasi.id_verifikasi', 'desc')
        ->orderBy('info.id_info', 'desc')
        ->get();
    }

    function get_validated()
    {
        return $this->join('verifikasi', 'info.id_verifikasi', '=', 'verifikasi.id_verifikasi')
        ->join('validasi', 'info.id_validasi', '=', 'validasi.id_validasi')
        ->join('pegawai', 'info.id_pegawai', '=', 'pegawai.id_pegawai')
        ->where('verifikasi', 'Lengkap')
        ->where('validasi', 'Valid')
        ->select([
            'info.nama_info', 'info.konten', 'info.tanggal_info',
            'pegawai.nama_pegawai'
        ])
        ->orderBy('info.tanggal_info', 'desc');
    }

    function get_by_lower_name($name)
    {
        $sure_name = str_replace("+", " ", $name);

        return $this->join('verifikasi', 'info.id_verifikasi', '=', 'verifikasi.id_verifikasi')
        ->join('validasi', 'info.id_validasi', '=', 'validasi.id_validasi')
        ->join('pegawai', 'info.id_pegawai', '=', 'pegawai.id_pegawai')
        ->where('nama_info', $sure_name)
        ->select([
            'info.id_info', 'info.nama_info', 'info.konten', 'info.tanggal_info',
            'verifikasi.verifikasi',
            'validasi.validasi',
            'pegawai.nama_pegawai'
        ])->first();
    }

    function get_latest()
    {
        $latest = $this->orderBy('id_info', 'desc')->first();
        return $latest;
    }

    function get_info_api()
    {
        return $this->join('verifikasi', 'info.id_verifikasi', '=', 'verifikasi.id_verifikasi')
        ->join('validasi', 'info.id_validasi', '=', 'validasi.id_validasi')
        ->where('verifikasi.verifikasi', 'Lengkap')
        ->where('validasi.validasi', 'Valid')
        ->select([
            'info.nama_info', 'info.konten', 'info.tanggal_info',
        ])->get();
    }

    #=====================================
    public function getTanggalInfoAttribute($tanggal_info){
        return $this->asDateTime($tanggal_info)->format('D, d M Y');
    }
}
