<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    
    #----------------------------------#
    #
    # Setting attribut for model
    # @user
    protected $table = 'user';
    protected $primaryKey = 'id_user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user', 'id_level',
        'username', 'kata_sandi'
    ];

    public $incrementing = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'kata_sandi', 'remember_token',
    ];

    public $timestamps = false;
    public function getAuthPassword()
    {
        return $this->kata_sandi;
    }

    #----------------------------------#
    #
    # Adding relation for another model
    #
    public function pegawai(){
        return $this->hasOne('App\Models\Pegawai', 'id_user');
    }
    public function organisasi(){
        return $this->hasOne('App\Models\Organisasi', 'id_user');
    }

    public function level(){
        return $this->belongsTo('App\Models\Level', 'id_level');
    }

    #----------------------------------#
    #
    # Set function for model
    #
    function get_latest()
    {
        $last = $this->orderBy('id_user', 'desc')->first();
        return $last;
    }
}
