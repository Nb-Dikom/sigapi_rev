<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Verifikasi extends Model
{
    #----------------------------------#
    #
    # Setting attribut for model
    # @verifikasi
    protected $table = 'verifikasi';
    protected $primaryKey = 'id_verifikasi';

    public $incrementing = false;
    public $hidden = ['id_verifikasi'];

    #----------------------------------#
    #
    # Adding relation for another model
    #
    //

    #----------------------------------#
    #
    # Set function for model
    #
    function get_by_name($name)
    {
        return $this->where('verifikasi', $name)->first();
    }
}