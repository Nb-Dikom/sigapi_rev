<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gunung extends Model
{
    #----------------------------------#
    #
    # Setting attribut for model
    # @gunung
    protected $table = 'gunung';
    protected $primaryKey = 'id_gunung';

    // public $hidden = ['id_gunung'];
    public $timestamps = false;
    public $incrementing = false;

    #----------------------------------#
    #
    # Set function for model
    #
    function get_by_name($name)
    {
        return $this->where('gunung.gunung', $name)
            ->first();
    }
}
