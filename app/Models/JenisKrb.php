<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JenisKrb extends Model
{
    #----------------------------------#
    #
    # Setting attribut for model
    # @jenis krb
    protected $table = 'jenis_krb';
    protected $primaryKey = 'id_jenis_krb';

    public $incrementing = false;

    #----------------------------------#
    #
    # Set function for model
    #
    function get_by_name($name)
    {
        return $this->where('jenis_krb.jenis_krb', $name)
            ->first();
    }
}
