<?php

namespace App\Providers;

use App\Models\Organisasi;
use App\Models\FilePeta;
use App\Models\Fasilitas;
use App\Models\Krb;
use App\Models\Pengamatan;
use App\Models\Prabencana;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        view()->composer('layouts._app', function ($view){
            $view->with('user_provider', Auth::user());
        });
        
        view()->composer('layouts._app-masyarakat', function ($view){
            $view->with('user_provider', Auth::user());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
