<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\LoginController;
use App\Models\Jabatan;
use App\Models\Level;
use App\Models\Pegawai;
use App\Models\User;
use Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PegawaiController extends Controller
{
    function __construct()
    {
        $this->m_pegawai = new Pegawai();
        $this->m_jabatan = new Jabatan();
    }

    protected function get_jabatan()
    {
        return Auth::user()->pegawai->jabatan->jabatan;
    }

    protected function get_level()
    {
        return Auth::user()->level->level;
    }

    protected function create_new_id()
    {
        $user = new User();
        $latest_user = $user->get_latest();
        $new_user = $latest_user->id_user;
        $new_user ++;

        $latest_pegawai = $this->m_pegawai->get_latest();
        $new_pegawai = $latest_pegawai->id_pegawai;
        $new_pegawai ++;

        $data = [
            'id_user' => $new_user,
            'id_pegawai' => $new_pegawai
        ];
        
        return $data;
    }

    function index_pegawai(){
        if($this->get_jabatan() == 'Admin'){
            $list_pegawai = $this->m_pegawai->get_all();
        	return view ('user.pegawai.index-pegawai', compact('list_pegawai'));
        }
        else return response('Unauthorized.', 401);
    }

    function create_pegawai(){
    	if($this->get_jabatan() == 'Admin'){
    		$list_jabatan = Jabatan::all();
    		return view ('user.pegawai.create-pegawai', compact('list_jabatan'));
    	}
    	else return response('Unauthorized.', 401);
    }

    function edit_pegawai($name){
        if($this->get_jabatan() == 'Admin'){
            $pegawai = $this->m_pegawai->get_by_lower_name($name);
            $pegawai->setAttribute('lower_name', $name);
            $list_jabatan = Jabatan::all();

            return view ('user.pegawai.edit-pegawai', compact('pegawai', 'list_jabatan'));
        }
        else return response('Unauthorized.', 401);
    }

    function edit_profile($name){
        if($this->get_level() == 'Pegawai'){
            $pegawai = $this->m_pegawai->get_by_lower_name($name);
            $pegawai->setAttribute('lower_name', $name);
            $list_jabatan = Jabatan::all();

            return view ('user.pegawai.edit-profile', compact('pegawai', 'list_jabatan'));
        }
        else return response('Unauthorized.', 401);
    }

    function checking_validate($type, $data, $model){
        $message = [
            'required' => 'Form tidak boleh kosong',
            'username.alpha_dash' => 'Hanya berisi huruf dan karakter',
            'username.min' => 'Username minimal 3 karakter',
            'username.max' => 'Username maksimal 16 karakter',
            'username.unique' => 'Username telah terdaftar',
            'password.min' => 'Kata sandi minimal 8 karakter',
            'password.confirmed' => 'Kata sandi tidak sama',
            'nama.min' => 'Nama minimal 4 karakter',
            'email.email' => 'Isi dengan alamat email yang valid',
            'email.max' => 'Alamat email maksimal 255 karakter',
            'email.unique' => 'Email telah terdaftar',
            'nomor.min' => 'Nomor telepon minimal 11 karakter',
            'nomor.max' => 'Nomor telepon maksimal 13 karakter',
        ];

        switch($type){
            case'post':
            $this->validate($data, [
                'username' => 'required|alpha_dash|min:4|max:16|unique:user',
                'password' => 'required|string|min:8|confirmed',
                'nama' => 'required|string|min:4',
                'email' => 'required|email|max:255|unique:pegawai,email_pegawai',
                'nomor' => 'required|min:11|max:13',
                'instansi' => 'required',
                'jabatan' => 'required',
            ], $message);
            break;

            case'patch':
                $username_rule = 'required|alpha_dash|min:4|max:16|unique:user';
                $password_rule = 'required|string|min:8|confirmed';
                $email_rule = 'required|email|max:255|unique:pegawai,email_pegawai';

                if($model->user->username == $data->input('username')){
                    $username_rule = '';
                }
                if($model->email_pegawai == $data->input('email')){
                    $email_rule = '';
                }
                if($data->input('password') == null){
                    $password_rule = '';
                }

                $validator = $this->validate($data, [
                        // 'username' => $username_rule,
                        'password' => $password_rule,
                        'nama' => 'required|string|min:4',
                        'email' => $email_rule,
                        'nomor' => 'required|min:11|max:13',
                        'instansi' => 'required',
                        'jabatan' => 'required',
                    ], $message);

            break;
        }
    }

    function store_pegawai(Request $request){
        if($this->get_jabatan() == 'Admin'){
            $this->checking_validate('post', $request, null);
            DB::BeginTransaction();
            try {
                $daftar_level = Level::all();
                $daftar_jabatan = Jabatan::all();
                $create_new = $this->create_new_id();

                $user = User::create([
                    'id_user' => $create_new['id_user'],
                    'id_level' => $daftar_level[0]->id_level,
                	'username' => $request->input('username'),
                	'kata_sandi' => bcrypt($request->input('password')),
                ]);

                $this->m_pegawai->id_pegawai = $create_new['id_pegawai'];
                $this->m_pegawai->id_jabatan = $this->m_jabatan->get_by_name($request->input('jabatan'))->id_jabatan;
                $this->m_pegawai->user()->associate($user);
                $this->m_pegawai->nama_pegawai = $request->input('nama');
                $this->m_pegawai->email_pegawai = $request->input('email');
                $this->m_pegawai->no_telp_pegawai = $request->input('nomor');
                $this->m_pegawai->instansi_pegawai = $request->input('instansi');
                
                if($request->hasFile('photo')){
                    $photo = md5(time()) . '.' . $request->file('photo')->getClientOriginalExtension();
                    $path = public_path() . DIRECTORY_SEPARATOR . '/upload/pegawai';

                    $request->file('photo')->move($path, $photo);
                    $this->m_pegawai->foto_pegawai = $photo;
                }
                $this->m_pegawai->save();

                DB::commit();
                session()->flash('success_message', 'data pegawai telah disimpan');
                return redirect()->route('pegawai_page');

            }catch (\Exception $e) {
                DB::rollback();
                session()->flash('error_message', 'data pegawai tidak disimpan');
                return redirect()->route('pegawai_page');
            }
        }
        else return response('Unauthorized.', 401);
    }

    function update_pegawai($name, Request $request){
        if($this->get_level() == 'Pegawai'){
            $pegawai = $this->m_pegawai->get_by_lower_name($name);
            $this->checking_validate('patch', $request, $pegawai);
            
            DB::BeginTransaction();
            try {
                if($request->input('password') != null){
                    $pegawai->user->kata_sandi = bcrypt($request->input('password'));
                    // $pegawai->user->username = $request->input('username');
                    $pegawai->user->update();
                }

                $pegawai->id_jabatan = $this->m_jabatan->get_by_name($request->input('jabatan'))->id_jabatan;
                $pegawai->nama_pegawai = $request->input('nama');
                $pegawai->email_pegawai = $request->input('email');
                $pegawai->no_telp_pegawai = $request->input('nomor');
                $pegawai->instansi_pegawai = $request->input('instansi');
                if($request->hasFile('photo')){
                    $photo = $pegawai->foto_pegawai;
                    $path = public_path() . DIRECTORY_SEPARATOR . '/upload/pegawai';
                    \File::delete($path .'/'. $photo);

                    $photo = md5(time()) . '.' . $request->file('photo')->getClientOriginalExtension();
                    $request->file('photo')->move($path, $photo);

                    $pegawai->foto_pegawai = $photo;
                }
                $pegawai->update();

                DB::commit();
                session()->flash('success_message', 'data pegawai telah disimpan');

                if($request->input('profile') == "profile"){
                    return redirect()->route('dashboard_page');
                }
                else return redirect()->route('pegawai_page');
                
            } catch (\Exception $e) {
                DB::rollback();
                session()->flash('error_message', 'data pegawai tidak disimpan');
                if($request->input('profile') == "profile"){
                    return redirect()->route('dashboard_page');
                }
                else return redirect()->route('pegawai_page');
            }
        }
        else return response('Unauthorized.', 401);
    }

    function destroy_pegawai($name){
        if($this->get_jabatan() == 'Admin'){
            $pegawai = $this->m_pegawai->get_by_lower_name($name);
            $pegawai->user->delete();

            session()->flash('success_message', 'data pegawai telah dihapus');
            return redirect()->route('pegawai_page');
        }
        else return response('Unauthorized.', 401);
    }
}
