<?php

namespace App\Http\Controllers;

use App\Models\Gunung;
use App\Models\Pengamatan;
use App\Models\Verifikasi;
use App\Models\Validasi;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PengamatanController extends Controller
{
    function __construct()
    {
        $this->m_pengamatan = new Pengamatan();
    }

    protected function get_jabatan()
    {
        return Auth::user()->pegawai->jabatan->jabatan;
    }

    protected function get_level()
    {
        return Auth::user()->level->level;
    }

    protected function create_new_id()
    {
        $latest_pengamatan = $this->m_pengamatan->get_latest();
        $new_pengamatan = $latest_pengamatan->id_pengamatan;
        $new_pengamatan ++;
        
        return $new_pengamatan;
    }
    
    public function index_pengamatan()
    {
        //
        if($this->get_level() == "Pegawai"){
            $jabatan = $this->get_jabatan();
            switch ($jabatan) {
                case "Sub Bagian Penanggulangan Bencana":
                case "Sub Bagian Umum dan Kepegawaian":
                    $list_pengamatan = $this->m_pengamatan->get_validated()->paginate(8);
                    break;

                case "Kepala Bidang Pencegahan dan Kesiapsiagaan":
                    $list_pengamatan = $this->m_pengamatan->sort_desc_verified();
                    break;

                case "Kepala BPBD Jawa Timur":
                    $list_pengamatan = $this->m_pengamatan->sort_desc_validated();
                    break;
                    
                default:
                    $list_pengamatan = $this->m_pengamatan->get_all();
                    break;
            }

            return view ('pengamatan.index-pengamatan', compact('list_pengamatan', 'jabatan'));
        }
        else return response('Unauthorized.', 401);
    }

    public function pengamatan_page(Request $request)
    {
        $list_pengamatan = $this->m_pengamatan->get_validated()->get();
        $jumlah = $list_pengamatan->count();

        foreach ($list_pengamatan as $key => $pengamatan) {
            $year_obj = date('Y', strtotime($pengamatan->tanggal_pengamatan));
            $year[$year_obj] = 'tahun';
        }
        foreach($year as $key => $string){
            $index = 0;
            foreach ($list_pengamatan as $pengamatan) {
                $year_obj = date('Y', strtotime($pengamatan->tanggal_pengamatan));
                if( $key == $year_obj)
                    $index++;
            }
            $list_tahun[] = [
                "tahun" => $key,
                "total" => $index
            ];
        }

        if($request->input('tahun'))
            $list_pengamatan = $this->m_pengamatan->whereYear('tanggal_pengamatan', $request->input('tahun'))->paginate(10);
        else
            $list_pengamatan = $this->m_pengamatan->get_validated()->paginate(10);

        return view ('pengamatan', compact('list_pengamatan', 'jumlah', 'list_tahun'));
    }

    public function view_pengamatan($name)
    {
        $list_pengamatan = $this->m_pengamatan->get_validated()->get();
        $jumlah = $list_pengamatan->count();

        foreach ($list_pengamatan as $key => $pengamatan) {
            $year_obj = date('Y', strtotime($pengamatan->tanggal_pengamatan));
            $year[$year_obj] = 'tahun';
        }
        foreach($year as $key => $string){
            $index = 0;
            foreach ($list_pengamatan as $pengamatan) {
                $year_obj = date('Y', strtotime($pengamatan->tanggal_pengamatan));
                if( $key == $year_obj)
                    $index++;
            }
            $list_tahun[] = [
                "tahun" => $key,
                "total" => $index
            ];
        }

        $pengamatan = $this->m_pengamatan->get_by_lower_name($name);
        $pengamatan->setAttribute('lower_name', $name);

        return view ('detail-pengamatan', compact('pengamatan', 'jumlah', 'list_tahun'));
    }

    public function create_pengamatan()
    {
        //
        if($this->get_level() == "Pegawai"){
            $list_gunung = Gunung::all();
            return view ('pengamatan.create-pengamatan', compact('list_gunung'));
        }
        else return response('Unauthorized.', 401);
    }

    public function edit_pengamatan($name)
    {
        //
        if($this->get_level() == "Pegawai"){
            $list_gunung = Gunung::all();
            $pengamatan = $this->m_pengamatan->get_by_lower_name($name);
            $pengamatan->setAttribute('lower_name', $name);

            return view ('pengamatan.edit-pengamatan', compact('pengamatan', 'list_gunung'));
        }
        else return response('Unauthorized.', 401);
    }

    public function show_pengamatan($name)
    {
        //
        if($this->get_level() == "Pegawai"){
            $jabatan = $this->get_jabatan();
            try{
                $pengamatan = $this->m_pengamatan->get_by_lower_name($name);
                $pengamatan->setAttribute('lower_name', $name);
                switch ($jabatan) {
                    case "Sub Bagian Penanggulangan Bencana":
                    case "Sub Bagian Umum dan Kepegawaian":
                        return view ('pengamatan.detail-pengamatan', compact('pengamatan'));
                        break;

                    default:
                        if($pengamatan) return response()->json($pengamatan, 200);
                        else return response()->json('error', 400);
                        break;
                }

            } catch (\Exception $exception){
                $message = $exception->getMessage();
                $response = [
                    'code' => 2,
                    'message' => $message
                ];
                switch ($jabatan) {
                    case "Sub Bagian Penanggulangan Bencana":
                    case "Sub Bagian Umum dan Kepegawaian":
                        session()->flash('error_message', 'terjadi kesalahan');
                        return redirect(url()->previous());
                        break;

                    default:
                        return response()->json($response, 400);
                        break;
                }
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function checking_validate($type, $data){
        $message = [
            'required' => 'Form tidak boleh kosong',
            'nama.string' => 'Hanya berisi huruf dan karakter',
            'nama.min' => 'Username minimal 3 karakter',
            'file.images' => 'File hanya gambar',
            'file.max' => 'Ukuran file maksimal 8mb'
        ];

        switch($type){
            case'post':
            $this->validate($data, [
                'nama' => 'required|string|min:3',
                'file' => 'required|image|mimes:jpeg,png,jpg,JPG|max:8000'
            ], $message);
            break;

            case'patch':
            $file_rule = '';
            if($data->hasFile('file')){
                $file_rule = 'image|mimes:jpeg,png,jpg,JPG|max:8000';
            }
            $this->validate($data, [
                'nama' => 'required|string|min:3',
                'gunung' => 'required',
                'file' => $file_rule
            ], $message);

            default:break;
        }
    }
    
    public function store_pengamatan(Request $request, Gunung $m_gunung){
        //
        if($this->get_level() == "Pegawai"){
            $this->checking_validate('post', $request);
            if($request->hasFile('file')){
                DB::BeginTransaction();
                try {
                    $daftar_verifikasi = Verifikasi::all();
                    $daftar_validasi = Validasi::all();
                    $create_new = $this->create_new_id();
                    $gunung = $m_gunung->get_by_name($request->input('gunung'));

                    $pengamatan_name = md5(time()) . '.' . $request->file('file')->getClientOriginalExtension();
                    $path = public_path() . DIRECTORY_SEPARATOR . '/upload/pengamatan';

                    $pengamatan = Pengamatan::create([
                        'id_pengamatan' => $create_new,
                        'id_gunung' => $gunung->id_gunung,
                        'id_verifikasi' => $daftar_verifikasi[0]->id_verifikasi,
                        'id_validasi' => $daftar_validasi[0]->id_validasi,
                        'id_pegawai' => Auth::user()->pegawai->id_pegawai,
                        'nama_pengamatan' => $request->input('nama'),
                        'foto_pengamatan' => $pengamatan_name,
                        'tanggal_pengamatan' => Carbon::createFromFormat('d/m/Y', $request->input('tanggal')),
                    ]);
                    $request->file('file')->move($path, $pengamatan_name);

                    DB::commit();
                    session()->flash('success_message', 'data pengamatan telah disimpan');
                    return redirect()->route('pengamatan_page');

                }catch (\Exception $e) {
                    DB::rollback();
                    session()->flash('error_message', 'data pengamatan tidak disimpan');
                    return redirect()->route('pengamatan_page');
                }
            }
            else{
                session()->flash('error_message', 'data pengamatan tidak disimpan');
                return redirect()->back()->withInput($request);
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function update_pengamatan($name, Request $request, Gunung $m_gunung)
    {
        //
        if($this->get_level() == "Pegawai"){
            $this->checking_validate('patch', $request);
            DB::BeginTransaction();
            try {
                $pengamatan = $this->m_pengamatan->get_by_lower_name($name);
                $gunung = $m_gunung->get_by_name($request->input('gunung'));
                $name_file = $pengamatan->foto_pengamatan;

                if($request->hasFile('file')){
                    $path = public_path() . DIRECTORY_SEPARATOR . '/upload/pengamatan';
                    \File::delete($path .'/'. $name_file);

                    $name_file = md5(time()) . '.' . $request->file('file')->getClientOriginalExtension();
                }
                $pengamatan->update([
                    'id_gunung' => $gunung->id_gunung,
                    'nama_pengamatan' => $request->input('nama'),
                    'foto_pengamatan' => $name_file,
                    'tanggal_pengamatan' => Carbon::createFromFormat('d/m/Y', $request->input('tanggal'))
                ]);

                if($request->hasFile('file')) $request->file('file')->move($path, $name_file);

                DB::commit();
                session()->flash('success_message', 'data pengamatan telah disimpan');
                return redirect()->route('pengamatan_page');

            }catch (\Exception $e) {
                DB::rollback();
                session()->flash('error_message', 'data pengamatan tidak disimpan');
                return redirect()->route('pengamatan_page');
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function destroy_pengamatan($name)
    {
        //
        if($this->get_level() == "Pegawai"){
            $pengamatan = $this->m_pengamatan->get_by_lower_name($name);
            $path = public_path() . DIRECTORY_SEPARATOR . '/upload/pengamatan';
            \File::delete($path .'/'. $pengamatan->foto_pengamatan);
            $pengamatan->delete();

            session()->flash('success_message', 'data pengamatan telah dihapus');
            return redirect()->route('pengamatan_page');
        }
        else return response('Unauthorized.', 401);
    }

    public function verified(Request $request)
    {
        if($this->get_level() == "Pegawai"){
            DB::BeginTransaction();
            try {
                $pengamatan = $this->m_pengamatan->get_by_lower_name($request->input('name'));
                $m_verifikasi = new Verifikasi();
                $verified = $m_verifikasi->get_by_name("Lengkap");

                if($pengamatan->verifikasi == "Pending")
                {
                    $pengamatan->update([
                        'id_verifikasi' => $verified->id_verifikasi
                    ]);
                    
                    DB::commit();
                    $pengamatan = $this->m_pengamatan->get_by_lower_name($request->input('name'));
                }

                $pengamatan->setAttribute('lower_name', $request->input('name'));                
                return $this->success($pengamatan);

            }catch (\Exception $e) {
                DB::rollback();
                return $this->error($e->getMessage());
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function unverified(Request $request)
    {
        if($this->get_level() == "Pegawai"){
            DB::BeginTransaction();
            try {
                $pengamatan = $this->m_pengamatan->get_by_lower_name($request->input('name'));
                $m_verifikasi = new Verifikasi();
                $unverified = $m_verifikasi->get_by_name("Tidak Lengkap");

                if($pengamatan->verifikasi == "Pending")
                {
                    $pengamatan->update([
                        'id_verifikasi' => $unverified->id_verifikasi
                    ]);
                    
                    DB::commit();
                    $pengamatan = $this->m_pengamatan->get_by_lower_name($request->input('name'));
                }
                $pengamatan->setAttribute('lower_name', $request->input('name'));                
                return $this->success($pengamatan);

            }catch (\Exception $e) {
                DB::rollback();
                return $this->error($e->getMessage());
            }
        }
        else return response('Unauthorized.', 401);
    }
    
    public function validated(Request $request)
    {
        if($this->get_level() == "Pegawai"){
            DB::BeginTransaction();
            try {
                $pengamatan = $this->m_pengamatan->get_by_lower_name($request->input('name'));
                $m_validasi = new Validasi();
                $validated = $m_validasi->get_by_name("Valid");

                if($pengamatan->verifikasi == "Lengkap" && $pengamatan->validasi == "Pending")
                {
                    $pengamatan->update([
                        'id_validasi' => $validated->id_validasi
                    ]);
                    
                    DB::commit();
                    $pengamatan = $this->m_pengamatan->get_by_lower_name($request->input('name'));
                }

                $pengamatan->setAttribute('lower_name', $request->input('name'));                
                return $this->success($pengamatan);

            }catch (\Exception $e) {
                DB::rollback();
                return $this->error($e->getMessage());
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function unvalidated(Request $request)
    {
        if($this->get_level() == "Pegawai"){
            DB::BeginTransaction();
            try {
                $pengamatan = $this->m_pengamatan->get_by_lower_name($request->input('name'));
                $m_validasi = new Validasi();
                $unvalidated = $m_validasi->get_by_name("Tidak Valid");

                if($pengamatan->verifikasi == "Lengkap" && $pengamatan->validasi == "Pending")
                {
                    $pengamatan->update([
                        'id_validasi' => $unvalidated->id_validasi
                    ]);
                    
                    DB::commit();
                    $pengamatan = $this->m_pengamatan->get_by_lower_name($request->input('name'));
                }
                $pengamatan->setAttribute('lower_name', $request->input('name'));                
                return $this->success($pengamatan);

            }catch (\Exception $e) {
                DB::rollback();
                return $this->error($e->getMessage());
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function download($name){
        if(Auth::user()){
            $pengamatan = $this->m_pengamatan->get_by_lower_name($name);
            $path = public_path('/upload/pengamatan'). '/' . $pengamatan->foto_pengamatan;

            return response()->download($path);
        }
        else return redirect()->route('form_login');
    }

    /**
     * List module for Mobile API
     */

    public function list_pengamatan(){
        try{
            $list_pengamatan = $this->m_pengamatan->get_pengamatan_api();
            foreach ($list_pengamatan as $pengamatan) {
                $data[] = [
                    'nama' => $pengamatan->nama_pengamatan,
                    'img' => $pengamatan->foto_pengamatan,
                    'img_path' => asset('/upload/pengamatan'). '/',
                    'tanggal' => $pengamatan->tanggal_pengamatan,
                    'gunung' => $pengamatan->gunung,
                ];
            }
            
            return $this->success($data);

        }catch (\Exception $e) {
            
            return $this->error($e->getMessage());
        }
   }
}
