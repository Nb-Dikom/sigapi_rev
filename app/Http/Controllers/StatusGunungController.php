<?php

namespace App\Http\Controllers;

use App\Models\Gunung;
use App\Models\StatusGunung;
use App\Models\Status;
use App\Models\Verifikasi;
use App\Models\Validasi;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StatusGunungController extends Controller
{
    function __construct()
    {
        $this->m_status_gunung = new StatusGunung();
    }

    protected function get_jabatan()
    {
        return Auth::user()->pegawai->jabatan->jabatan;
    }

    protected function get_level()
    {
        return Auth::user()->level->level;
    }

    protected function create_new_id()
    {
        $latest_status_gunung = $this->m_status_gunung->get_latest();
        $new_status_gunung = $latest_status_gunung->id_status_gunung;
        $new_status_gunung ++;
        
        return $new_status_gunung;
    }
    
    public function index_status_gunung()
    {
        //
        if($this->get_level() == "Pegawai"){
            $jabatan = $this->get_jabatan();
            switch ($jabatan) {
                case "Sub Bagian Umum dan Kepegawaian":
                    $list_status_gunung = $this->m_status_gunung->get_validated();
                    break;

                case "Kepala Bidang Pencegahan dan Kesiapsiagaan":
                    $list_status_gunung = $this->m_status_gunung->sort_desc_verified();
                    break;

                case "Kepala BPBD Jawa Timur":
                    $list_status_gunung = $this->m_status_gunung->sort_desc_validated();
                    break;

                default:
                    $list_status_gunung = $this->m_status_gunung->get_all();
                    break;
            }

            return view ('status-gunung.index-status-gunung', compact('list_status_gunung', 'jabatan'));
        }
        else return response('Unauthorized.', 401);
    }

    public function status_gunung_page(Request $request)
    {
        $list_status_gunung = $this->m_status_gunung->get_valid_status()->get();
        $jumlah = $list_status_gunung->count();

        return view ('status-gunung', compact('list_status_gunung', 'jumlah'));
    }

    public function view_status_gunung($name)
    {
        $list_status_gunung = $this->m_status_gunung->get_valid_status()->get();
        $jumlah = $list_status_gunung->count();
        $awas = 0; $siaga = 0; $waspada = 0;
        foreach($list_status_gunung as $status_gunung){
            switch ($status_gunung->status) {
                case 'AWAS':
                    $awas++;
                    break;
                case 'SIAGA':
                    $siaga++;
                    break;
                case 'WASPADA':
                    $waspada++;
                    break;
            }
        }
        $data['jumlah'] = $jumlah; $data['awas'] = $awas; $data['siaga'] = $siaga; $data['waspada'] = $waspada;

        $status_gunung = $this->m_status_gunung->get_by_lower_name($name);
        $status_gunung->setAttribute('lower_name', $name);

        return view ('detail-status-gunung', compact('status_gunung'))->with($data);
    }

    public function create_status_gunung()
    {
        //
        if($this->get_level() == "Pegawai"){
            $list_gunung = Gunung::all();
            $list_status = Status::all();
            return view ('status-gunung.create-status-gunung', compact('list_status', 'list_gunung'));
        }
        else return response('Unauthorized.', 401);
    }

    public function edit_status_gunung($name)
    {
        //
        if($this->get_level() == "Pegawai"){
            $list_gunung = Gunung::all();
            $status_gunung = $this->m_status_gunung->get_by_lower_name($name);
            $status_gunung->setAttribute('lower_name', $name);
            $list_status = Status::all();

            return view ('status-gunung.edit-status-gunung', compact('status_gunung', 'list_gunung', 'list_status'));
        }
        else return response('Unauthorized.', 401);
    }

    public function show_status_gunung($name)
    {
        //
        if($this->get_level() == "Pegawai"){
            $jabatan = $this->get_jabatan();
            try{
                $status_gunung = $this->m_status_gunung->get_by_lower_name($name);
                $status_gunung->setAttribute('lower_name', $name);
                switch ($jabatan) {
                    case "Sub Bagian Umum dan Kepegawaian":
                        return view ('status-gunung.detail-status-gunung', compact('status_gunung'));
                        break;

                    default:
                        if($status_gunung) return response()->json($status_gunung, 200);
                        else return response()->json('error', 400);
                        break;
                }
                
            } catch (\Exception $exception){
                $message = $exception->getMessage();
                $response = [
                    'code' => 2,
                    'message' => $message
                ];
                switch ($jabatan) {
                    case "Sub Bagian Penanggulangan Bencana":
                    case "Sub Bagian Umum dan Kepegawaian":
                        session()->flash('error_message', 'terjadi kesalahan');
                        return redirect(url()->previous());
                        break;

                    default:
                    return response()->json($response, 400);
                        break;
                }
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function checking_validate($type, $data){
        $message = [
            'required' => 'Form tidak boleh kosong',
        ];

        $this->validate($data, [
            'gunung' => 'required',
            'status' => 'required',
            'tanggal' => 'required',
        ], $message);
    }
    
    public function store_status_gunung(Request $request, Status $m_status, Gunung $m_gunung){
        //
        if($this->get_level() == "Pegawai"){
            $this->checking_validate('post', $request);
            DB::BeginTransaction();
            try {
                $daftar_verifikasi = Verifikasi::all();
                $daftar_validasi = Validasi::all();
                $create_new = $this->create_new_id();
                $status = $m_status->get_by_name($request->input('status'));
                $gunung = $m_gunung->get_by_name($request->input('gunung'));

                $status_gunung = StatusGunung::create([
                    'id_status_gunung' => $create_new,
                    'id_status' => $status->id_status,
                    'id_gunung' => $gunung->id_gunung,
                    'id_verifikasi' => $daftar_verifikasi[0]->id_verifikasi,
                    'id_validasi' => $daftar_validasi[0]->id_validasi,
                    'id_pegawai' => Auth::user()->pegawai->id_pegawai,
                    'tanggal_status_gunung' => Carbon::createFromFormat('d/m/Y', $request->input('tanggal')),
                ]);

                DB::commit();
                session()->flash('success_message', 'data status gunung telah disimpan');
                return redirect()->route('status_gunung_page');

            }catch (\Exception $e) {
                return $e;
                DB::rollback();
                session()->flash('error_message', 'data status gunung tidak disimpan');
                return redirect()->route('status_gunung_page');
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function update_status_gunung($name, Request $request, Status $m_status, Gunung $m_gunung)
    {
        //
        if($this->get_level() == "Pegawai"){
            $this->checking_validate('patch', $request);
            DB::BeginTransaction();
            try {
                $status_gunung = $this->m_status_gunung->get_by_lower_name($name);
                $status = $m_status->get_by_name($request->input('status'));
                $gunung = $m_gunung->get_by_name($request->input('gunung'));

                $status_gunung->update([
                    'id_status' => $status->id_status,
                    'id_gunung' => $gunung->id_gunung,
                    'tanggal_status' => Carbon::createFromFormat('d/m/Y', $request->input('tanggal'))
                ]);

                DB::commit();
                session()->flash('success_message', 'data status gunung telah disimpan');
                return redirect()->route('status_gunung_page');

            }catch (\Exception $e) {
                return $e;
                DB::rollback();
                session()->flash('error_message', 'data status gunung tidak disimpan');
                return redirect()->route('status_gunung_page');
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function destroy_status_gunung($name)
    {
        //
        if($this->get_level() == "Pegawai"){
            $status_gunung = $this->m_status_gunung->get_by_lower_name($name);
            $status_gunung->delete();

            session()->flash('success_message', 'data status gunung telah dihapus');
            return redirect()->route('status_gunung_page');
        }
        else return response('Unauthorized.', 401);
    }

    public function verified(Request $request)
    {
        if($this->get_level() == "Pegawai"){
            DB::BeginTransaction();
            try {
                $status_gunung = $this->m_status_gunung->get_by_lower_name($request->input('name'));
                $m_verifikasi = new Verifikasi();
                $verified = $m_verifikasi->get_by_name("Lengkap");

                if($status_gunung->verifikasi == "Pending")
                {
                    $status_gunung->update([
                        'id_verifikasi' => $verified->id_verifikasi
                    ]);
                    
                    DB::commit();
                    $status_gunung = $this->m_status_gunung->get_by_lower_name($request->input('name'));
                }

                $status_gunung->setAttribute('lower_name', $request->input('name'));                
                return $this->success($status_gunung);

            }catch (\Exception $e) {
                DB::rollback();
                return $this->error($e->getMessage());
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function unverified(Request $request)
    {
        if($this->get_level() == "Pegawai"){
            DB::BeginTransaction();
            try {
                $status_gunung = $this->m_status_gunung->get_by_lower_name($request->input('name'));
                $m_verifikasi = new Verifikasi();
                $unverified = $m_verifikasi->get_by_name("Tidak Lengkap");

                if($status_gunung->verifikasi == "Pending")
                {
                    $status_gunung->update([
                        'id_verifikasi' => $unverified->id_verifikasi
                    ]);
                    
                    DB::commit();
                    $status_gunung = $this->m_status_gunung->get_by_lower_name($request->input('name'));
                }
                $status_gunung->setAttribute('lower_name', $request->input('name'));                
                return $this->success($status_gunung);

            }catch (\Exception $e) {
                DB::rollback();
                return $this->error($e->getMessage());
            }
        }
        else return response('Unauthorized.', 401);
    }
    
    public function validated(Request $request)
    {
        if($this->get_level() == "Pegawai"){
            DB::BeginTransaction();
            try {
                $status_gunung = $this->m_status_gunung->get_by_lower_name($request->input('name'));
                $m_validasi = new Validasi();
                $validated = $m_validasi->get_by_name("Valid");

                if($status_gunung->verifikasi == "Lengkap" && $status_gunung->validasi == "Pending")
                {
                    $status_gunung->update([
                        'id_validasi' => $validated->id_validasi
                    ]);
                    
                    DB::commit();
                    $status_gunung = $this->m_status_gunung->get_by_lower_name($request->input('name'));
                }

                $status_gunung->setAttribute('lower_name', $request->input('name'));                
                return $this->success($status_gunung);

            }catch (\Exception $e) {
                DB::rollback();
                return $this->error($e->getMessage());
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function unvalidated(Request $request)
    {
        if($this->get_level() == "Pegawai"){
            DB::BeginTransaction();
            try {
                $status_gunung = $this->m_status_gunung->get_by_lower_name($request->input('name'));
                $m_validasi = new Validasi();
                $unvalidated = $m_validasi->get_by_name("Tidak Valid");

                if($status_gunung->verifikasi == "Lengkap" && $status_gunung->validasi == "Pending")
                {
                    $status_gunung->update([
                        'id_validasi' => $unvalidated->id_validasi
                    ]);
                    
                    DB::commit();
                    $status_gunung = $this->m_status_gunung->get_by_lower_name($request->input('name'));
                }
                $status_gunung->setAttribute('lower_name', $request->input('name'));                
                return $this->success($status_gunung);

            }catch (\Exception $e) {
                DB::rollback();
                return $this->error($e->getMessage());
            }
        }
        else return response('Unauthorized.', 401);
    }

    /**
     * List module for Mobile API
     */

    public function list_status_gunung(){
        try{
            $list_status = Status::all();
            $list_status_gunung = $this->m_status_gunung->get_valid_status()->get();
            
            foreach ($list_status as $status) {
                if($status->status !== "NORMAL"){
                    $obj = null;
                    foreach ($list_status_gunung as $status_gunung) {
                        if($status->status == $status_gunung->status){
                            $obj[] = [
                                'gunung' => $status_gunung->gunung,
                                'status' => $status->status,
                                'tanggal' => $status_gunung->tanggal_status_gunung,
                            ];
                        }
                    }
                    $data[] = [
                        'status' => $status->status,
                        'status_gunung' => $obj
                    ];
                }
            }
            return $this->success($data);

        }catch (\Exception $e) {
            return $this->error($e->getMessage());
        }
   }

   public function list_status_gunung_by_name($status){
        try{
            $list_status_gunung = $this->m_status_gunung->get_valid_status_by_status($status)->get();
            
            foreach ($list_status_gunung as $status_gunung) {
                $data[] = [
                    'gunung' => $status_gunung->gunung,
                    'status' => $status_gunung->status,
                    'tanggal' => $status_gunung->tanggal_status_gunung,
                ];
            }

            return $this->success($data);

        }catch (\Exception $e) {
            return $this->error($e->getMessage());
        }   
   }
}
