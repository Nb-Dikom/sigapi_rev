<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'username' => 'required|string|min:4|max:16|unique:user',
            'password' => 'required|string|min:8|confirmed'
            ],[
            'required' => 'Form tidak boleh kosong',
            'username.min' => 'Username minimal 3 karakter',
            'username.max' => 'Username maksimal 16 karakter',
            'password.min' => 'Username minimal 8 karakter',
            'password.confirmed' => 'Kata sandi tidak sama'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'username' => $data['username'],
            'kata_sandi' => bcrypt($data['password']),
        ]);
    }

    public function showRegistrationForm()
    {
        return redirect('/daftar');
        // return view('auth._register');
    }
}
