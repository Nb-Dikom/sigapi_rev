<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\Models\User;
use App\Models\Organisasi;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function show_login()
    {
        return view('auth.login');
    }

    protected function validate_login(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);
    }

    public function login(Request $request)
    {
        $this->validate_login($request);

        // checking account has level 'Pegawai' or 'Member=>Setuju'
        $user = User::where('username', $request->input('username'))->first();
        if($user){
            if($user->level->level == 'Pegawai'){
                return $this->build_login($request);
            }
            if($user->level->level == "Organisasi"){
                if($user->organisasi->status_organisasi->status_organisasi == "Setuju"){
                    return $this->build_login($request);
                }
                else{
                    $errors = [$this->username() => trans('auth.not_validated')];
                    return redirect()->back()
                        ->withInput($request->only($this->username(), 'remember'))
                        ->withErrors($errors);
                }
            }
            
        }
        else {
            return $this->build_login($request);
        }
    }

    public function build_login($request){
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function username()
    {
        return 'username';
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect()->route('form_login');
    }

    public function dashboard(){
        if(Auth::user()){
            $total_organisasi = count(Organisasi::all());
            return view('dashboard', compact('total_organisasi'));
        }
        else return response('Unauthorized.', 401);
    }

    protected function authenticated(Request $request, $user)
    {
        //
        if(Auth::user()->level->level == "Pegawai"){
            return redirect()->route('dashboard_page');
        }
        else{
            return redirect('/');
        }
    }
}
