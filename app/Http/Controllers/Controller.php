<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function success($data)
    {
        $response = [
            'code' => 1,
            'data' => $data,
            'message' => 'success'
        ];
        return response()->json($response, 200);
    }

    public function error($error)
    {
        $response = [
            'code' => 2,
            'message' => "Data tidak ditemukan"
        ];
        return response()->json($response, 502);
    }
}
