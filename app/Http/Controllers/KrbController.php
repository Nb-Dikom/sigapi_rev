<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ArcgisServiceController;
use App\Models\Krb;
use App\Models\Gunung;
use App\Models\JenisKrb;
use App\Models\Verifikasi;
use App\Models\Validasi;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KrbController extends Controller
{
    function __construct()
    {
        $this->m_krb = new Krb();
    }

    protected function get_jabatan()
    {
        return Auth::user()->pegawai->jabatan->jabatan;
    }

    protected function get_level()
    {
        return Auth::user()->level->level;
    }

    protected function create_new_id()
    {
        $latest_krb = $this->m_krb->get_latest();
        $new_krb = $latest_krb->id_krb;
        $new_krb ++;
        
        return $new_krb;
    }

    public function index_krb(){
        if($this->get_level() == "Pegawai"){
            $jabatan = $this->get_jabatan();
            switch ($jabatan) {
                case "Kepala Bidang Pencegahan dan Kesiapsiagaan":
                    $list_krb = $this->m_krb->sort_desc_verified();
                    break;

                case "Kepala BPBD Jawa Timur":
                    $list_krb = $this->m_krb->sort_desc_validated();
                    break;

                default:
                    $list_krb = $this->m_krb->get_all();
                    break;
            }

            return view ('krb.index-krb', compact('list_krb', 'jabatan'));
        }
        else return response('Unauthorized.', 401);
    }

    public function create_krb(ArcgisServiceController $service){
        if($this->get_level() == "Pegawai"){
            $collection = [
                'jenis_krb' => JenisKrb::all(),
                'gunung' => Gunung::all(),
                'file_krb' => $service->sync_feature_service(),
            ];
            return view ('krb.create-krb', compact('collection'));
        }
        else return response('Unauthorized.', 401);
    }

    public function edit_krb($name, ArcgisServiceController $service){
        if($this->get_level() == "Pegawai"){
            $krb = $this->m_krb->get_by_lower_name($name);
            $krb->setAttribute('lower_name', $name);

            $collection = [
                'krb' => $krb,
                'jenis_krb' => JenisKrb::all(),
                'gunung' => Gunung::all(),
                'file_krb' => $service->sync_feature_service()
            ];
            return view ('krb.edit-krb', compact('collection'));
        }
        else return response('Unauthorized.', 401);
    }

    public function show_krb($name){
        if($this->get_level() == "Pegawai"){
            try{
                $krb = $this->m_krb->get_by_lower_name($name);
                $krb->setAttribute('lower_name', $name);

                if($krb) return response()->json($krb, 200);
                else return response()->json('error', 400);
                
            } catch (\ModelNotFoundException $exception){
                $message = $exception->getMessage();
                $response = [
                    'code' => 2,
                    'message' => $message
                ];
                return response()->json($response, 400);
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function checking_validate($type, $data){
        $message = [
            'required' => 'Tidak boleh kosong',
        ];

        switch($type){
            case'post':
            $this->validate($data, [
                'jenis_krb' => 'required',
                'gunung' => 'required',
                'json_file' => 'required'
            ], $message);
            break;

            case'patch':
            $this->validate($data, [
                'jenis_krb' => 'required',
                'gunung' => 'required',
                'json_file' => 'required'
            ], $message);
            default:break;
        }
    }

    public function store_krb(Request $request, Gunung $m_gunung, JenisKrb $m_jenis_krb){
        if($this->get_level() == "Pegawai"){
            $this->checking_validate('post', $request);
            DB::BeginTransaction();
            try {
                $daftar_verifikasi = Verifikasi::all();
                $daftar_validasi = Validasi::all();
                $create_new = $this->create_new_id();
                $gunung = $m_gunung->get_by_name($request->input('gunung'));
                $jenis_krb = $m_jenis_krb->get_by_name($request->input('jenis_krb'));

                $krb = Krb::create([
                    'id_krb' => $create_new,
                    'id_gunung' => $gunung->id_gunung,
                    'id_jenis_krb' => $jenis_krb->id_jenis_krb,
                    'id_verifikasi' => $daftar_verifikasi[0]->id_verifikasi,
                    'id_validasi' => $daftar_validasi[0]->id_validasi,
                    'id_pegawai' => Auth::user()->pegawai->id_pegawai,
                    'file_krb' => $request->input('json_file'),
                    'tanggal_krb' => Carbon::now(),
                ]);

                DB::commit();
                session()->flash('success_message', 'data krb telah disimpan');
                return redirect()->route('krb_page');

            }catch (\Exception $e) {
                DB::rollback();
                session()->flash('error_message', 'data krb tidak disimpan');
                return redirect()->route('krb_page');
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function update_krb(Request $request, $name, Gunung $m_gunung, JenisKrb $m_jenis_krb){
        if($this->get_level() == "Pegawai"){
            $this->checking_validate('patch', $request);
            DB::BeginTransaction();
            try {
                $krb = $this->m_krb->get_by_lower_name($name);
                $gunung = $m_gunung->get_by_name($request->input('gunung'));
                $jenis_krb = $m_jenis_krb->get_by_name($request->input('jenis_krb'));

                $krb->update([
                    'id_gunung' => $gunung->id_gunung,
                    'id_jenis_krb' => $jenis_krb->id_jenis_krb,
                    'file_krb' => $request->input('json_file'),
                ]);

                DB::commit();
                session()->flash('success_message', 'data krb telah disimpan');
                return redirect()->route('krb_page');

            }catch (\Exception $e) {
                DB::rollback();
                session()->flash('error_message', 'data krb tidak disimpan');
                return redirect()->route('krb_page');
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function destroy_krb($name){
        if($this->get_level() == "Pegawai"){
            $krb = $this->m_krb->get_by_lower_name($name);
            $krb->delete();

            session()->flash('success_message', 'data krb telah dihapus');
            return redirect()->route('krb_page');
        }
        else return response('Unauthorized.', 401);
    }

    public function verified(Request $request)
    {
        if($this->get_level() == "Pegawai"){
            DB::BeginTransaction();
            try {
                $krb = $this->m_krb->get_by_lower_name($request->input('name'));
                $m_verifikasi = new Verifikasi();
                $verified = $m_verifikasi->get_by_name("Lengkap");

                if($krb->verifikasi == "Pending")
                {
                    $krb->update([
                        'id_verifikasi' => $verified->id_verifikasi
                    ]);
                    
                    DB::commit();
                    $krb = $this->m_krb->get_by_lower_name($request->input('name'));
                }

                $krb->setAttribute('lower_name', $request->input('name'));                
                return $this->success($krb);

            }catch (\Exception $e) {
                DB::rollback();
                return $this->error($e->getMessage());
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function unverified(Request $request)
    {
        if($this->get_level() == "Pegawai"){
            DB::BeginTransaction();
            try {
                $krb = $this->m_krb->get_by_lower_name($request->input('name'));
                $m_verifikasi = new Verifikasi();
                $unverified = $m_verifikasi->get_by_name("Tidak Lengkap");

                if($krb->verifikasi == "Pending")
                {
                    $krb->update([
                        'id_verifikasi' => $unverified->id_verifikasi
                    ]);
                    
                    DB::commit();
                    $krb = $this->m_krb->get_by_lower_name($request->input('name'));
                }
                $krb->setAttribute('lower_name', $request->input('name'));                
                return $this->success($krb);

            }catch (\Exception $e) {
                DB::rollback();
                return $this->error($e->getMessage());
            }
        }
        else return response('Unauthorized.', 401);
    }
    
    public function validated(Request $request)
    {
        if($this->get_level() == "Pegawai"){
            DB::BeginTransaction();
            try {
                $krb = $this->m_krb->get_by_lower_name($request->input('name'));
                $m_validasi = new Validasi();
                $validated = $m_validasi->get_by_name("Valid");

                if($krb->verifikasi == "Lengkap" && $krb->validasi == "Pending")
                {
                    $krb->update([
                        'id_validasi' => $validated->id_validasi
                    ]);
                    
                    DB::commit();
                    $krb = $this->m_krb->get_by_lower_name($request->input('name'));
                }

                $krb->setAttribute('lower_name', $request->input('name'));                
                return $this->success($krb);

            }catch (\Exception $e) {
                DB::rollback();
                return $this->error($e->getMessage());
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function unvalidated(Request $request)
    {
        if($this->get_level() == "Pegawai"){
            DB::BeginTransaction();
            try {
                $krb = $this->m_krb->get_by_lower_name($request->input('name'));
                $m_validasi = new Validasi();
                $unvalidated = $m_validasi->get_by_name("Tidak Valid");

                if($krb->verifikasi == "Lengkap" && $krb->validasi == "Pending")
                {
                    $krb->update([
                        'id_validasi' => $unvalidated->id_validasi
                    ]);
                    
                    DB::commit();
                    $krb = $this->m_krb->get_by_lower_name($request->input('name'));
                }
                $krb->setAttribute('lower_name', $request->input('name'));                
                return $this->success($krb);

            }catch (\Exception $e) {
                DB::rollback();
                return $this->error($e->getMessage());
            }
        }
        else return response('Unauthorized.', 401);
    }
}
