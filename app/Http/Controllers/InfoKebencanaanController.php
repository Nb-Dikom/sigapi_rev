<?php

namespace App\Http\Controllers;

use App\Models\Info;
use App\Models\Verifikasi;
use App\Models\Validasi;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InfoKebencanaanController extends Controller
{
    function __construct()
    {
        $this->m_info = new Info();
    }

    protected function get_jabatan()
    {
        return Auth::user()->pegawai->jabatan->jabatan;
    }

    protected function get_level()
    {
        return Auth::user()->level->level;
    }

    protected function create_new_id()
    {
        $latest_info = $this->m_info->get_latest();
        $new_info = $latest_info->id_info;
        $new_info ++;
        
        return $new_info;
    }

    public function index_info()
    {
        //        
        if($this->get_level() == "Pegawai"){
            $jabatan = $this->get_jabatan();
            switch ($jabatan) {
                case "Sub Bagian Penanggulangan Bencana":
                case "Sub Bagian Umum dan Kepegawaian":
                    $list_info = $this->m_info->get_validated()->paginate(2);
                    break;

                case "Kepala Bidang Pencegahan dan Kesiapsiagaan":
                    $list_info = $this->m_info->sort_desc_verified();
                    break;

                case "Kepala BPBD Jawa Timur":
                    $list_info = $this->m_info->sort_desc_validated();
                    break;
                
                default:
                    $list_info = $this->m_info->get_all();
                    break;
            }
            return view ('info-kebencanaan.index-info', compact('list_info', 'jabatan'));
        }
        else return response('Unauthorized.', 401);
    }

    public function info_page(Request $request)
    {
        $list_info = $this->m_info->get_validated()->get();
        $jumlah = $list_info->count();
        
        foreach ($list_info as $key => $info) {
            // $year_obj = Carbon::createFromFormat('d-M-Y', $info->tanggal_info)->year;
            $year_obj = date('Y', strtotime($info->tanggal_info));
            $year[$year_obj] = 'tahun';
        }
        foreach($year as $key => $string){
            $index = 0;
            foreach ($list_info as $info) {
                $year_obj = $year_obj = date('Y', strtotime($info->tanggal_info));
                if( $key == $year_obj)
                    $index++;
            }
            $list_tahun[] = [
                "tahun" => $key,
                "total" => $index
            ];
        }
        if($request->input('tahun'))
            $list_info = $this->m_info->get_validated()->whereYear('tanggal_info', $request->input('tahun'))->paginate(2);
        else
            $list_info = $this->m_info->get_validated()->paginate(2);

        return view ('info', compact('list_info', 'list_tahun', 'jumlah'));
    }

    public function view_info($name)
    {
        $list_info = $this->m_info->get_validated()->get();
        $jumlah = $list_info->count();

        foreach ($list_info as $key => $info) {
            // $year_obj = Carbon::createFromFormat('d-M-Y', $info->tanggal_info)->year;
            $year_obj = date('Y', strtotime($info->tanggal_info));
            $year[$year_obj] = 'tahun';
        }
        foreach($year as $key => $string){
            $index = 0;
            foreach ($list_info as $info) {
                $year_obj = $year_obj = date('Y', strtotime($info->tanggal_info));
                if( $key == $year_obj)
                    $index++;
            }
            $list_tahun[] = [
                "tahun" => $key,
                "total" => $index
            ];
        }

        $info = $this->m_info->get_by_lower_name($name);
        $info->setAttribute('lower_name', $name);

        return view ('detail-info', compact('info', 'list_tahun', 'jumlah'));
    }

    public function create_info()
    {
        //
        if($this->get_level() == "Pegawai"){
            return view ('info-kebencanaan.create-info');
        }
        else return response('Unauthorized.', 401);
    }

    public function edit_info($name)
    {
        //
        if($this->get_level() == "Pegawai"){
            $info = $this->m_info->get_by_lower_name($name);
            $info->setAttribute('lower_name', $name);
            return view ('info-kebencanaan.edit-info', compact('info'));
        }
        else return response('Unauthorized.', 401);
    }
    
    public function show_info($name)
    {
        //
        if($this->get_level() == "Pegawai"){
            $jabatan = $this->get_jabatan();
            try{
                $info = $this->m_info->get_by_lower_name($name);
                $info->setAttribute('lower_name', $name);
                
                switch ($jabatan) {
                    case "Sub Bagian Penanggulangan Bencana":
                    case "Sub Bagian Umum dan Kepegawaian":
                        return view ('info-kebencanaan.detail-info', compact('info'));
                        break;

                    default:
                        if($info) return response()->json($info, 200);
                        else return response()->json('error', 400);
                        break;
                }

            }catch (\Exception $e) {
                $message = $e->getMessage();
                $response = [
                    'code' => 2,
                    'message' => $message
                ];
                switch ($jabatan) {
                    case "Sub Bagian Penanggulangan Bencana":
                    case "Sub Bagian Umum dan Kepegawaian":
                        session()->flash('error_message', 'terjadi kesalahan');
                        return redirect(url()->previous());
                        break;

                    default:
                        return response()->json($response, 400);
                        break;
                }
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function checking_validate($data){
        $message = [
            'required' => 'Form tidak boleh kosong',
            'nama.string' => 'Hanya berisi huruf dan karakter',
            'nama.min' => 'Username minimal 3 karakter',
        ];

        $this->validate($data, [
            'nama' => 'required|string|min:3',
        ], $message);
    }

    public function store_info(Request $request)
    {
        //
        if($this->get_level() == "Pegawai"){
            $this->checking_validate($request);            
            DB::BeginTransaction();
            try {
                $daftar_verifikasi = Verifikasi::all();
                $daftar_validasi = Validasi::all();
                $create_new = $this->create_new_id();
                
                $info = Info::create([
                    'id_info' => $create_new,
                    'id_verifikasi' => $daftar_verifikasi[0]->id_verifikasi,
                    'id_validasi' => $daftar_validasi[0]->id_validasi,
                    'id_pegawai' => Auth::user()->pegawai->id_pegawai,
                    'nama_info' => $request->input('nama'),
                    'konten' => $request->input('konten'),
                    'tanggal_info' => Carbon::now(),
                ]);

                DB::commit();
                session()->flash('success_message', 'data info telah disimpan');
                return redirect()->route('info_page');

            }catch (\Exception $e) {
                DB::rollback();
                session()->flash('error_message', 'data info tidak disimpan');
                return redirect()->route('info_page');
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function update_info($name, Request $request)
    {
        if($this->get_level() == "Pegawai"){
            $this->checking_validate($request);
            DB::BeginTransaction();
            try {
                $info = $this->m_info->get_by_lower_name($name);
                $info->update([
                    'nama_info' => $request->input('nama'),
                    'konten' => $request->input('konten'),
                ]);
                
                DB::commit();
                
                session()->flash('success_message', 'data info telah disimpan');
                return redirect()->route('info_page');

            }catch (\Exception $e) {
                DB::rollback();
                session()->flash('error_message', 'data info tidak disimpan');
                return redirect()->route('info_page');
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function destroy_info($name)
    {
        //
        if($this->get_level() == "Pegawai"){
            $info = $this->m_info->get_by_lower_name($name);
            $info->delete();
            
            session()->flash('success_message', 'data info telah dihapus');
            return redirect()->route('info_page');
        }
        else return response('Unauthorized.', 401);
    }

    public function verified(Request $request)
    {
        if($this->get_level() == "Pegawai"){
            DB::BeginTransaction();
            try {
                $info = $this->m_info->get_by_lower_name($request->input('name'));
                $m_verifikasi = new Verifikasi();
                $verified = $m_verifikasi->get_by_name("Lengkap");

                if($info->verifikasi == "Pending")
                {
                    $info->update([
                        'id_verifikasi' => $verified->id_verifikasi
                    ]);
                    
                    DB::commit();
                    $info = $this->m_info->get_by_lower_name($request->input('name'));
                }

                $info->setAttribute('lower_name', $request->input('name'));                
                return $this->success($info);

            }catch (\Exception $e) {
                DB::rollback();
                $message = $e->getMessage();
                $response = [
                    'code' => 2,
                    'message' => $message
                ];
                return response()->json($response, 400);
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function unverified(Request $request)
    {
        if($this->get_level() == "Pegawai"){
            DB::BeginTransaction();
            try {
                $info = $this->m_info->get_by_lower_name($request->input('name'));
                $m_verifikasi = new Verifikasi();
                $unverified = $m_verifikasi->get_by_name("Tidak Lengkap");

                if($info->verifikasi == "Pending")
                {
                    $info->update([
                        'id_verifikasi' => $unverified->id_verifikasi
                    ]);
                    
                    DB::commit();
                    $info = $this->m_info->get_by_lower_name($request->input('name'));
                }
                $info->setAttribute('lower_name', $request->input('name'));                
                return $this->success($info);

            }catch (\Exception $e) {
                DB::rollback();
                $message = $e->getMessage();
                $response = [
                    'code' => 2,
                    'message' => $message
                ];
                return response()->json($response, 400);
            }
        }
        else return response('Unauthorized.', 401);
    }
    
    public function validated(Request $request)
    {
        if($this->get_level() == "Pegawai"){
            DB::BeginTransaction();
            try {
                $info = $this->m_info->get_by_lower_name($request->input('name'));
                $m_validasi = new Validasi();
                $validated = $m_validasi->get_by_name("Valid");

                if($info->verifikasi == "Lengkap" && $info->validasi == "Pending")
                {
                    $info->update([
                        'id_validasi' => $validated->id_validasi
                    ]);
                    
                    DB::commit();
                    $info = $this->m_info->get_by_lower_name($request->input('name'));
                }

                $info->setAttribute('lower_name', $request->input('name'));                
                return $this->success($info);

            }catch (\Exception $e) {
                DB::rollback();
                return $this->error($e->getMessage());
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function unvalidated(Request $request)
    {
        if($this->get_level() == "Pegawai"){
            DB::BeginTransaction();
            try {
                $info = $this->m_info->get_by_lower_name($request->input('name'));
                $m_validasi = new Validasi();
                $unvalidated = $m_validasi->get_by_name("Tidak Valid");

                if($info->verifikasi == "Lengkap" && $info->validasi == "Pending")
                {
                    $info->update([
                        'id_validasi' => $unvalidated->id_validasi
                    ]);
                    
                    DB::commit();
                    $info = $this->m_info->get_by_lower_name($request->input('name'));
                }
                $info->setAttribute('lower_name', $request->input('name'));                
                return $this->success($info);

            }catch (\Exception $e) {
                DB::rollback();
                return $this->error($e->getMessage());
            }
        }
        else return response('Unauthorized.', 401);
    }

    /**
     * List module for Mobile API
     */

     public function list_info(){
        try{
            $list_info = $this->m_info->get_info_api();
            foreach ($list_info as $info) {
                $data[] = [
                    'nama' => $info->nama_info,
                    'konten' => $info->konten,
                    'tanggal' => $info->tanggal_info,
                ];
            }
            
            return $this->success($data);

        }catch (\Exception $e) {
            
            return $this->error($e->getMessage());
        }
    }
}