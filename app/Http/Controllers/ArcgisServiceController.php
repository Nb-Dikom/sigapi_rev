<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ArcgisServiceController extends Controller
{
    public function sync_feature_service()
    {
        $items = $this->get_item();
        $collection = [];
        foreach ($items as $item) {
            if($item['type'] == "Feature Service") {
                $feature_service = $this->get_feature_service($item['url']);
                foreach($feature_service as $layer){
                    $collection[] = [
                        'url' => $item['url'] . "/" . $layer['id'],
                        'name' => $layer['name'],
                        'tags' => $item['tags'][0]
                    ];
                }
            }
        }
        return $collection;
    }

    public function sync_shapefile()
    {
        $items = $this->get_item();
        $collection = [];
        foreach ($items as $item) {
            if($item['type'] == "Shapefile") {
                $collection[] = [
                    'id' => $item['id'],
                    'title' => $item['title'],
                    'item' => $item['item'],
                    'tags' => $item['tags'][0]
                ];
            }
        }
        return $collection;
    }

    public function get_item()
    {
        $token = $this->get_token_arcgis();
        $url = "http://diarikom.maps.arcgis.com/sharing/content/users/nbdikom/";
        $postdata = http_build_query(
            array(
                'token' => $token,
                'f' => 'pjson'
            )
        );

        $opts = array(
            'http' => array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => $postdata
            )
        );
        $context  = stream_context_create($opts);
        $result = file_get_contents($url, false, $context);
        $content = json_decode($result, true);
        $items = $content['items'];
        return $items;
    }

    public function get_token_arcgis()
    {
        $url = "https://www.arcgis.com/sharing/generateToken";
        $postdata = http_build_query(
            array(
                'username' => 'nbdikom',
                'password' => 'gktaulg2592',
                'referer' => url()->current(),
                'f' => 'json'
            )
        );

        $opts = array(
            'http' => array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => $postdata
            )
        );
        $context  = stream_context_create($opts);
        $result = file_get_contents($url, false, $context);
        $json = json_decode($result, true);
        $token = $json['token'];
        return $token;
    }

    public function get_feature_service($path)
    {
        $token = $this->get_token_arcgis();
        $url = $path;
        $postdata = http_build_query(
            array(
                'token' => $token,
                'f' => 'pjson'
            )
        );

        $opts = array(
            'http' => array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => $postdata
            )
        );
        $context  = stream_context_create($opts);
        $result = file_get_contents($url, false, $context);
        $json = json_decode($result, true);
        $layers = $json['layers'];
        return $layers;
    }
}
