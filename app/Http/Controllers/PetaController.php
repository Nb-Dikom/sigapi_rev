<?php

namespace App\Http\Controllers;

use App\Http\Controllers\KrbController;
use App\Models\Fasilitas;
use App\Models\JenisFasilitas;
use App\Models\JalurEvakuasi;
use App\Models\Krb;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PetaController extends Controller
{
    //
    function __construct()
    {
        $this->m_fasilitas = new Fasilitas();
        $this->m_krb = new Krb();
        $this->m_jalur_evakuasi = new JalurEvakuasi();
    }

    function index_peta(){
        return view ('peta.index-peta');
    }

    function peta_page(){
        return view ('peta');
    }

    function show_peta($jenis){
        $string = route('load_map', $jenis);
        $data['url'] = $string;
        $data['type'] = $jenis;

        return view ('peta.detail-peta')->with($data);
    }

    function view_peta($jenis){
        $string = route('load_map', $jenis);
        $data['url'] = $string;
        $data['type'] = $jenis;
        return view ('detail-peta')->with($data);
    }

    function load_peta($type){
        if($type){
            $data_fasilitas = $this->m_fasilitas->get_validated()->get();
                
            $list_jenis = JenisFasilitas::all();
            $features_point = [];
            $features_polygon = [];
    
            foreach($data_fasilitas as $key => $object){
                $collection = collect([$object]);
                $temporary = [
                    "geometry" => [
                        "type" => "point",
                        "longitude" => $object->longitude,
                        "latitude" => $object->latitude
                    ],
                    "attributes" => [
                        "ObjectID" => $key,
                        "nama_fasilitas" => $object->nama_fasilitas,
                        "alamat_fasilitas" => $object->alamat_fasilitas,
                        "jenis_fasilitas" => $object->jenis_fasilitas,
                        "foto_fasilitas" =>  $object->foto_fasilitas,
                        "gunung" => $object->gunung,
                        "longitude" => $object->longitude,
                        "latitude" => $object->latitude
                    ]
                ];
                foreach($list_jenis as $jenis){
                    if($collection->contains('jenis_fasilitas', $jenis->jenis_fasilitas)){
                        $key = $jenis->id_jenis_fasilitas;
                        $features_point[$key][] = $temporary;
                    }
                }
            }

            if($type == "krb"){
                $content[$type] = $this->m_krb->get_validated_by_name('Semeru')->get();
            }
            else if($type == "jalur-evakuasi") {
                $content[$type] = $this->m_jalur_evakuasi->get_validated_by_name('Semeru')->get();
            }
            else {
                return $this->error("Unknown type");
            }
    
            $data = [
                'point' => $features_point,
                'geometry' => $content[$type],
            ];
            return $this->success($data);
        }
        else return $this->error("No type");
    }

    /**
     * List module for Mobile API
     */

    public function list_peta($type){
        try{
            
            $list_fasilitas = $this->m_fasilitas->get_validated()
                ->get();
            foreach ($list_fasilitas as $item) {
                $fasilitas[] = [
                    'nama' => $item->nama_fasilitas,
                    'alamat' => $item->alamat_fasilitas,
                    'latitude' => $item->latitude,
                    'longitude' => $item->longitude,
                    'img' => $item->foto_fasilitas,
                    'img_path' => asset('/upload/fasilitas'). '/',
                    'tanggal' => $item->tanggal_fasilitas,
                    'jenis' => $item->jenis_fasilitas,
                    'gunung' => $item->gunung,
                ];
            }
            
            $data = ['fasilitas' => $fasilitas];

            switch ($type) {
                case 'krb':
                    $list_krb = $this->m_krb->get_validated_by_name('Semeru')
                        ->orderBy('jenis_krb.id_jenis_krb', 'asc')
                        ->get();

                    if(!$list_krb->isEmpty()){
                        foreach ($list_krb as $item) {
                            $krb[] = [
                                'jenis' => $item->jenis_krb,
                                'file' => $item->file_krb,
                                'gunung' => $item->gunung
                            ];
                        }
                        $data = $data + array('krb' => $krb);
                    }
                    break;
                case 'jalur-evakuasi':
                    $list_jalur_evakuasi = $this->m_jalur_evakuasi->get_validated_by_name('Semeru')
                        ->get();

                    if(!$list_jalur_evakuasi->isEmpty()){
                        foreach ($list_jalur_evakuasi as $item) {
                            $jalur_evakuasi[] = [
                                'file' => $item->file_jalur_evakuasi,
                                'gunung' => $item->gunung
                            ];
                        }
                        $data = $data + array('jalur_evakuasi' => $jalur_evakuasi);
                    }
                    break;
            }
            
            return $this->success($data);

        } catch(\Exception $exception){
            return $this->error('Data tidak di temukan');
        }
    }

    public function route(Request $request){
        $data = [
            'longitude' => $request->input('longitude'),
            'latitude' => $request->input('latitude')
        ];

        return view('peta-rute', compact('data'));
    }
}
