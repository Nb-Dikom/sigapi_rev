<?php

namespace App\Http\Controllers;

use App\Http\Controllers\KrbController;
use App\Models\Prabencana;
use App\Models\Fasilitas;
use App\Models\FilePeta;
use App\Models\Info;
use App\Models\JenisFasilitas;
use App\Models\Krb;
use App\Models\Pengamatan;
use App\Models\Pegawai;
use App\Models\ProfilDaerah;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApplicationController extends Controller
{    
    public function index_home(){
        $info_collection = Info::orderBy('tanggal_info', 'desc')->get();
        return view('beranda', compact('info_collection'));
    }

    public function index_pengamatan(){
        $pengamatan_collection = Pengamatan::join('verifikasi', 'pengamatan.id_verifikasi', '=', 'verifikasi.id_verifikasi')
            ->join('validasi', 'pengamatan.id_validasi', '=', 'validasi.id_validasi')
            ->where('verifikasi.verifikasi', 'Lengkap')
            ->where('validasi.validasi', 'Valid')
            ->orderBy('tanggal_pengamatan', 'desc')->get();
        return view('pengamatan', compact('pengamatan_collection'));
    }

    public function index_peta(){
        return view ('peta');
    }
    
    public function index_peta_mapguide(){
        return view ('peta-mapguide');
    }

    public function load_peta(){
        $data_fasilitas = Fasilitas::join('jenis_fasilitas', 'fasilitas.id_jenis_fasilitas', '=', 'jenis_fasilitas.id_jenis_fasilitas')
            ->join('gunung', 'fasilitas.id_gunung', '=', 'gunung.id_gunung')
            ->join('verifikasi', 'fasilitas.id_verifikasi', '=', 'verifikasi.id_verifikasi')
            ->join('validasi', 'fasilitas.id_validasi', '=', 'validasi.id_validasi')
            ->where('verifikasi.verifikasi', 'Lengkap')
            ->where('validasi.validasi', 'Valid')
            ->select([
                'fasilitas.nama_fasilitas', 'fasilitas.alamat_fasilitas', 'fasilitas.longitude', 'fasilitas.latitude',
                'jenis_fasilitas.id_jenis_fasilitas', 'jenis_fasilitas.jenis_fasilitas',
                'gunung.gunung'
            ])
            ->orderBy('jenis_fasilitas.id_jenis_fasilitas', 'asc')
            ->get();
        $data_rawan = Krb::join('jenis_krb', 'krb.id_jenis_krb', '=', 'jenis_krb.id_jenis_krb')
            ->join('gunung', 'krb.id_gunung', '=', 'gunung.id_gunung')
            ->join('verifikasi', 'krb.id_verifikasi', '=', 'verifikasi.id_verifikasi')
            ->join('validasi', 'krb.id_validasi', '=', 'validasi.id_validasi')
            ->where('gunung.gunung', 'Semeru')
            ->where('verifikasi.verifikasi', 'Lengkap')
            ->where('validasi.validasi', 'Valid')
            ->select([
                'krb.id_krb', 'krb.file_krb',
                'jenis_krb.jenis_krb',
                'gunung.gunung'
            ])->get();
        $list_jenis = JenisFasilitas::all();
        $features_point = [];
        $features_polygon = [];

        foreach($data_fasilitas as $key => $object){
            $collection = collect([$object]);
            $temporary = [
                "geometry" => [
                    "type" => "point",
                    "longitude" => $object->longitude,
                    "latitude" => $object->latitude
                ],
                "attributes" => [
                    "ObjectID" => $key,
                    "nama_fasilitas" => $object->nama_fasilitas,
                    "alamat_fasilitas" => $object->alamat_fasilitas,
                    "jenis_fasilitas" => $object->jenis_fasilitas,
                    "gunung" => $object->gunung,
                    "longitude" => $object->longitude,
                    "latitude" => $object->latitude
                ]
            ];
            foreach($list_jenis as $jenis){
                if($collection->contains('jenis_fasilitas', $jenis->jenis_fasilitas)){
                    $key = $jenis->id_jenis_fasilitas;
                    $features_point[$key][] = $temporary;
                }
            }
        }

        // $krb_controllers = new KrbController;
        // foreach($data_rawan as $object){
        //     $collection = collect([$object]);
        //     $temporary = [
        //         "geometry" => [
        //             "type" => "polygon",
		// 		    "rings" => $krb_controllers->get_response($object)
        //         ],
        //         "attributes" => [
        //             "ObjectID" => $key,
        //             "jenis_rawan" => $object->jenis_krb,
        //             "gunung" => $object->gunung,
        //         ]
        //     ];
        //     $key = strtolower(str_replace(' ', '_', $object->jenis_krb));
        //     $features_polygon[$key][] = $temporary;
        // }

        $response = [
            'point' => $features_point,
            'polygon' => $data_rawan,
        ];

        return response()->json($response, 200);
    }

    public function index_file(){
        $file_peta_collection = FilePeta::join('verifikasi', 'file_peta.id_verifikasi', '=', 'verifikasi.id_verifikasi')
            ->join('validasi', 'file_peta.id_validasi', '=', 'validasi.id_validasi')
            ->where('verifikasi', 'Lengkap')
            ->where('validasi', 'Valid')->orderBy('tanggal_file_peta', 'desc')
            ->get();
        // $file_peta_collection = FilePeta::orderBy('tanggal', 'desc')->get();

        return view('file', compact('file_peta_collection'));
    }

    public function index_prabencana(){
        $profil_daerah_collection = ProfilDaerah::all();
        $prabencana_collection = Prabencana::orderBy('id_verifikasi', 'asc')->orderBy('tahun', 'asc')->get();
        $tahun_collection = $prabencana_collection->groupBy('tahun');
        $tahun_collection = $tahun_collection->keys();
        $sumber_data_collection = $prabencana_collection->groupBy('sumber_data');
        $sumber_data_collection = $sumber_data_collection->keys();

        return view ('data-prabencana', compact('profil_daerah_collection', 'tahun_collection', 'sumber_data_collection'));
    }

    public function show_detail_info($info){
        $info = Info::find($info);
        return view('detail', compact('info'));
    }

    public function show_detail_pengamatan($pengamatan){
        $pengamatan = Pengamatan::find($pengamatan);
        return response()->json($pengamatan, 200);
    }

    public function verified(Request $request){
        if(Auth::user()->pegawai->jabatan->jabatan == 'Kepala Seksi Kesiapsiagaan'){
            try{
            	if($request->input('jenis_data') == 'File Peta'){
                	$object = FilePeta::find($request->input('id'));
                    $total = count(FilePeta::join('verifikasi', 'file_peta.id_verifikasi', '=', 'verifikasi.id_verifikasi')->where('verifikasi.verifikasi', 'Pending')->get());
            	}
            	else if($request->input('jenis_data') == 'Pengamatan'){
                	$object = Pengamatan::find($request->input('id'));
                    $total = count(Pengamatan::join('verifikasi', 'pengamatan.id_verifikasi', '=', 'verifikasi.id_verifikasi')->where('verifikasi.verifikasi', 'Pending')->get());
            	}
            	else if($request->input('jenis_data') == 'Sarana Kesehatan'){
                	$object = SaranaKesehatan::find($request->input('id'));
                    $total = count(SaranaKesehatan::where('verifikasi', 'Pending')->get());
            	}
            	else if($request->input('jenis_data') == 'Sarana Peribadatan'){
                	$object = SaranaPeribadatan::find($request->input('id'));
                    $total = count(SaranaPeribadatan::where('verifikasi', 'Pending')->get());
            	}
            	else if($request->input('jenis_data') == 'Sarana Pendidikan'){
                	$object = SaranaPendidikan::find($request->input('id'));
                    $total = count(SaranaPendidikan::where('verifikasi', 'Pending')->get());
            	}
            	else if($request->input('jenis_data') == 'Tata Lahan'){
                	$object = TataLahan::find($request->input('id'));
                    $total = count(TataLahan::where('verifikasi', 'Pending')->get());
            	}
            	else if($request->input('jenis_data') == 'Demografi'){
                	$object = Demografi::find($request->input('id'));
                    $total = count(Demografi::where('verifikasi', 'Pending')->get());
            	}
                else if($request->input('jenis_data') == 'Bandar Udara'){
                    $object = BandarUdara::find($request->input('id'));
                    $total = count(BandarUdara::where('verifikasi', 'Pending')->get());
                }
            	else if($request->input('jenis_data') == 'Data Pra Bencana'){
                	$object = Prabencana::find($request->input('id'));
                    $total = count(Prabencana::join('verifikasi', 'prabencana.id_verifikasi', '=', 'verifikasi.id_verifikasi')->where('verifikasi.verifikasi', 'Pending')->get());
            	}
            	else if($request->input('jenis_data') == 'Kawasan Rawan Bencana'){
                	$object = Krb::find($request->input('id'));
                    $total = count(Krb::join('verifikasi', 'krb.id_verifikasi', '=', 'verifikasi.id_verifikasi')->where('verifikasi.verifikasi', 'Pending')->get());
            	}
            	else if($request->input('jenis_data') == 'Fasilitas'){
                	$object = Fasilitas::find($request->input('id'));
                    $total = count(Fasilitas::join('verifikasi', 'fasilitas.id_verifikasi', '=', 'verifikasi.id_verifikasi')->where('verifikasi.verifikasi', 'Pending')->get());
            	}
                
				$object->id_verifikasi = 2;
                $object->save();

                $data = collect($object);
                $data->put('total', $total-1);
                
                if($object) return response()->json($data, 200);
                else return response()->json('error', 400);

            } catch (ModelNotFoundException $exception){
                $message = $exception->getMessage();
                $response = [
                    'code' => 2,
                    'message' => $message
                ];
                return response()->json($response, 400);
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function un_verified(Request $request){
        if(Auth::user()->pegawai->jabatan->jabatan == 'Kepala Seksi Kesiapsiagaan'){
            try{
            	if($request->input('jenis_data') == 'File Peta'){
                    $object = FilePeta::find($request->input('id'));
                    $total = count(FilePeta::join('verifikasi', 'file_peta.id_verifikasi', '=', 'verifikasi.id_verifikasi')->where('verifikasi.verifikasi', 'Pending')->get());
                }
                else if($request->input('jenis_data') == 'Pengamatan'){
                    $object = Pengamatan::find($request->input('id'));
                    $total = count(Pengamatan::join('verifikasi', 'pengamatan.id_verifikasi', '=', 'verifikasi.id_verifikasi')->where('verifikasi.verifikasi', 'Pending')->get());
                }
                else if($request->input('jenis_data') == 'Sarana Kesehatan'){
                    $object = SaranaKesehatan::find($request->input('id'));
                    $total = count(SaranaKesehatan::where('verifikasi', 'Pending')->get());
                }
                else if($request->input('jenis_data') == 'Sarana Peribadatan'){
                    $object = SaranaPeribadatan::find($request->input('id'));
                    $total = count(SaranaPeribadatan::where('verifikasi', 'Pending')->get());
                }
                else if($request->input('jenis_data') == 'Sarana Pendidikan'){
                    $object = SaranaPendidikan::find($request->input('id'));
                    $total = count(SaranaPendidikan::where('verifikasi', 'Pending')->get());
                }
                else if($request->input('jenis_data') == 'Tata Lahan'){
                    $object = TataLahan::find($request->input('id'));
                    $total = count(TataLahan::where('verifikasi', 'Pending')->get());
                }
                else if($request->input('jenis_data') == 'Demografi'){
                    $object = Demografi::find($request->input('id'));
                    $total = count(Demografi::where('verifikasi', 'Pending')->get());
                }
                else if($request->input('jenis_data') == 'Bandar Udara'){
                    $object = BandarUdara::find($request->input('id'));
                    $total = count(BandarUdara::where('verifikasi', 'Pending')->get());
                }
                else if($request->input('jenis_data') == 'Data Pra Bencana'){
                    $object = Prabencana::find($request->input('id'));
                    $total = count(Prabencana::join('verifikasi', 'prabencana.id_verifikasi', '=', 'verifikasi.id_verifikasi')->where('verifikasi.verifikasi', 'Pending')->get());
                }
                else if($request->input('jenis_data') == 'Kawasan Rawan Bencana'){
                    $object = Krb::find($request->input('id'));
                    $total = count(Krb::join('verifikasi', 'krb.id_verifikasi', '=', 'verifikasi.id_verifikasi')->where('verifikasi.verifikasi', 'Pending')->get());
                }
                else if($request->input('jenis_data') == 'Fasilitas'){
                    $object = Fasilitas::find($request->input('id'));
                    $total = count(Fasilitas::join('verifikasi', 'fasilitas.id_verifikasi', '=', 'verifikasi.id_verifikasi')->where('verifikasi.verifikasi', 'Pending')->get());
                }
                
                $object->id_verifikasi = 3;
                $object->save();

                $data = collect($object);
                $data->put('total', $total-1);
                
                if($object) return response()->json($data, 200);
                else return response()->json('error', 400);

            } catch (ModelNotFoundException $exception){
                $message = $exception->getMessage();
                $response = [
                    'code' => 2,
                    'message' => $message
                ];
                return response()->json($response, 400);
            }
        }
        else return response('Unauthorized.', 401);
    }
    
    public function validated(Request $request){
        if(Auth::user()->pegawai->jabatan->jabatan == 'Kepala Bidang Pencegahan dan Kesiapsiagaan'){
            try{
            	if($request->input('jenis_data') == 'File Peta'){
                	$object = FilePeta::find($request->input('id'));
                    $total = count(FilePeta::where('id_verifikasi', 2)->where('id_validasi', 1)->get());
            	}
            	else if($request->input('jenis_data') == 'Pengamatan'){
                	$object = Pengamatan::find($request->input('id'));
                    $total = count(Pengamatan::where('id_verifikasi', 2)->where('id_validasi', 1)->get());
            	}
            	else if($request->input('jenis_data') == 'Sarana Kesehatan'){
                	$object = SaranaKesehatan::find($request->input('id'));
                    $total = count(SaranaKesehatan::where('id_verifikasi', 2)->where('id_validasi', 1)->get());
            	}
            	else if($request->input('jenis_data') == 'Sarana Peribadatan'){
                	$object = SaranaPeribadatan::find($request->input('id'));
                    $total = count(SaranaPeribadatan::where('id_verifikasi', 2)->where('id_validasi', 1)->get());
            	}
            	else if($request->input('jenis_data') == 'Sarana Pendidikan'){
                	$object = SaranaPendidikan::find($request->input('id'));
                    $total = count(SaranaPendidikan::where('id_verifikasi', 2)->where('id_validasi', 1)->get());
            	}
            	else if($request->input('jenis_data') == 'Tata Lahan'){
                	$object = TataLahan::find($request->input('id'));
                    $total = count(TataLahan::where('id_verifikasi', 2)->where('id_validasi', 1)->get());
            	}
            	else if($request->input('jenis_data') == 'Demografi'){
                	$object = Demografi::find($request->input('id'));
                    $total = count(Demografi::where('id_verifikasi', 2)->where('id_validasi', 1)->get());
            	}
                else if($request->input('jenis_data') == 'Bandar Udara'){
                    $object = BandarUdara::find($request->input('id'));
                    $total = count(BandarUdara::where('id_verifikasi', 2)->where('id_validasi', 1)->get());
                }
            	else if($request->input('jenis_data') == 'Data Pra Bencana'){
                	$object = Prabencana::find($request->input('id'));
                	$total = count(Prabencana::where('id_verifikasi', 2)->where('id_validasi', 1)->get());
            	}
            	else if($request->input('jenis_data') == 'Kawasan Rawan Bencana'){
                	$object = Krb::find($request->input('id'));
                    $total = count(Krb::where('id_verifikasi', 2)->where('id_validasi', 1)->get());
            	}
            	else if($request->input('jenis_data') == 'Fasilitas'){
                	$object = Fasilitas::find($request->input('id'));
                    $total = count(Fasilitas::where('id_verifikasi', 2)->where('id_validasi', 1)->get());
            	}
                
				$object->id_validasi = 2;
                $object->save();

                $data = collect($object);
                $data->put('total', $total-1);
                
                if($object) return response()->json($data, 200);
                else return response()->json('error', 400);

            } catch (ModelNotFoundException $exception){
                $message = $exception->getMessage();
                $response = [
                    'code' => 2,
                    'message' => $message
                ];
                return response()->json($response, 400);
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function un_validated(Request $request){
        if(Auth::user()->pegawai->jabatan->jabatan == 'Kepala Bidang Pencegahan dan Kesiapsiagaan'){
            try{
            	if($request->input('jenis_data') == 'File Peta'){
                    $object = FilePeta::find($request->input('id'));
                    $total = count(FilePeta::where('id_verifikasi', 2)->where('id_validasi', 1)->get());
                }
                else if($request->input('jenis_data') == 'Pengamatan'){
                    $object = Pengamatan::find($request->input('id'));
                    $total = count(Pengamatan::where('id_verifikasi', 2)->where('id_validasi', 1)->get());
                }
                else if($request->input('jenis_data') == 'Sarana Kesehatan'){
                    $object = SaranaKesehatan::find($request->input('id'));
                    $total = count(SaranaKesehatan::where('id_verifikasi', 2)->where('id_validasi', 1)->get());
                }
                else if($request->input('jenis_data') == 'Sarana Peribadatan'){
                    $object = SaranaPeribadatan::find($request->input('id'));
                    $total = count(SaranaPeribadatan::where('id_verifikasi', 2)->where('id_validasi', 1)->get());
                }
                else if($request->input('jenis_data') == 'Sarana Pendidikan'){
                    $object = SaranaPendidikan::find($request->input('id'));
                    $total = count(SaranaPendidikan::where('id_verifikasi', 2)->where('id_validasi', 1)->get());
                }
                else if($request->input('jenis_data') == 'Tata Lahan'){
                    $object = TataLahan::find($request->input('id'));
                    $total = count(TataLahan::where('id_verifikasi', 2)->where('id_validasi', 1)->get());
                }
                else if($request->input('jenis_data') == 'Demografi'){
                    $object = Demografi::find($request->input('id'));
                    $total = count(Demografi::where('id_verifikasi', 2)->where('id_validasi', 1)->get());
                }
                else if($request->input('jenis_data') == 'Bandar Udara'){
                    $object = BandarUdara::find($request->input('id'));
                    $total = count(BandarUdara::where('id_verifikasi', 2)->where('id_validasi', 1)->get());
                }
                else if($request->input('jenis_data') == 'Data Pra Bencana'){
                    $object = DataPrabencana::find($request->input('id'));
                    $total = count(DataPrabencana::where('id_verifikasi', 2)->where('id_validasi', 1)->get());
                }
                else if($request->input('jenis_data') == 'Kawasan Rawan Bencana'){
                    $object = Krb::find($request->input('id'));
                    $total = count(Krb::where('id_verifikasi', 2)->where('id_validasi', 1)->get());
                }
                else if($request->input('jenis_data') == 'Fasilitas'){
                    $object = Fasilitas::find($request->input('id'));
                    $total = count(Fasilitas::where('id_verifikasi', 2)->where('id_validasi', 1)->get());
                }
                
                $object->id_validasi = 3;
                $object->save();

                $data = collect($object);
                $data->put('total', $total-1);
                
                if($object) return response()->json($data, 200);
                else return response()->json('error', 400);

            } catch (ModelNotFoundException $exception){
                $message = $exception->getMessage();
                $response = [
                    'code' => 2,
                    'message' => $message
                ];
                return response()->json($response, 400);
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function route(Request $request){
        $data = [
            'longitude' => $request->input('longitude'),
            'latitude' => $request->input('latitude')
        ];

        return view('peta-rute', compact('data'));
    }


    /**
     * List module for Mobile API
     */

    public function list_peta(){
        try{
            $fasilitas = Fasilitas::join('jenis_fasilitas', 'fasilitas.id_jenis_fasilitas', '=', 'jenis_fasilitas.id_jenis_fasilitas')
            ->join('gunung', 'fasilitas.id_gunung', '=', 'gunung.id_gunung')
            ->select([
                'fasilitas.id_fasilitas', 'fasilitas.nama_fasilitas', 'fasilitas.alamat_fasilitas', 'fasilitas.longitude', 'fasilitas.latitude',
                'jenis_fasilitas.id_jenis_fasilitas', 'jenis_fasilitas.jenis_fasilitas',
                'gunung.gunung'
            ])
            ->orderBy('jenis_fasilitas.id_jenis_fasilitas', 'asc')
            ->get();

            $krb = Krb::join('jenis_krb', 'krb.id_jenis_krb', '=', 'jenis_krb.id_jenis_krb')
            ->join('gunung', 'krb.id_gunung', '=', 'gunung.id_gunung')
            ->select([
                'krb.id_krb', 'krb.file_krb',
                'jenis_krb.id_jenis_krb', 'jenis_krb.jenis_krb',
                'gunung.gunung'
            ])
            ->orderBy('jenis_krb.id_jenis_krb', 'asc')
            ->get();

            $response  = [
                'code' => 1,
                'data' => [
                    'fasilitas' => $fasilitas,
                    'krb' => $krb
                ],
                'message' => 'success'
            ];
            return response()->json($response, 200);

        } catch(ModelNotFoundException $exception){
            $response  = [
                'code' => 2,
                'message' => "Data tidak ditemukan"
            ];
            return response()->json($response, 400);
        }
    }
}
