<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ArcgisServiceController;
use App\Models\Gunung;
use App\Models\JalurEvakuasi;
use App\Models\Verifikasi;
use App\Models\Validasi;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JalurEvakuasiController extends Controller
{
    function __construct()
    {
        $this->m_jalur_evakuasi = new JalurEvakuasi();
    }

    protected function get_jabatan()
    {
        return Auth::user()->pegawai->jabatan->jabatan;
    }

    protected function get_level()
    {
        return Auth::user()->level->level;
    }

    protected function create_new_id()
    {
        $latest_jalur_evakuasi = $this->m_jalur_evakuasi->get_latest();
        $new_jalur_evakuasi = $latest_jalur_evakuasi->id_jalur_evakuasi;
        $new_jalur_evakuasi ++;
        
        return $new_jalur_evakuasi;
    }

    public function index_jalur_evakuasi(){
        if($this->get_level() == "Pegawai"){
            $jabatan = $this->get_jabatan();
            switch ($jabatan) {
                case "Kepala Bidang Pencegahan dan Kesiapsiagaan":
                    $list_jalur_evakuasi = $this->m_jalur_evakuasi->sort_desc_verified();
                    break;

                case "Kepala BPBD Jawa Timur":
                    $list_jalur_evakuasi = $this->m_jalur_evakuasi->sort_desc_validated();
                    break;

                default:
                    $list_jalur_evakuasi = $this->m_jalur_evakuasi->get_all();
                    break;
            }
            return view ('jalur-evakuasi.index-jalur-evakuasi', compact('list_jalur_evakuasi', 'jabatan'));
        }
        else return response('Unauthorized.', 401);
    }

    public function create_jalur_evakuasi(ArcgisServiceController $service){
        if($this->get_level() == "Pegawai"){
            $collection = [
                'gunung' => Gunung::all(),
                'file_jalur_evakuasi' => $service->sync_feature_service(),
            ];
            return view ('jalur-evakuasi.create-jalur-evakuasi', compact('collection'));
        }
        else return response('Unauthorized.', 401);
    }

    public function edit_jalur_evakuasi($name, ArcgisServiceController $service){
        if($this->get_level() == "Pegawai"){
            $jalur_evakuasi = $this->m_jalur_evakuasi->get_by_lower_name($name);
            $jalur_evakuasi->setAttribute('lower_name', $name);

            $collection = [
                'jalur_evakuasi' => $jalur_evakuasi,
                'gunung' => Gunung::all(),
                'file_jalur_evakuasi' => $service->sync_feature_service()
            ];
            return view ('jalur-evakuasi.edit-jalur-evakuasi', compact('collection'));
        }
        else return response('Unauthorized.', 401);
    }

    public function show_jalur_evakuasi($name){
        if($this->get_level() == "Pegawai"){
            try{
                $jalur_evakuasi = $this->m_jalur_evakuasi->get_by_lower_name($name);
                $jalur_evakuasi->setAttribute('lower_name', $name);

                if($jalur_evakuasi) return response()->json($jalur_evakuasi, 200);
                else return response()->json('error', 400);
                
            } catch (\ModelNotFoundException $exception){
                $message = $exception->getMessage();
                $response = [
                    'code' => 2,
                    'message' => $message
                ];
                return response()->json($response, 400);
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function checking_validate($type, $data){
        $message = [
            'required' => 'Tidak boleh kosong',
        ];

        $this->validate($data, [
            'gunung' => 'required',
            'json_file' => 'required'
        ], $message);
    }

    public function store_jalur_evakuasi(Request $request, Gunung $m_gunung){
        if($this->get_level() == "Pegawai"){
            $this->checking_validate('post', $request);
            DB::BeginTransaction();
            try {
                $daftar_verifikasi = Verifikasi::all();
                $daftar_validasi = Validasi::all();
                $create_new = $this->create_new_id();
                $gunung = $m_gunung->get_by_name($request->input('gunung'));

                $jalur_evakuasi = JalurEvakuasi::create([
                    'id_jalur_evakuasi' => $create_new,
                    'id_gunung' => $gunung->id_gunung,
                    'id_verifikasi' => $daftar_verifikasi[0]->id_verifikasi,
                    'id_validasi' => $daftar_validasi[0]->id_validasi,
                    'id_pegawai' => Auth::user()->pegawai->id_pegawai,
                    'file_jalur_evakuasi' => $request->input('json_file'),
                    'tanggal_jalur_evakuasi' => Carbon::now(),
                ]);

                DB::commit();
                session()->flash('success_message', 'data jalur evakuasi telah disimpan');
                return redirect()->route('jalur_evakuasi_page');

            }catch (\Exception $e) {
                DB::rollback();
                session()->flash('error_message', 'data jalur evakuasi tidak disimpan');
                return redirect()->route('jalur_evakuasi_page');
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function update_jalur_evakuasi(Request $request, $name, Gunung $m_gunung, JalurEvakuasi $m_jenis_JalurEvakuasi){
        if($this->get_level() == "Pegawai"){
            $this->checking_validate('patch', $request);
            DB::BeginTransaction();
            try {
                $jalur_evakuasi = $this->m_jalur_evakuasi->get_by_lower_name($name);
                $gunung = $m_gunung->get_by_name($request->input('gunung'));

                $jalur_evakuasi->update([
                    'id_gunung' => $gunung->id_gunung,
                    'file_jalur_evakuasi' => $request->input('json_file'),
                ]);
                
                DB::commit();
                session()->flash('success_message', 'data jalur evakuasi telah disimpan');
                return redirect()->route('jalur_evakuasi_page');

            }catch (\Exception $e) {
                DB::rollback();
                session()->flash('error_message', 'data jalur evakuasi tidak disimpan');
                return redirect()->route('jalur_evakuasi_page');
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function destroy_jalur_evakuasi($name){
        if($this->get_level() == "Pegawai"){
            $jalur_evakuasi = $this->m_jalur_evakuasi->get_by_lower_name($name);
            $jalur_evakuasi->delete();

            session()->flash('success_message', 'data jalur evakuasi telah dihapus');
            return redirect()->route('jalur_evakuasi_page');
        }
        else return response('Unauthorized.', 401);
    }

    public function verified(Request $request)
    {
        if($this->get_level() == "Pegawai"){
            DB::BeginTransaction();
            try {
                $jalur_evakuasi = $this->m_jalur_evakuasi->get_by_lower_name($request->input('name'));
                $m_verifikasi = new Verifikasi();
                $verified = $m_verifikasi->get_by_name("Lengkap");

                if($jalur_evakuasi->verifikasi == "Pending")
                {
                    $jalur_evakuasi->update([
                        'id_verifikasi' => $verified->id_verifikasi
                    ]);
                    
                    DB::commit();
                    $jalur_evakuasi = $this->m_jalur_evakuasi->get_by_lower_name($request->input('name'));
                }

                $jalur_evakuasi->setAttribute('lower_name', $request->input('name'));                
                return $this->success($jalur_evakuasi);

            }catch (\Exception $e) {
                DB::rollback();
                $message = $e->getMessage();
                $response = [
                    'code' => 2,
                    'message' => $message
                ];
                return response()->json($response, 400);
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function unverified(Request $request)
    {
        if($this->get_level() == "Pegawai"){
            DB::BeginTransaction();
            try {
                $jalur_evakuasi = $this->m_jalur_evakuasi->get_by_lower_name($request->input('name'));
                $m_verifikasi = new Verifikasi();
                $unverified = $m_verifikasi->get_by_name("Tidak Lengkap");

                if($jalur_evakuasi->verifikasi == "Pending")
                {
                    $jalur_evakuasi->update([
                        'id_verifikasi' => $unverified->id_verifikasi
                    ]);
                    
                    DB::commit();
                    $jalur_evakuasi = $this->m_jalur_evakuasi->get_by_lower_name($request->input('name'));
                }
                $jalur_evakuasi->setAttribute('lower_name', $request->input('name'));                
                return $this->success($jalur_evakuasi);

            }catch (\Exception $e) {
                DB::rollback();
                $message = $e->getMessage();
                $response = [
                    'code' => 2,
                    'message' => $message
                ];
                return response()->json($response, 400);
            }
        }
        else return response('Unauthorized.', 401);
    }
    
    public function validated(Request $request)
    {
        if($this->get_level() == "Pegawai"){
            DB::BeginTransaction();
            try {
                $jalur_evakuasi = $this->m_jalur_evakuasi->get_by_lower_name($request->input('name'));
                $m_validasi = new Validasi();
                $validated = $m_validasi->get_by_name("Valid");

                if($jalur_evakuasi->verifikasi == "Lengkap" && $jalur_evakuasi->validasi == "Pending")
                {
                    $jalur_evakuasi->update([
                        'id_validasi' => $validated->id_validasi
                    ]);
                    
                    DB::commit();
                    $jalur_evakuasi = $this->m_jalur_evakuasi->get_by_lower_name($request->input('name'));
                }

                $jalur_evakuasi->setAttribute('lower_name', $request->input('name'));                
                return $this->success($jalur_evakuasi);

            }catch (\Exception $e) {
                DB::rollback();
                return $this->error($e->getMessage());
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function unvalidated(Request $request)
    {
        if($this->get_level() == "Pegawai"){
            DB::BeginTransaction();
            try {
                $jalur_evakuasi = $this->m_jalur_evakuasi->get_by_lower_name($request->input('name'));
                $m_validasi = new Validasi();
                $unvalidated = $m_validasi->get_by_name("Tidak Valid");

                if($jalur_evakuasi->verifikasi == "Lengkap" && $jalur_evakuasi->validasi == "Pending")
                {
                    $jalur_evakuasi->update([
                        'id_validasi' => $unvalidated->id_validasi
                    ]);
                    
                    DB::commit();
                    $jalur_evakuasi = $this->m_jalur_evakuasi->get_by_lower_name($request->input('name'));
                }
                $jalur_evakuasi->setAttribute('lower_name', $request->input('name'));                
                return $this->success($jalur_evakuasi);

            }catch (\Exception $e) {
                DB::rollback();
                return $this->error($e->getMessage());
            }
        }
        else return response('Unauthorized.', 401);
    }
}
