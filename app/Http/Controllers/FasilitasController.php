<?php

namespace App\Http\Controllers;

use App\Models\Fasilitas;
use App\Models\Gunung;
use App\Models\JenisFasilitas;
use App\Models\Verifikasi;
use App\Models\Validasi;
use Auth;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FasilitasController extends Controller
{
    function __construct()
    {
        $this->m_fasilitas = new Fasilitas();
    }

    protected function get_jabatan()
    {
        return Auth::user()->pegawai->jabatan->jabatan;
    }

    protected function get_level()
    {
        return Auth::user()->level->level;
    }

    protected function create_new_id()
    {
        $latest_fasilitas = $this->m_fasilitas->get_latest();
        $new_fasilitas = $latest_fasilitas->id_fasilitas;
        $new_fasilitas ++;
        
        return $new_fasilitas;
    }

    public function index_fasilitas(){
        if($this->get_level() == "Pegawai"){
            $jabatan = $this->get_jabatan();
            switch ($jabatan) {
                case "Sub Bagian Penanggulangan Bencana":
                case "Sub Bagian Umum dan Kepegawaian":
                    $list_fasilitas = $this->m_fasilitas->get_validated()->paginate(10);
                    break;

                case "Kepala Bidang Pencegahan dan Kesiapsiagaan":
                    $list_fasilitas = $this->m_fasilitas->sort_desc_verified();
                    break;

                case "Kepala BPBD Jawa Timur":
                    $list_fasilitas = $this->m_fasilitas->sort_desc_validated();
                    break;

                default:
                    $list_fasilitas = $this->m_fasilitas->get_all();
                    break;
            }

            return view ('fasilitas.index-fasilitas', compact('list_fasilitas', 'jabatan'));
        }
        else return response('Unauthorized.', 401);
    }

    public function fasilitas_page(Request $request)
    {
        $list_fasilitas = $this->m_fasilitas->get_validated()->get();
        $jumlah = $list_fasilitas->count();

        $list_jenis_fasilitas = JenisFasilitas::all();
        if($request->input('kategori')){
            $category = str_replace("+", " ", ucwords($request->input('kategori')));
            $list_fasilitas = $this->m_fasilitas->get_by_category_validated($category)->paginate(7);
        }
        else
            $list_fasilitas = $this->m_fasilitas->get_validated()->paginate(7);
        
        foreach ($list_jenis_fasilitas as $key => $item) {
            $result = $this->m_fasilitas->get_by_jenis($item->jenis_fasilitas)->get();
            $kategori[] = [
                'jenis_fasilitas' => $item->jenis_fasilitas,
                'alt' => str_replace(" ", "+", strtolower($item->jenis_fasilitas)),
                'total' => count($result)
            ];
        }

        return view ('fasilitas', compact('list_fasilitas', 'jumlah', 'kategori'));
    }

    public function view_fasilitas($name)
    {
        $list_fasilitas = $this->m_fasilitas->get_validated()->get();
        $list_jenis_fasilitas = JenisFasilitas::all();
        
        $fasilitas = $this->m_fasilitas->get_by_lower_name($name);
        $fasilitas->setAttribute('lower_name', $name);

        foreach ($list_jenis_fasilitas as $key => $item) {
            $result = $this->m_fasilitas->get_by_jenis($item->jenis_fasilitas)->get();
            $kategori[] = [
                'jenis_fasilitas' => $item->jenis_fasilitas,
                'alt' => $item->jenis_fasilitas,
                'total' => count($result)
            ];
        }

        return view ('detail-fasilitas', compact('fasilitas', 'kategori'));
    }

    public function create_fasilitas(){
        if($this->get_level() == "Pegawai"){
            $collection = [
                'jenis_fasilitas' => JenisFasilitas::all(),
                'gunung' => Gunung::all()
            ];
            return view ('fasilitas.create-fasilitas', compact('collection'));
        }
        else return response('Unauthorized.', 401);
    }

    public function edit_fasilitas($name){
        if($this->get_level() == "Pegawai"){
            $fasilitas = $this->m_fasilitas->get_by_lower_name($name);
            $fasilitas->setAttribute('lower_name', $name);

            $collection = [
                'fasilitas' => $fasilitas,
                'jenis_fasilitas' => JenisFasilitas::all(),
                'gunung' => Gunung::all()
            ];
            return view ('fasilitas.edit-fasilitas', compact('collection'));
        }
        else return response('Unauthorized.', 401);
    }

    public function show_fasilitas($name){
        if($this->get_level() == "Pegawai"){
            $jabatan = $this->get_jabatan();
            try{
                $fasilitas = $this->m_fasilitas->get_by_lower_name($name);
                $fasilitas->setAttribute('lower_name', $name);
                switch ($jabatan) {
                    case "Sub Bagian Penanggulangan Bencana":
                    case "Sub Bagian Umum dan Kepegawaian":
                        return view ('fasilitas.detail-fasilitas', compact('fasilitas'));
                        break;

                    default:
                        if($fasilitas) return response()->json($fasilitas, 200);
                        else return response()->json('error', 400);
                        break;
                }

            } catch (\Exception $exception){
                $message = $exception->getMessage();
                $response = [
                    'code' => 2,
                    'message' => $message
                ];
                switch ($jabatan) {
                    case "Sub Bagian Penanggulangan Bencana":
                    case "Sub Bagian Umum dan Kepegawaian":
                        session()->flash('error_message', 'terjadi kesalahan');
                        return redirect(url()->previous());
                        break;

                    default:
                        return response()->json($response, 400);
                        break;
                }
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function checking_validate($type, $data){
        $message = [
            'required' => 'Tidak boleh kosong',
            'string' => 'Hanya berisi huruf dan karakter',
            'min' => 'Minimal 3 karakter',
        ];

        $this->validate($data, [
            'nama' => 'required|string|min:3',
            'alamat' => 'required|string|min:3',
            'longitude' => 'required',
            'latitude' => 'required',
            'jenis_fasilitas' => 'required',
            'gunung' => 'required'
        ], $message);
    }

    public function store_fasilitas(Request $request, JenisFasilitas $m_jenis_fasilitas){
        if($this->get_level() == "Pegawai"){
            $this->checking_validate('post', $request);
            DB::BeginTransaction();
            try {
                $daftar_verifikasi = Verifikasi::all();
                $daftar_validasi = Validasi::all();
                $create_new = $this->create_new_id();
                $gunung = $m_gunung->get_by_name($request->input('gunung'))->first();
                $jenis_fasilitas = $m_jenis_fasilitas->get_by_name($request->input('jenis_fasilitas'))->first();

                $foto = null;
                if($request->hasFile('foto')){
                    $foto = md5(time()) . '.' . $request->file('foto')->getClientOriginalExtension();
                    $path = public_path() . DIRECTORY_SEPARATOR . '/upload/fasilitas';

                    $request->file('foto')->move($path, $foto);
                }
                
                $fasilitas = Fasilitas::create([
                    'id_fasilitas' => $create_new,
                    'id_gunung' => $gunung->id_gunung,
                    'id_jenis_fasilitas' => $jenis_fasilitas->id_jenis_fasilitas,
                    'id_verifikasi' => $daftar_verifikasi[0]->id_verifikasi,
                    'id_validasi' => $daftar_validasi[0]->id_validasi,
                    'id_pegawai' => Auth::user()->pegawai->id_pegawai,
                    'nama_fasilitas' => $request->input('nama'),
                    'alamat_fasilitas' => $request->input('alamat'),
                    'foto_fasilitas' => $foto,
                    'longitude' => $request->input('longitude'),
                    'latitude' => $request->input('latitude'),
                    'tanggal_fasilitas' => Carbon::now(),
                ]);

                DB::commit();
                
                session()->flash('success_message', 'data fasilitas telah disimpan');
                return redirect()->route('fasilitas_page');

            }catch (\Exception $e) {
                DB::rollback();
                session()->flash('error_message', 'data fasilitas tidak disimpan');
                return redirect()->route('fasilitas_page');
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function update_fasilitas(Request $request, $name, Gunung $m_gunung, JenisFasilitas $m_jenis_fasilitas){
        if($this->get_level() == "Pegawai"){
            $this->checking_validate('patch', $request);
            DB::BeginTransaction();
            try {
                $fasilitas = $this->m_fasilitas->get_by_lower_name($name);
                $gunung = $m_gunung->get_by_name($request->input('gunung'));
                $jenis_fasilitas = $m_jenis_fasilitas->get_by_name($request->input('jenis_fasilitas'))->first();

                $foto = $fasilitas->foto_fasilitas;
                
                if($request->hasFile('foto')){
                    $path = public_path() . DIRECTORY_SEPARATOR . '/upload/fasilitas';
                    \File::delete($path .'/'. $foto);

                    $foto = md5(time()) . '.' . $request->file('foto')->getClientOriginalExtension();
                    $request->file('foto')->move($path, $foto);
                }
            
                $fasilitas->update([
                    'nama_fasilitas' => $request->input('nama'),
                    'alamat_fasilitas' => $request->input('alamat'),
                    'foto_fasilitas' => $foto,
                    'id_gunung' => $gunung->id_gunung,
                    'id_jenis_fasilitas' => $jenis_fasilitas->id_jenis_fasilitas,
                    'longitude' => $request->input('longitude'),
                    'latitude' => $request->input('latitude'),
                ]);
                
                DB::commit();

                session()->flash('success_message', 'data fasilitas telah disimpan');
                return redirect()->route('fasilitas_page');

            }catch (\Exception $e) {
                DB::rollback();
                session()->flash('error_message', 'data fasilitas tidak disimpan');
                return redirect()->route('fasilitas_page');
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function destroy_fasilitas($name){
        if($this->get_level() == "Pegawai"){
            $fasilitas = $this->m_fasilitas->get_by_lower_name($name);
            $fasilitas->delete();

            session()->flash('success_message', 'data fasilitas telah dihapus');
            return redirect()->route('fasilitas_page');
        }
        else return response('Unauthorized.', 401);
    }

    public function verified(Request $request)
    {
        if($this->get_level() == "Pegawai"){
            DB::BeginTransaction();
            try {
                $fasilitas = $this->m_fasilitas->get_by_lower_name($request->input('name'));
                $m_verifikasi = new Verifikasi();
                $verified = $m_verifikasi->get_by_name("Lengkap")->first();

                if($fasilitas->verifikasi == "Pending")
                {
                    $fasilitas->update([
                        'id_verifikasi' => $verified->id_verifikasi
                    ]);
                    
                    DB::commit();
                    $fasilitas = $this->m_fasilitas->get_by_lower_name($request->input('name'));
                }

                $fasilitas->setAttribute('lower_name', $request->input('name'));                
                return $this->success($fasilitas);

            }catch (\Exception $e) {
                DB::rollback();
                return $this->error($e->getMessage());
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function unverified(Request $request)
    {
        if($this->get_level() == "Pegawai"){
            DB::BeginTransaction();
            try {
                $fasilitas = $this->m_fasilitas->get_by_lower_name($request->input('name'));
                $m_verifikasi = new Verifikasi();
                $unverified = $m_verifikasi->get_by_name("Tidak Lengkap")->first();

                if($fasilitas->verifikasi == "Pending")
                {
                    $fasilitas->update([
                        'id_verifikasi' => $unverified->id_verifikasi
                    ]);
                    
                    DB::commit();
                    $fasilitas = $this->m_fasilitas->get_by_lower_name($request->input('name'));
                }
                $fasilitas->setAttribute('lower_name', $request->input('name'));                
                return $this->success($fasilitas);

            }catch (\Exception $e) {
                DB::rollback();
                return $this->error($e->getMessage());
            }
        }
        else return response('Unauthorized.', 401);
    }
    
    public function validated(Request $request)
    {
        if($this->get_level() == "Pegawai"){
            DB::BeginTransaction();
            try {
                $fasilitas = $this->m_fasilitas->get_by_lower_name($request->input('name'));
                $m_validasi = new Validasi();
                $validated = $m_validasi->get_by_name("Valid")->first();

                if($fasilitas->verifikasi == "Lengkap" && $fasilitas->validasi == "Pending")
                {
                    $fasilitas->update([
                        'id_validasi' => $validated->id_validasi
                    ]);
                    
                    DB::commit();
                    $fasilitas = $this->m_fasilitas->get_by_lower_name($request->input('name'));
                }

                $fasilitas->setAttribute('lower_name', $request->input('name'));                
                return $this->success($fasilitas);

            }catch (\Exception $e) {
                DB::rollback();
                return $this->error($e->getMessage());
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function unvalidated(Request $request)
    {
        if($this->get_level() == "Pegawai"){
            DB::BeginTransaction();
            try {
                $fasilitas = $this->m_fasilitas->get_by_lower_name($request->input('name'));
                $m_validasi = new Validasi();
                $unvalidated = $m_validasi->get_by_name("Tidak Valid")->first();

                if($fasilitas->verifikasi == "Lengkap" && $fasilitas->validasi == "Pending")
                {
                    $fasilitas->update([
                        'id_validasi' => $unvalidated->id_validasi
                    ]);
                    
                    DB::commit();
                    $fasilitas = $this->m_fasilitas->get_by_lower_name($request->input('name'));
                }
                $fasilitas->setAttribute('lower_name', $request->input('name'));                
                return $this->success($fasilitas);

            }catch (\Exception $e) {
                DB::rollback();
                return $this->error($e->getMessage());
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function download(Request $request){
        if(Auth::user()){
            if($request->input('kategori')){
                $category = str_replace("+", " ", ucwords($request->input('kategori')));
                $list_fasilitas = $this->m_fasilitas->get_by_category_validated($category)->get();
            }
            else
                $list_fasilitas = $this->m_fasilitas->get_validated()->get();
            
            $list_jenis_fasilitas = JenisFasilitas::all();
            foreach ($list_jenis_fasilitas as $jenis_fasilitas) {
                foreach ($list_fasilitas as $fasilitas) {
                    if($jenis_fasilitas->jenis_fasilitas == $fasilitas->jenis_fasilitas){
                        $data[$jenis_fasilitas->jenis_fasilitas][] = [
                            'nama' => $fasilitas->nama_fasilitas,
                            'alamat' => $fasilitas->alamat_fasilitas,
                            'latitude' => $fasilitas->latitude,
                            'longitude' => $fasilitas->longitude,
                            'img_path' => asset('/upload/fasilitas'). '/',
                            'img' => $fasilitas->foto_fasilitas,
                            'jenis' => $jenis_fasilitas->jenis_fasilitas,
                            'gunung' => $fasilitas->gunung,
                        ];
                    }
                }
            }
            $pdf = PDF::loadView('unduh-fasilitas', compact('data'));
            return $pdf->download(md5(time()) . '.pdf');
        }
        else return redirect()->route('form_login');
    }

    /**
     * List module for Mobile API
     */

    public function list_fasilitas(){
        try{
            $list_jenis_fasilitas = JenisFasilitas::all();
            $list_fasilitas = $this->m_fasilitas->get_validated()->get();
            
            /*
            foreach ($list_jenis_fasilitas as $jenis_fasilitas) {
                foreach ($list_fasilitas as $fasilitas) {
                    if($jenis_fasilitas->jenis_fasilitas == $fasilitas->jenis_fasilitas){
                        $data[$jenis_fasilitas->jenis_fasilitas][] = [
                            'nama' => $fasilitas->nama_fasilitas,
                            'alamat' => $fasilitas->alamat_fasilitas,
                            'latitude' => $fasilitas->latitude,
                            'longitude' => $fasilitas->longitude,
                            'img_path' => asset('/upload/fasilitas'). '/',
                            'img' => $fasilitas->foto_fasilitas,
                            'jenis' => $jenis_fasilitas->jenis_fasilitas,
                            'gunung' => $fasilitas->gunung,
                        ];
                    }
                }
            }
            */
            foreach ($list_fasilitas as $fasilitas) {
                $fasilitas = [
                    'nama' => $fasilitas->nama_fasilitas,
                    'alamat' => $fasilitas->alamat_fasilitas,
                    'latitude' => $fasilitas->latitude,
                    'longitude' => $fasilitas->longitude,
                    'img_path' => asset('/upload/fasilitas'). '/',
                    'img' => $fasilitas->foto_fasilitas,
                    'jenis' => $fasilitas->jenis_fasilitas,
                    'gunung' => $fasilitas->gunung,
                ];
                $data[] = $fasilitas;
            }
            return $this->success($data);

        }catch (\Exception $e) {
            return $e;
            return $this->error($e->getMessage());
        }
   }

}