<?php

namespace App\Http\Controllers;

use App\Models\BandarUdara;
use App\Models\Prabencana;
use App\Models\DataDemografi;
use App\Models\Demografi;
use App\Models\Kecamatan;
use App\Models\KelasBandara;
use App\Models\LandasanBandara;
use App\Models\SaranaKesehatan;
use App\Models\SaranaPeribadatan;
use App\Models\SaranaPendidikan;
use App\Models\TataLahan;
use App\Models\ProfilDaerah;
use App\Models\UsiaDemografi;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\App;

class DataPraBencanaController extends Controller
{
    public function index_prabencana()
    {
        //
        if(Auth::user()){
            if(Auth::user()->pegawai->jabatan->jabatan == 'Pengolah Data Pra Bencana' || Auth::user()->pegawai->jabatan->jabatan == 'Kepala Seksi Kesiapsiagaan'){
                $prabencana_collection = Prabencana::orderBy('id_verifikasi', 'asc')->orderBy('tanggal_prabencana', 'desc')->get();
            }
            else if(Auth::user()->pegawai->jabatan->jabatan == 'Kepala Bidang Pencegahan dan Kesiapsiagaan'){
                $prabencana_collection = Prabencana::where('id_verifikasi', 2)
                ->where('id_validasi', 1)
                ->orWhere('id_validasi', 2)
                ->orderBy('id_verifikasi', 'desc')->get();
            }
            else if(Auth::user()->pegawai->jabatan->jabatan == 'Kepala BPBD Jawa Timur'){
                $prabencana_collection = Prabencana::where('id_verifikasi', 2)
                ->where('id_validasi', 2)
                ->orderBy('id_verifikasi', 'desc')->get();
            }
            else if(Auth::user()->pegawai->jabatan->jabatan == 'Sub Bagian Penanggulangan Bencana'){
                $profil_daerah_collection = ProfilDaerah::all();
                $prabencana_collection = Prabencana::orderBy('id_verifikasi', 'asc')->orderBy('tahun', 'asc')->get();
                $tahun_collection = $prabencana_collection->groupBy('tahun');
                $tahun_collection = $tahun_collection->keys();
                $sumber_data_collection = $prabencana_collection->groupBy('sumber_data');
                $sumber_data_collection = $sumber_data_collection->keys();
                return view ('data-prabencana.laporan-prabencana', compact('profil_daerah_collection', 'tahun_collection', 'sumber_data_collection'));
            }
            return view ('data-prabencana.index-prabencana', compact('prabencana_collection'));
        }
        else return response('Unauthorized.', 401);
    }

    public function create_prabencana()
    {
        //
        if(Auth::user()->pegawai->jabatan->jabatan == 'Pengolah Data Pra Bencana'){
            $profil_daerah_collection = ProfilDaerah::all();
            $kelas_collection = KelasBandara::all();
            $landasan_collection = LandasanBandara::all();
            $kecamatan_collection = Kecamatan::all();
            return view('data-prabencana.create-prabencana', compact('profil_daerah_collection', 'kelas_collection', 'landasan_collection', 'kecamatan_collection'));
        }
        else return response('Unauthorized.', 401);
    }

    public function edit_prabencana(Prabencana $data_prabencana)
    {
        //
        if(Auth::user()->pegawai->jabatan->jabatan == 'Pengolah Data Pra Bencana'){
            $profil_daerah_collection = ProfilDaerah::all();
            $kelas_collection = KelasBandara::all();
            $landasan_collection = LandasanBandara::all();
            $kecamatan_collection = Kecamatan::all();

            $prabencana = Prabencana::where('id_prabencana', $data_prabencana->id_prabencana)
            ->with([
                'kesehatan', 'peribadatan', 'pendidikan', 'tata_lahan', 'bandar_udara', 'demografi', 'demografi.data_demografi'
            ])->first();
            return view ('data-prabencana.edit-prabencana', compact('prabencana', 'profil_daerah_collection', 'kelas_collection', 'landasan_collection', 'kecamatan_collection'));
        }
        else return response('Unauthorized.', 401);
    }
    
    public function show_prabencana(Prabencana $data_prabencana)
    {
        //
        if(Auth::user()->pegawai->jabatan->jabatan == 'Pengolah Data Pra Bencana' ||
            Auth::user()->pegawai->jabatan->jabatan == 'Kepala Seksi Kesiapsiagaan' ||
            Auth::user()->pegawai->jabatan->jabatan == 'Kepala Bidang Pencegahan dan Kesiapsiagaan' ||
            Auth::user()->pegawai->jabatan->jabatan == 'Kepala BPBD Jawa Timur'){
            try{
                $prabencana = Prabencana::join('profil_daerah', 'prabencana.id_profil_daerah', '=', 'profil_daerah.id_profil_daerah')
                    ->where('id_prabencana', $data_prabencana->id_prabencana)
                    ->with([
                        'kesehatan', 'peribadatan', 'pendidikan', 'tata_lahan', 'bandar_udara', 'demografi', 'demografi.data_demografi'
                    ])->first();
                
                return view ('data-prabencana.view-prabencana', compact('prabencana'));
                if($prabencana) return response()->json($prabencana, 200);
                else return response()->json('error', 400);

            } catch (\ModelNotFoundException $exception){
                $message = $exception->getMessage();
                $response = [
                    'code' => 2,
                    'message' => $message
                ];
                return response()->json($response, 400);
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function pdf_prabencana(Request $request){
        $prabencana = Prabencana::join('profil_daerah', 'prabencana.id_profil_daerah', '=', 'profil_daerah.id_profil_daerah')
            ->with([
                'kesehatan', 'peribadatan', 'pendidikan', 'tata_lahan', 'bandar_udara', 'demografi', 'demografi.data_demografi'
            ])
            ->where('prabencana.id_profil_daerah', $request->input('profil_daerah'))
            ->where('prabencana.tahun', $request->input('tahun'))
            ->where('prabencana.sumber_data', $request->input('sumber_data'))
            ->where('id_verifikasi', 2)
            ->where('id_validasi', 2)
            ->first();

        // return view('data-prabencana.view-laporan-prabencana', compact('prabencana'));
        $pdf = App::make('dompdf.wrapper');
        $pdf->setOptions(['defaultFont' => 'sans-serif'])
            ->setPaper('a4', 'potrait')
            ->loadView('data-prabencana.view-laporan-prabencana', compact('prabencana'));
        return $pdf->stream();
    }

    public function checking_validate(Request $request){
        $message = [
            'required' => 'Form tidak boleh kosong',
            'string' => 'Hanya berisi huruf dan karakter',
            'min' => 'Jumlah kata terlalu sedikit',
            'tahun.min' => 'Jumlah karakter terlalu sedikit',
        ];
        
        $this->validate($request, [
            'profil_daerah' => 'required',
            'sumber_data' => 'required|string|min:3',
            'tahun' => 'required|min:4',
        ], $message);
    }

    public function checking_prabencana(Request $request){
        $message = [
            'required' => 'Pilihan tidak boleh kosong',
        ];
        
        $this->validate($request, [
            'profil_daerah' => 'required',
            'sumber_data' => 'required|string|min:3',
            'tahun' => 'required|min:4',
        ], $message);

        $prabencana = Prabencana::join('profil_daerah', 'prabencana.id_profil_daerah', '=', 'profil_daerah.id_profil_daerah')
            ->with([
                'kesehatan', 'peribadatan', 'pendidikan', 'tata_lahan', 'bandar_udara', 'demografi', 'demografi.data_demografi'
            ])
            ->where('prabencana.id_profil_daerah', $request->input('profil_daerah'))
            ->where('prabencana.tahun', $request->input('tahun'))
            ->where('prabencana.sumber_data', $request->input('sumber_data'))
            ->where('id_verifikasi', 2)
            ->where('id_validasi', 2)
            ->first();

        if(!$prabencana){
            $data = [
                'status' => false
            ];
        }
        else{
            $data = [
                'status' => true,
                'profil_daerah' => $request->input('profil_daerah'),
                'sumber_data' => $request->input('sumber_data'),
                'tahun' => $request->input('tahun'),
            ];
        }
        return response()->json($data, 200);
    }

    public function store_prabencana(Request $request)
    {
        //
        if(Auth::user()->pegawai->jabatan->jabatan == 'Pengolah Data Pra Bencana'){
            $this->checking_validate($request);
            DB::BeginTransaction();
            try {
                $data_prabencana = Prabencana::create([
                    'id_profil_daerah' => $request->input('profil_daerah'),
                    'id_verifikasi' => 1,
                    'id_validasi' => 1,
                    'id_pegawai' => Auth::user()->pegawai->id_pegawai,
                    'sumber_data' => $request->input('sumber_data'),
                    'tahun' => $request->input('tahun'),
                    'tanggal_prabencana' => Carbon::now(),
                ]);

                if($request->input('jumlah-kesehatan') > 0){
                    $jumlah = $request->input('jumlah-kesehatan');
                    for($i = 1; $i <= $jumlah; $i++){
                        $kesehatan = new SaranaKesehatan();
                        $kesehatan->prabencana()->associate($data_prabencana);
                        $kesehatan->instansi_kesehatan = $request->input('instansi-'.$i);
                        $kesehatan->sdm = $request->input('sdm-'.$i);
                        $kesehatan->sarana_prasarana_kesehatan = $request->input('sarana_prasarana-'.$i);
                        $kesehatan->logistik = $request->input('logistik-'.$i);
                        $kesehatan->peralatan = $request->input('peralatan-'.$i);
                        $kesehatan->ket_sarana_kesehatan = $request->input('keterangan-'.$i);
                        $kesehatan->save();
                    }
                }
                
                if($request->input('jumlah-peribadatan') > 0){
                    $jumlah = $request->input('jumlah-peribadatan');
                    for($i = 1; $i <= $jumlah; $i++){
                        $peribadatan = new SaranaPeribadatan();
                        $peribadatan->prabencana()->associate($data_prabencana);
                        $peribadatan->id_kecamatan = $request->input('kecamatan-peribadatan-'.$i);
                        $peribadatan->masjid = $request->input('masjid-'.$i);
                        $peribadatan->gereja = $request->input('gereja-'.$i);
                        $peribadatan->pura = $request->input('pura-'.$i);
                        $peribadatan->wihara = $request->input('wihara-'.$i);
                        $peribadatan->lainnya = $request->input('lainnya-'.$i);
                        $peribadatan->save();
                    }
                }
                
                if($request->input('jumlah-pendidikan') > 0){
                    $jumlah = $request->input('jumlah-pendidikan');
                    for($i = 1; $i <= $jumlah; $i++){
                        $pendidikan = new SaranaPendidikan();
                        $pendidikan->id_kecamatan = $request->input('kecamatan-pendidikan-'.$i);
                        $pendidikan->prabencana()->associate($data_prabencana);
                        $pendidikan->tk = $request->input('tk-'.$i);
                        $pendidikan->slb = $request->input('slb-'.$i);
                        $pendidikan->sd = $request->input('sd-'.$i);
                        $pendidikan->mi = $request->input('mi-'.$i);
                        $pendidikan->smp = $request->input('smp-'.$i);
                        $pendidikan->mts = $request->input('mts-'.$i);
                        $pendidikan->sma = $request->input('sma-'.$i);
                        $pendidikan->ma = $request->input('ma-'.$i);
                        $pendidikan->smk = $request->input('smk-'.$i);
                        $pendidikan->pt = $request->input('pt-'.$i);
                        $pendidikan->save();
                    }
                }
                if($request->input('jumlah-lahan') > 0){
                    $jumlah = $request->input('jumlah-lahan');
                    for($i = 1; $i <= $jumlah; $i++){
                        $lahan = new TataLahan();
                        $lahan->prabencana()->associate($data_prabencana);
                        $lahan->uraian = $request->input('uraian-'.$i);
                        $lahan->luas = $request->input('luas-'.$i);
                        $lahan->proporsi = $request->input('proporsi-'.$i);
                        $lahan->save();
                    }
                }
                if($request->input('jumlah-bandara') > 0){
                    $jumlah = $request->input('jumlah-bandara');
                    for($i = 1; $i <= $jumlah; $i++){
                        $bandara = new BandarUdara();
                        $bandara->prabencana()->associate($data_prabencana);
                        $bandara->id_kelas_bandara = $request->input('kelas-'.$i);
                        $bandara->id_landasan_bandara = $request->input('panjang-'.$i);
                        $bandara->lokasi = $request->input('lokasi-'.$i);
                        $bandara->jenis_landasan = $request->input('jenis-'.$i);
                        $bandara->sarana_prasarana_bandara = $request->input('sarana_prasarana_bandara-'.$i);
                        $bandara->save();
                    }
                }
                if($request->input('jumlah-demografi') > 0){
                    $jumlah = $request->input('jumlah-demografi');
                    $usia_collection = UsiaDemografi::all();

                    for($i = 1; $i <= $jumlah; $i++){
                        $demografi = new Demografi();
                        $demografi->id_kecamatan = $request->input('kecamatan-demografi-'.$i);
                        $demografi->prabencana()->associate($data_prabencana);
                        $demografi->save();

                        for($j=0; $j < count($usia_collection); $j++){
                            $data_demografi = new DataDemografi();
                            $data_demografi->id_usia_demografi = $usia_collection[$j]->id_usia_demografi;
                            $data_demografi->demografi()->associate($demografi);
                            $data_demografi->pria = $request->input('pria-'.($j+1).'-'.$i);
                            $data_demografi->wanita = $request->input('wanita-'.($j+1).'-'.$i);
                            $data_demografi->save();
                        }
                    }
                }
                DB::commit();

                session()->flash('success_message', 'data pra bencana telah disimpan');
                // return redirect()->route('prabencana_page');
                return response()->json('data', 200);

            } catch (\Exception $e) {
                DB::rollback();
                return $e;
                session()->flash('error_message', 'data pra bencana tidak disimpan');
                // return redirect()->route('prabencana_page');
                return response()->json($data_prabencana, 400);
                // throw new \Exception($e, 1);
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function update_prabencana(Prabencana $data_prabencana, Request $request)
    {
        //
        /*if(Auth::user()->pegawai->jabatan->jabatan == 'Pengolah Data Pra Bencana'){
            $this->checking_validate($request);
            $prabencana->update([
                'instansi' => $request->input('instansi'),
                'sdm' => $request->input('sdm'),
                'sarana_prasarana' => $request->input('sarana_prasarana'),
                'logistik' => $request->input('logistik'),
                'peralatan' => $request->input('peralatan'),
                'keterangan' => $request->input('keterangan'),
                'sumber_data' => $request->input('sumber_data'),
                'tahun' => $request->input('tahun'),
                'id_profil_daerah' => $request->input('profil_daerah'),
            ]);
            
            session()->flash('success_message', 'data data pra bencana telah disimpan');
            return redirect()->route('prabencana_page');
        }
        else return response('Unauthorized.', 401);*/
        if(Auth::user()->pegawai->jabatan->jabatan == 'Pengolah Data Pra Bencana'){

            DB::BeginTransaction();
            try {
                    $data_prabencana->sumber_data = $request->input('sumber_data');
                    $data_prabencana->tahun = $request->input('tahun');
                    $data_prabencana->update();

                if($request->input('jumlah-kesehatan') > 0){
                    foreach ($data_prabencana->kesehatan as $kesehatan) {
                        $kesehatan->delete();
                    }

                    $jumlah = $request->input('jumlah-kesehatan');
                    for($i = 1; $i <= $jumlah; $i++){
                        $kesehatan = new SaranaKesehatan();
                        $kesehatan->prabencana()->associate($data_prabencana);
                        $kesehatan->instansi_kesehatan = $request->input('instansi-'.$i);
                        $kesehatan->sdm = $request->input('sdm-'.$i);
                        $kesehatan->sarana_prasarana_kesehatan = $request->input('sarana_prasarana-'.$i);
                        $kesehatan->logistik = $request->input('logistik-'.$i);
                        $kesehatan->peralatan = $request->input('peralatan-'.$i);
                        $kesehatan->ket_sarana_kesehatan = $request->input('keterangan-'.$i);
                        $kesehatan->save();
                    }
                } else {
                    foreach ($data_prabencana->kesehatan as $kesehatan) {
                        $kesehatan->delete();
                    }
                }
                
                if($request->input('jumlah-peribadatan') > 0){
                    foreach ($data_prabencana->peribadatan as $peribadatan) {
                        $peribadatan->delete();
                    }

                    $jumlah = $request->input('jumlah-peribadatan');
                    for($i = 1; $i <= $jumlah; $i++){
                        $peribadatan = new SaranaPeribadatan();
                        $peribadatan->id_kecamatan = $request->input('kecamatan-peribadatan-'.$i);
                        $peribadatan->prabencana()->associate($data_prabencana);
                        $peribadatan->masjid = $request->input('masjid-'.$i);
                        $peribadatan->gereja = $request->input('gereja-'.$i);
                        $peribadatan->pura = $request->input('pura-'.$i);
                        $peribadatan->wihara = $request->input('wihara-'.$i);
                        $peribadatan->lainnya = $request->input('lainnya-'.$i);
                        $peribadatan->save();
                    }
                } else {
                    foreach ($data_prabencana->peribadatan as $peribadatan) {
                        $peribadatan->delete();
                    }
                }
                
                if($request->input('jumlah-pendidikan') > 0){
                    foreach ($data_prabencana->pendidikan as $pendidikan) {
                        $pendidikan->delete();
                    }

                    $jumlah = $request->input('jumlah-pendidikan');
                    for($i = 1; $i <= $jumlah; $i++){
                        $pendidikan = new SaranaPendidikan();
                        $pendidikan->id_kecamatan = $request->input('kecamatan-pendidikan-'.$i);
                        $pendidikan->prabencana()->associate($data_prabencana);
                        $pendidikan->tk = $request->input('tk-'.$i);
                        $pendidikan->slb = $request->input('slb-'.$i);
                        $pendidikan->sd = $request->input('sd-'.$i);
                        $pendidikan->mi = $request->input('mi-'.$i);
                        $pendidikan->smp = $request->input('smp-'.$i);
                        $pendidikan->mts = $request->input('mts-'.$i);
                        $pendidikan->sma = $request->input('sma-'.$i);
                        $pendidikan->ma = $request->input('ma-'.$i);
                        $pendidikan->smk = $request->input('smk-'.$i);
                        $pendidikan->pt = $request->input('pt-'.$i);
                        $pendidikan->save();
                    }
                } else {
                    foreach ($data_prabencana->pendidikan as $pendidikan) {
                        $pendidikan->delete();
                    }
                }

                if($request->input('jumlah-lahan') > 0){
                    foreach ($data_prabencana->tata_lahan as $tata_lahan) {
                        $tata_lahan->delete();
                    }

                    $jumlah = $request->input('jumlah-lahan');
                    for($i = 1; $i <= $jumlah; $i++){
                        $lahan = new TataLahan();
                        $lahan->prabencana()->associate($data_prabencana);
                        $lahan->uraian = $request->input('uraian-'.$i);
                        $lahan->luas = $request->input('luas-'.$i);
                        $lahan->proporsi = $request->input('proporsi-'.$i);
                        $lahan->save();
                    }
                } else {
                    foreach ($data_prabencana->tata_lahan as $tata_lahan) {
                        $tata_lahan->delete();
                    }
                }

                if($request->input('jumlah-bandara') > 0){
                    foreach ($data_prabencana->bandar_udara as $bandar_udara) {
                        $bandar_udara->delete();
                    }

                    $jumlah = $request->input('jumlah-bandara');
                    for($i = 1; $i <= $jumlah; $i++){
                        $bandara = new BandarUdara();
                        $bandara->id_kelas_bandara = $request->input('kelas-'.$i);
                        $bandara->id_landasan_bandara = $request->input('panjang-'.$i);
                        $bandara->prabencana()->associate($data_prabencana);
                        $bandara->lokasi = $request->input('lokasi-'.$i);
                        $bandara->jenis_landasan = $request->input('jenis-'.$i);
                        $bandara->sarana_prasarana_bandara = $request->input('sarana_prasarana_bandara-'.$i);
                        $bandara->save();
                    }
                } else {
                    foreach ($data_prabencana->bandar_udara as $bandar_udara) {
                        $bandar_udara->delete();
                    }
                }

                if($request->input('jumlah-demografi') > 0){
                    foreach ($data_prabencana->demografi as $demografi) {
                        $demografi->delete();
                    }

                    $jumlah = $request->input('jumlah-demografi');
                    $usia_collection = UsiaDemografi::all();

                    for($i = 1; $i <= $jumlah; $i++){
                        $demografi = new Demografi();
                        $demografi->id_kecamatan = $request->input('kecamatan-demografi-'.$i);
                        $demografi->prabencana()->associate($data_prabencana);
                        $demografi->save();

                        for($j=0; $j < count($usia_collection); $j++){
                            $data_demografi = new DataDemografi();
                            $data_demografi->id_usia_demografi = $usia_collection[$j]->id_usia_demografi;
                            $data_demografi->demografi()->associate($demografi);
                            $data_demografi->pria = $request->input('pria-'.($j+1).'-'.$i);
                            $data_demografi->wanita = $request->input('wanita-'.($j+1).'-'.$i);
                            $data_demografi->save();
                        }
                    }
                } else {
                    foreach ($data_prabencana->demografi as $demografi) {
                        $demografi->delete();
                    }
                }
                DB::commit();

                session()->flash('success_message', 'data pra bencana telah disimpan');
                // return redirect()->route('prabencana_page');
                return response()->json('data', 200);

            } catch (\Exception $e) {
                DB::rollback();
                return $e;
                session()->flash('error_message', 'data pra bencana tidak disimpan');
                // return redirect()->route('prabencana_page');
                return response()->json($data_prabencana, 400);
                // throw new \Exception($e, 1);
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function destroy_prabencana(Prabencana $data_prabencana)
    {
        //
        if(Auth::user()->pegawai->jabatan->jabatan == 'Pengolah Data Pra Bencana'){
            $data_prabencana->delete();
            session()->flash('success_message', 'data data pra bencana telah dihapus');
            return redirect()->route('prabencana_page');
        }
        else return response('Unauthorized.', 401);
    }

    public function kecamatan(Request $request){
        try{
            $kecamatan = Kecamatan::find($request->input('kecamatan'));

            $data = [
                'kecamatan' => $kecamatan->kecamatan,
            ];

            return response()->json($data, 200);

        } catch (ValidationException $exception) {
            $message = $exception->getMessage();
            $response = [
                'code' => 3,
                'message' => $message,
            ];
        }
        return response()->json($response, 200);
    }
}
