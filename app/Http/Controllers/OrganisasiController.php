<?php

namespace App\Http\Controllers;

use App\Mail\KirimSertifikat;
use App\Models\Level;
use App\Models\Organisasi;
use App\Models\StatusOrganisasi;
use App\Models\User;
use Auth;
use Illuminate\Database\Eloquent\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class OrganisasiController extends Controller
{
    function __construct()
    {
        $this->m_organisasi = new Organisasi();
    }

    protected function get_jabatan()
    {
        return Auth::user()->pegawai->jabatan->jabatan;
    }

    protected function get_level()
    {
        return Auth::user()->level->level;
    }

    protected function create_new_id()
    {
        $user = new User();
        $latest_user = $user->get_latest();
        $new_user = $latest_user->id_user;
        $new_user ++;

        $latest_organisasi = $this->m_organisasi->get_latest();
        $new_organisasi = $latest_organisasi->id_organisasi;
        $new_organisasi ++;

        $data = [
            'id_user' => $new_user,
            'id_organisasi' => $new_organisasi
        ];
        
        return $data;
    }

    public function index_organisasi(){
        if($this->get_level() == "Pegawai"){
            $list_organisasi = $this->m_organisasi->get_all();
            return view ('user.organisasi.index-organisasi', compact('list_organisasi'));
        }
        else return response('Unauthorized.', 401);
    }

    public function registrasi(){
        return view ('registrasi');
    }

    public function create_organisasi(){
        if($this->get_level() == "Pegawai"){
            return view ('user.organisasi.create-organisasi');
        }
        else return response('Unauthorized.', 401);
    }

    public function edit_organisasi($name){
        if($this->get_level() == "Pegawai"){
            $organisasi = $this->m_organisasi->get_by_lower_name($name);
            $organisasi->setAttribute('lower_name', $name);
            
            return view ('user.organisasi.edit-organisasi', compact('organisasi'));
        }
        else return response('Unauthorized.', 401);
    }

    public function edit_by_organisasi(Request $request){
        if($this->get_level() == "Organisasi"){
            $organisasi = $this->m_organisasi->get_by_lower_name($request->input('organisasi'));
            $organisasi->setAttribute('lower_name', $request->input('organisasi'));

            return view ('user.organisasi.organisasi', compact('organisasi'));
        }
        else return response('Unauthorized.', 401);
    }
    public function show_by_organisasi(Request $request){
        if($this->get_level() == "Organisasi"){
            $organisasi = $this->m_organisasi->get_by_lower_name($request->input('organisasi'));
            $organisasi->setAttribute('lower_name', $request->input('organisasi'));

            return view ('user.organisasi.profile', compact('organisasi'));
        }
        else return response('Unauthorized.', 401);
    }

    public function show_organisasi($name){
        if($this->get_level() == "Pegawai"){
            try{
                $organisasi = $this->m_organisasi->get_by_lower_name($name);
                $organisasi->setAttribute('lower_name', $name);

                if($organisasi) return response()->json($organisasi, 200);
                else return response()->json('error', 400);

            } catch (\Exception $e){
                $message = $e->getMessage();
                $response = [
                    'code' => 2,
                    'message' => $message
                ];
                return response()->json($response, 400);
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function checking_validate($type, $data, $model){
        $message = [
            'required' => 'Tidak boleh kosong',
            'min' => 'Jumlah karakter terlalu sedikit',
            'max' => 'Jumlah karakter melebihi batas',
            'file.image' => 'Tipe file berjenis gambar',
            'file.max' => 'Ukuran file maksimal 8mb',
            'email.email' => 'Isi dengan alamat email yang valid',
            'email.max' => 'Alamat email maksimal 255 karakter',
            'email.unique' => 'Email telah terdaftar',
            'no_telp.min' => 'Nomor telepon minimal 11 karakter',
            'no_telp.max' => 'Nomor telepon maksimal 13 karakter',
            'handphone.min' => 'Nomor handphone minimal 11 karakter',
            'handphone.max' => 'Nomor handphone maksimal 13 karakter',
        ];

        switch($type){
            case'post':
                $file_rule = 'required|image|mimes:jpeg,png,jpg,JPG|max:8000';
                $fax_rule = 'min:8|max:13';
                $kode_pos_rule = 'min:4|max:6';
                $handphone_rule = 'min:11|max:13';

                if($data->input('fax') == null){
                    $fax_rule = '';
                }
                if($data->input('kode_pos') == null){
                    $kode_pos_rule = '';
                }
                if($data->input('handphone') == null){
                    $handphone_rule = '';
                }

                $this->validate($data, [
                    'organisasi' => 'required',
                    'file' => $file_rule,
                    'alamat' => 'required|min:6',
                    'no_telp' => 'required|min:8|max:13',
                    'fax' => $fax_rule,
                    'kode_pos' => $kode_pos_rule,
                    'handphone' => $handphone_rule,
                    'email' => 'required|email|max:255|unique:organisasi,email_organisasi',
                ], $message);
            break;

            case'patch':
                $username_rule = 'required|alpha_dash|min:4|max:16|unique:user';
                $password_rule = 'required|string|min:8|confirmed';
                $email_rule = 'required|email|max:255|unique:organisasi,email_organisasi';
                $file_rule = 'image|mimes:jpeg,png,jpg,JPG|max:8000';
                $fax_rule = 'min:8|max:13';
                $kode_pos_rule = 'min:4|max:6';
                $handphone_rule = 'min:11|max:13';

                if($model->email_organisasi == $data->input('email')){
                    $email_rule = '';
                }
                if($data->input('password') == null){
                    $password_rule = '';
                }

                if($data->file('file') == null){
                    $file_rule = '';
                }
                if($data->input('fax') == null){
                    $fax_rule = '';
                }
                if($data->input('kode_pos') == null){
                    $kode_pos_rule = '';
                }
                if($data->input('handphone') == null){
                    $handphone_rule = '';
                }

                $this->validate($data, [
                    'password' => $password_rule,
                    'email' => $email_rule,
                    'organisasi' => 'required',
                    'file' => $file_rule,
                    'alamat' => 'required|min:6',
                    'no_telp' => 'required|min:8|max:13',
                    'fax' => $fax_rule,
                    'kode_pos' => $kode_pos_rule,
                    'handphone' => $handphone_rule,
                ], $message);
            break;
        }
    }

    public function store_organisasi(Request $request){
        $this->checking_validate('post', $request, null);
        DB::BeginTransaction();
        try {
            $daftar_level = Level::all();
            $daftar_status = StatusOrganisasi::all();
            $create_new = $this->create_new_id();

            $user = User::create([
                'id_user' => $create_new['id_user'],
                'id_level' => $daftar_level[1]->id_level,
                'username' => strtolower(str_replace(' ', '.', substr($request->input('organisasi'), 0, 8))),
                'kata_sandi' => bcrypt(12345678)
            ]);

            $organisasi = new Organisasi();
            $organisasi->id_organisasi = $create_new['id_organisasi'];
            $organisasi->user()->associate($user);
            $organisasi->id_status_organisasi = $daftar_status[0]->id_status_organisasi;
            $organisasi->nama_organisasi = $request->input('organisasi');
            $organisasi->alamat_organisasi = $request->input('alamat');
            $organisasi->no_telp_organisasi = $request->input('no_telp');
            $organisasi->handphone_organisasi = $request->input('handphone');
            $organisasi->email_organisasi = $request->input('email');
            $organisasi->fax = $request->input('fax');
            $organisasi->kode_pos = $request->input('kode_pos');
            $organisasi->web = $request->input('web');
            
            if($request->hasFile('file')){
                $file_name = md5(time()) . '.' . $request->file('file')->getClientOriginalExtension();
                $path = public_path() . DIRECTORY_SEPARATOR . '/upload/sertifikat';

                $organisasi->foto_sertifikat = $file_name;
                // $request->file('file')->move($path, $file_name);
            }
            $user->save();
            $organisasi->save();

            DB::commit();
            session()->flash('success_message', 'data organisasi telah disimpan');
            
            if(!Auth::user())
                return redirect('/daftar');
            else
                if($this->get_level() == 'Pegawai') return redirect()->route('organisasi_page');

        }catch (\Exception $e) {
            DB::rollback();
            return $e;
            if($this->get_level() == 'Pegawai'){
                session()->flash('error_message', 'data organisasi tidak disimpan');
                return redirect()->route('organisasi_page');
            }
            else return redirect('/daftar');
        }
    }

    public function update_organisasi($name, Request $request){
        if(Auth::user())
        {
            $organisasi = $this->m_organisasi->get_by_lower_name($name);
            $this->checking_validate('patch', $request, $organisasi);
            DB::BeginTransaction();
            try {
                if($request->input('password') != null){
                    $organisasi->user->kata_sandi = bcrypt($request->input('password'));
                    $organisasi->user->update();
                }
                $organisasi->nama_organisasi = $request->input('organisasi');
                $organisasi->alamat_organisasi = $request->input('alamat');
                $organisasi->no_telp_organisasi = $request->input('no_telp');
                $organisasi->handphone_organisasi = $request->input('handphone');
                $organisasi->email_organisasi = $request->input('email');
                $organisasi->fax = $request->input('fax');
                $organisasi->kode_pos = $request->input('kode_pos');
                $organisasi->web = $request->input('web');
                if($request->hasFile('file')){
                    $name_file = $organisasi->sertifikat;
                    $path = public_path() . DIRECTORY_SEPARATOR . '/upload/sertifikat';
                    \File::delete($path .'/'. $name_file);

                    $name_file = md5(time()) . '.' . $request->file('file')->getClientOriginalExtension();
                    $request->file('file')->move($path, $name_file);
                    
                    $organisasi->foto_sertifikat = $name_file;
                }
                $organisasi->update();
                
                DB::commit();
                session()->flash('success_message', 'data organisasi telah disimpan');
    
                if($this->get_level() == "Pegawai")
                    return redirect()->route('organisasi_page');
                else
                    return redirect('/akun?organisasi=' . $name);
                
            } catch (\Exception $e) {
                DB::rollback();
                session()->flash('error_message', 'data organisasi tidak disimpan');
                
                if($this->get_level() == "Pegawai")
                    return redirect()->route('organisasi_page');
                else
                    return redirect('/organisasi');
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function destroy_organisasi($name){
        if($this->get_jabatan() == "Admin"){
            $organisasi = $this->m_organisasi->get_by_lower_name($name);
            $organisasi->user->delete();

            session()->flash('success_message', 'data organisasi telah dihapus');
            return redirect()->route('organisasi_page');
        }
        else return response('Unauthorized.', 401);
    }

    public function verified(Request $request)
    {
        if($this->get_level() == "Pegawai"){
            DB::BeginTransaction();
            try {
                $organisasi = $this->m_organisasi->get_by_lower_name($request->input('name'));
                $m_status = new StatusOrganisasi();
                $validated = $m_status->get_by_name("Setuju");

                if($organisasi->status_organisasi == "Pending")
                {
                    $organisasi->update([
                        'id_status_organisasi' => $validated->id_status_organisasi
                    ]);
                    
                    DB::commit();
                    $organisasi = $this->m_organisasi->get_by_lower_name($request->input('name'));
                    $this->sending_email($organisasi);
                }

                $organisasi->setAttribute('lower_name', $request->input('name'));                
                return $this->success($organisasi);

            }catch (\Exception $e) {
                DB::rollback();
                return $this->error($e->getMessage());
            }
        }
        else return response('Unauthorized.', 401);
    }

    public function unverified(Request $request)
    {
        if($this->get_level() == "Pegawai"){
            DB::BeginTransaction();
            try {
                $organisasi = $this->m_organisasi->get_by_lower_name($request->input('name'));
                $m_status = new StatusOrganisasi();
                $unvalidated = $m_status->get_by_name("Tidak Setuju");

                if($organisasi->status_organisasi == "Pending")
                {
                    $organisasi->update([
                        'id_status_organisasi' => $unvalidated->id_status_organisasi
                    ]);
                    
                    DB::commit();
                    $organisasi = $this->m_organisasi->get_by_lower_name($request->input('name'));
                }
                $organisasi->setAttribute('lower_name', $request->input('name'));             
                return $this->success($organisasi);

            }catch (\Exception $e) {
                DB::rollback();
                return $this->error($e->getMessage());
            }
        }
        else return response('Unauthorized.', 401);
    }

    function sending_email($organisasi){
        $user = User::find($organisasi->user->id_user);
        $data_email = [
            'nama' => $user->organisasi->nama_organisasi,
            'username' => $user->username,
            'kata_sandi' => strtolower(str_replace(' ', '', $user->organisasi->nama_organisasi)) . $user->id_user
        ];
        
        // Mail::to($user->organisasi->email_organisasi)
        // ->send(new KirimSertifikat($data_email));

        Mail::to('diar.ichrom@gmail.com')
        ->send(new KirimSertifikat($data_email));
    }
}
