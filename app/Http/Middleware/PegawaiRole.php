<?php

namespace App\Http\Middleware;

use Closure;

class PegawaiRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles)
    {
        $role = collect($roles);

        if($request->user()->level->level == "Pegawai"){
            if(!$role->contains($request->user()->pegawai->jabatan->jabatan)){
                return response('Unauthorized.', 401);
            }
            return $next($request);
        }
        else if($request->user()->level->level == "Organisasi"){
            return $next($request);
        }
        else{
            return response('Unauthorized.', 401);
        }

    }
}
